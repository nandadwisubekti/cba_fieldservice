package invent.cba.Interface;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ConnectionInterface {

    //-------------UPLOAD---------------------------------
    //UPLOAD DELIVERY ORDER
    @POST("DeliveryOrderService.svc/UpdateDO")
    Call<JsonObject> postUpdateDeliveryOrder(@Body JsonArray param);

    //UPLOAD DELIVERY ORDER DETAIL
    @POST("DeliveryOrderService.svc/UpdateDODetail")
    Call<JsonObject> postUpdateDeliveryOrederDetail(@Body JsonArray param);

    //UPLOAD RETUR ORDER
    @POST("DeliveryOrderService.svc/UpdateRetur")
    Call<JsonObject> postUpdateRetur(@Body JsonArray param);

    //UPLOAD RETUR ORDER DETAIL
    @POST("DeliveryOrderService.svc/UpdateReturDetail")
    Call<JsonObject> postUpdateReturDetail(@Body JsonArray param);

    //UPLOAD RETUR NEW ORDER
    @POST("DeliveryOrderService.svc/InsertReturOrder")
    Call<JsonObject> postUpdateReturNew(@Body JsonArray param);

    //UPLOAD RETUR ORDER NEW DETAIL
    @POST("DeliveryOrderService.svc/InsertReturOrderDetail")
    Call<JsonObject> postUpdateReturDetailNew(@Body JsonArray param);

    //UPLOAD VISIT
    @POST("DeliveryOrderService.svc/InsertVisit")
    Call<JsonObject> postUploadVisit(@Body JsonArray param);

    //UPLOAD VISIT DETAIL
    @POST("DeliveryOrderService.svc/InsertVisitDetail")
    Call<JsonObject> postUploadVisitDetail(@Body JsonArray param);

    //UPLOAD DAILY COST
    @POST("DeliveryOrderService.svc/InsertDailyCost")
    Call<JsonObject> postUploadDailyCost(@Body JsonArray param);

    //UPLOAD CUSTOMER IMAGE
    @POST("DeliveryOrderService.svc/InsertCustImage")
    Call<JsonObject> postUploadCustomerImage(@Body JsonArray param);

    String CACHE = "Cache-Control: max-age=0";
    String AGENT = "User-Agent: mobile";

    // NEW!! UPLOAD CUST IMAGE HERE. yang di atas cuma buat ngirim path / file namenya aja
    @Headers({CACHE, AGENT})
    @POST("DeliveryOrderService.svc/writeSurveyImage")
    Call<JsonObject> postUploadSurveyImages(
            @Body MultipartBody images
    );

    //UPLOAD LIVE TRACKING
    @POST("DeliveryOrderService.svc/InsertTracking")
    Call<JsonObject> postUploadLiveTracking(@Body JsonArray param);

    //UPLOAD LIVE TRACKING ONLINE
    @POST("DeliveryOrderService.svc/InsertTracking")
    Call<JsonObject> postUploadLiveTrackingOnlien(@Body JsonObject param);

    //UPLOAD TOKEN FIREBASE KE SERVER
    @POST("DeliveryOrderService.svc/insertandroidkey")
    Call<JsonObject> postFirebaseToServer(@Body JsonObject param);

    //UPLOAD CHEACK IN LAT LNG
    @POST("DeliveryOrderService.svc/checkincourierradius")
    Call<JsonObject> postCheackInLatLng(@Body JsonObject param);

    //-------------DOWNLOAD---------------------------------
    //DOWNLOAD DATABES
    @POST("DeliveryOrderService.svc/getJson")
    Call<JsonObject> postDownload(@Body JsonObject download);

    //POST NOTIF DAILY MESAGE
    @POST("DeliveryOrderService.svc/getdailymessage")
    Call<JsonObject> postDailyMessageList(@Body JsonObject param);

    @POST("DeliveryOrderService.svc/getmobileconfig")
    Call<JsonArray> postMobilConnfig(@Body JsonObject param);

    //CHECK VERISON
    @POST("DeliveryOrderService.svc/checkversion")
    Call<JsonObject> postCheckVersionApk(@Body JsonObject param);

    //UPLOAD LOG VISIT
    @POST("DeliveryOrderService.svc/InsertLogVisit")
    Call<JsonObject> postUploadLogVisit(@Body JsonArray param);

    //DOWNLOAD USER CONFIG
    @POST("DeliveryOrderService.svc/setuserconfig")
    Call<JsonObject> postUserConfig(@Body JsonObject param);

    //pushnotificationconfirmation
    @POST("DeliveryOrderService.svc/pushnotificationconfirmation")
    Call<JsonObject> postKonfirmasiUserConfig(@Body JsonObject param);

    //pushnotificationconfirmation
    @POST("DeliveryOrderService.svc/checkconnection")
    Call<JsonObject> postTestKoneksi();

    //UPLOAD COST VISIT
    @POST("DeliveryOrderService.svc/insertcostvisit")
    Call<JsonObject> postUploadCostVisit(@Body JsonArray param);

    //DOWNLOAD USER CONFIG
    @POST("DeliveryOrderService.svc/LoadVehicle")
    Call<JsonObject> postVehicleNumber(@Body JsonObject param);

    //UPLOAD RATION
    @POST("DeliveryOrderService.svc/UploadBBMRatio")
    Call<JsonObject> postUploadRatio(@Body JsonArray param);

    //DOWNLOAD DATABES
    @POST("DeliveryOrderService.svc/getlogin")
    Call<JsonObject> postMobileButton(@Body JsonObject download);


    //InsertSisaStockVisit
    @POST("DeliveryOrderService.svc/InsertSisaStockVisit")
    Call<JsonObject> postInsertSisaStockVisit(@Body JsonArray param);

    //InsertPromoVisit
    @POST("DeliveryOrderService.svc/InsertPromoVisit")
    Call<JsonObject> postInsertPromoVisit(@Body JsonArray param);

    //InsertPromoVisit
    @POST("DeliveryOrderService.svc/InsertPOSMStock")
    Call<JsonObject> postInsertPOSMStock(@Body JsonArray param);

    @Multipart
    @POST("DeliveryOrderService.svc/UploadSqlLite")
    Call<JsonObject> postUploadFile(@Header("Content-Type") String type , @Part MultipartBody.Part file);

    @Multipart
    @POST("DeliveryOrderService.svc/UploadSqlLite")
    Call<JsonObject> postUploadFileSqlite(@Part MultipartBody.Part file);

    //Insert CompetitorActivityVisit
    @POST("DeliveryOrderService.svc/InsertCompetitorActivityVisit")
    Call<JsonObject> postInsertCompetitorActivityVisit(@Body JsonArray param);

    //Insert CompetitorActivityImage
    @POST("DeliveryOrderService.svc/InsertCompetitorActivityImage")
    Call<JsonObject> postInsertCompetitorActivityImage(@Body JsonArray param);

    //Insert PromoCompetitor
    @POST("DeliveryOrderService.svc/InsertPromoCompetitor")
    Call<JsonObject> postInsertPromoCompetitor(@Body JsonArray param);

    //Insert InsertPOSMVisit
    @POST("DeliveryOrderService.svc/InsertPOSMVisit")
    Call<JsonObject> postInsertPOSMVisit(@Body JsonArray param);

    //Insert InsertResultSurvey
    @POST("DeliveryOrderService.svc/InsertResultSurvey")
    Call<JsonObject> postInsertResultSurvey(@Body JsonArray param);

    //Insert RegisterNewCustomer
    @POST("DeliveryOrderService.svc/InsertResultSurveyDetail")
    Call<JsonObject> postInsertResultSurveyDetail(@Body JsonArray param);

    // ambil date dari server
    @POST("DeliveryOrderService.svc/GetDate")
    Call<String> getCurrentServerDate();
}
