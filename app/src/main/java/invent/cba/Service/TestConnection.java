package invent.cba.Service;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Interface.ConnectionInterface;

/**
 * Created by kahfizain on 27/08/2017.
 */

public class TestConnection {

    Context context;
    ConnectionInterface apiService;
    ConnectionClient connectionClient;
    DatabesHelper databesHelper;
    public TestConnection(Context context){

        this.context = context;
        connectionClient = new ConnectionClient(context);
        apiService = connectionClient.getApiService();

        new testConnection().execute();

    }


    //PROSES  TEST Connection
    public class testConnection extends AsyncTask<Void, Void, Void> {
        boolean isSuccas = true;
        ProgressDialog pDialog;

        @Override
        protected Void doInBackground(Void... params) {

            try{

                isSuccas = connectionClient.postpostTestKoneksi();

            }catch (Exception e){
                e.printStackTrace();
                isSuccas = false;
            }


            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setTitle("Mohon tunggu!");
            pDialog.setMessage("Memuat");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pDialog.isShowing()){
                if(isSuccas == true){
                    pDialog.dismiss();
                    DialogHelper.dialogMessage(context,"Success","Test connection berhasil",Constant.DIALOG_SUCCESS);
                }else {
                    pDialog.dismiss();
                    DialogHelper.dialogMessage(context,"Error","Test connection gagal,cek kembali IP dan PORT",Constant.DIALOG_ERROR);

                    // ConnectionAlternatif();


                }
            }


        }
    }

    //KONEKSI ALTERNATIF 1
    private void ConnectionAlternatif(){

        databesHelper = new DatabesHelper(context);
        String ipConfig = databesHelper.getMobileConfig(Constant.IPCONFIG);
        String portConfig = databesHelper.getMobileConfig(Constant.PORT);
      //  databesHelper.close();

        ConnectionClientAlternatif connectionClientAlternatif = new ConnectionClientAlternatif(context,ipConfig,portConfig);

        new testConnectionAlternatif(connectionClientAlternatif).execute();

    }

    //PROSES  TEST KONEKSI ALTERNATIF 1
    private class testConnectionAlternatif extends AsyncTask<Void, Void, Void> {
        boolean isSuccas = true;
        ProgressDialog pDialog;
        ConnectionClientAlternatif connectionClientAlternatif;

        private testConnectionAlternatif(ConnectionClientAlternatif connectionClientAlternatif){
            this.connectionClientAlternatif = connectionClientAlternatif;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try{

                isSuccas = connectionClientAlternatif.postpostTestKoneksi();

            }catch (Exception e){
                e.printStackTrace();
                isSuccas = false;
            }


            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setTitle("Mohon tunggu!");
            pDialog.setMessage("Memuat");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pDialog.isShowing()){
                if(isSuccas == true){
                    pDialog.dismiss();

                    DialogHelper.dialogMessage(context,"Succas","Test connection berhasil",Constant.DIALOG_SUCCESS);

                }else {
                    pDialog.dismiss();

                    ConnectionAlternatif2();

                }
            }


        }
    }


    ////////////////////////////////////////////////////////////////////////////
    //KONESI ALTERNATIF 2
    private void ConnectionAlternatif2(){

        databesHelper = new DatabesHelper(context);
        String ipAlternarif = databesHelper.getMobileConfig(Constant.IPALTERNATIF);
        String portAlternarif = databesHelper.getMobileConfig(Constant.PORTALTERNATIF);
      //  databesHelper.close();
        ConnectionClientAlternatif connectionClientAlternatif = new ConnectionClientAlternatif(context,ipAlternarif,portAlternarif);

        new testConnectionAlternatif2(connectionClientAlternatif).execute();

    }

    //PROSES  TEST KONEKSI ALTERNATIF 2
    private class testConnectionAlternatif2 extends AsyncTask<Void, Void, Void> {
        boolean isSuccas = true;
        ProgressDialog pDialog;
        ConnectionClientAlternatif connectionClientAlternatif;

        private testConnectionAlternatif2(ConnectionClientAlternatif connectionClientAlternatif){
            this.connectionClientAlternatif = connectionClientAlternatif;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try{

                isSuccas = connectionClientAlternatif.postpostTestKoneksi();

            }catch (Exception e){
                e.printStackTrace();
                isSuccas = false;
            }


            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setTitle("Mohon tunggu!");
            pDialog.setMessage("Memuat");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pDialog.isShowing()){
                if(isSuccas == true){
                    pDialog.dismiss();
                    DialogHelper.dialogMessage(context,"Succas","Test connection berhasil",Constant.DIALOG_SUCCESS);

                }else {
                    pDialog.dismiss();


                    DialogHelper.dialogMessage(context,"Error","Test connection gagal,cek kembali IP dan PORT",Constant.DIALOG_ERROR);


                }
            }


        }
    }



}
