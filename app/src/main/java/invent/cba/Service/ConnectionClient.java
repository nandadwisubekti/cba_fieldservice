package invent.cba.Service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Helper.data.SharedPreferenceHelper;
import invent.cba.Interface.ConnectionInterface;
import invent.cba.R;
import invent.cba.View.Activity.Home.HomeActivity;
import invent.cba.View.Activity.Notif.NotifActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ConnectionClient {

    static Retrofit retrofit = null;
    private static String TAG = "ConnectionClient";
    DatabesHelper databesHelper;
    Context context;
    String IPUSER = "IPUSER";
    String PORTUSER = "PORTUSER";
    String ipConfig;
    String IpStation;
    String IpUser;
    String portConfig;
    String PortStation;
    String PortUser;
    private ConnectionInterface apiService;
    private WifiInfo wifiInfo = null;       //The obtained Wifi information making
    private WifiManager wifiManager = null; //The Wifi manager making
    private int level;


    public ConnectionClient(Context context) {
        this.context = context;
        databesHelper = new DatabesHelper(context);
        // CheckWifi();
        /**
         * buat logging
         */
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(300, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();
        //IP & PORT USER
          /*  IpUser = helper.getMobileConfig(Constant.IPUSER);
            PortUser = helper.getMobileConfig(Constant.PORTUSER);*/
        ipConfig = databesHelper.getMobileConfig(Constant.IPCONFIG);
        portConfig = databesHelper.getMobileConfig(Constant.PORT);
        if (ipConfig != null && portConfig != null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://" + ipConfig.trim().replace(" ", "") + ":" + portConfig.trim().replace(" ", "") + "/")
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        } else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.IPAlternatif.trim().replace(" ", ""))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        apiService = retrofit.create(ConnectionInterface.class);
    }

    public ConnectionInterface getApiService() {
        if (apiService == null)
            apiService = retrofit.create(ConnectionInterface.class);
        return apiService;
    }

    // -----------------UPLOAD------------------------------
    //POST UPLOAD RETUR ORDER
    public boolean postUpdateRetur(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateRetur = apiService.postUpdateRetur(param);
        try {
            Response<JsonObject> response = updateRetur.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD RETUR ORDER DETAIL
    public boolean postUpdateReturDetail(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateReturDetail = apiService.postUpdateReturDetail(param);
        try {
            Response<JsonObject> response = updateReturDetail.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD RETUR NEW ORDER
    public boolean postUpdateReturNew(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateRetur = apiService.postUpdateReturNew(param);
        try {
            Response<JsonObject> response = updateRetur.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD RETUR NEW ORDER DETAIL
    public boolean postUpdateReturDetailNew(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateReturDetail = apiService.postUpdateReturDetailNew(param);
        try {
            Response<JsonObject> response = updateReturDetail.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD DELEVERY ORDER AUTO
    public int postUpdateDeleveryOrderAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccess = 0;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrder(param);
        try {
            Response<JsonObject> response = updateDelivery.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccess = 1;
                } else {
                    isSuccess = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccess = 0;
        }
        return isSuccess;
    }

    //POST UPLOAD DELEVERY ORDER DETAIL AUTO
    public int postUpdateDeleveryOrderDetailAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrederDetail(param);
        try {
            Response<JsonObject> response = updateDelivery.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //POST UPLOAD DELEVERY ORDER
    public boolean postUpdateDeleveryOrder(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccess = false;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrder(param);
        try {
            Response<JsonObject> response = updateDelivery.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccess = true;
                } else {
                    isSuccess = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccess = false;
        }
        return isSuccess;
    }

    //POST UPLOAD DELEVERY ORDER DETAIL
    public boolean postUpdateDeleveryOrderDetail(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrederDetail(param);
        try {
            Response<JsonObject> response = updateDelivery.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD VISIT  AuTO
    public int postUploadVisitAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadVisit = apiService.postUploadVisit(param);
        try {
            Response<JsonObject> response = uploadVisit.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //POST UPLOAD VISIT DETAIL AUTO
    public int postUploadVisitDetailAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadVisitDetail = apiService.postUploadVisitDetail(param);
        try {
            Response<JsonObject> response = uploadVisitDetail.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //POST UPLOAD VISIT
    public boolean postUploadVisit(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadVisit = apiService.postUploadVisit(param);
        try {
            Response<JsonObject> response = uploadVisit.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD VISIT DETAIL
    public boolean postUploadVisitDetail(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadVisitDetail = apiService.postUploadVisitDetail(param);
        try {
            Response<JsonObject> response = uploadVisitDetail.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD DAILY COST
    public boolean postUploadDailyCost(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement, resultElementT;
        String result = null;
        String resultT = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadDailyCost = apiService.postUploadDailyCost(param);
        try {
            Response<JsonObject> response = uploadDailyCost.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD DAILY COST AUTO
    public int postUploadDailyCostAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement, resultElementT;
        String result = null;
        String resultT = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadDailyCost = apiService.postUploadDailyCost(param);
        try {
            Response<JsonObject> response = uploadDailyCost.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //POST UPLOAD DAILY COST
    public boolean postUploadCustomerImage(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadCustomerImage = apiService.postUploadCustomerImage(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD DAILY COST AUTO
    public int postUploadCustomerImageAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postUploadCustomerImage(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //InsertSisaStockVisit
    public int postUploadInsertSisaStockVisit(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postInsertSisaStockVisit(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //InsertSisaStockVisit
    public int postUploadInsertPromoVisit(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postInsertPromoVisit(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //InsertPOSMStock
    public int postUploadInsertPOSMStock(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postInsertPOSMStock(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //Insert CompetitorActivityVisit
    public int postUploadInsertCompetitorActivityVisit(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postInsertCompetitorActivityVisit(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //Insert CompetitorActivityImage
    public int postUploadInsertCompetitorActivityImage(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postInsertCompetitorActivityImage(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //Insert PromoCompetitor
    public int postUploadInsertPromoCompetitor(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postInsertPromoCompetitor(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //Insert InsertPOSMVisit
    public int postUploadInsertPOSMVisit(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postInsertPOSMVisit(param);
        try {
            Response<JsonObject> response = uploadCustomerImage.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //Insert InsertRegisterNewCustomer
    public int postUploadInsertRegisterNewCustomer(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> postInsertRegisterNewCustomer = apiService.postInsertResultSurvey(param);
        try {
            Response<JsonObject> response = postInsertRegisterNewCustomer.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //Insert InsertRegisterNewCustomerDetail
    public int postUploadInsertRegisterNewCustomerDetail(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> postInsertRegisterNewCustomerDetail = apiService.postInsertResultSurveyDetail(param);
        try {
            Response<JsonObject> response = postInsertRegisterNewCustomerDetail.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //POST UPLOAD LIVE TRACKING
    public boolean postUploadLiveTracking(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadLiveTracking = apiService.postUploadLiveTracking(param);
        try {
            Response<JsonObject> response = uploadLiveTracking.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD LOG VISIT
    public boolean postUploadLogVisit(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadLogVisit = apiService.postUploadLogVisit(param);
        try {
            Response<JsonObject> response = uploadLogVisit.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD COST VISIT
    public boolean postUploadCostVisit(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadCostVisit(param);
        try {
            Response<JsonObject> response = uploadCostVisit.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD COST VISIT AUTO
    public int postUploadCostVisitAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadCostVisit(param);
        try {
            Response<JsonObject> response = uploadCostVisit.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist") || result.contains("0")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //POST UPLOAD UPLOAD RATION BBM
    public boolean postUploadRatio(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadRatio(param);
        try {
            Response<JsonObject> response = uploadCostVisit.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST UPLOAD UPLOAD RATION BBM AUTO
    public int postUploadRatioAuto(JsonArray param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadRatio(param);
        try {
            Response<JsonObject> response = uploadCostVisit.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") || result.contains("IDExist")) {
                    isSuccas = 1;
                } else {
                    isSuccas = 3;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = 0;
        }
        return isSuccas;
    }

    //POST UPLOAD LIVE TRACKING ONLINE
    public boolean postUploadLiveTrackingOnlien(JsonObject param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = true;
        final Call<JsonObject> uploadLiveTrackingOnlien = apiService.postUploadLiveTrackingOnlien(param);
        try {
            Response<JsonObject> response = uploadLiveTrackingOnlien.execute();
            if (response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("SQLSuccess") || result.contains("IDExist")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST TOKEN FIREBASE KE SERVER
    public boolean postFirebaseToServer(JsonObject param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadFirebaseToServer = apiService.postFirebaseToServer(param);
        try {
            Response<JsonObject> response = uploadFirebaseToServer.execute();
            if (response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //POST Download Notification DailyMessage
    public boolean postDailyMessageList(JsonObject param) {
        JsonObject jsonObjectDailyMessage;
        JsonArray jsonarray;
        JsonObject jsonobject;
        boolean isSuccas = false;
        databesHelper = new DatabesHelper(context);
        final Call<JsonObject> uploadDailyMessageList = apiService.postDailyMessageList(param);
        try {
            Response<JsonObject> response = uploadDailyMessageList.execute();
            if (response.isSuccessful()) {
               /* helper.deleteDailyMessage(); //Delete Daily Message
                jsonobject = response.body();
                jsonarray = jsonobject.getAsJsonArray("DailyMessageList");
                for (int i=0; i<jsonarray.size(); i++){
                    jsonObjectDailyMessage = jsonarray.get(i).getAsJsonObject();
                    helper.InserDailyMessage(
                            jsonObjectDailyMessage.get(Constant.MESSAGEID).getAsString(),
                            jsonObjectDailyMessage.get(Constant.MESSAGENAME).getAsString(),
                            jsonObjectDailyMessage.get(Constant.MESSAGEDESC).getAsString(),
                            jsonObjectDailyMessage.get(Constant.MESSAGEIMG).getAsString(),
                            jsonObjectDailyMessage.get("Date").getAsString(),
                            jsonObjectDailyMessage.get(Constant.CREATEDBY).getAsString());
                }*/
                isSuccas = true;
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //KONFIRMASI NOTIF
    public boolean postKonfirmasiNotif(JsonObject param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadKonfirmasi = apiService.postKonfirmasiUserConfig(param);
        try {
            Response<JsonObject> response = uploadKonfirmasi.execute();
            if (response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            // System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //CHECK VERSION APK
    public boolean postKonfirmasiApk(JsonObject param) {
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadKonfirmasi = apiService.postCheckVersionApk(param);
        try {
            Response<JsonObject> response = uploadKonfirmasi.execute();
            if (response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            // System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //TEST KONESKSI SERVER
    public boolean postpostTestKoneksi() {
        JsonObject object;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> testKoneksi = apiService.postTestKoneksi();
        try {
            Response<JsonObject> response = testKoneksi.execute();
            if (response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")) {
                    isSuccas = true;
                } else {
                    isSuccas = false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }

    //DOWNLOAD DATA PERJALANAN
    public void downloadDataAll(JsonObject param) {
        final int[] isSuccessDownload = {0};
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(context.getString(R.string.dialog_PleaseWait));
        mProgressDialog.setMessage(context.getString(R.string.dialog_loading));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        final String[] result = new String[1];
        final JsonArray[] jsonarray = new JsonArray[1];
        final JsonObject[] object = new JsonObject[1];
        final JsonObject[] jsonobject = new JsonObject[1];
        final JsonObject[] jsonObjectCallPlanList = new JsonObject[1];
        final JsonObject[] jsonObjectCallPlanDetailList = new JsonObject[1];
        final JsonObject[] jsonObjectCompetitorActivityList = new JsonObject[1];
        final JsonObject[] jsonObjectCompetitorList = new JsonObject[1];
        final JsonObject[] jsonObjectCompetitorProductList = new JsonObject[1];
        final JsonObject[] jsonObjectCustomerTypeList = new JsonObject[1];
        final JsonObject[] jsonObjectReason = new JsonObject[1];
        final JsonObject[] jsonObjectCustomerList = new JsonObject[1];
        final JsonObject[] jsonObjectPOSMStock = new JsonObject[1];
        final JsonObject[] jsonObjectPromoList = new JsonObject[1];
        final JsonObject[] jsonObjectMobileConfigList = new JsonObject[1];
        final JsonObject[] jsonObjectProductList = new JsonObject[1];
        final JsonObject[] jsonObjectInsertSisaStockType = new JsonObject[1];
        final JsonObject[] jsonObjectDailyMessage = new JsonObject[1];
        final JsonObject[] jsonObjectCompetitorActivityType = new JsonObject[1];
        final JsonObject[] jsonObjectQuestionList = new JsonObject[1];
        final JsonObject[] jsonObjectAnswerList = new JsonObject[1];
        final JsonObject[] jsonObjectAnswerTypeList = new JsonObject[1];
        final JsonObject[] jsonObjectQuestionSetList = new JsonObject[1];
        final JsonObject[] jsonObjectQuestionCategoryList = new JsonObject[1];
        final String[] StatusCode = new String[1];
        final String[] StatusMessage = new String[1];
        final String[] Title = new String[1];
        final Call<JsonObject> mDownload = apiService.postDownload(param);
        mDownload.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    databesHelper = new DatabesHelper(context);
                    object[0] = response.body();
                    Title[0] = object[0].get(Constant.Title).getAsString();
                    StatusCode[0] = object[0].get(Constant.StatusCode).getAsString();
                    StatusMessage[0] = object[0].get(Constant.StatusMessage).getAsString();
                    if (StatusCode[0].equals("01")) {
                        jsonobject[0] = object[0].getAsJsonObject(Constant.Value);
                        jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.mdlJson);
                        jsonobject[0] = jsonarray[0].get(0).getAsJsonObject();
                        //Result
                        result[0] = jsonobject[0].get(Constant.Result).getAsString();
                        if (result[0].equalsIgnoreCase("Unduh berhasil")) {
                            databesHelper.deleteHistoryLocation();  //Delete tabel HistoryLocation
                            databesHelper.deleteDeliveryOrder(); //Delete tabel Delivery Order
                            databesHelper.deleteDeliveryOrderDetail(); //Delete tabel Delivery Order Detail
                            databesHelper.deleteCustomer();  //Delete tabel Customer
                            databesHelper.deleteCustomerType(); //Delete tabel CustomerType
                            databesHelper.deleteProduct(); //Delete tabel deleteProduct
                            databesHelper.deleteReturOrder(); //Delete ReturOrder
                            databesHelper.deleteReturOrderDetail(); //Delete ReturOrderDetail
                            databesHelper.deleteCustomerImage(); //Delete Image
                            databesHelper.deleteDailyCost(); //Delete Cost
                            databesHelper.deleteProductUOM();
                            databesHelper.deleteWarehouse();
                            databesHelper.deleteLogVisit();
                            databesHelper.deleteCallPlan(); //Delete tabel CallPlan
                            databesHelper.deleteCallPlanDetail(); //Delete tabel deleteCallPlanDetail
                            databesHelper.deleteVisit(); //Delete tabel Visit
                            databesHelper.deleteVisitDetail();
                            databesHelper.deleteDailyMessage(); //Delete Daily Message
                            databesHelper.deleteReason(); //Delete Reason
                            //tabel baru wyeth
                            databesHelper.deleteCompetitor();
                            databesHelper.deleteCompetitorActivity();
                            databesHelper.deleteCompetitorProduct();
                            databesHelper.deletePOSMProduct();
                            databesHelper.deletePOSMStock();
                            databesHelper.deleteSisaStockType();
                            databesHelper.deleteSisaStockVisit();
                            databesHelper.deletePromoVisit();
                            databesHelper.deletePromo();
                            databesHelper.deleteCompetitorActivityType();
                            databesHelper.deletePromoCompetitor();
                            databesHelper.deleteCompetitorActivityVisit();
                            databesHelper.deleteCompetitorActivityImage();
                            databesHelper.deletePOSMStockVisit();
                            //register cutomer
                            databesHelper.deleteResultSurvey();
                            databesHelper.deleteResultSurveyDetail();
                            //CallPlanList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CallPlanList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectCallPlanList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.InsertCallPlan(
                                        !jsonObjectCallPlanList[0].get(Constant.CallPlanID).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.CallPlanID).getAsString() : "",
                                        !jsonObjectCallPlanList[0].get(Constant.Date).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.Date).getAsString() : "",
                                        !jsonObjectCallPlanList[0].get(Constant.VehicleID).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.VehicleID).getAsString() : "",
                                        !jsonObjectCallPlanList[0].get(Constant.EmployeeID).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.EmployeeID).getAsString() : "",
                                        !jsonObjectCallPlanList[0].get(Constant.Helper1).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.Helper1).getAsString() : "",
                                        !jsonObjectCallPlanList[0].get(Constant.Helper2).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.Helper2).getAsString() : "",
                                        !jsonObjectCallPlanList[0].get(Constant.KMAkhir).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.KMAkhir).getAsString() : "0");
                                databesHelper.updateMobileConfig(
                                        Constant.VEHICLENUMBER,
                                        jsonObjectCallPlanList[0].get(Constant.VehicleID).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.VehicleID).getAsString() : "");
                            }
                            //CALL PLAN DETAIL
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CallPlanDetailList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectCallPlanDetailList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.InsertCallPlanDetail(
                                        !jsonObjectCallPlanDetailList[0].get(Constant.CallPlanID).isJsonNull() ? jsonObjectCallPlanDetailList[0].get(Constant.CallPlanID).getAsString() : "",
                                        !jsonObjectCallPlanDetailList[0].get(Constant.CustomerID).isJsonNull() ? jsonObjectCallPlanDetailList[0].get(Constant.CustomerID).getAsString() : "",
                                        !jsonObjectCallPlanDetailList[0].get(Constant.Sequence).isJsonNull() ? jsonObjectCallPlanDetailList[0].get(Constant.Sequence).getAsString() : "",
                                        !jsonObjectCallPlanDetailList[0].get(Constant.WarehouseID).isJsonNull() ? jsonObjectCallPlanDetailList[0].get(Constant.WarehouseID).getAsString() : "",
                                        !jsonObjectCallPlanDetailList[0].get(Constant.Time).isJsonNull() ? jsonObjectCallPlanDetailList[0].get(Constant.Time).getAsString() : "");
                            }
                          /*  //Competitor Activity
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CompetitorActivityList);
                            for (int i = 0; i< jsonarray[0].size(); i++) {
                                jsonObjectCompetitorActivityList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.insetCompetitorActivity(
                                        !jsonObjectCompetitorActivityList[0].get(Constant.ActivityID).isJsonNull() ? jsonObjectCompetitorActivityList[0].get(Constant.ActivityID).getAsString() : "",
                                        !jsonObjectCompetitorActivityList[0].get(Constant.ActivityName).isJsonNull() ? jsonObjectCompetitorActivityList[0].get(Constant.ActivityName).getAsString() : "",
                                        !jsonObjectCompetitorActivityList[0].get(Constant.CompetitorID).isJsonNull() ? jsonObjectCompetitorActivityList[0].get(Constant.CompetitorID).getAsString() : "");
                            }*/
                            //Competitor
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CompetitorList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectCompetitorList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.insetCompetitor(
                                        !jsonObjectCompetitorList[0].get(Constant.CompetitorID).isJsonNull() ? jsonObjectCompetitorList[0].get(Constant.CompetitorID).getAsString() : "",
                                        !jsonObjectCompetitorList[0].get(Constant.CompetitorName).isJsonNull() ? jsonObjectCompetitorList[0].get(Constant.CompetitorName).getAsString() : "");
                            }
                            //Competitor Product
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CompetitorProductList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectCompetitorProductList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.insetCompetitorProduct(
                                        !jsonObjectCompetitorProductList[0].get(Constant.CompetitorID).isJsonNull() ? jsonObjectCompetitorProductList[0].get(Constant.CompetitorID).getAsString() : "",
                                        !jsonObjectCompetitorProductList[0].get(Constant.CompetitorProductID).isJsonNull() ? jsonObjectCompetitorProductList[0].get(Constant.CompetitorProductID).getAsString() : "",
                                        !jsonObjectCompetitorProductList[0].get(Constant.CompetitorProductName).isJsonNull() ? jsonObjectCompetitorProductList[0].get(Constant.CompetitorProductName).getAsString() : "");
                            }
                            //Competitor Customer
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CustomerList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectCustomerList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.insetCustomer(
                                        !jsonObjectCustomerList[0].get(Constant.CustomerID).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.CustomerID).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.CustomerName).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.CustomerName).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.PIC).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.PIC).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.CustomerAddress).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.CustomerAddress).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.Phone).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.Phone).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.CustomerTypeID).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.CustomerTypeID).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.Radius).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.Radius).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.Latitude).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.Latitude).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.Longitude).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.Longitude).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.City).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.City).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.BranchID).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.BranchID).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.Account).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.Account).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.CountryRegionCode).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.CountryRegionCode).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.Distributor).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.Distributor).getAsString() : "",
                                        !jsonObjectCustomerList[0].get(Constant.Channel).isJsonNull() ? jsonObjectCustomerList[0].get(Constant.Channel).getAsString() : "");
                            }
                            //CustomerType
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CustomerTypeList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectCustomerTypeList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.InsertCustomerType(
                                        !jsonObjectCustomerTypeList[0].get(Constant.CustomerTypeID).isJsonNull() ? jsonObjectCustomerTypeList[0].get(Constant.CustomerTypeID).getAsString() : "",
                                        !jsonObjectCustomerTypeList[0].get(Constant.CustomerTypeName).isJsonNull() ? jsonObjectCustomerTypeList[0].get(Constant.CustomerTypeName).getAsString() : "",
                                        !jsonObjectCustomerTypeList[0].get(Constant.Description).isJsonNull() ? jsonObjectCustomerTypeList[0].get(Constant.Description).getAsString() : "");
                            }
                            //Reason
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.ReasonList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectReason[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.InserReason(
                                        !jsonObjectReason[0].get(Constant.ReasonID).isJsonNull() ? jsonObjectReason[0].get(Constant.ReasonID).getAsString() : "",
                                        !jsonObjectReason[0].get(Constant.ReasonType).isJsonNull() ? jsonObjectReason[0].get(Constant.ReasonType).getAsString() : "",
                                        !jsonObjectReason[0].get(Constant.Value).isJsonNull() ? jsonObjectReason[0].get(Constant.Value).getAsString() : "");
                            }
                            //POSMProductList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.POSMProductList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectPOSMStock[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.InsertPOSMIDPOSMProduct(
                                        !jsonObjectPOSMStock[0].get(Constant.POSMID).isJsonNull() ? jsonObjectPOSMStock[0].get(Constant.POSMID).getAsString() : "",
                                        !jsonObjectPOSMStock[0].get(Constant.POSMName).isJsonNull() ? jsonObjectPOSMStock[0].get(Constant.POSMName).getAsString() : "");
                                databesHelper.InsertPOSMID(
                                        !jsonObjectCallPlanList[0].get(Constant.CallPlanID).isJsonNull() ? jsonObjectCallPlanList[0].get(Constant.CallPlanID).getAsString() : "",
                                        !jsonObjectPOSMStock[0].get(Constant.POSMID).isJsonNull() ? jsonObjectPOSMStock[0].get(Constant.POSMID).getAsString() : "",
                                        "0", "1991-01-01", "", "0");
                            }
                            //PromoList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.PromoList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectPromoList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.InsertPromo(
                                        !jsonObjectPromoList[0].get(Constant.PromoID).isJsonNull() ? jsonObjectPromoList[0].get(Constant.PromoID).getAsString() : "",
                                        !jsonObjectPromoList[0].get(Constant.PromoName).isJsonNull() ? jsonObjectPromoList[0].get(Constant.PromoName).getAsString() : "",
                                        !jsonObjectPromoList[0].get(Constant.PromoCategory).isJsonNull() ? jsonObjectPromoList[0].get(Constant.PromoCategory).getAsString() : "");
                            }
                            //UPDATE MOBILE CONFIG
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.MobileConfigList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectMobileConfigList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.updateMobileConfig(
                                        jsonObjectMobileConfigList[0].get(Constant.ID).getAsString(),
                                        jsonObjectMobileConfigList[0].get(Constant.Value).getAsString());
                            }
                            //PRODUCT LIST
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.ProductList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectProductList[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.insertProduct(
                                        jsonObjectProductList[0].get(Constant.ProductID).getAsString(),
                                        jsonObjectProductList[0].get(Constant.ProductName).getAsString(),
                                        jsonObjectProductList[0].get(Constant.ProductType).getAsString(),
                                        jsonObjectProductList[0].get(Constant.ProductGroup).getAsString(),
                                        jsonObjectProductList[0].get(Constant.ProductWeight).getAsString(),
                                        jsonObjectProductList[0].get(Constant.UOM).getAsString(),
                                        jsonObjectProductList[0].get(Constant.ArticleNumber).getAsString());
                            }
                            //SisaStockTypeList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.SisaStockTypeList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectInsertSisaStockType[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.InsertSisaStockType(
                                        jsonObjectInsertSisaStockType[0].get(Constant.SisaStockTypeID).getAsString(),
                                        jsonObjectInsertSisaStockType[0].get(Constant.Description).getAsString(),
                                        jsonObjectInsertSisaStockType[0].get(Constant.Category).getAsString(),
                                        jsonObjectInsertSisaStockType[0].get(Constant.Seq).getAsString());
                            }
                            //DailyMessage
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.DailyMessageList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectDailyMessage[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.inserDailyMessage(
                                        jsonObjectDailyMessage[0].get(Constant.MessageID).getAsString(),
                                        jsonObjectDailyMessage[0].get(Constant.MessageName).getAsString(),
                                        jsonObjectDailyMessage[0].get(Constant.MessageDesc).getAsString(),
                                        jsonObjectDailyMessage[0].get(Constant.MessageImg).getAsString(),
                                        jsonObjectDailyMessage[0].get(Constant.Date).getAsString(),
                                        jsonObjectDailyMessage[0].get(Constant.CreatedBy).getAsString());
                            }
                            //jsonObjectCompetitorActivityType
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.CompetitorActivityTypeList);
                            for (int i = 0; i < jsonarray[0].size(); i++) {
                                jsonObjectCompetitorActivityType[0] = jsonarray[0].get(i).getAsJsonObject();
                                databesHelper.insertCompetitorActivityType(
                                        jsonObjectCompetitorActivityType[0].get(Constant.CompetitorActivityTypeID).getAsString(),
                                        jsonObjectCompetitorActivityType[0].get(Constant.Description).getAsString(),
                                        jsonObjectCompetitorActivityType[0].get(Constant.Category).getAsString(),
                                        jsonObjectCompetitorActivityType[0].get(Constant.Seq).getAsString());
                            }
                            //Register customer
                            ///////////////////////////////////////////////////////////////////////////
                            //QuestionList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.QuestionList);
                            if (jsonarray[0].size() > 0) {
                                databesHelper.deletems_question();
                                for (int i = 0; i < jsonarray[0].size(); i++) {
                                    jsonObjectQuestionList[0] = jsonarray[0].get(i).getAsJsonObject();
                                    databesHelper.insertms_question("",
                                            jsonObjectQuestionList[0].get(Constant.QuestionID).getAsString(),
                                            jsonObjectQuestionList[0].get(Constant.QuestionText).getAsString(),
                                            jsonObjectQuestionList[0].get(Constant.AnswerTypeID).getAsString(),
                                            jsonObjectQuestionList[0].get(Constant.IsSubQuestion).getAsBoolean(),
                                            jsonObjectQuestionList[0].get(Constant.Sequence).getAsInt(),
                                            jsonObjectQuestionList[0].get(Constant.QuestionSetID).getAsString(),
                                            jsonObjectQuestionList[0].get(Constant.QuestionCategoryID).getAsString(),
                                            jsonObjectQuestionList[0].get(Constant.AnswerID).getAsString(),
                                            jsonObjectQuestionList[0].get(Constant.IsActive).getAsBoolean(),
                                            jsonObjectQuestionList[0].get(Constant.Mandatory).getAsBoolean());
                                }
                            }
                            //AnswerList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.AnswerList);
                            if (jsonarray[0].size() > 0) {
                                databesHelper.deletems_answer();
                                for (int i = 0; i < jsonarray[0].size(); i++) {
                                    jsonObjectAnswerList[0] = jsonarray[0].get(i).getAsJsonObject();
                                    databesHelper.insertms_answer(
                                            jsonObjectAnswerList[0].get(Constant.AnswerID).getAsString(),
                                            jsonObjectAnswerList[0].get(Constant.AnswerText).getAsString(),
                                            jsonObjectAnswerList[0].get(Constant.QuestionID).getAsString(),
                                            jsonObjectAnswerList[0].get(Constant.SubQuestion).getAsBoolean(),
                                            jsonObjectAnswerList[0].get(Constant.IsSubQuestion).getAsBoolean(),
                                            jsonObjectAnswerList[0].get(Constant.Sequence).getAsInt(),
                                            jsonObjectAnswerList[0].get(Constant.No).getAsString(),
                                            jsonObjectAnswerList[0].get(Constant.IsActive).getAsBoolean());
                                }
                            }
                            //AnswerTypeList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.AnswerTypeList);
                            if (jsonarray[0].size() > 0) {
                                databesHelper.deletems_answer_type();
                                for (int i = 0; i < jsonarray[0].size(); i++) {
                                    jsonObjectAnswerTypeList[0] = jsonarray[0].get(i).getAsJsonObject();
                                    databesHelper.insertms_answer_type(
                                            jsonObjectAnswerTypeList[0].get(Constant.AnswerTypeID).getAsString(),
                                            jsonObjectAnswerTypeList[0].get(Constant.AnswerTypeText).getAsString());
                                }
                            }
                            //QuestionSetList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.QuestionSetList);
                            if (jsonarray[0].size() > 0) {
                                databesHelper.deletems_question_set();
                                for (int i = 0; i < jsonarray[0].size(); i++) {
                                    jsonObjectQuestionSetList[0] = jsonarray[0].get(i).getAsJsonObject();
                                    databesHelper.insertms_question_set(
                                            jsonObjectQuestionSetList[0].get(Constant.QuestionSetID).getAsString(),
                                            jsonObjectQuestionSetList[0].get(Constant.QuestionSetText).getAsString());
                                }
                            }
                            //QuestionCategoryList
                            jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.QuestionCategoryList);
                            if (jsonarray[0].size() > 0) {
                                databesHelper.deleteQuestion_Category();
                                for (int i = 0; i < jsonarray[0].size(); i++) {
                                    jsonObjectQuestionCategoryList[0] = jsonarray[0].get(i).getAsJsonObject();
                                    databesHelper.insertQuestion_Category(
                                            jsonObjectQuestionCategoryList[0].get(Constant.QuestionCategoryID).getAsString(),
                                            jsonObjectQuestionCategoryList[0].get(Constant.QuestionCategoryText).getAsString());
                                }
                            }
                            ///////////////////////////////////////////////////////////////////////////
                            databesHelper.updateModuleButton(Constant.PERSIAPAN, Constant.HOME, "1");
                            //databesHelper.updateModuleButton(Constant.MULAIPERJALANAN,Constant.HOME,"0");
                            // hotfix: skip persiapan, langsung ke mulai perjalanan
                            databesHelper.updateModuleButton(Constant.MULAIPERJALANAN, Constant.HOME, "1");
                            databesHelper.updateModuleButton(Constant.DOWNLOAD, Constant.DOWNLOAD_UPLOAD, "0");
                            databesHelper.updateModuleButton(Constant.UPLOAD, Constant.DOWNLOAD_UPLOAD, "0");
                            databesHelper.updateModuleButton(Constant.KUNJUNGAN, Constant.HOME, "0");
                            databesHelper.updateModuleButton(Constant.ISSU, Constant.HOME, "0");
                            databesHelper.updateModuleButton(Constant.AKHIRIKEGIATAN, Constant.HOME, "0");
                            databesHelper.updateModuleButton(Constant.REPORT, Constant.HOME, "0");
                            // hotfix: add 2 jenis kunjungan type
                            databesHelper.InitKunjunganType();


                            SharedPreferenceHelper.saveCustomerID(context,null);
                            SharedPreferenceHelper.saveCallPlanId(context,null);
                            SharedPreferenceHelper.saveEnableModulePersipanTools(context,null);
                            SharedPreferenceHelper.saveCompetitorProductID(context,null);


                            databesHelper.updateMobileFlag(Constant.ITEMGOODS, "0");
                            databesHelper.updateMobileFlag(Constant.DOWNLOAD, "1");
                        }
                        DialogHelper.dialogMessage(context, "Informasi", result[0], Constant.DIALOG_SUCCESS);
                    }
                } else {
                    DialogHelper.dialogMessage(context, "Informasi", "Tidak terhubung ke server (Bad Request)", Constant.DIALOG_ERROR);
                }
                databesHelper.close();
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                DialogHelper.dialogMessage(context, "Informasi", "Tidak terhubung ke server,cek kembali ip dan port", Constant.DIALOG_ERROR);
            }
        });
    }

    //CHECK LOGIN BUTTON LIST
    public void getButtonList(JsonObject param) {
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(context.getString(R.string.dialog_PleaseWait));
        mProgressDialog.setMessage(context.getString(R.string.dialog_loading));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        final String[] result = new String[1];
        final JsonArray[] jsonarray = new JsonArray[1];
        final JsonObject[] jsonobject = new JsonObject[1];
        final String[] StatusCode = new String[1];
        final String[] StatusMessage = new String[1];
        final String[] Title = new String[1];
        final JsonObject[] jsonObjectModuleButton = new JsonObject[1];
        final Call<JsonObject> mMobileButton = apiService.postMobileButton(param);
        mMobileButton.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    databesHelper = new DatabesHelper(context);
                    jsonobject[0] = response.body();
                    Title[0] = jsonobject[0].get(Constant.Title).getAsString();
                    StatusCode[0] = jsonobject[0].get(Constant.StatusCode).getAsString();
                    StatusMessage[0] = jsonobject[0].get(Constant.StatusMessage).getAsString();
                    if (StatusCode[0].equals("01")) {
                        jsonarray[0] = jsonobject[0].getAsJsonArray(Constant.Value);
                        if (jsonarray[0] != null) {
                            if (jsonarray[0].size() > 0) {
                                for (int i = 0; i < jsonarray[0].size(); i++) {
                                    jsonObjectModuleButton[0] = jsonarray[0].get(i).getAsJsonObject();
                                    databesHelper.updateModuleButtonRole(
                                            !jsonObjectModuleButton[0].get(Constant.ModuleID).isJsonNull() ? jsonObjectModuleButton[0].get(Constant.ModuleID).getAsString() : "",
                                            !jsonObjectModuleButton[0].get(Constant.ModuleType).isJsonNull() ? jsonObjectModuleButton[0].get(Constant.ModuleType).getAsString() : "",
                                            !jsonObjectModuleButton[0].get(Constant.IsActive).isJsonNull() ? jsonObjectModuleButton[0].get(Constant.IsActive).getAsString() : "",
                                            !jsonObjectModuleButton[0].get(Constant.Role).isJsonNull() ? jsonObjectModuleButton[0].get(Constant.Role).getAsString() : "",
                                            !jsonObjectModuleButton[0].get(Constant.Title).isJsonNull() ? jsonObjectModuleButton[0].get(Constant.Title).getAsString() : "");
                                }
                                //berhasil
                                Intent intent = new Intent(context, HomeActivity.class);
                                context.startActivity(intent);
                            } else {
                                DialogHelper.dialogMessage(context, "Informasi", "Value kosong", Constant.DIALOG_ERROR);
                            }
                        }
                    } else {
                        DialogHelper.dialogMessage(context, "Informasi", StatusMessage[0], Constant.DIALOG_ERROR);
                    }
                } else {
                    DialogHelper.dialogMessage(context, "Informasi", "Tidak terhubung ke server (Bad Request)", Constant.DIALOG_ERROR);
                }
                databesHelper.close();
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                DialogHelper.dialogMessage(context, "Informasi", "Tidak terhubung ke server,cek kembali ip dan port", Constant.DIALOG_ERROR);
            }
        });
    }

    //DOWNLOAD KONFIGURASI
    public void downloadConfiguration() {
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(context.getString(R.string.dialog_PleaseWait));
        mProgressDialog.setMessage(context.getString(R.string.dialog_loading));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        String deViceID = ValidationHelper.getMacAddrss();
        JsonObject jsonObjectParam;
        jsonObjectParam = new JsonObject();
        jsonObjectParam.addProperty(Constant.deviceID, deViceID);
        jsonObjectParam.addProperty(Constant.token, "");
        final JsonObject[] object = new JsonObject[1];
        final boolean[] isRegister = {false};
        final String[] Title = {null};
        final String[] StatusCode = {null};
        final String[] StatusMessage = {null};
        final JsonObject[] objectValue = new JsonObject[1];
        final Call<JsonObject> mDownloadUserConfig = apiService.postUserConfig(jsonObjectParam);
        mDownloadUserConfig.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    object[0] = response.body();
                    Title[0] = object[0].get(Constant.Title).getAsString();
                    StatusCode[0] = object[0].get(Constant.StatusCode).getAsString();
                    StatusMessage[0] = object[0].get(Constant.StatusMessage).getAsString();
                    if (StatusCode[0].contains("01")) {
                        objectValue[0] = object[0].getAsJsonObject(Constant.Value);
                        if (!objectValue[0].get("EmployeeID").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.DRIVERID, objectValue[0].get("EmployeeID").getAsString());
                        }
                        if (!objectValue[0].get("BranchID").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.BRANCHID, objectValue[0].get("BranchID").getAsString());
                        }
                        if (!objectValue[0].get("BranchName").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.BRANCHNAME, objectValue[0].get("BranchName").getAsString());
                        }
                        if (!objectValue[0].get("IpPublic").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.IPCONFIG, objectValue[0].get("IpPublic").getAsString());
                        }
                        if (!objectValue[0].get("PortPublic").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PORT, objectValue[0].get("PortPublic").getAsString());
                        }
                        if (!objectValue[0].get("IpLocal").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.IPSTATION, objectValue[0].get("IpLocal").getAsString());
                        }
                        if (!objectValue[0].get("PortLocal").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PORTSTATION, objectValue[0].get("PortLocal").getAsString());
                        }
                        if (!objectValue[0].get("IpAlternative").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.IPALTERNATIF, objectValue[0].get("IpAlternative").getAsString());
                        }
                        if (!objectValue[0].get("PortAlternative").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PORTALTERNATIF, objectValue[0].get("PortAlternative").getAsString());
                        }
                        if (!objectValue[0].get("EmployeeName").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.DRIVER_NAME, objectValue[0].get("EmployeeName").getAsString());
                        }
                        if (!objectValue[0].get("Result").isJsonNull()) {
                            databesHelper.updateMobileFlag(Constant.DOWNLOAD_USER_CONFIG, objectValue[0].get("Result").getAsString());
                        }
                        if (!objectValue[0].get("Password").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PASSWORD, objectValue[0].get("Password").getAsString());
                        }
                        if (!objectValue[0].get("PasswordReset").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PASSWORDRESET, objectValue[0].get("PasswordReset").getAsString());
                        }
                        if (!objectValue[0].get(Constant.Role).isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.Role, objectValue[0].get(Constant.Role).getAsString());
                        }
                    } else {
                        isRegister[0] = false;
                        Toast.makeText(context, StatusMessage[0], Toast.LENGTH_LONG).show();
                    }
                    Toast.makeText(context, StatusMessage[0], Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Tidak terhubung ke server (Bad Request)", Toast.LENGTH_LONG).show();
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "Unduh konfigurasi pengguna gagal,tidak terhubung ke server", Toast.LENGTH_LONG).show();
                mProgressDialog.dismiss();
            }
        });
    }

    //DOWNLOAD KONFIGURASI CEK VERSION
    public void downloadCekVersionApk() {
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(context.getString(R.string.dialog_PleaseWait));
        mProgressDialog.setMessage(context.getString(R.string.dialog_loading));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        String deViceID = ValidationHelper.getMacAddrss();
        JsonObject jsonObjectParam;
        jsonObjectParam = new JsonObject();
        jsonObjectParam.addProperty(Constant.deviceID, deViceID);
        jsonObjectParam.addProperty(Constant.token, "");
        final JsonObject[] object = new JsonObject[1];
        final boolean[] isRegister = {false};
        final String[] versionServer = new String[1];
        final String[] versionMobail = new String[1];
        final String[] Title = {null};
        final String[] StatusCode = {null};
        final String[] StatusMessage = {null};
        final JsonObject[] objectValue = new JsonObject[1];
        final Call<JsonObject> mDownloadUserConfig = apiService.postUserConfig(jsonObjectParam);
        mDownloadUserConfig.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    object[0] = response.body();
                    Title[0] = object[0].get(Constant.Title).getAsString();
                    StatusCode[0] = object[0].get(Constant.StatusCode).getAsString();
                    StatusMessage[0] = object[0].get(Constant.StatusMessage).getAsString();
                    if (StatusCode[0].contains("01")) {
                        objectValue[0] = object[0].getAsJsonObject(Constant.Value);
                        String isStart = databesHelper.getVisitIsStartCheckVersion();
                        String isUpload = databesHelper.getMobilFlag(Constant.DOWNLOAD);
                        if (isStart != null) {
                            if (isStart.equals("1") && isUpload.equals("1")) {
                                isRegister[0] = false;
                                Toast.makeText(context, "Selesaikan dulu aktifitas anda ", Toast.LENGTH_LONG).show();
                            } else {
                                ////////////VERSION DARI SERVER/////////////////////
                                if (!objectValue[0].get("Version").isJsonNull()) {
                                    versionServer[0] = objectValue[0].get("Version").getAsString();
                                    String arrTempServer[] = versionServer[0].split("_");
                                    String version_server = arrTempServer[2].toString().replace(".apk", "");
                                    int mVersionServer = Integer.parseInt(version_server);
                                    ////////////VERSION DARI HP/////////////////////
                                    if (context != null) {
                                        versionMobail[0] = ValidationHelper.versionApp(context);
                                        if (versionMobail[0] != null) {
                                            int mVersionApp = Integer.parseInt(versionMobail[0]);
                                            ///////CHECK KONDISI VERSION////////////////////////
                                            if (mVersionServer > mVersionApp) {
                                                isRegister[0] = true;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            ////////////VERSION DARI SERVER/////////////////////
                            if (!objectValue[0].get("Version").isJsonNull()) {
                                versionServer[0] = objectValue[0].get("Version").getAsString();
                                String arrTempServer[] = versionServer[0].split("_");
                                String version_server = arrTempServer[2].toString().replace(".apk", "").trim();
                                int mVersionServer = Integer.parseInt(version_server);
                                ////////////VERSION DARI HP/////////////////////
                                versionMobail[0] = ValidationHelper.versionApp(context);
                                if (versionMobail[0] != null) {
                                    int mVersionApp = Integer.parseInt(versionMobail[0]);
                                    ///////CHECK KONDISI VERSION////////////////////////
                                    if (mVersionServer > mVersionApp) {
                                        isRegister[0] = true;
                                    }
                                }
                            }
                        }
                        if (isRegister[0] == true) {
                            //fungsi untuk download apk
                            Intent intent = new Intent(context, NotifActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(Constant.DOWNLOAD_APP_VERSION_NEW, Constant.DOWNLOAD_APP_VERSION_NEW);
                            if (versionServer[0] != null) {
                                intent.putExtra(Constant.NAME_VERSION, versionServer[0] = objectValue[0].get(Constant.Version).getAsString());
                            } else {
                                intent.putExtra(Constant.NAME_VERSION, "");
                            }
                            context.startActivity(intent);
                            Toast.makeText(context, "fungsi download apk", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        } else {
                            Toast.makeText(context, "Belum ada update aplikasi baru ", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        isRegister[0] = false;
                        Toast.makeText(context, StatusMessage[0], Toast.LENGTH_LONG).show();
                    }
                } else {
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "Unduh konfigurasi pengguna gagal,tidak terhubung ke server", Toast.LENGTH_LONG).show();
                mProgressDialog.dismiss();
            }
        });
    }

    //////UPLOAD  SQLITE
    public void uploadFileSqlite(final Context context) {
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(context.getString(R.string.dialog_PleaseWait));
        mProgressDialog.setMessage(context.getString(R.string.dialog_proses_upload_sqlite));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        DatabesHelper databesHelper = new DatabesHelper(context);
        String BranchID = databesHelper.getBranchID();
        String DriverId = databesHelper.getEmployeeID();
        SimpleDateFormat Date = new SimpleDateFormat("yyyyMMdd");
        String date = Date.format(new Date());
        databesHelper.close();
        try {
            ////////////VERSION APP/////////////////////
            String app_ver = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (app_ver != null) {
                if (BranchID != null && DriverId != null) {
                    final String[] result = {null};
                    final JsonObject[] object = new JsonObject[1];
                    String namefile = DriverId + "-v" + app_ver;
                    String path = context.getExternalFilesDir(null).getAbsolutePath() + "/" + Constant.DB_NAME;
                    final File file = new File(path);
                    // create RequestBody instance from file
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("file", namefile + "-" + file.getName(), requestFile);
                    final Call<JsonObject> uploadFile = apiService.postUploadFile("application/octet-stream", body);
                    /*RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("file", imageId + ".jpeg", requestFile);*/
                    //final Call<JsonObject> uploadFile = apiService.postUploadFileSqlite(body);
                    uploadFile.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.isSuccessful()) {
                                object[0] = response.body();
                                result[0] = object[0].get("Result").getAsString();
                            }
                            mProgressDialog.dismiss();
                            Toast.makeText(context, result[0], Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            // resultBoolean   = false;
                            mProgressDialog.dismiss();
                            Toast.makeText(context, String.valueOf(t), Toast.LENGTH_LONG).show();
                        }
                    });

                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(context, "BranchID dan Employee tidak di temukan", Toast.LENGTH_LONG).show();
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
            mProgressDialog.dismiss();
        }
    }

    public void CheckWifi() {
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiInfo = wifiManager.getConnectionInfo();
        //Received signal strength value making
        level = wifiInfo.getRssi();
        //According to the obtained signal intensity of transmitted information making
        //IP & PORT CONFIG /CLOUD
        ipConfig = databesHelper.getMobileConfig(Constant.IPCONFIG);
        portConfig = databesHelper.getMobileConfig(Constant.PORT);
        //IP & PORT STATION
        IpStation = databesHelper.getMobileConfig(Constant.IPSTATION);
        PortStation = databesHelper.getMobileConfig(Constant.PORTSTATION);
        //IP & PORT USER
        IpUser = databesHelper.getMobileConfig(Constant.IPUSER);
        PortUser = databesHelper.getMobileConfig(Constant.PORTUSER);
        if (level <= 0 && level >= -100) {
            if (IpUser != null && IpStation != null) {
                if (IpUser != IpStation) {
                    //Update Ipuser Menjadi IpStation
                    IpUser = IpStation;
                    databesHelper.updateMobileConfig(IPUSER, IpStation);
                }
                //  helper.close();
            }
            if (PortUser != null && PortStation != null) {
                if (PortUser != PortStation) {
                    //Update PortUser Menjadi PortStation
                    PortUser = PortStation;
                    databesHelper.updateMobileConfig(PORTUSER, PortStation);
                    //  helper.close();
                }
            }
        } else if (level < -50 && level >= -70) {
            if (IpUser != null && IpStation != null) {
                if (IpUser != IpStation) {
                    //Update Ipuser Menjadi IpStation
                    IpUser = IpStation;
                    databesHelper.updateMobileConfig(IPUSER, IpStation);
                    // helper.close();
                }
            }
            if (PortUser != null && PortStation != null) {
                if (PortUser != PortStation) {
                    //Update PortUser Menjadi PortStation
                    PortUser = PortStation;
                    databesHelper.updateMobileConfig(PORTUSER, PortStation);
                    //helper.close();
                }
            }
        } else if (level < -70 && level >= -80) {
            if (IpUser != null && IpStation != null) {
                if (IpUser != IpStation) {
                    //Update Ipuser Menjadi IpStation
                    IpUser = IpStation;
                    databesHelper.updateMobileConfig(IPUSER, IpStation);
                    // helper.close();
                }
            }
            if (PortUser != null && PortStation != null) {
                if (PortUser != PortStation) {
                    //Update PortUser Menjadi PortStation
                    PortUser = PortStation;
                    databesHelper.updateMobileConfig(PORTUSER, PortStation);
                    // helper.close();
                }
            }
        } else if (level < -80 && level >= -100) {
            if (IpUser != null && IpStation != null) {
                if (IpUser != IpStation) {
                    //Update Ipuser Menjadi IpStation
                    IpUser = IpStation;
                    databesHelper.updateMobileConfig(IPUSER, IpStation);
                    //   helper.close();
                }
            }
            if (PortUser != null && PortStation != null) {
                if (PortUser != PortStation) {
                    //Update PortUser Menjadi PortStation
                    PortUser = PortStation;
                    databesHelper.updateMobileConfig(PORTUSER, PortStation);
                    // helper.close();
                }
            }
        } else {
            if (IpUser != null && ipConfig != null) {
                if (IpUser != ipConfig) {
                    //Update Ipuser Menjadi IpStation
                    IpUser = ipConfig;
                    databesHelper.updateMobileConfig(IPUSER, ipConfig);
                    //  helper.close();
                }
            }
            if (PortUser != null && portConfig != null) {
                if (PortUser != portConfig) {
                    //Update PortUser Menjadi PortStation
                    PortUser = portConfig;
                    databesHelper.updateMobileConfig(PORTUSER, portConfig);
                    // helper.close();
                }
            }
        }
    }
}