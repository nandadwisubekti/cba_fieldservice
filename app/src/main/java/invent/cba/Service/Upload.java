package invent.cba.Service;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Helper.data.SharedPreferenceHelper;
import invent.cba.Model.ModelPOSMStock;
import invent.cba.Model.ModelPromoVisit;
import invent.cba.Model.ModelRegisterNewCustomer;
import invent.cba.Model.ModelVisit;
import invent.cba.Model.ModelVisitDetail;
import invent.cba.Model.ModuleCompetitor;
import invent.cba.Model.ModuleCustomerImage;
import invent.cba.Model.ModuleSisaStockVisit;
import invent.cba.Model.ModuleTools;

/**
 * Created by kahfizain on 28/09/2017.
 */

public class Upload {

    Context context;
    DatabesHelper databesHelper;

    String EmployeeID,
            branchID;

    String deViceID;
    ConnectionClient connectionClient;

    public Upload(Context context, Boolean... runFlag){
        this.context = context;
        databesHelper = new DatabesHelper(context);
        connectionClient = new ConnectionClient(context);

        EmployeeID = databesHelper.getEmployeeID();
        branchID = databesHelper.getBranchID();
        deViceID = ValidationHelper.getMacAddrss();

        Boolean runImmediately = runFlag.length > 0 ? runFlag[0] : true;
        if(runImmediately) new postUpload().execute();

    }


    private class postUpload extends AsyncTask<Void, Void, Void> {
        boolean isSuccess = true;
        ProgressDialog pDialog;
        @Override
        protected Void doInBackground(Void... params) {

            try{


             /*   isSuccess = openTabelRegisterNewCustomer();
                isSuccess = openTabelRegisterNewCustomerDetail();
*/


                    // UPLOAD VISIT
                isSuccess = openTabelVisit();
                if (isSuccess == true)
                    // UPLOAD VISIT DETAIL
                    isSuccess = openTabelVisitDetail();

                if (isSuccess ==  true)
                    //SISA STOCK VISIT
                    isSuccess = openTabelSisaStockVisit();

                if (isSuccess == true)
                    //UPLOAD CUSTEMOR IMAGE
                    isSuccess = openTabelCustomerImage();

                    //PROMO VISIT
                if (isSuccess == true)
                    isSuccess = openTabelPromoVisit();

                //PROMO POSM STCOK
                if (isSuccess == true)
                    isSuccess = openTabelPOSMStock();

                //CompetitorActivityVisit
                if (isSuccess == true)
                    isSuccess = openTabelCompetitorActivityVisit();

                //CompetitorActivityImage
                if (isSuccess == true)
                    isSuccess = openTabelCompetitorActivityImage();

                //openTabelPromoCompetitor
                if (isSuccess == true)
                    isSuccess = openTabelPromoCompetitor();

                if (isSuccess == true)
                    isSuccess = openTabelPOSMStockVisit();

                //register new Cutomer
                if (isSuccess == true)
                    isSuccess = openTabelRegisterNewCustomer();
                if (isSuccess == true)
                    isSuccess = openTabelRegisterNewCustomerDetail();


            }catch (Exception e){
                e.printStackTrace();
                isSuccess = false;
            }


            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setTitle("Mohon tunggu!");
            pDialog.setMessage("Memuat");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pDialog.isShowing()){
                databesHelper.updateDownloadUpload(Constant.UPLOAD, ValidationHelper.dateFormat());

                if(isSuccess == true){

                    SharedPreferenceHelper.saveEnableModulePersipanTools(context,null);

                    databesHelper.updateModuleButton(Constant.PERSIAPAN,Constant.HOME,"0");
                    databesHelper.updateModuleButton(Constant.MULAIPERJALANAN,Constant.HOME,"0");
                    databesHelper.updateModuleButton(Constant.DOWNLOAD,Constant.DOWNLOAD_UPLOAD,"0");
                    databesHelper.updateModuleButton(Constant.UPLOAD,Constant.DOWNLOAD_UPLOAD,"0");
                    databesHelper.updateModuleButton(Constant.KUNJUNGAN,Constant.HOME,"0");
                    databesHelper.updateModuleButton(Constant.ISSU,Constant.HOME,"0");
                    databesHelper.updateModuleButton(Constant.AKHIRIKEGIATAN,Constant.HOME,"0");
                    databesHelper.updateModuleButton(Constant.REPORT,Constant.HOME,"0");

                    databesHelper.updateMobileFlag(Constant.DOWNLOAD, "0");
                    databesHelper.updateModuleButton(Constant.NEWCOUNTER,Constant.HOME,"0");

                    DialogHelper.dialogMessage(context,"Informasi","Upload berhasil",Constant.DIALOG_SUCCESS);
                    pDialog.dismiss();


                }else {

                    pDialog.dismiss();
                    DialogHelper.dialogMessage(context,"Informasi","Upload gagal,coba lagi!",Constant.DIALOG_ERROR);

                }
            }


        }
    }



    //tabel visit
    public boolean openTabelVisit(){
        boolean isSuccess = false;
        databesHelper = new DatabesHelper(context);

        ArrayList<ModelVisit> arrayListVisit = new ArrayList<>();
        arrayListVisit.clear();
        JsonArray jsonArrayVisit ;
        JsonObject jsonObjectVisit;

        arrayListVisit = databesHelper.getArrayListVisit();


        if (arrayListVisit.size()>0){

            jsonArrayVisit = new JsonArray();
            for (ModelVisit data : arrayListVisit) {
                jsonObjectVisit = new JsonObject();
                jsonObjectVisit.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectVisit.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectVisit.addProperty(Constant.VehicleID, data.getVehicleNumber().toString());
                jsonObjectVisit.addProperty(Constant.isStart, data.getIsStart().toString());
                jsonObjectVisit.addProperty(Constant.StartDate, data.getStartDate().toString());
                jsonObjectVisit.addProperty(Constant.EndDate, data.getFinishDate().toString());
                jsonObjectVisit.addProperty(Constant.isFinish, data.getIsFinish().toString());
                jsonObjectVisit.addProperty(Constant.KMStart, data.getKMStart().toString());
                jsonObjectVisit.addProperty(Constant.KMFinish, data.getKMFinish().toString());
                jsonObjectVisit.addProperty(Constant.VisitDate, data.getVisitDate().toString());
                jsonObjectVisit.addProperty(Constant.deviceID, deViceID);
                jsonObjectVisit.addProperty(Constant.BranchID,branchID);
                jsonArrayVisit.add(jsonObjectVisit);
            }

            if (jsonArrayVisit.size()>0){
                Gson gsonOrder = new Gson();
                String jsonrequestSalesOrder = gsonOrder.toJson(jsonArrayVisit);
                Log.d("upload-sales-order", jsonrequestSalesOrder);
                Log.i("assist",jsonrequestSalesOrder);
                int checkIsSucess = connectionClient.postUploadVisitAuto(jsonArrayVisit);
                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }
        }else {
            isSuccess = true;
        }

        databesHelper.close();
        return isSuccess;
    }


    public boolean openTabelVisitDetail(){

        //OPEN TABEL VISIT
        boolean isSuccess = false;
        ArrayList<ModelVisitDetail> arrayListVisitDetail = new ArrayList<>();
        JsonArray jsonArrayVisitDetail ;
        JsonObject jsonObjectVisitDetail;

        arrayListVisitDetail.clear();
        databesHelper = new DatabesHelper(context);

        arrayListVisitDetail = databesHelper.getArrayListVisitDetail();


        if (arrayListVisitDetail.size()>0){

            jsonArrayVisitDetail = new JsonArray();
            for (ModelVisitDetail data : arrayListVisitDetail) {
                jsonObjectVisitDetail = new JsonObject();
                jsonObjectVisitDetail.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectVisitDetail.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectVisitDetail.addProperty(Constant.isStart, data.getIsStart().toString());
                jsonObjectVisitDetail.addProperty(Constant.isFinish, data.getIsFinish().toString());
                jsonObjectVisitDetail.addProperty(Constant.StartDate, data.getStartDate().toString());
                jsonObjectVisitDetail.addProperty(Constant.EndDate, data.getFinishDate().toString());
                jsonObjectVisitDetail.addProperty(Constant.ReasonID, data.getReasonID().toString());
                jsonObjectVisitDetail.addProperty(Constant.ReasonDescription, data.getReasonDescription().toString());
//                jsonObjectVisitDetail.addProperty(Constant.Latitude, data.getLatitude().toString());
                jsonObjectVisitDetail.addProperty(Constant.Latitude, data.getLatitude() == null ? "" : data.getLatitude().toString());
//                jsonObjectVisitDetail.addProperty(Constant.Longitude, data.getLongitude().toString());
                jsonObjectVisitDetail.addProperty(Constant.Longitude, data.getLongitude() == null ? "" : data.getLongitude().toString());
                jsonObjectVisitDetail.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectVisitDetail.addProperty(Constant.isDeliver, data.getIsDeliver().toString());
                jsonObjectVisitDetail.addProperty(Constant.WarehouseID, data.getWarehouseID().toString());
                jsonObjectVisitDetail.addProperty(Constant.isInRange, data.getIsInRangeCheckin().toString());
                jsonObjectVisitDetail.addProperty(Constant.isInRangeCheckout, data.getIsInRangeCheckout().toString());
                jsonObjectVisitDetail.addProperty(Constant.Distance, data.getDistance().toString());
                jsonObjectVisitDetail.addProperty(Constant.Duration, data.getDuration().toString());
                jsonObjectVisitDetail.addProperty(Constant.DistanceCheckout, data.getDistanceCheckout().toString());
                jsonObjectVisitDetail.addProperty(Constant.deviceID,deViceID);
                jsonObjectVisitDetail.addProperty(Constant.BranchID,branchID);
//                jsonObjectVisitDetail.addProperty(Constant.LatitudeCheckOut,data.getLatitudeCheckOut().toString());
                jsonObjectVisitDetail.addProperty(Constant.LatitudeCheckOut,data.getLatitudeCheckOut() == null ? "" : data.getLatitudeCheckOut().toString());
//                jsonObjectVisitDetail.addProperty(Constant.LongitudeCheckOut, data.getLongitudeCheckOut().toString());
                jsonObjectVisitDetail.addProperty(Constant.LongitudeCheckOut, data.getLongitudeCheckOut() == null ? "" : data.getLongitudeCheckOut().toString());
                jsonObjectVisitDetail.addProperty(Constant.isVisit, data.getIsVisit().toString());
                jsonObjectVisitDetail.addProperty(Constant.ReasonSequence, data.getReasonSequence().toString());

                jsonArrayVisitDetail.add(jsonObjectVisitDetail);
            }

            if (jsonArrayVisitDetail.size()>0){
                int checkIsSucess = connectionClient.postUploadVisitDetailAuto(jsonArrayVisitDetail);
                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }

        }else {

        }

        databesHelper.close();
        return isSuccess;
    }


    //tabel customer iage
    public boolean openTabelCustomerImage(){

        //OPEN TABEL VISIT
        boolean isSuccess = false;
        JsonArray jsonArrayCustmerImage ;
        JsonObject jsonObjectCustmerImage;

        ArrayList<ModuleCustomerImage> arrayListCustmerImage = new ArrayList<>();
        arrayListCustmerImage.clear();
        databesHelper = new DatabesHelper(context);
        arrayListCustmerImage = databesHelper.getArrayListCustomerImageUpload();

        if (arrayListCustmerImage.size()>0){
            jsonArrayCustmerImage = new JsonArray();
            for (ModuleCustomerImage data : arrayListCustmerImage) {
                String fullApkPath = data.getImageBase64().toString();
                String[] apkPathSegments = fullApkPath.split("/");
                String splittedApkName = apkPathSegments[apkPathSegments.length-1];

                jsonObjectCustmerImage = new JsonObject();
                jsonObjectCustmerImage.addProperty(Constant.ImageID, data.getImageID().toString());
                jsonObjectCustmerImage.addProperty(Constant.ImageDate, data.getImageDate().toString());
                jsonObjectCustmerImage.addProperty(Constant.ImageBase64, splittedApkName);
                jsonObjectCustmerImage.addProperty(Constant.ImageType, data.getImageType().toString());
                jsonObjectCustmerImage.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectCustmerImage.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectCustmerImage.addProperty(Constant.DocNumber, data.getDocNumber().toString());
                jsonObjectCustmerImage.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectCustmerImage.addProperty(Constant.WarehouseID, data.getWarehouseID().toString());
                jsonObjectCustmerImage.addProperty(Constant.BranchID, branchID);
                jsonObjectCustmerImage.addProperty(Constant.deviceID,deViceID);
                jsonObjectCustmerImage.addProperty(Constant.Latitude,data.getLatitude());
                jsonObjectCustmerImage.addProperty(Constant.Longitude,data.getLongitude());
                jsonArrayCustmerImage.add(jsonObjectCustmerImage);

            }

            if (jsonArrayCustmerImage.size()>0){

                int checkIsSucess = connectionClient.postUploadCustomerImageAuto(jsonArrayCustmerImage);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }


    //tabel SisaStockVisit
    public boolean openTabelSisaStockVisit(){

        //OPEN TABEL VISIT
        boolean isSuccess = false;
        JsonArray jsonArraySisaStockVisit ;
        JsonObject jsonObjectSisaStockVisit;

        ArrayList<ModuleSisaStockVisit> moduleSisaStockVisitArrayList = new ArrayList<>();
        moduleSisaStockVisitArrayList.clear();
        databesHelper = new DatabesHelper(context);
        moduleSisaStockVisitArrayList = databesHelper.getArraySisaStockVisitUpload();

        if (moduleSisaStockVisitArrayList.size()>0){
            jsonArraySisaStockVisit = new JsonArray();
            for (ModuleSisaStockVisit data : moduleSisaStockVisitArrayList) {
                jsonObjectSisaStockVisit = new JsonObject();
                jsonObjectSisaStockVisit.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.ProductID, data.getProductID().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.BranchID, branchID);
                jsonObjectSisaStockVisit.addProperty(Constant.UOM, data.getUOM().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.SisaStockTypeID, data.getSisaStockTypeID().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.Value, Double.valueOf(data.getValue().toString()));
                jsonObjectSisaStockVisit.addProperty(Constant.Status, data.getStatus().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.Notes, data.getNotes().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.CreatedDate, data.getCreatedDate().toString());
                jsonObjectSisaStockVisit.addProperty(Constant.CreatedBy, data.getCreatedBy().toString());
                jsonArraySisaStockVisit.add(jsonObjectSisaStockVisit);

            }

            if (jsonArraySisaStockVisit.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertSisaStockVisit(jsonArraySisaStockVisit);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }


    //tabel promo visit
    public boolean openTabelPromoVisit(){

        //OPEN TABEL VISIT
        boolean isSuccess = false;
        JsonArray jsonArrayPromoVisit ;
        JsonObject jsonObjectPromoVisit;

        ArrayList<ModelPromoVisit> modelPromoVisitArrayList = new ArrayList<>();
        modelPromoVisitArrayList.clear();
        databesHelper = new DatabesHelper(context);
        modelPromoVisitArrayList = databesHelper.getArrayListPromoVisit();

        if (modelPromoVisitArrayList.size()>0){
            jsonArrayPromoVisit = new JsonArray();
            for (ModelPromoVisit data : modelPromoVisitArrayList) {
                jsonObjectPromoVisit = new JsonObject();
                jsonObjectPromoVisit.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectPromoVisit.addProperty(Constant.ProductID, data.getProductID().toString());
                jsonObjectPromoVisit.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectPromoVisit.addProperty(Constant.PromoID, data.getPromoID().toString());
                jsonObjectPromoVisit.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectPromoVisit.addProperty(Constant.BranchID, branchID);
                jsonObjectPromoVisit.addProperty(Constant.Image, "");
                jsonObjectPromoVisit.addProperty(Constant.Notes, "");
                jsonObjectPromoVisit.addProperty(Constant.CreatedDate, ValidationHelper.dateFormat());
                jsonObjectPromoVisit.addProperty(Constant.CreatedBy,data.getEmployeeID().toString());
                jsonArrayPromoVisit.add(jsonObjectPromoVisit);

            }

            if (jsonArrayPromoVisit.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertPromoVisit(jsonArrayPromoVisit);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }


    //tabel POSMStock
    public boolean openTabelPOSMStock(){


        boolean isSuccess = false;
        JsonArray jsonArrayPOSMStock ;
        JsonObject jsonObjectPOSMStock ;

        ArrayList<ModuleTools> moduleToolsArrayList = new ArrayList<>();
        moduleToolsArrayList.clear();
        databesHelper = new DatabesHelper(context);
        moduleToolsArrayList = databesHelper.getArraListPOSMStock();

        if (moduleToolsArrayList.size()>0){
            jsonArrayPOSMStock = new JsonArray();
            for (ModuleTools data : moduleToolsArrayList) {
                jsonObjectPOSMStock = new JsonObject();
                jsonObjectPOSMStock.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectPOSMStock.addProperty(Constant.POSMID, data.getPOSMID().toString());
                jsonObjectPOSMStock.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectPOSMStock.addProperty(Constant.BranchID, branchID);
                jsonObjectPOSMStock.addProperty(Constant.Quantity, data.getQuantity().toString());
                jsonObjectPOSMStock.addProperty(Constant.CreatedDate, ValidationHelper.dateFormat());
                jsonObjectPOSMStock.addProperty(Constant.CreatedBy,data.getEmployeeID().toString());
                jsonArrayPOSMStock.add(jsonObjectPOSMStock);

            }

            if (jsonArrayPOSMStock.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertPOSMStock(jsonArrayPOSMStock);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }


    //tabel CompetitorActivityVisit
    public boolean openTabelCompetitorActivityVisit(){


        boolean isSuccess = false;
        JsonArray jsonArrayCompetitorArrayList ;
        JsonObject jsonObjectCompetitorArrayList;

        ArrayList<ModuleCompetitor> moduleCompetitorArrayList = new ArrayList<>();
        moduleCompetitorArrayList.clear();
        databesHelper = new DatabesHelper(context);
        moduleCompetitorArrayList = databesHelper.getArrayListCompetitorActivityVisit("1");

        if (moduleCompetitorArrayList.size()>0){
            jsonArrayCompetitorArrayList = new JsonArray();
            for (ModuleCompetitor data : moduleCompetitorArrayList) {
                jsonObjectCompetitorArrayList = new JsonObject();
                jsonObjectCompetitorArrayList.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.CompetitorID, data.getCompetitorID().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.CompetitorProductID, data.getCompetitorProductID().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.BranchID,branchID);
                jsonObjectCompetitorArrayList.addProperty(Constant.CompetitorActivityTypeID, data.getCompetitorActivityTypeID().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.Value,data.getValue().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.Notes,data.getNotes().toString());
                jsonObjectCompetitorArrayList.addProperty(Constant.CreatedDate,ValidationHelper.dateFormat());
                jsonObjectCompetitorArrayList.addProperty(Constant.CreatedBy,data.getEmployeeID().toString());
                jsonArrayCompetitorArrayList.add(jsonObjectCompetitorArrayList);

            }

            if (jsonArrayCompetitorArrayList.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertCompetitorActivityVisit(jsonArrayCompetitorArrayList);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }


    //tabel CompetitorActivityImage
    public boolean openTabelCompetitorActivityImage(){


        boolean isSuccess = false;
        JsonArray jsonArrayCompetitorActivityImage;
        JsonObject jsonObjectCompetitorActivityImage;

        ArrayList<ModuleCompetitor> moduleCompetitorActivityImageArrayList = new ArrayList<>();
        moduleCompetitorActivityImageArrayList.clear();
        databesHelper = new DatabesHelper(context);
        moduleCompetitorActivityImageArrayList = databesHelper.getArrayListCompetitorActivityImage();

        if (moduleCompetitorActivityImageArrayList.size()>0){
            jsonArrayCompetitorActivityImage = new JsonArray();
            for (ModuleCompetitor data : moduleCompetitorActivityImageArrayList) {
                jsonObjectCompetitorActivityImage = new JsonObject();
                jsonObjectCompetitorActivityImage.addProperty(Constant.ImageID, data.getCompetitorActivityImageID().toString());
                jsonObjectCompetitorActivityImage.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectCompetitorActivityImage.addProperty(Constant.CompetitorID, data.getCompetitorID().toString());
                jsonObjectCompetitorActivityImage.addProperty(Constant.CompetitorProductID, data.getCompetitorProductID().toString());
                jsonObjectCompetitorActivityImage.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectCompetitorActivityImage.addProperty(Constant.EmployeeID, EmployeeID);
                jsonObjectCompetitorActivityImage.addProperty(Constant.BranchID,branchID);
                jsonObjectCompetitorActivityImage.addProperty(Constant.Image, data.getImage().toString());
                jsonObjectCompetitorActivityImage.addProperty(Constant.CreatedDate,ValidationHelper.dateFormat());
                jsonObjectCompetitorActivityImage.addProperty(Constant.CreatedBy,EmployeeID);

                jsonArrayCompetitorActivityImage.add(jsonObjectCompetitorActivityImage);

            }

            if (jsonArrayCompetitorActivityImage.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertCompetitorActivityImage(jsonArrayCompetitorActivityImage);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }

    //tabel PromoCompetitor
    public boolean openTabelPromoCompetitor(){


        boolean isSuccess = false;
        JsonArray jsonArrayPromoCompetitor;
        JsonObject jsonObjectPromoCompetitor;

        ArrayList<ModuleCompetitor> modulePromoCompetitorArrayList = new ArrayList<>();
        modulePromoCompetitorArrayList.clear();
        databesHelper = new DatabesHelper(context);
        modulePromoCompetitorArrayList = databesHelper.getArrayListPromoCompetitor("true");

        if (modulePromoCompetitorArrayList.size()>0){
            jsonArrayPromoCompetitor = new JsonArray();
            for (ModuleCompetitor data : modulePromoCompetitorArrayList) {
                jsonObjectPromoCompetitor = new JsonObject();
                jsonObjectPromoCompetitor.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectPromoCompetitor.addProperty(Constant.CompetitorID, data.getCompetitorID().toString());
                jsonObjectPromoCompetitor.addProperty(Constant.CompetitorProductID,data.getCompetitorProductID().toString());
                jsonObjectPromoCompetitor.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectPromoCompetitor.addProperty(Constant.PromoID, data.getPromoID().toString());
                jsonObjectPromoCompetitor.addProperty(Constant.EmployeeID, EmployeeID);
                jsonObjectPromoCompetitor.addProperty(Constant.BranchID,branchID);
                jsonObjectPromoCompetitor.addProperty(Constant.CreatedDate, ValidationHelper.dateFormat());
                jsonObjectPromoCompetitor.addProperty(Constant.CreatedBy, EmployeeID);

                jsonArrayPromoCompetitor.add(jsonObjectPromoCompetitor);

            }

            if (jsonArrayPromoCompetitor.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertPromoCompetitor(jsonArrayPromoCompetitor);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }


    //tabel POSMStockVisit
    public boolean openTabelPOSMStockVisit(){


        boolean isSuccess = false;
        JsonArray jsonArrayPOSMStockVisit;
        JsonObject jsonObjectPOSMStockVisit;

        ArrayList<ModelPOSMStock> modelPOSMStockArrayList = new ArrayList<>();
        modelPOSMStockArrayList.clear();
        databesHelper = new DatabesHelper(context);
        modelPOSMStockArrayList = databesHelper.getArrayListPOSMStockVisitUpload();

        if (modelPOSMStockArrayList.size()>0){
            jsonArrayPOSMStockVisit = new JsonArray();
            for (ModelPOSMStock data : modelPOSMStockArrayList) {
                jsonObjectPOSMStockVisit = new JsonObject();
                jsonObjectPOSMStockVisit.addProperty(Constant.VisitID, data.getVisitID().toString());
                jsonObjectPOSMStockVisit.addProperty(Constant.CustomerID, data.getCustomerID().toString());
                jsonObjectPOSMStockVisit.addProperty(Constant.POSMID,data.getPOSMID().toString());
                jsonObjectPOSMStockVisit.addProperty(Constant.EmployeeID, EmployeeID);
                jsonObjectPOSMStockVisit.addProperty(Constant.BranchID,branchID);
                jsonObjectPOSMStockVisit.addProperty(Constant.Quantity, data.getQuantity().toString());
                jsonObjectPOSMStockVisit.addProperty(Constant.CreatedDate, data.getCreatedDate().toString());
                jsonObjectPOSMStockVisit.addProperty(Constant.CreatedBy, data.getCreatedBy().toString());
                jsonArrayPOSMStockVisit.add(jsonObjectPOSMStockVisit);

            }

            if (jsonArrayPOSMStockVisit.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertPOSMVisit(jsonArrayPOSMStockVisit);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }



    //tabel RegisterNewCustomer
    public boolean openTabelRegisterNewCustomer(){


        boolean isSuccess = false;
        JsonArray jsonArrayRegisterNewCustomer;
        JsonObject jsonObjectRegisterNewCustomer;

        ArrayList<ModelRegisterNewCustomer> registerNewCustomerArrayList = new ArrayList<>();
        registerNewCustomerArrayList.clear();
        databesHelper = new DatabesHelper(context);
        registerNewCustomerArrayList = databesHelper.getArrayListRegisterNewCustomer();

        if (registerNewCustomerArrayList.size()>0){
            jsonArrayRegisterNewCustomer = new JsonArray();
            for (ModelRegisterNewCustomer data : registerNewCustomerArrayList) {
                jsonObjectRegisterNewCustomer = new JsonObject();
                jsonObjectRegisterNewCustomer.addProperty(Constant.SurveyID, data.getRegisterID().toString());
                jsonObjectRegisterNewCustomer.addProperty(Constant.EmployeeID, data.getEmployeeID().toString());
                jsonObjectRegisterNewCustomer.addProperty(Constant.VisitID,data.getVisitID().toString());
                jsonObjectRegisterNewCustomer.addProperty(Constant.Date, data.getDate());
                jsonObjectRegisterNewCustomer.addProperty(Constant.Latitude,data.getLatitude());
                jsonObjectRegisterNewCustomer.addProperty(Constant.Longitude, data.getLongitude());
                jsonObjectRegisterNewCustomer.addProperty(Constant.BranchID, branchID);
                jsonObjectRegisterNewCustomer.addProperty(Constant.CustomerID, data.getCustomerID());
                jsonObjectRegisterNewCustomer.addProperty(Constant.QuestionCategoryID, data.getQuestionCategoryID());


                jsonArrayRegisterNewCustomer.add(jsonObjectRegisterNewCustomer);

            }

            //fuction upload
            if (jsonArrayRegisterNewCustomer.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertRegisterNewCustomer(jsonArrayRegisterNewCustomer);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }

    //tabel RegisterNewCustomerDetail
    public boolean openTabelRegisterNewCustomerDetail(){


        boolean isSuccess = false;
        JsonArray jsonArrayRegisterNewCustomer;
        JsonObject jsonObjectRegisterNewCustomer;

        ArrayList<ModelRegisterNewCustomer> registerNewCustomerArrayList = new ArrayList<>();
        registerNewCustomerArrayList.clear();
        databesHelper = new DatabesHelper(context);
        registerNewCustomerArrayList = databesHelper.getArrayListRegisterNewCustomertDetail();

        if (registerNewCustomerArrayList.size()>0){
            jsonArrayRegisterNewCustomer = new JsonArray();
            for (ModelRegisterNewCustomer data : registerNewCustomerArrayList) {
                jsonObjectRegisterNewCustomer = new JsonObject();
                jsonObjectRegisterNewCustomer.addProperty(Constant.SurveyID, data.getRegisterID().toString());
                jsonObjectRegisterNewCustomer.addProperty(Constant.QuestionID, data.getQuestionID().toString());
                jsonObjectRegisterNewCustomer.addProperty(Constant.AnswerID,data.getAnswerID().toString());
                jsonObjectRegisterNewCustomer.addProperty(Constant.Value, data.getValue());
                jsonArrayRegisterNewCustomer.add(jsonObjectRegisterNewCustomer);

            }

            if (jsonArrayRegisterNewCustomer.size()>0){

                int checkIsSucess = connectionClient.postUploadInsertRegisterNewCustomerDetail(jsonArrayRegisterNewCustomer);

                if (checkIsSucess == 1){
                    isSuccess = true;
                }else {
                    isSuccess = false;
                }

            }else {
                isSuccess = true;
            }


        }else {
            isSuccess = true;

        }

        databesHelper.close();
        return isSuccess;
    }




}
