package invent.cba.Service;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;


/**
 * Created by kahfizain on 02/10/2017.
 */

public class DownloadVersionApp {

    Context context;

    public DownloadVersionApp(Context context,String NameVersion){
        this.context = context;

        DatabesHelper databesHelper = new DatabesHelper(context);
        String ipConfig = databesHelper.getMobileConfig(Constant.IPCONFIG);
        String portConfig = databesHelper.getMobileConfig(Constant.PORT);

        String file_url = "http://"+ipConfig.trim()+":"+portConfig.trim()+"/"+NameVersion;
        new DownloadFileFromURL(NameVersion).execute(file_url);
    }

    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        String NameVersionApp;
        ProgressDialog pDialog;
        boolean timeoutDownload = false;


        private DownloadFileFromURL (String NameVersionApp){
            this.NameVersionApp= NameVersionApp;
        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Pembaharuan Aplikasi.....");
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;

            timeoutDownload = false;

            try {

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
              //  String outFileName = context.getExternalFilesDir(null).getAbsolutePath() +"/sdcard/"+ NameVersionApp;

                OutputStream output = new FileOutputStream("/sdcard/"+NameVersionApp);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                timeoutDownload = true;
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            super.onPostExecute(file_url);

            if (pDialog.isShowing()){
                pDialog.dismiss();
                if (timeoutDownload == true){
                    Toast.makeText(context, "Update aplikasi gagal.", Toast.LENGTH_LONG).show();

                }else{

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        try {
                            File file =  new File(Environment.getExternalStorageDirectory().getPath()+"/"+NameVersionApp);
                            //Uri fileUri = Uri.fromFile(file);

                            Uri fileUri = FileProvider.getUriForFile(context, "invent.cfc",
                                    file);

                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                                if (!context.getPackageManager().canRequestPackageInstalls()) {
                                    requestPackageInstallsPermission(context);
                                }
                                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                                intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                context.startActivity(intent);

                            } else {
                                Intent intent = new Intent(Intent.ACTION_VIEW, fileUri);
                                intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                                intent.setDataAndType(fileUri, "application/vnd.android" + ".package-archive");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                context.startActivity(intent);
                            }



                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(new File("/sdcard/"+NameVersionApp)), "application/vnd.android.package-archive");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);


                    }





                }
            }



        }

        @TargetApi(Build.VERSION_CODES.O)
        private void requestPackageInstallsPermission(Context context) {
            context.startActivity(
                    new Intent(
                            Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES,
                            Uri.parse(String.format("package:%s", context.getPackageName()))
                    )
            );

        }


    }

}
