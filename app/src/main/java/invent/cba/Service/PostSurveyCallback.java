package invent.cba.Service;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import invent.cba.Helper.DatabesHelper;
import invent.cba.Model.ResultSurvey;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

abstract public class PostSurveyCallback implements Callback<JsonObject> {

    private Context mContext;

    private ResultSurvey mItem;

    protected PostSurveyCallback(Context context, ResultSurvey item) {
        this.mContext = context;
        this.mItem = item;
    }

    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
        if (response.isSuccessful()) {
            try {
                JsonObject object = response.body();
                JsonArray array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                JsonElement resultElement = object.get("Result");
                String result = resultElement.getAsString();
                if (result.equals("1")) {
                    DatabesHelper helper = new DatabesHelper(mContext);
                    if (helper.openDataBase()) {
                        helper.updateIsUploadResultSurvey(mItem, "1");
                        helper.close();
                    }
                    onSuccess(response);
                } else {
                    onFailure(call, new RuntimeException("Server was responded, but probably not inserted to server. " +
                            "Result doesn\'t equals to 1. " +
                            "Ask to the server administrator if the recent data sent to the server was saved. " +
                            "Here's are the server response : \n" + result));
                }
            } catch (Throwable reason) {
                onFailure(call, reason);
            }
        } else {
            onFailure(call, new RuntimeException(response.message()));
        }
    }

    public abstract void onSuccess(Response<JsonObject> response);

}
