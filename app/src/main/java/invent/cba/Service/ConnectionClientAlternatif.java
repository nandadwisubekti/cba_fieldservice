package invent.cba.Service;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Interface.ConnectionInterface;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ConnectionClientAlternatif {

    static Retrofit retrofit = null;
    private ConnectionInterface apiService;

    DatabesHelper helper;
    Context context;
    private WifiInfo wifiInfo = null;       //The obtained Wifi information making
    private WifiManager wifiManager = null; //The Wifi manager making
    private int level;

    String IPUSER = "IPUSER";
    String PORTUSER = "PORTUSER";

    String ipConfig;
    String IpStation;
    String IpUser;

    String portConfig;
    String PortStation;
    String PortUser;

    public ConnectionClientAlternatif(Context context,String ip, String port) {

        this.context = context;
        helper = new DatabesHelper(context);


       // CheckWifi();

        /**
         * buat logging
         */
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();


            //IP & PORT USER
           /* IpUser = helper.getMobileConfig(Constant.IPUSER);
            PortUser = helper.getMobileConfig(Constant.PORTUSER);*/
            if (ip != null && port !=null){

                retrofit = new Retrofit.Builder()
                        .baseUrl("http://"+ip.trim().replace(" ","")+":"+port.trim().replace(" ","")+"/")
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create()).build();

            }else{


                //IP & PORT CONFIG /CLOUD
                ipConfig = helper.getMobileConfig(Constant.IPCONFIG);
                portConfig = helper.getMobileConfig(Constant.PORT);

                if (ipConfig !=null && portConfig !=null){
                    retrofit = new Retrofit.Builder()
                            .baseUrl("http://"+ipConfig.trim().replace(" ","")+":"+portConfig.trim().replace(" ","")+"/")
                            .client(okHttpClient)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }else {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(Constant.IPAlternatif)
                            .client(okHttpClient)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }

            }


        apiService = retrofit.create(ConnectionInterface.class);
    }

    public  ConnectionInterface getApiService (){
        if (apiService == null)
            apiService = retrofit.create(ConnectionInterface.class);

        return apiService;
    }


    // -----------------UPLOAD------------------------------

    //POST UPLOAD RETUR ORDER
    public boolean postUpdateRetur(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateRetur = apiService.postUpdateRetur(param);

        try {

            Response<JsonObject> response = updateRetur.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD RETUR ORDER DETAIL
    public boolean postUpdateReturDetail(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateReturDetail = apiService.postUpdateReturDetail(param);

        try {

            Response<JsonObject> response = updateReturDetail.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD RETUR NEW ORDER
    public boolean postUpdateReturNew(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateRetur = apiService.postUpdateReturNew(param);

        try {

            Response<JsonObject> response = updateRetur.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD RETUR NEW ORDER DETAIL
    public boolean postUpdateReturDetailNew(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateReturDetail = apiService.postUpdateReturDetailNew(param);

        try {

            Response<JsonObject> response = updateReturDetail.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                System.out.println(result);
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD DELEVERY ORDER AUTO
    public int postUpdateDeleveryOrderAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccess = 0;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrder(param);

        try {

            Response<JsonObject> response = updateDelivery.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccess = 1;
                }else{
                    isSuccess = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccess = 0;
        }

        return isSuccess;

    }


    //POST UPLOAD DELEVERY ORDER DETAIL AUTO
    public int postUpdateDeleveryOrderDetailAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrederDetail(param);

        try {

            Response<JsonObject> response = updateDelivery.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = 1;
                }else{
                    isSuccas = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = 0;
        }

        return isSuccas;

    }


    //POST UPLOAD DELEVERY ORDER
    public boolean postUpdateDeleveryOrder(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccess = false;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrder(param);

        try {

            Response<JsonObject> response = updateDelivery.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccess = true;
                }else{
                    isSuccess = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccess = false;
        }

        return isSuccess;

    }

    //POST UPLOAD DELEVERY ORDER DETAIL
    public boolean postUpdateDeleveryOrderDetail(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> updateDelivery = apiService.postUpdateDeliveryOrederDetail(param);

        try {

            Response<JsonObject> response = updateDelivery.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD VISIT  AuTO
    public int postUploadVisitAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadVisit = apiService.postUploadVisit(param);

        try {

            Response<JsonObject> response = uploadVisit.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = 1;
                }else{
                    isSuccas = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = 0;
        }

        return isSuccas;

    }

    //POST UPLOAD VISIT DETAIL AUTO
    public int postUploadVisitDetailAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadVisitDetail = apiService.postUploadVisitDetail(param);

        try {

            Response<JsonObject> response = uploadVisitDetail.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = 1;
                }else{
                    isSuccas = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = 0;
        }

        return isSuccas;

    }


    //POST UPLOAD VISIT
    public boolean postUploadVisit(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadVisit = apiService.postUploadVisit(param);

        try {

            Response<JsonObject> response = uploadVisit.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD VISIT DETAIL
    public boolean postUploadVisitDetail(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadVisitDetail = apiService.postUploadVisitDetail(param);

        try {

            Response<JsonObject> response = uploadVisitDetail.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD DAILY COST
    public boolean postUploadDailyCost(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement,resultElementT;
        String result = null;
        String resultT = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadDailyCost = apiService.postUploadDailyCost(param);

        try {

            Response<JsonObject> response = uploadDailyCost.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist") || result.contains("0")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }


    //POST UPLOAD DAILY COST AUTO
    public int postUploadDailyCostAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement,resultElementT;
        String result = null;
        String resultT = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadDailyCost = apiService.postUploadDailyCost(param);

        try {

            Response<JsonObject> response = uploadDailyCost.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist") || result.contains("0")){
                    isSuccas = 1;
                }else{
                    isSuccas = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = 0;
        }

        return isSuccas;

    }


    //POST UPLOAD DAILY COST
    public boolean postUploadCustomerImage(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadCustomerImage = apiService.postUploadCustomerImage(param);

        try {

            Response<JsonObject> response = uploadCustomerImage.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }


    //POST UPLOAD DAILY COST AUTO
    public int postUploadCustomerImageAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCustomerImage = apiService.postUploadCustomerImage(param);

        try {

            Response<JsonObject> response = uploadCustomerImage.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = 1;
                }else{
                    isSuccas = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = 0;
        }

        return isSuccas;

    }


    //POST UPLOAD LIVE TRACKING
    public boolean postUploadLiveTracking(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadLiveTracking = apiService.postUploadLiveTracking(param);

        try {

            Response<JsonObject> response = uploadLiveTracking.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }


    //POST UPLOAD LOG VISIT
    public boolean postUploadLogVisit(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadLogVisit = apiService.postUploadLogVisit(param);

        try {

            Response<JsonObject> response = uploadLogVisit.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist") || result.contains("0")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }



    //POST UPLOAD COST VISIT
    public boolean postUploadCostVisit(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadCostVisit(param);

        try {

            Response<JsonObject> response = uploadCostVisit.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist") || result.contains("0")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD COST VISIT AUTO
    public int postUploadCostVisitAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadCostVisit(param);

        try {

            Response<JsonObject> response = uploadCostVisit.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist") || result.contains("0")){
                    isSuccas = 1;
                }else{
                    isSuccas = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = 0;
        }

        return isSuccas;

    }

    //POST UPLOAD UPLOAD RATION BBM
    public boolean postUploadRatio(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadRatio(param);

        try {

            Response<JsonObject> response = uploadCostVisit.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST UPLOAD UPLOAD RATION BBM AUTO
    public int postUploadRatioAuto(JsonArray param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        int isSuccas = 0;
        final Call<JsonObject> uploadCostVisit = apiService.postUploadRatio(param);

        try {

            Response<JsonObject> response = uploadCostVisit.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("1") ||  result.contains("IDExist")){
                    isSuccas = 1;
                }else{
                    isSuccas = 3;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = 0;
        }

        return isSuccas;

    }

    //POST UPLOAD LIVE TRACKING ONLINE
    public boolean postUploadLiveTrackingOnlien(JsonObject param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = true;
        final Call<JsonObject> uploadLiveTrackingOnlien = apiService.postUploadLiveTrackingOnlien(param);

        try {

            Response<JsonObject> response = uploadLiveTrackingOnlien.execute();
            if(response.isSuccessful()) {
                object = response.body();
                array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                resultElement = object.get("Result");
                result = resultElement.getAsString();
                if (result.contains("SQLSuccess") || result.contains("IDExist")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST TOKEN FIREBASE KE SERVER
    public boolean postFirebaseToServer(JsonObject param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadFirebaseToServer = apiService.postFirebaseToServer(param);

        try {
            Response<JsonObject> response = uploadFirebaseToServer.execute();
            if(response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }


            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }

    //POST Download Notification DailyMessage
    public boolean postDailyMessageList(JsonObject param){
        JsonObject jsonObjectDailyMessage;
        JsonArray jsonarray;
        JsonObject jsonobject ;
        boolean isSuccas = false;
        helper = new DatabesHelper(context);
        final Call<JsonObject> uploadDailyMessageList = apiService.postDailyMessageList(param);
        try {
            Response<JsonObject> response = uploadDailyMessageList.execute();
            if(response.isSuccessful()) {
               /* helper.deleteDailyMessage(); //Delete Daily Message
                jsonobject = response.body();
                jsonarray = jsonobject.getAsJsonArray("DailyMessageList");
                for (int i=0; i<jsonarray.size(); i++){
                    jsonObjectDailyMessage = jsonarray.get(i).getAsJsonObject();
                    helper.InserDailyMessage(
                            jsonObjectDailyMessage.get(Constant.MESSAGEID).getAsString(),
                            jsonObjectDailyMessage.get(Constant.MESSAGENAME).getAsString(),
                            jsonObjectDailyMessage.get(Constant.MESSAGEDESC).getAsString(),
                            jsonObjectDailyMessage.get(Constant.MESSAGEIMG).getAsString(),
                            jsonObjectDailyMessage.get("Date").getAsString(),
                            jsonObjectDailyMessage.get(Constant.CREATEDBY).getAsString());

                }*/
                isSuccas = true;

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }


   /* //POST CHEACK IN LAT LNG
    public boolean postCheackInLatLng(JsonObject param, String CustomerID,String isDistance,String WarehouseID){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        String distance = null;
        boolean isSuccas = false;
        helper = new Helper(context);
        final Call<JsonObject> uploadCheackInLatLng = apiService.postCheackInLatLng(param);

        try {
            Response<JsonObject> response = uploadCheackInLatLng.execute();
            if(response.isSuccessful()) {
                object = response.body();
                result = object.get("InRange").getAsString();
                if (result.contains("1")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }
                distance = object.get("Distance").getAsString();
                if (isDistance.equalsIgnoreCase("Distance")){

                    //UPDATE DISTANCE CHECKIN
                    helper.UpdateVisitDetailDistanceCheckIn(CustomerID,distance,WarehouseID);
                }
                if (isDistance.equalsIgnoreCase("DistanceCheckout")){

                    //UPDATE DISTANCE CHECK OUT
                    helper.UpdateVisitDetailDistanceCheckOut(CustomerID,distance,WarehouseID);
                }

            }
        }catch (IOException e){
           // System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }*/

    //KONFIRMASI NOTIF
    public boolean postKonfirmasiNotif(JsonObject param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadKonfirmasi = apiService.postKonfirmasiUserConfig(param);

        try {
            Response<JsonObject> response = uploadKonfirmasi.execute();
            if(response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            // System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }


    //CHECK VERSION APK
    public boolean postKonfirmasiApk(JsonObject param){
        JsonObject object;
        JsonArray array;
        JsonElement resultElement;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> uploadKonfirmasi = apiService.postCheckVersionApk(param);

        try {
            Response<JsonObject> response = uploadKonfirmasi.execute();
            if(response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            // System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }


    //DOWNLOAD CONFIG USER
    public boolean downloadConfigUser(JsonObject param){
        JsonObject object;
        String result = null;
        helper = new DatabesHelper(context);
        boolean isSuccas = false;
        final Call<JsonObject> upuserConfig = apiService.postUserConfig(param);

        try {
            Response<JsonObject> response = upuserConfig.execute();
            if(response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")){
                    helper.updateMobileConfig(Constant.DRIVERID,object.get("EmployeeID").getAsString());
                    helper.updateMobileConfig(Constant.BRANCHID,object.get("BranchID").getAsString());
                    helper.updateMobileConfig(Constant.BRANCHNAME,object.get("BranchName").getAsString());
                    helper.updateMobileConfig(Constant.IPCONFIG,object.get("IpPublic").getAsString());
                    helper.updateMobileConfig(Constant.PORT,object.get("PortPublic").getAsString());
                    helper.updateMobileConfig(Constant.IPSTATION,object.get("IpLocal").getAsString());
                    helper.updateMobileConfig(Constant.PORTSTATION,object.get("PortLocal").getAsString());
                    helper.updateMobileConfig(Constant.IPALTERNATIF,object.get("IpAlternative").getAsString());
                    helper.updateMobileConfig(Constant.PORTALTERNATIF,object.get("PortAlternative").getAsString());
                   // helper.updateMobailConfig("VEHICLENUMBER",object.get("VehicleNumber").getAsString());
                    helper.updateMobileConfig(Constant.DRIVER_NAME,object.get("EmployeeName").getAsString());
                    helper.updateMobileConfig(Constant.DOWNLOAD_USER_CONFIG,object.get("Result").getAsString());
                    helper.updateMobileConfig(Constant.PASSWORD,object.get("Password").getAsString());
                    helper.updateMobileConfig(Constant.PASSWORDRESET, object.get("PasswordReset").getAsString());

                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

                helper.close();
            }
        }catch (IOException e){
            // System.out.println(e.getMessage());
            isSuccas = false;
        }
        return isSuccas;
    }


    //TEST KONESKSI SERVER
    public boolean postpostTestKoneksi(){
        JsonObject object;
        String result = null;
        boolean isSuccas = false;
        final Call<JsonObject> testKoneksi = apiService.postTestKoneksi();

        try {
            Response<JsonObject> response = testKoneksi.execute();
            if(response.isSuccessful()) {
                object = response.body();
                result = object.get("Result").getAsString();
                if (result.contains("1")){
                    isSuccas = true;
                }else{
                    isSuccas = false;
                }

            }
        }catch (IOException e){
            // System.out.println(e.getMessage());
            isSuccas = false;
        }

        return isSuccas;

    }



    public void CheckWifi(){
        wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

        wifiInfo = wifiManager.getConnectionInfo();
        //Received signal strength value making
        level = wifiInfo.getRssi();
        //According to the obtained signal intensity of transmitted information making

        //IP & PORT CONFIG /CLOUD
        ipConfig = helper.getMobileConfig(Constant.IPCONFIG);
        portConfig = helper.getMobileConfig(Constant.PORT);

        //IP & PORT STATION
        IpStation = helper.getMobileConfig(Constant.IPSTATION);
        PortStation = helper.getMobileConfig(Constant.PORTSTATION);


        //IP & PORT USER
        IpUser = helper.getMobileConfig(Constant.IPUSER);
        PortUser = helper.getMobileConfig(Constant.PORTUSER);


        if (level <= 0 && level >= -100) {
            if (IpUser !=null && IpStation !=null){
                if(IpUser != IpStation){

                    //Update Ipuser Menjadi IpStation
                    IpUser=IpStation;
                    helper.updateMobileConfig(IPUSER, IpStation);
            }

              //  helper.close();
            }

            if (PortUser !=null && PortStation!=null){
                if (PortUser != PortStation){

                    //Update PortUser Menjadi PortStation
                    PortUser=PortStation;
                    helper.updateMobileConfig(PORTUSER, PortStation);
                    //  helper.close();
                }
            }

        } else if (level < -50 && level >= -70) {

            if (IpUser!=null && IpStation !=null){
                if(IpUser != IpStation){

                    //Update Ipuser Menjadi IpStation
                    IpUser=IpStation;
                    helper.updateMobileConfig(IPUSER, IpStation);
                    // helper.close();

                }
            }

            if (PortUser !=null && PortStation !=null){
                if (PortUser != PortStation){

                    //Update PortUser Menjadi PortStation
                    PortUser=PortStation;
                    helper.updateMobileConfig(PORTUSER, PortStation);
                    //helper.close();
                }
            }

        } else if (level < -70 && level >= -80) {

            if (IpUser !=null && IpStation !=null){
                if(IpUser != IpStation){

                    //Update Ipuser Menjadi IpStation
                    IpUser=IpStation;
                    helper.updateMobileConfig(IPUSER, IpStation);
                    helper.close();

                }
            }

            if (PortUser !=null && PortStation!=null){
                if (PortUser != PortStation){

                    //Update PortUser Menjadi PortStation
                    PortUser=PortStation;
                    helper.updateMobileConfig(PORTUSER, PortStation);
                    // helper.close();
                }
            }

        } else if (level < -80 && level >= -100) {

            if (IpUser !=null && IpStation !=null){
                if(IpUser != IpStation){

                    //Update Ipuser Menjadi IpStation
                    IpUser=IpStation;
                    helper.updateMobileConfig(IPUSER, IpStation);
                    //   helper.close();

                }
            }

            if (PortUser !=null && PortStation!=null){
                if (PortUser != PortStation){

                    //Update PortUser Menjadi PortStation
                    PortUser=PortStation;
                    helper.updateMobileConfig(PORTUSER, PortStation);
                    helper.close();
                }
            }

        } else {

            if (IpUser !=null && ipConfig!=null){
                if(IpUser != ipConfig){

                    //Update Ipuser Menjadi IpStation
                    IpUser=ipConfig;
                    helper.updateMobileConfig(IPUSER, ipConfig);
                    //  helper.close();

                }
            }

            if (PortUser !=null && portConfig!=null  ){
                if (PortUser != portConfig){

                    //Update PortUser Menjadi PortStation
                    PortUser=portConfig;
                    helper.updateMobileConfig(PORTUSER, portConfig);
                    // helper.close();
                }
            }

        }


    }
}