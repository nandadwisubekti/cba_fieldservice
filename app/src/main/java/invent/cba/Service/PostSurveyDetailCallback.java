package invent.cba.Service;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

import invent.cba.Helper.DatabesHelper;
import invent.cba.Model.ResultSurveyDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

abstract public class PostSurveyDetailCallback implements Callback<JsonObject> {

    private Context mContext;

    private List<ResultSurveyDetail> mItems;

    protected PostSurveyDetailCallback(Context context, List<ResultSurveyDetail> items) {
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
        if (response.isSuccessful()) {
            try {
                JsonObject object = response.body();
                JsonArray array = object.getAsJsonArray("ResultList");
                object = array.get(0).getAsJsonObject();
                JsonElement resultElement = object.get("Result");
                String result = resultElement.getAsString();
                if (result.equals("1")) {
                    DatabesHelper helper = new DatabesHelper(mContext);
                    if (helper.openDataBase()) {
                        for (ResultSurveyDetail item : mItems) {
                            helper.updateIsUploadResultSurveyDetail(item, "1");
                        }
                        helper.close();
                    }
                    onSuccess(response);
                } else {
                    onFailure(call, new RuntimeException("Server was responded, but probably not inserted to server. " +
                            "Result doesn\'t equals to 1. " +
                            "Ask to the server administrator if the recent data sent to the server was saved. " +
                            "Here's are the server response : \n" + result));
                }
            } catch (Throwable reason) {
                onFailure(call, reason);
            }
        } else {
            onFailure(call, new RuntimeException(response.message()));
        }
    }

    public abstract void onSuccess(Response<JsonObject> response);

}
