package invent.cba.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModuleTools {




    private String VisitID;
    private String POSMName;
    private String POSMID;
    private String Quantity;
    private String QuantityIn;
    private String QuantityOut;

    public String getQuantityIn() {
        return QuantityIn;
    }

    public void setQuantityIn(String quantityIn) {
        QuantityIn = quantityIn;
    }

    public String getQuantityOut() {
        return QuantityOut;
    }

    public void setQuantityOut(String quantityOut) {
        QuantityOut = quantityOut;
    }



    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    private String EmployeeID;



    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    private String CreatedDate;
    private String CreatedBy;





    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getPOSMName() {
        return POSMName;
    }

    public void setPOSMName(String POSMName) {
        this.POSMName = POSMName;
    }

    public String getPOSMID() {
        return POSMID;
    }

    public void setPOSMID(String POSMID) {
        this.POSMID = POSMID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }







}
