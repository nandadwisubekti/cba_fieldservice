package invent.cba.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Created by INVENT on 8/1/2018.
 */

public class ResultSurvey implements Parcelable {



    String ResultSurveyID;
    String EmployeeID;
    String VisitID;
    String Date;
    String Latitude;
    String Longitude;
    String IsSave;

    String CustomerID;
    String QuestionCategoryID;



    String BranchID;

    String IsUpload;

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    String StartDate;
    String EndDate;
    public double CompletedPercentage;

    public final static String TABLE_NAME = "ResultSurvey";

    public ResultSurvey(){}

    protected ResultSurvey(Parcel in) {
        ResultSurveyID = in.readString();
        EmployeeID = in.readString();
        VisitID = in.readString();
        Date = in.readString();
        Latitude = in.readString();
        Longitude = in.readString();
        IsSave = in.readString();
        CustomerID = in.readString();
        QuestionCategoryID = in.readString();
        BranchID = in.readString();
        IsUpload = in.readString();
        StartDate = in.readString();
        EndDate = in.readString();
        CompletedPercentage = in.readDouble();
    }

    public static final Creator<ResultSurvey> CREATOR = new Creator<ResultSurvey>() {
        @Override
        public ResultSurvey createFromParcel(Parcel in) {
            return new ResultSurvey(in);
        }

        @Override
        public ResultSurvey[] newArray(int size) {
            return new ResultSurvey[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ResultSurveyID);
        parcel.writeString(EmployeeID);
        parcel.writeString(VisitID);
        parcel.writeString(Date);
        parcel.writeString(Latitude);
        parcel.writeString(Longitude);
        parcel.writeString(IsSave);
        parcel.writeString(CustomerID);
        parcel.writeString(QuestionCategoryID);
        parcel.writeString(BranchID);
        parcel.writeString(IsUpload);
        parcel.writeString(StartDate);
        parcel.writeString(EndDate);
        parcel.writeDouble(CompletedPercentage);
    }


    public enum Constant{
        ResultSurveyID,
        EmployeeID,
        BranchID,
        VisitID,
        Date,
        Latitude,
        Longitude,
        CustomerID,
        QuestionCategoryID,
        IsSave,
        IsUpload,
        StartDate,
        EndDate,
        CompletedPercentage
    }

    public String getIsUpload() {
        return IsUpload;
    }

    public void setIsUpload(String isUpload) {
        IsUpload = isUpload;
    }

    public String getBranchID() {
        return BranchID;
    }

    public void setBranchID(String branchID) {
        BranchID = branchID;
    }

    public String getResultSurveyID() {
        return ResultSurveyID;
    }

    public void setResultSurveyID(String resultSurveyID) {
        ResultSurveyID = resultSurveyID;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getIsSave() {
        return IsSave;
    }

    public void setIsSave(String isSave) {
        IsSave = isSave;
    }



    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getQuestionCategoryID() {
        return QuestionCategoryID;
    }

    public void setQuestionCategoryID(String questionCategoryID) {
        QuestionCategoryID = questionCategoryID;
    }

    public JsonArray toParams() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("SurveyID", getResultSurveyID());
        jsonObject.addProperty("EmployeeID", getEmployeeID());
        jsonObject.addProperty("BranchID", getBranchID());
        jsonObject.addProperty("VisitID", getVisitID());
        jsonObject.addProperty("Date", getDate());
        jsonObject.addProperty("StartTime", getStartDate());
        jsonObject.addProperty("EndTime", getEndDate());
        jsonObject.addProperty("Latitude", getLatitude());
        jsonObject.addProperty("Longitude", getLongitude());
        jsonObject.addProperty("CustomerID", getCustomerID());
        jsonObject.addProperty("QuestionCategoryID", getQuestionCategoryID());
        jsonObject.addProperty("CompletedPercentage", CompletedPercentage);

        JsonArray jsonArray = new JsonArray();
        jsonArray.add(jsonObject);
        return jsonArray;
    }

}
