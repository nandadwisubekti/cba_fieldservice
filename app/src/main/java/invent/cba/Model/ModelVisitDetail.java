package invent.cba.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModelVisitDetail {

    private String VisitID;
    private String CustomerID;
    private String isStart;
    private String isFinish;
    private String StartDate;
    private String FinishDate;
    private String Latitude;
    private String Longitude;
    private String LatitudeCheckOut;
    private String LongitudeCheckOut;
    private String isDeliver;
    private String ReasonID;
    private String ReasonDescription;
    private String WarehouseID;
    private String isInRangeCheckin;
    private String isInRangeCheckout;
    private String Distance;
    private String Sequence;
    private String Duration;
    private String DistanceCheckout;
    private String isVisit;
    private String Time;
    private String ReasonSequence;
    private String EmployeeID;



    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getIsStart() {
        return isStart;
    }

    public void setIsStart(String isStart) {
        this.isStart = isStart;
    }

    public String getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(String isFinish) {
        this.isFinish = isFinish;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getFinishDate() {
        return FinishDate;
    }

    public void setFinishDate(String finishDate) {
        FinishDate = finishDate;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitudeCheckOut() {
        return LatitudeCheckOut;
    }

    public void setLatitudeCheckOut(String latitudeCheckOut) {
        LatitudeCheckOut = latitudeCheckOut;
    }

    public String getLongitudeCheckOut() {
        return LongitudeCheckOut;
    }

    public void setLongitudeCheckOut(String longitudeCheckOut) {
        LongitudeCheckOut = longitudeCheckOut;
    }

    public String getIsDeliver() {
        return isDeliver;
    }

    public void setIsDeliver(String isDeliver) {
        this.isDeliver = isDeliver;
    }

    public String getReasonID() {
        return ReasonID;
    }

    public void setReasonID(String reasonID) {
        ReasonID = reasonID;
    }

    public String getReasonDescription() {
        return ReasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        ReasonDescription = reasonDescription;
    }

    public String getWarehouseID() {
        return WarehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        WarehouseID = warehouseID;
    }

    public String getIsInRangeCheckin() {
        return isInRangeCheckin;
    }

    public void setIsInRangeCheckin(String isInRangeCheckin) {
        this.isInRangeCheckin = isInRangeCheckin;
    }

    public String getIsInRangeCheckout() {
        return isInRangeCheckout;
    }

    public void setIsInRangeCheckout(String isInRangeCheckout) {
        this.isInRangeCheckout = isInRangeCheckout;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getDistanceCheckout() {
        return DistanceCheckout;
    }

    public void setDistanceCheckout(String distanceCheckout) {
        DistanceCheckout = distanceCheckout;
    }

    public String getIsVisit() {
        return isVisit;
    }

    public void setIsVisit(String isVisit) {
        this.isVisit = isVisit;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getReasonSequence() {
        return ReasonSequence;
    }

    public void setReasonSequence(String reasonSequence) {
        ReasonSequence = reasonSequence;
    }








}
