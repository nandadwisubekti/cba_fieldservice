package invent.cba.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModuleSisaStockVisit {

    private String VisitID;
    private String ProductID;
    private String CustomerID;
    private String PromoPrice;
    private String Price;
    private String enam;
    private String enam_sembilan;
    private String sembilan_dua_belas;
    private String lebih_dua_belas;
    private String NotListed;
    private String Notes;
    private String CreatedDate;
    private String CreatedBy;
    private String IsStok;
    private String ProductName;
    private String stock;
    private String NormalPrice;
    private String Status;
    private String SisaStockTypeID;
    private String Description;
    private String Value;
    private String IsMandatory;

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    private String EmployeeID;


    public String getUOM() {
        return UOM;
    }

    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    private String UOM;



    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }





    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }




    public String getSisaStockTypeID() {
        return SisaStockTypeID;
    }

    public void setSisaStockTypeID(String sisaStockTypeID) {
        SisaStockTypeID = sisaStockTypeID;
    }





    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }





    public String getNormalPrice() {
        return NormalPrice;
    }

    public void setNormalPrice(String normalPrice) {
        NormalPrice = normalPrice;
    }





    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }




    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }




    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getPromoPrice() {
        return PromoPrice;
    }

    public void setPromoPrice(String promoPrice) {
        PromoPrice = promoPrice;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getEnam() {
        return enam;
    }

    public void setEnam(String enam) {
        this.enam = enam;
    }

    public String getEnam_sembilan() {
        return enam_sembilan;
    }

    public void setEnam_sembilan(String enam_sembilan) {
        this.enam_sembilan = enam_sembilan;
    }

    public String getSembilan_dua_belas() {
        return sembilan_dua_belas;
    }

    public void setSembilan_dua_belas(String sembilan_dua_belas) {
        this.sembilan_dua_belas = sembilan_dua_belas;
    }

    public String getLebih_dua_belas() {
        return lebih_dua_belas;
    }

    public void setLebih_dua_belas(String lebih_dua_belas) {
        this.lebih_dua_belas = lebih_dua_belas;
    }

    public String getNotListed() {
        return NotListed;
    }

    public void setNotListed(String notListed) {
        NotListed = notListed;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getIsStok() {
        return IsStok;
    }

    public void setIsStok(String isStok) {
        IsStok = isStok;
    }


    public String getIsMandatory() {
        return IsMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        IsMandatory = isMandatory;
    }
}
