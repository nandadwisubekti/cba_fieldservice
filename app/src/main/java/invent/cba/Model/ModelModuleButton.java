package invent.cba.Model;

/**
 * Created by kahfizain on 26/08/2017.
 */

public class ModelModuleButton {

    private String Module_Id;
    private String Module_type;
    private String Value;
    private String IsActive;
    private String IsRole;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    private String Title;

    public String getModule_Id() {
        return Module_Id;
    }

    public void setModule_Id(String module_Id) {
        Module_Id = module_Id;
    }

    public String getModule_type() {
        return Module_type;
    }

    public void setModule_type(String module_type) {
        Module_type = module_type;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getIsRole() {
        return IsRole;
    }

    public void setIsRole(String isRole) {
        IsRole = isRole;
    }









}
