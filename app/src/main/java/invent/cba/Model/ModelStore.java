
package invent.cba.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModelStore {



    private String CustomerName;
    private String CustomerAddress;
    private String CustomerID;
    private String Distributor;
    private String CountryRegionCode;
    private String City;
    private String Account;
    private String Time;
    private String Sequence;
    private String isDeliver;
    private String VisitID;
    private String isStart;
    private String isFinish;
    private String Value;
    private String Latitude;
    private String Longitude;

    public String getIsStart() {
        return isStart;
    }

    public void setIsStart(String isStart) {
        this.isStart = isStart;
    }

    public String getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(String isFinish) {
        this.isFinish = isFinish;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }








    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }



    public String getIsDeliver() {
        return isDeliver;
    }

    public void setIsDeliver(String isDeliver) {
        this.isDeliver = isDeliver;
    }




    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }


    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }

    private String Channel;





    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }


    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerAddress() {
        return CustomerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        CustomerAddress = customerAddress;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getDistributor() {
        return Distributor;
    }

    public void setDistributor(String distributor) {
        Distributor = distributor;
    }

    public String getCountryRegionCode() {
        return CountryRegionCode;
    }

    public void setCountryRegionCode(String countryRegionCode) {
        CountryRegionCode = countryRegionCode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String account) {
        Account = account;
    }






}
