package invent.cba.Model;

/**
 * Created by kahfizain on 19/09/2017.
 */

public class ModelPOSMStock {

    private String POSMStock;
    private String VisitID;
    private String Quantity;
    private String CreatedDate;
    private String CreatedBy;
    private String POSMID;
    private String POSMName;
    private String QuantityOut;
    private String QuantitySisa;
    private String CustomerID;

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }



    public String getQuantitySisa() {
        return QuantitySisa;
    }

    public void setQuantitySisa(String quantitySisa) {
        QuantitySisa = quantitySisa;
    }



    public String getPOSMName() {
        return POSMName;
    }

    public void setPOSMName(String POSMName) {
        this.POSMName = POSMName;
    }


    public String getQuantityOut() {
        return QuantityOut;
    }

    public void setQuantityOut(String quantityOut) {
        QuantityOut = quantityOut;
    }


    public String getPOSMID() {
        return POSMID;
    }

    public void setPOSMID(String POSMID) {
        this.POSMID = POSMID;
    }


    public String getPOSMStock() {
        return POSMStock;
    }

    public void setPOSMStock(String POSMStock) {
        this.POSMStock = POSMStock;
    }

    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }





}
