package invent.cba.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModelVisit {




    private String VisitID;
    private String VehicleNumber;
    private String VisitDate;
    private String KMStart;
    private String KMFinish;
    private String isStart;
    private String isFinish;
    private String StartDate;
    private String FinishDate;
    private String EmployeeID;

    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getVehicleNumber() {
        return VehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        VehicleNumber = vehicleNumber;
    }

    public String getVisitDate() {
        return VisitDate;
    }

    public void setVisitDate(String visitDate) {
        VisitDate = visitDate;
    }

    public String getKMStart() {
        return KMStart;
    }

    public void setKMStart(String KMStart) {
        this.KMStart = KMStart;
    }

    public String getKMFinish() {
        return KMFinish;
    }

    public void setKMFinish(String KMFinish) {
        this.KMFinish = KMFinish;
    }

    public String getIsStart() {
        return isStart;
    }

    public void setIsStart(String isStart) {
        this.isStart = isStart;
    }

    public String getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(String isFinish) {
        this.isFinish = isFinish;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getFinishDate() {
        return FinishDate;
    }

    public void setFinishDate(String finishDate) {
        FinishDate = finishDate;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }








}
