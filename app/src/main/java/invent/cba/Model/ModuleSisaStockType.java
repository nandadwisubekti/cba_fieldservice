package invent.cba.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModuleSisaStockType {




    private String SisaStockTypeID;
    private String Description;
    private String Category;
    private String Seq;

    public String getSisaStockTypeID() {
        return SisaStockTypeID;
    }

    public void setSisaStockTypeID(String sisaStockTypeID) {
        SisaStockTypeID = sisaStockTypeID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getSeq() {
        return Seq;
    }

    public void setSeq(String seq) {
        Seq = seq;
    }








}
