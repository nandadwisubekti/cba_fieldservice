package invent.cba.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by INVENT on 7/26/2018.
 */

public class HelperNamed {
    public static String getFileNameUniq(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }




    public static String getUniqID(String prefix){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String  currentTimeStamp = dateFormat.format(new Date());
        return prefix+""+currentTimeStamp;
    }

    public static String getTimeForID(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HHmmssSSS");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }
    public static String getDateForID(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }

    public static String getFileName(String url){
        return url.substring( url.lastIndexOf('/')+1, url.length() );
    }
    public static String getExtention(String url){
        if(url.contains(".")) {
            return  url.substring(url.lastIndexOf("."));
        }
        return null;
    }

    public static String getDateForDB(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }

    public static String getDateDeafultForDB(){
        return "1910-01-01";
    }
    public static String getDateTimeForDB(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }


    public static String getYearString(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
        return dateFormat.format(new Date());
    }
    public static String getMonthString(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
        return dateFormat.format(new Date());
    }

    public static String getDayString(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        return dateFormat.format(new Date());
    }

    public static String getDateNowFormatLocaleID(){
        Date date = new Date();
        return getDay(date) +" "+getMonth(date)+" "+getYear(date);
    }
    public static String getDateFormatLocaleID(String valueDate){

        if (valueDate!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = dateFormat.parse(valueDate);
                return getDay(date) +" "+getMonth(date)+" "+getYear(date);

            } catch (ParseException e) {


            }
        }
        return "";
    }

    public static Date getDateFromLocaleID(String valueDate){

        if (valueDate!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = dateFormat.parse(valueDate);
                return date;

            } catch (ParseException e) {

            }
        }
        return null;
    }

    private static String getDay(Date date){
        SimpleDateFormat dateFormatMonth = new SimpleDateFormat("dd");
        return dateFormatMonth.format(date);
    }
    private static String getMonth(Date date){
        SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MM");
        switch (dateFormatMonth.format(date)){
            case "01": return "JAN";
            case "02": return "FEB";
            case "03": return "MAR";
            case "04": return "APR";
            case "05": return "MEI";
            case "06": return "JUN";
            case "07": return "JUL";
            case "08": return "AGS";
            case "09": return "SEP";
            case "10": return "OKT";
            case "11": return "NOV";
            case "12": return "DES";
        }
        return "";
    }
    private static String getYear(Date date){
        SimpleDateFormat dateFormatMonth = new SimpleDateFormat("yyyy");
        return dateFormatMonth.format(date);
    }
}
