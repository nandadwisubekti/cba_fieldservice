package invent.cba.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import invent.cba.Adapter.AdapterListFoto;
import invent.cba.Model.ModelAnswer;
import invent.cba.Model.ModelCallPlanDetail;
import invent.cba.Model.ModelCompetitor;
import invent.cba.Model.ModelCompetitorActivityType;
import invent.cba.Model.ModelModuleButton;
import invent.cba.Model.ModelPOSMStock;
import invent.cba.Model.ModelProduct;
import invent.cba.Model.ModelPromoVisit;
import invent.cba.Model.ModelQuestion;
import invent.cba.Model.ModelRegisterNewCustomer;
import invent.cba.Model.ModelStore;
import invent.cba.Model.ModelVisit;
import invent.cba.Model.ModelVisitDetail;
import invent.cba.Model.ModuleCompetitor;
import invent.cba.Model.ModuleCustomerImage;
import invent.cba.Model.ModuleDailyMessage;
import invent.cba.Model.ModulePromo;
import invent.cba.Model.ModuleReason;
import invent.cba.Model.ModuleSisaStockType;
import invent.cba.Model.ModuleSisaStockVisit;
import invent.cba.Model.ModuleTools;
import invent.cba.Model.QuestionSet;
import invent.cba.Model.ResultSurvey;
import invent.cba.Model.ResultSurveyDetail;

public class DatabesHelper extends SQLiteOpenHelper {


    private static String TAG = DatabesHelper.class.getSimpleName();
    private static final String ENCODING_SETTING = "PRAGMA encoding ='UTF-8'";
    private SQLiteDatabase myDataBase = null;
    private final Context myContext;
    private static String DB_PATH ;
    private final static int DATABASE_VERSION = 5;
    //public final static String DB_NAME = "EasyPay.sqlite";

    private int resultInt;

    public DatabesHelper(Context context) {
        super(context, context.getExternalFilesDir(null).getAbsolutePath()+"/"+ Constant.DB_NAME, null, DATABASE_VERSION);
        // super(context, DB_NAME, null, DATABASE_VERSION);
        //databes save ke sdcard
        DB_PATH = context.getExternalFilesDir(null).getAbsolutePath() + "/";


        this.myContext = context;
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            Log.v("log_tag", "database does exist");
        } else {
            Log.v("log_tag", "database does not exist");
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
                throw new Error("Error copying database");
            }
        }
    }

    private void copyDataBase() throws IOException {

        InputStream myInput = myContext.getAssets().open(Constant.DB_NAME);
        String outFileName = myContext.getExternalFilesDir(null).getAbsolutePath() +"/"+ Constant.DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = myContext.getExternalFilesDir(null).getAbsolutePath()+"/"+ Constant.DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        } catch (SQLiteException e) {
            Log.v(TAG, e.toString() + " database doesn't exists yet..");
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    public boolean openDataBase() throws SQLException {
        String myPath = myContext.getExternalFilesDir(null).getAbsolutePath()+"/"+Constant.DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        myDataBase.close();
        return myDataBase != null;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {

        if (!db.isReadOnly()) {
            db.execSQL(ENCODING_SETTING);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.v(TAG, "Upgrading database, this will drop database and recreate.");
    }



    //PORT CONFIG
    public String getMobileConfig(String ID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from MobileConfig WHERE  ID = '"+ID+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("Value"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETPORT", e.getMessage());
        }
        return value;
    }

    public void updateMobileConfig(String ID ,String Value) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            // values.put("ID", ID);
            values.put("Value",  Value == null ? "" : Value);
            sqlite.update("MobileConfig", values, "ID = '" + ID + "'", null);
            sqlite.close();

        } catch (SQLiteException ex) {

        }
    }


    public void updateRegisterNewCustomerIsSave(String RegisterID ,String IsSave) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            // values.put("ID", ID);
            values.put(Constant.IsSave,  IsSave == null ? "" : IsSave);
            sqlite.update(Constant.RegisterNewCustomer, values, "RegisterID = '" + RegisterID + "'", null);
            sqlite.close();

        } catch (SQLiteException ex) {

        }
    }


    public void updateRegisterNewCustomerDetailIsSave(String RegisterID ,String IsSave) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            // values.put("ID", ID);
            values.put(Constant.IsSave,  IsSave == null ? "" : IsSave);
            sqlite.update(Constant.RegisterNewCustomerDetail, values, "RegisterID = '" + RegisterID + "'", null);
            sqlite.close();

        } catch (SQLiteException ex) {

        }
    }

    public void updateRegisterNewCustomerDetailVisit(String RegisterID ,String QuestionID,String Value) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            // values.put("ID", ID);
            values.put(Constant.Value,  Value == null ? "" : Value);
            sqlite.update(Constant.RegisterNewCustomerDetail, values, "RegisterID = '" + RegisterID + "' and QuestionID = '"+QuestionID+"' ", null);
            sqlite.close();

        } catch (SQLiteException ex) {

        }
    }


    //GET MODULE BUTTON
    public ArrayList<ModelModuleButton> getArralyModuleButton(String Module_type,String IsRole,String IsActive ) {
        ArrayList<ModelModuleButton> reasonArrayList = new ArrayList<ModelModuleButton>();
        ModelModuleButton modelModuleButton = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT Title,Module_Id,Value,IsRole,IsActive from ModuleButton WHERE Module_type = '"+Module_type+"' and IsRole = '"+IsRole+"' and IsActive = '"+IsActive+"'  ORDER BY IsSequence ASC", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelModuleButton = new ModelModuleButton();
                            modelModuleButton.setTitle(c.getString(c.getColumnIndex(Constant.Title)));
                            modelModuleButton.setModule_Id(c.getString(c.getColumnIndex(Constant.Module_Id)));
                            modelModuleButton.setIsRole(c.getString(c.getColumnIndex(Constant.IsRole)));
                            modelModuleButton.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            modelModuleButton.setIsActive(c.getString(c.getColumnIndex(Constant.IsActive)));
                            reasonArrayList.add(modelModuleButton);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }

    public ArrayList<ModelModuleButton> getNonVisitCustomerAsModuleButton() {
        ArrayList<ModelModuleButton> reasonArrayList = new ArrayList<ModelModuleButton>();
        ModelModuleButton modelModuleButton = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CustomerID,CustomerName from Customer WHERE CustomerID NOT IN (SELECT CustomerID from VisitDetail) ORDER BY CustomerID ASC", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelModuleButton = new ModelModuleButton();
                            modelModuleButton.setTitle(c.getString(c.getColumnIndex("CustomerName")));
                            modelModuleButton.setModule_Id(c.getString(c.getColumnIndex("CustomerID")));
                            modelModuleButton.setIsRole(this.getMobileConfig(Constant.Role));
                            modelModuleButton.setValue("1");
                            modelModuleButton.setIsActive("1");
                            reasonArrayList.add(modelModuleButton);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET ResultSurvey
    public ArrayList<ModelRegisterNewCustomer> getArrayListResultSurvey(String isSave,String QuestionCategoryID,String VisitID,String CustomerID) {
        ArrayList<ModelRegisterNewCustomer> registerNewCustomerArrayList = new ArrayList<ModelRegisterNewCustomer>();
        ModelRegisterNewCustomer modelRegisterNewCustomer = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT RegisterID,EmployeeID,VisitID,Date from "+Constant.RegisterNewCustomer+" WHERE IsSave = '"+isSave+"' and QuestionCategoryID = '"+QuestionCategoryID+"' and VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelRegisterNewCustomer = new ModelRegisterNewCustomer();
                            modelRegisterNewCustomer.setRegisterID(c.getString(c.getColumnIndex(Constant.RegisterID)));
                            modelRegisterNewCustomer.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            modelRegisterNewCustomer.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelRegisterNewCustomer.setDate(c.getString(c.getColumnIndex(Constant.Date)));
                            registerNewCustomerArrayList.add(modelRegisterNewCustomer);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return registerNewCustomerArrayList;
    }



    //GET MODULE BUTTON
    public ArrayList<ModuleDailyMessage> getArrayDailyMessageList( ) {
        ArrayList<ModuleDailyMessage> reasonArrayList = new ArrayList<ModuleDailyMessage>();
        ModuleDailyMessage moduleDailyMessage = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT MessageID,MessageName,MessageDesc,MessageImg,DATE,CreatedBy from DailyMessage", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleDailyMessage = new ModuleDailyMessage();
                            moduleDailyMessage.setMessageID(c.getString(c.getColumnIndex(Constant.MessageID)));
                            moduleDailyMessage.setMessageName(c.getString(c.getColumnIndex(Constant.MessageName)));
                            moduleDailyMessage.setMessageDesc(c.getString(c.getColumnIndex(Constant.MessageDesc)));
                            moduleDailyMessage.setMessageImg(c.getString(c.getColumnIndex(Constant.MessageImg)));
                            moduleDailyMessage.setDATE(c.getString(c.getColumnIndex(Constant.DATE)));
                            moduleDailyMessage.setCreatedBy(c.getString(c.getColumnIndex(Constant.CreatedBy)));

                            reasonArrayList.add(moduleDailyMessage);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET Reason
    public ArrayList<ModuleReason> getArrayListReason(String ReasonType ) {
        ArrayList<ModuleReason> reasonArrayList = new ArrayList<ModuleReason>();
        ModuleReason moduleReason = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery(" SELECT ReasonID,Value FROM Reason WHERE ReasonType = '"+ReasonType+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleReason = new ModuleReason();
                            moduleReason.setReasonID(c.getString(c.getColumnIndex(Constant.ReasonID)));
                            moduleReason.setValue(c.getString(c.getColumnIndex(Constant.Value)));

                            reasonArrayList.add(moduleReason);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET CompetitorProduct
    public ArrayList<ModuleCompetitor> getArrayListCompetitorProduct( ) {
        ArrayList<ModuleCompetitor> competitorArrayList = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CompetitorID,CompetitorProductID from CompetitorProduct", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCompetitorProductID(c.getString(c.getColumnIndex(Constant.CompetitorProductID)));

                            competitorArrayList.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return competitorArrayList;
    }


    //GET REGISTER CUTEOMER BERDASARAKAN REG ID DAN QuestionSetID
    public ArrayList<ModelQuestion>   getArrayLitRegisterCutomerDetail(String RegisterID, String QuestionSetID ) {

        String StartDate = ValidationHelper.date();
        ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<ModelQuestion>();
        ModelQuestion modelQuestion = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT b.QuestionSetID , a.QuestionID, b.QuestionText, a.Value,  b.AnswerTypeID,a.AnswerID from "+Constant.RegisterNewCustomerDetail+" a\n" +
                    "INNER JOIN ms_question b on a.QuestionID = b.QuestionID  WHERE a.RegisterID = '"+RegisterID+"' and b.QuestionSetID = '"+QuestionSetID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelQuestion = new ModelQuestion();
                            modelQuestion.setQuestionSetID(c.getString(c.getColumnIndex(Constant.QuestionSetID)));
                            modelQuestion.setQuestionID(c.getString(c.getColumnIndex(Constant.QuestionID)));
                            modelQuestion.setQuestionText(c.getString(c.getColumnIndex(Constant.QuestionText)));
                            modelQuestion.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            modelQuestion.setAnswerTypeID(c.getString(c.getColumnIndex(Constant.AnswerTypeID)));
                            modelQuestion.setAnswerID(c.getString(c.getColumnIndex(Constant.AnswerID)));
                            modelQuestionArrayList.add(modelQuestion);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelQuestionArrayList;
    }


    //GET REGISTER CUTEOMER BERDASARAKAN VISIT ID DAN CUSTOMER ID
    public boolean getArrayLitResultSurveyVisitCustomerID(String isSave,String VisitID, String CustomerID,String QuestionCategoryID ) {

        boolean isNow = false;
        String StartDate = ValidationHelper.date();
        ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<ModelQuestion>();
        ModelQuestion modelQuestion = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT * from ResultSurvey WHERE IsSave = '"+isSave+"'  VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"' and QuestionCategoryID = '"+QuestionCategoryID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {

                            isNow = true;
                        } catch (NullPointerException ex) {
                            isNow = false;
                        } catch (Exception e) {
                            isNow = false;
                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            isNow = false;
            Log.i("GETModuleButton", e.getMessage());
        }
        return isNow;
    }


    //GET REGISTER CUTEOMER BERDASARAKAN VISIT ID DAN CUSTOMER ID
    public int getLitResultSurveyVisitCustomerID(String isSave,String VisitID, String CustomerID,String QuestionCategoryID ) {

        int totalResultSurvey =0;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT count(*) as totalResultSurvey  from ResultSurvey WHERE IsSave = '"+isSave+"' and  VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"' and QuestionCategoryID = '"+QuestionCategoryID+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                totalResultSurvey = cursor.getInt(cursor.getColumnIndex("totalResultSurvey"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETisFinish", e.getMessage());
        }



        return totalResultSurvey;
    }


    //GET ms_question
    public ArrayList<ModelQuestion> getArrayListQuestion(String QuestionSetID, String IsActive,String QuestionCategoryID  ) {

        String StartDate = ValidationHelper.date();
        ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<ModelQuestion>();
        ModelQuestion modelQuestion = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("\n" +
                    "SELECT QuestionID,QuestionText,AnswerTypeID,IsSubQuestion,Sequence,Mandatory\n" +
                    " from ms_question \n" +
                    "WHERE QuestionSetID = '"+QuestionSetID+"'\n" +
                    "and IsActive =  '"+IsActive+"'\n" +
                    "and QuestionCategoryID = '"+QuestionCategoryID+"'\n" +
                    "ORDER BY Sequence ASC;", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelQuestion = new ModelQuestion();
                            modelQuestion.setQuestionID(c.getString(c.getColumnIndex(Constant.QuestionID)));
                            modelQuestion.setQuestionText(c.getString(c.getColumnIndex(Constant.QuestionText)));
                            modelQuestion.setAnswerTypeID(c.getString(c.getColumnIndex(Constant.AnswerTypeID)));
                            modelQuestion.setIsSubQuestion(c.getString(c.getColumnIndex(Constant.IsSubQuestion)));
                            modelQuestion.setMandatory(c.getString(c.getColumnIndex(Constant.Mandatory)));
                            modelQuestion.setAnswerID("");

                            //jikat tipeDate Type maka value default date sudah ter isi
                            if (c.getString(c.getColumnIndex(Constant.AnswerTypeID)).equals(Constant.DateType)){
                                modelQuestion.setValue(StartDate);
                            }else {
                                modelQuestion.setValue("");
                            }

                            modelQuestionArrayList.add(modelQuestion);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelQuestionArrayList;
    }

    public ArrayList<ModelQuestion> getArrayListQuestionWithoutCategory(String QuestionSetID) {

        String StartDate = ValidationHelper.date();
        ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<ModelQuestion>();
        ModelQuestion modelQuestion = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("\n" +
                    "SELECT QuestionID,QuestionText,AnswerTypeID,IsSubQuestion,QuestionCategoryID,Sequence,Mandatory\n" +
                    " from ms_question \n" +
                    "WHERE QuestionSetID = '"+QuestionSetID+"'\n" +
                    "and IsActive =  '1'\n" +
                    "ORDER BY Sequence ASC;", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelQuestion = new ModelQuestion();
                            modelQuestion.setQuestionID(c.getString(c.getColumnIndex(Constant.QuestionID)));
                            modelQuestion.setQuestionText(c.getString(c.getColumnIndex(Constant.QuestionText)));
                            modelQuestion.setAnswerTypeID(c.getString(c.getColumnIndex(Constant.AnswerTypeID)));
                            modelQuestion.setIsSubQuestion(c.getString(c.getColumnIndex(Constant.IsSubQuestion)));
                            modelQuestion.setQuestionCategoryID(c.getString(c.getColumnIndex(Constant.QuestionCategoryID)));
                            modelQuestion.setMandatory(c.getString(c.getColumnIndex(Constant.Mandatory)));
                            modelQuestion.setAnswerID("");

                            //jikat tipeDate Type maka value default date sudah ter isi
                            if (c.getString(c.getColumnIndex(Constant.AnswerTypeID)).equals(Constant.DateType)){
                                modelQuestion.setValue(StartDate);
                            }else {
                                modelQuestion.setValue("");
                            }

                            modelQuestionArrayList.add(modelQuestion);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelQuestionArrayList;
    }


    //GET ms_question
    public ArrayList<ModelQuestion> getArrayListQuestionSudahDiisi(String VisitID, String CustomerID,String QuestionSetID, String IsActive, String QuestionCategoryID ) {

        String StartDate = ValidationHelper.date();
        ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<ModelQuestion>();
        ModelQuestion modelQuestion = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT c.RegisterID, b.QuestionID,b.QuestionText,b.AnswerTypeID,b.IsSubQuestion,b.Sequence,b.Mandatory, a.Value,c.VisitID,c.CustomerID\n" +
                    "from ResultSurveyDetail  a\n" +
                    "INNER JOIN  ms_question b on a.QuestionID = b.QuestionID  \n" +
                    "INNER JOIN ResultSurvey c on c.RegisterID = a.RegisterID \n" +
                    "WHERE c.VisitID = '"+VisitID+"'\n" +
                    "and c.CustomerID = '"+CustomerID+"' \n" +
                    "AND b.QuestionSetID = '"+QuestionSetID+"' \n" +
                    "and b.IsActive = '"+IsActive+"'\n" +
                    " and b.QuestionCategoryID = '"+QuestionCategoryID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelQuestion = new ModelQuestion();
                            modelQuestion.setQuestionID(c.getString(c.getColumnIndex(Constant.QuestionID)));
                            modelQuestion.setQuestionText(c.getString(c.getColumnIndex(Constant.QuestionText)));
                            modelQuestion.setAnswerTypeID(c.getString(c.getColumnIndex(Constant.AnswerTypeID)));
                            modelQuestion.setIsSubQuestion(c.getString(c.getColumnIndex(Constant.IsSubQuestion)));
                            modelQuestion.setMandatory(c.getString(c.getColumnIndex(Constant.Mandatory)));
                            modelQuestion.setRegisterID(c.getString(c.getColumnIndex(Constant.RegisterID)));
                            modelQuestion.setAnswerID("");

                            //jikat tipeDate Type maka value default date sudah ter isi
                            if (c.getString(c.getColumnIndex(Constant.AnswerTypeID)).equals(Constant.DateType)){
                                modelQuestion.setValue(StartDate);
                            }else {
                                modelQuestion.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            }

                            modelQuestionArrayList.add(modelQuestion);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelQuestionArrayList;
    }



    //GET ms_question
    public ArrayList<ModelAnswer> getArrayListModelAnswer(String QuestionID, String IsActive,String typeInput ) {

        ArrayList<ModelAnswer> modelAnswerArrayList = new ArrayList<ModelAnswer>();
        ModelAnswer modelAnswer = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT AnswerID,AnswerText,QuestionID,SubQuestion,IsSubQuestion,Sequence,IsActive FROM ms_answer \n" +
                    "WHERE QuestionID = '"+QuestionID+"' and IsActive = '"+IsActive+"' ORDER BY Sequence ASC;", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelAnswer = new ModelAnswer();
                            modelAnswer.setAnswerID(c.getString(c.getColumnIndex(Constant.AnswerID)));
                            modelAnswer.setAnswerText(c.getString(c.getColumnIndex(Constant.AnswerText)));
                            modelAnswer.setQuestionID(c.getString(c.getColumnIndex(Constant.QuestionID)));
                            modelAnswer.setSubQuestion(c.getString(c.getColumnIndex(Constant.SubQuestion)));
                            modelAnswer.setIsSubQuestion(c.getString(c.getColumnIndex(Constant.IsSubQuestion)));
                            modelAnswer.setSequence(c.getString(c.getColumnIndex(Constant.Sequence)));
                            modelAnswer.setIsActive(c.getString(c.getColumnIndex(Constant.IsActive)));
                            if (typeInput.equals(Constant.CheckBox)){
                                modelAnswer.setStatus(false);
                            }

                            modelAnswerArrayList.add(modelAnswer);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelAnswerArrayList;
    }



    //GET getArrayListCompetitorActivityVisitInput
    public ArrayList<ModuleCompetitor> getArrayListCompetitorActivityVisitInput(String VisitID,
                                                                                String EmployeeID,
                                                                                String CustomerID,
                                                                                String CompetitorID ) {
        ArrayList<ModuleCompetitor> competitorArrayList = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("select C.VisitID as VisitID, \n" +
                    "C.CustomerID as CustomerID ,\n" +
                    "F.CompetitorProductName as CompetitorProductName,\n" +
                    "C.CompetitorProductID as CompetitorProductID,\n" +
                    "C.PromoPrice as PromoPrice,\n" +
                    "D.NormalPrice as NormalPrice,\n" +
                    "C.Notes as Notes,\n" +
                    "c.CompetitorID as CompetitorID,\n" +
                    "c.CompetitorActivityTypeID as CompetitorActivityTypeID,\n" +
                    "c.isSave as isSave\n" +
                    "from \n" +
                    "(select a.CompetitorProductID, a.value PromoPrice, a.CustomerID,a.VisitID, a.Notes, a.CompetitorID, a.CompetitorActivityTypeID, a.isSave from CompetitorActivityVisit a\n" +
                    "where a.CompetitorActivityTypeID='PromoPrice') C\n" +
                    "inner join\n" +
                    "(select a.CompetitorProductID, a.value NormalPrice, a.CustomerID,a.VisitID, a.Notes, a.CompetitorID from CompetitorActivityVisit a\n" +
                    "where a.CompetitorActivityTypeID='NormalPrice') D\n" +
                    "on C.CompetitorProductID=D.CompetitorProductID \n" +
                    "and c.CustomerID=d.CustomerID \n" +
                    "and c.VisitID=d.VisitID\n" +
                    "and c.CompetitorID=d.CompetitorID\n" +
                    "inner join CompetitorProduct F on C.CompetitorProductID=F.CompetitorProductID\n" +
                    "where c.customerid='"+CustomerID+"' and c.CompetitorID='"+CompetitorID+"' and c.VisitID='"+VisitID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCompetitor.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCompetitor.setCompetitorProductName(c.getString(c.getColumnIndex(Constant.CompetitorProductName)));
                            moduleCompetitor.setCompetitorProductID(c.getString(c.getColumnIndex(Constant.CompetitorProductID)));
                            moduleCompetitor.setPromoPrice(c.getString(c.getColumnIndex(Constant.PromoPrice)));
                            moduleCompetitor.setNormalPrice(c.getString(c.getColumnIndex(Constant.NormalPrice)));
                            moduleCompetitor.setNotes(c.getString(c.getColumnIndex(Constant.Notes)));
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCompetitorActivityTypeID(c.getString(c.getColumnIndex(Constant.CompetitorActivityTypeID)));
                            moduleCompetitor.setIsSave(c.getString(c.getColumnIndex(Constant.isSave)));

                            competitorArrayList.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return competitorArrayList;
    }



    public String getVisitCustomerName(String CustomerID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT CustomerName from Customer WHERE CustomerID = '"+CustomerID+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("CustomerName"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETisFinish", e.getMessage());
        }
        return value;
    }

    //GET MODULE CustomerImage
    public ArrayList<ModuleCustomerImage> getArrayListCustomerImage(String CustomerID,String VisitID ) {
        ArrayList<ModuleCustomerImage> reasonArrayList = new ArrayList<ModuleCustomerImage>();
        ModuleCustomerImage moduleCustomerImage = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT ImageID,CustomerID,VisitID,ImageDate,ImageBase64,ImageType \n" +
                    "from CustomerImage WHERE ImageType != 'checklist' AND  CustomerID = '"+CustomerID+"' and VisitID = '"+VisitID+"'\n", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCustomerImage = new ModuleCustomerImage();
                            moduleCustomerImage.setImageID(c.getString(c.getColumnIndex(Constant.ImageID)));
                            moduleCustomerImage.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCustomerImage.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCustomerImage.setImageDate(c.getString(c.getColumnIndex(Constant.ImageDate)));
                            moduleCustomerImage.setImageBase64(c.getString(c.getColumnIndex(Constant.ImageBase64)));
                            moduleCustomerImage.setImageType(c.getString(c.getColumnIndex(Constant.ImageType)));

                            reasonArrayList.add(moduleCustomerImage);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET MODULE CustomerImage
    public ArrayList<ModuleCustomerImage> getArrayListCustomerImageUpload() {
        ArrayList<ModuleCustomerImage> reasonArrayList = new ArrayList<ModuleCustomerImage>();
        ModuleCustomerImage moduleCustomerImage = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("select a.*, b.EmployeeID from CustomerImage a inner join Visit b on a.VisitID = b.VisitID", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCustomerImage = new ModuleCustomerImage();
                            moduleCustomerImage.setImageID(c.getString(c.getColumnIndex(Constant.ImageID)));
                            moduleCustomerImage.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCustomerImage.setDocNumber(c.getString(c.getColumnIndex(Constant.DocNumber)));
                            moduleCustomerImage.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCustomerImage.setImageDate(c.getString(c.getColumnIndex(Constant.ImageDate)));
                            moduleCustomerImage.setImageBase64(c.getString(c.getColumnIndex(Constant.ImageBase64)));
                            moduleCustomerImage.setImageType(c.getString(c.getColumnIndex(Constant.ImageType)));
                            moduleCustomerImage.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            moduleCustomerImage.setWarehouseID(c.getString(c.getColumnIndex(Constant.WarehouseID)));
                            moduleCustomerImage.setLatitude(c.getString(c.getColumnIndex(Constant.Latitude)));
                            moduleCustomerImage.setLongitude(c.getString(c.getColumnIndex(Constant.Longitude)));



                            reasonArrayList.add(moduleCustomerImage);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }



    //GET CompetitorActivityImage
    public ArrayList<ModuleCompetitor> getArrayListCompetitorActivityImage(String VisitID,String CompetitorID,String CompetitorProductID,String CustomerID) {
        ArrayList<ModuleCompetitor> reasonArrayList = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CompetitorActivityImageID,VisitID,CompetitorID,CompetitorProductID,CustomerID,Image,CreatedDate \n" +
                    "from CompetitorActivityImage\n" +
                    "WHERE VisitID = '"+VisitID+"' and CompetitorID = '"+CompetitorID+"' and CompetitorProductID = '"+CompetitorProductID+"' and CustomerID = '"+CustomerID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setCompetitorActivityImageID(c.getString(c.getColumnIndex(Constant.CompetitorActivityImageID)));
                            moduleCompetitor.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCompetitorProductID(c.getString(c.getColumnIndex(Constant.CompetitorProductID)));
                            moduleCompetitor.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCompetitor.setImage(c.getString(c.getColumnIndex(Constant.Image)));
                            moduleCompetitor.setCreatedDate(c.getString(c.getColumnIndex(Constant.CreatedDate)));



                            reasonArrayList.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }



    //GET Promo
    public ArrayList<ModulePromo> getArrayListPromo(String PromoCategory, boolean isStatus) {
        ArrayList<ModulePromo> reasonArrayList = new ArrayList<ModulePromo>();
        ModulePromo modulePromo = null;
        String status = "false";
        if (isStatus == true){
            status = "true";
        }
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT PromoID,PromoName,PromoCategory from Promo WHERE PromoCategory = '"+PromoCategory+"'", null);
            //Cursor c = sql.rawQuery("SELECT PromoID,PromoName from Promo ", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modulePromo = new ModulePromo();
                            modulePromo.setPromoID(c.getString(c.getColumnIndex(Constant.PromoID)));
                            modulePromo.setPromoName(c.getString(c.getColumnIndex(Constant.PromoName)));
                            modulePromo.setPromoCategory(c.getString(c.getColumnIndex(Constant.PromoCategory)));
                            modulePromo.setIsStatus(status);
                            reasonArrayList.add(modulePromo);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET Promo competitor activity kahfi
    public ArrayList<ModulePromo> getArrayListPromoCompetitorActivity(String VisitID,
                                                                      String CompetitorID,
                                                                      String CustomerID,
                                                                      String CompetitorProductID,
                                                                      String PromoCategory,
                                                                      boolean isStatus) {
        ArrayList<ModulePromo> reasonArrayList = new ArrayList<ModulePromo>();
        ModulePromo modulePromo = null;

        boolean isCompetitorActivity = false;

        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.PromoID,b.PromoName,b.PromoCategory,a.isStatus from PromoCompetitor a \n" +
                    "INNER JOIN Promo b on a.PromoID = b.PromoID\n" +
                    "WHERE a.VisitID = '"+VisitID+"'\n" +
                    "and a.CompetitorID = '"+CompetitorID+"'\n" +
                    "and a.CustomerID = '"+CustomerID+"'\n" +
                    "and a.CompetitorProductID = '"+CompetitorProductID+"'\n" +
                    "and b.PromoCategory = '"+PromoCategory+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modulePromo = new ModulePromo();
                            modulePromo.setPromoID(c.getString(c.getColumnIndex(Constant.PromoID)));
                            modulePromo.setPromoName(c.getString(c.getColumnIndex(Constant.PromoName)));
                            modulePromo.setPromoCategory(c.getString(c.getColumnIndex(Constant.PromoCategory)));
                            modulePromo.setIsStatus(c.getString(c.getColumnIndex(Constant.isStatus)));
                            reasonArrayList.add(modulePromo);
                            isCompetitorActivity = true;
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }


        if (isCompetitorActivity !=true){
            String status = "false";
            if (isStatus == true){
                status = "true";
            }
            try {
                SQLiteDatabase sql = this.getWritableDatabase();
                Cursor c = sql.rawQuery("SELECT PromoID,PromoName,PromoCategory from Promo WHERE PromoCategory = '"+PromoCategory+"'", null);
                //Cursor c = sql.rawQuery("SELECT PromoID,PromoName from Promo ", null);
                if (c != null) {
                    if (c.moveToFirst()) {
                        do {
                            try {
                                modulePromo = new ModulePromo();
                                modulePromo.setPromoID(c.getString(c.getColumnIndex(Constant.PromoID)));
                                modulePromo.setPromoName(c.getString(c.getColumnIndex(Constant.PromoName)));
                                modulePromo.setPromoCategory(c.getString(c.getColumnIndex(Constant.PromoCategory)));
                                modulePromo.setIsStatus(status);
                                reasonArrayList.add(modulePromo);
                            } catch (NullPointerException ex) {

                            } catch (Exception e) {

                            }
                        } while (c.moveToNext());
                    }
                }
                c.close();
                sql.close();
            } catch (Exception e) {
                Log.i("GETModuleButton", e.getMessage());
            }

        }
        return reasonArrayList;
    }



    //GET Promo competitor activity kahfi
    public ArrayList<ModuleCompetitor> getArrayListPromoCompetitorActivityValue(String VisitID,
                                                                      String CompetitorID,
                                                                      String CustomerID,
                                                                      String CompetitorProductID) {
        ArrayList<ModuleCompetitor> arrayListValue = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;

        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.VisitID,\n" +
                    "a.EmployeeID,\n" +
                    "a.CompetitorID,\n" +
                    "a.CustomerID,\n" +
                    "a.CompetitorProductID,\n" +
                    "a.Value, \n" +
                    "b.Description,\n" +
                    "a.CompetitorActivityTypeID,\n" +
                    "b.Seq from CompetitorActivityVisit a INNER JOIN CompetitorActivityType b \n" +
                    "on a.CompetitorActivityTypeID = b.CompetitorActivityTypeID\n" +
                    "WHERE a.VisitID = '"+VisitID+"'\n" +
                    "and a.CompetitorID = '"+CompetitorID+"' \n" +
                    "and a.CustomerID = '"+CustomerID+"' \n" +
                    "and a.CompetitorProductID = '"+CompetitorProductID+"'  ORDER BY b.Seq ASC;", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCompetitor.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCompetitor.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            moduleCompetitor.setDescription(c.getString(c.getColumnIndex(Constant.Description)));
                            moduleCompetitor.setCompetitorActivityTypeID(c.getString(c.getColumnIndex(Constant.CompetitorActivityTypeID)));
                            moduleCompetitor.setCompetitorProductID(c.getString(c.getColumnIndex(Constant.CompetitorProductID)));


                            arrayListValue.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }



        return arrayListValue;
    }



    //GET Sisa Stock Visit
    public ArrayList<ModuleSisaStockVisit> getArraySisaStockVisit(String CustomerID,String VisitID) {
        ArrayList<ModuleSisaStockVisit> moduleSisaStockVisits = new ArrayList<ModuleSisaStockVisit>();
        SQLiteDatabase sql = this.getReadableDatabase();
            /*Cursor c = sql.rawQuery("select C.VisitID as VisitID, \n" +
                    "C.CustomerID as CustomerID ,\n" +
                    "F.ProductName as ProductName,\n" +
                    "C.productid as ProductID,\n" +
                    "C.PromoPrice as PromoPrice,\n" +
                    "D.NormalPrice as NormalPrice,\n" +
                    "E.Stock as Stock,\n" +
                    "C.Status as Status,\n" +
                    "C.IsStok as IsStok,\n" +
                    "C.Notes as Notes\n" +
                    "from \n" +
                    "(select a.productid, a.value PromoPrice, a.CustomerID,a.VisitID,a.Status,a.IsStok, a.Notes from SisaStockVisit a\n" +
                    "where a.SisaStockTypeID='PromoPrice') C\n" +
                    "inner join\n" +
                    "(select a.productid, a.value NormalPrice, a.CustomerID,a.VisitID from SisaStockVisit a\n" +
                    "where a.SisaStockTypeID='NormalPrice') D on C.productid=D.productid and c.CustomerID=d.CustomerID and c.VisitID=d.VisitID\n" +
                    "inner join\n" +
                    "(select a.productid, sum(a.value) Stock, a.CustomerID,a.VisitID from SisaStockVisit a\n" +
                    "inner join SisaStockType b on a.SisaStockTypeID=b.SisaStockTypeID\n" +
                    "where b.Category='Month'\n" +
                    "group by a.ProductID, a.CustomerID) E on C.productid=E.productid and c.CustomerID=E.CustomerID and c.VisitID=e.VisitID\n" +
                    "inner join Product F on C.productid=F.ProductID\n" +
                    "where c.CustomerID='"+CustomerID+"' and c.VisitID='"+VisitID+"'", null);*/

        String queryString = "SELECT a.*,\n" +
                "(SELECT Value from SisaStockVisit WHERE VisitID = '"+VisitID+"' AND ProductID = a.ProductID AND CustomerID = '"+CustomerID+"') as Stock \n" +
                "FROM Product a";
        Cursor c = sql.rawQuery(queryString, null);
        Log.d(TAG, queryString);
        int jumlah = c.getCount();
        if (c != null) {
            while(c.moveToNext()){
                ModuleSisaStockVisit moduleSisaStockVisit = new ModuleSisaStockVisit();
                moduleSisaStockVisit.setProductID(c.getString(c.getColumnIndex(Constant.ProductID)));
                moduleSisaStockVisit.setProductName(c.getString(c.getColumnIndex(Constant.ProductName)));
                //moduleSisaStockVisit.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                //moduleSisaStockVisit.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                //moduleSisaStockVisit.setPromoPrice(c.getString(c.getColumnIndex(Constant.PromoPrice)));
                //moduleSisaStockVisit.setNormalPrice(c.getString(c.getColumnIndex(Constant.NormalPrice)));
                moduleSisaStockVisit.setStock(c.getString(c.getColumnIndex(Constant.Stock)));
                //moduleSisaStockVisit.setStatus(c.getString(c.getColumnIndex(Constant.Status)));
                //moduleSisaStockVisit.setIsStok(c.getString(c.getColumnIndex(Constant.IsStok)));
                //moduleSisaStockVisit.setNotes(c.getString(c.getColumnIndex(Constant.Notes)));
                moduleSisaStockVisit.setUOM(c.getString(c.getColumnIndex(Constant.UOM)));
                moduleSisaStockVisit.setIsMandatory(c.getString(c.getColumnIndex(Constant.IsMandatory)));
                moduleSisaStockVisit.setStatus("");
                moduleSisaStockVisits.add(moduleSisaStockVisit);
            }
        }
        c.close();
        sql.close();
        return moduleSisaStockVisits;
    }



    //GET Sisa Stock Visit dialog isi value
    public ArrayList<ModuleSisaStockVisit> getArraySisaStockVisitInputValue(String CustomerID,String VisitID, String ProductID ) {
        ArrayList<ModuleSisaStockVisit> reasonArrayList = new ArrayList<ModuleSisaStockVisit>();
        ModuleSisaStockVisit moduleSisaStockVisit = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.IsStok,a.ProductID,a.CustomerID,a.VisitID,b.SisaStockTypeID,b.Description, a.Value \n" +
                    "from SisaStockVisit a \n" +
                    "INNER JOIN SisaStockType b on a.SisaStockTypeID = b.SisaStockTypeID\n" +
                    " WHERE a.CustomerID = '"+CustomerID+"' and a.VisitID = '"+VisitID+"' AND a.ProductID = '"+ProductID+"' ORDER BY b.Seq ASC", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleSisaStockVisit = new ModuleSisaStockVisit();
                            moduleSisaStockVisit.setProductID(c.getString(c.getColumnIndex(Constant.ProductID)));
                            moduleSisaStockVisit.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleSisaStockVisit.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleSisaStockVisit.setSisaStockTypeID(c.getString(c.getColumnIndex(Constant.SisaStockTypeID)));
                            moduleSisaStockVisit.setDescription(c.getString(c.getColumnIndex(Constant.Description)));
                            moduleSisaStockVisit.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            moduleSisaStockVisit.setIsStok(c.getString(c.getColumnIndex(Constant.IsStok)));

                            reasonArrayList.add(moduleSisaStockVisit);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET Sisa Stock Visit dialog isi value
    public ArrayList<ModuleSisaStockVisit> getArraySisaStockVisitUpload() {
        ArrayList<ModuleSisaStockVisit> reasonArrayList = new ArrayList<ModuleSisaStockVisit>();
        ModuleSisaStockVisit moduleSisaStockVisit = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.VisitID,a.ProductID,a.CustomerID,a.UOM,a.SisaStockTypeID,a.Value,a.Status,\n" +
                    "a.Notes,a.CreatedDate,a.CreatedBy,b.EmployeeID from SisaStockVisit a inner join Visit b on a.VisitID = b.VisitID", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleSisaStockVisit = new ModuleSisaStockVisit();
                            moduleSisaStockVisit.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleSisaStockVisit.setProductID(c.getString(c.getColumnIndex(Constant.ProductID)));
                            moduleSisaStockVisit.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleSisaStockVisit.setUOM(c.getString(c.getColumnIndex(Constant.UOM)));
                            moduleSisaStockVisit.setSisaStockTypeID(c.getString(c.getColumnIndex(Constant.SisaStockTypeID)));
                            moduleSisaStockVisit.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            moduleSisaStockVisit.setStatus(c.getString(c.getColumnIndex(Constant.Status)));
                            moduleSisaStockVisit.setNotes(c.getString(c.getColumnIndex(Constant.Notes)));
                            moduleSisaStockVisit.setCreatedDate(c.getString(c.getColumnIndex(Constant.CreatedDate)));
                            moduleSisaStockVisit.setCreatedBy(c.getString(c.getColumnIndex(Constant.CreatedBy)));
                            moduleSisaStockVisit.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            reasonArrayList.add(moduleSisaStockVisit);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET PROMO VISIT
    public ArrayList<ModelPromoVisit> getArrayListPromoVisit() {
        ArrayList<ModelPromoVisit> reasonArrayList = new ArrayList<ModelPromoVisit>();
        ModelPromoVisit modelPromoVisit = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.VisitID,a.ProductID,a.CustomerID,a.PromoID, b.EmployeeID from PromoVisit a inner join Visit b on a.VisitID = b.VisitID", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelPromoVisit = new ModelPromoVisit();
                            modelPromoVisit.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelPromoVisit.setProductID(c.getString(c.getColumnIndex(Constant.ProductID)));
                            modelPromoVisit.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            modelPromoVisit.setPromoID(c.getString(c.getColumnIndex(Constant.PromoID)));
                            modelPromoVisit.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            reasonArrayList.add(modelPromoVisit);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }



    //GET POSMStock
    public ArrayList<ModelPOSMStock> getArrayListPOSMStock() {
        ArrayList<ModelPOSMStock> reasonArrayList = new ArrayList<ModelPOSMStock>();
        ModelPOSMStock modelPOSMStock = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.VisitID,a.POSMID,a.QuantityOut,b.POSMName from POSMStock a INNER JOIN POSMProduct b on a.POSMID = b.POSMID", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelPOSMStock = new ModelPOSMStock();
                            modelPOSMStock.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelPOSMStock.setPOSMID(c.getString(c.getColumnIndex(Constant.POSMID)));
                            modelPOSMStock.setQuantity(c.getString(c.getColumnIndex(Constant.QuantityOut)));
                            modelPOSMStock.setPOSMName(c.getString(c.getColumnIndex(Constant.POSMName)));
                            modelPOSMStock.setQuantityOut("0");
                            modelPOSMStock.setQuantitySisa(c.getString(c.getColumnIndex(Constant.QuantityOut)));
                            reasonArrayList.add(modelPOSMStock);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }




    //GET POSMStockVisit input
    public ArrayList<ModelPOSMStock> getArrayListPOSMStockVisitUpload() {
        ArrayList<ModelPOSMStock> reasonArrayList = new ArrayList<ModelPOSMStock>();
        ModelPOSMStock modelPOSMStock = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT VisitID,POSMID,CustomerID,Quantity,CreatedDate,CreatedBy from POSMStockVisit", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelPOSMStock = new ModelPOSMStock();
                            modelPOSMStock.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelPOSMStock.setPOSMID(c.getString(c.getColumnIndex(Constant.POSMID)));
                            modelPOSMStock.setQuantity(c.getString(c.getColumnIndex(Constant.Quantity)));
                            modelPOSMStock.setCreatedDate(c.getString(c.getColumnIndex(Constant.CreatedDate)));
                            modelPOSMStock.setCreatedBy(c.getString(c.getColumnIndex(Constant.CreatedBy)));
                            modelPOSMStock.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));

                            reasonArrayList.add(modelPOSMStock);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET RegisterNewCustomert input
    public ArrayList<ModelRegisterNewCustomer> getArrayListRegisterNewCustomer() {
        ArrayList<ModelRegisterNewCustomer> registerNewCustomerArrayList = new ArrayList<ModelRegisterNewCustomer>();
        ModelRegisterNewCustomer modelRegisterNewCustomer = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT RegisterID,CustomerID,QuestionCategoryID,EmployeeID,VisitID,Date,Latitude,Longitude from ResultSurvey WHERE IsSave = '1';\n", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelRegisterNewCustomer = new ModelRegisterNewCustomer();
                            modelRegisterNewCustomer.setRegisterID(c.getString(c.getColumnIndex(Constant.RegisterID)));
                            modelRegisterNewCustomer.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            modelRegisterNewCustomer.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelRegisterNewCustomer.setDate(c.getString(c.getColumnIndex(Constant.Date)));
                            modelRegisterNewCustomer.setLatitude(c.getString(c.getColumnIndex(Constant.Latitude)));
                            modelRegisterNewCustomer.setLongitude(c.getString(c.getColumnIndex(Constant.Longitude)));
                            modelRegisterNewCustomer.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            modelRegisterNewCustomer.setQuestionCategoryID(c.getString(c.getColumnIndex(Constant.QuestionCategoryID)));


                            registerNewCustomerArrayList.add(modelRegisterNewCustomer);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return registerNewCustomerArrayList;
    }



    //GET RegisterNewCustomertDetail input
    public ArrayList<ModelRegisterNewCustomer> getArrayListRegisterNewCustomertDetail() {
        ArrayList<ModelRegisterNewCustomer> registerNewCustomerArrayList = new ArrayList<ModelRegisterNewCustomer>();
        ModelRegisterNewCustomer modelRegisterNewCustomer = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT RegisterID,QuestionID,AnswerID,Value from ResultSurveyDetail WHERE IsSave = '1'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelRegisterNewCustomer = new ModelRegisterNewCustomer();
                            modelRegisterNewCustomer.setRegisterID(c.getString(c.getColumnIndex(Constant.RegisterID)));
                            modelRegisterNewCustomer.setQuestionID(c.getString(c.getColumnIndex(Constant.QuestionID)));
                            modelRegisterNewCustomer.setAnswerID(c.getString(c.getColumnIndex(Constant.AnswerID)));
                            modelRegisterNewCustomer.setValue(c.getString(c.getColumnIndex(Constant.Value)));

                            registerNewCustomerArrayList.add(modelRegisterNewCustomer);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return registerNewCustomerArrayList;
    }


    //GET POSMStockVisit input
    public ArrayList<ModelPOSMStock> getArrayListPOSMStockVisitInput(String VisitID,String CustomerID) {
        ArrayList<ModelPOSMStock> reasonArrayList = new ArrayList<ModelPOSMStock>();
        ModelPOSMStock modelPOSMStock = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.VisitID,\n" +
                    "a.CustomerID,\n" +
                    "a.POSMID,\n" +
                    "a.Quantity,\n" +
                    "c.POSMName FROM POSMStockVisit a \n" +
                    "INNER JOIN POSMStock b on a.POSMID = b.POSMID\n" +
                    "INNER JOIN POSMProduct c ON a.POSMID = c.POSMID \n" +
                    "WHERE a.VisitID = '"+VisitID+"'\n" +
                    "and a.CustomerID = '"+CustomerID+"';", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelPOSMStock = new ModelPOSMStock();
                            modelPOSMStock.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelPOSMStock.setPOSMID(c.getString(c.getColumnIndex(Constant.POSMID)));
                            modelPOSMStock.setQuantity(c.getString(c.getColumnIndex(Constant.Quantity)));
                            modelPOSMStock.setPOSMName(c.getString(c.getColumnIndex(Constant.POSMName)));
                            reasonArrayList.add(modelPOSMStock);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }

    //GET TOOLS
    public ArrayList<ModuleTools> getArralyTools(String VisitID) {
        ArrayList<ModuleTools> reasonArrayList = new ArrayList<ModuleTools>();
        ModuleTools moduleTools = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.VisitID,b.POSMName,a.POSMID,a.Quantity,a.QuantityOut  from POSMStock a\n" +
                    "INNER JOIN POSMProduct b \n" +
                    "WHERE a.POSMID = b.POSMID  and a.VisitID = '"+VisitID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleTools = new ModuleTools();
                            moduleTools.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleTools.setPOSMID(c.getString(c.getColumnIndex(Constant.POSMID)));
                            moduleTools.setPOSMName(c.getString(c.getColumnIndex(Constant.POSMName)));
                            moduleTools.setQuantity(c.getString(c.getColumnIndex(Constant.Quantity)));
                            moduleTools.setQuantityOut(c.getString(c.getColumnIndex(Constant.QuantityOut)));
                            reasonArrayList.add(moduleTools);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET TOOLS
    public ArrayList<ModuleTools> getArraListPOSMStock() {
        ArrayList<ModuleTools> reasonArrayList = new ArrayList<ModuleTools>();
        ModuleTools moduleTools = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.VisitID,a.POSMID,a.Quantity,a.CreatedDate,a.CreatedBy,b.EmployeeID from POSMStock a INNER JOIN Visit b on a.VisitID = b.VisitID", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleTools = new ModuleTools();
                            moduleTools.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleTools.setPOSMID(c.getString(c.getColumnIndex(Constant.POSMID)));
                            moduleTools.setQuantity(c.getString(c.getColumnIndex(Constant.Quantity)));
                            moduleTools.setCreatedDate(c.getString(c.getColumnIndex(Constant.CreatedDate)));
                            moduleTools.setCreatedBy(c.getString(c.getColumnIndex(Constant.CreatedBy)));
                            moduleTools.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));


                            reasonArrayList.add(moduleTools);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET STORE
    public ArrayList<ModelStore> getArrayStore(String CallPlanID) {
        ArrayList<ModelStore> storeArrayList = new ArrayList<ModelStore>();
        ModelStore modelStore = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.CustomerName,a.CustomerAddress,a.CustomerID,a.Distributor,a.CountryRegionCode,a.City,a.Account,b.Time,a.Channel from Customer a \n" +
                    "INNER JOIN CallPlanDetail b\n" +
                    "WHERE a.CustomerID = b.CustomerID and b.CallPlanID = '"+CallPlanID+"' ORDER BY cast(b.Sequence as Sequence)  ASC ", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelStore = new ModelStore();
                            modelStore.setCustomerName(c.getString(c.getColumnIndex(Constant.CustomerName)));
                            modelStore.setCustomerAddress(c.getString(c.getColumnIndex(Constant.CustomerAddress)));
                            modelStore.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            modelStore.setDistributor(c.getString(c.getColumnIndex(Constant.Distributor)));
                            modelStore.setCountryRegionCode(c.getString(c.getColumnIndex(Constant.CountryRegionCode)));
                            modelStore.setCity(c.getString(c.getColumnIndex(Constant.City)));
                            modelStore.setAccount(c.getString(c.getColumnIndex(Constant.Account)));
                            modelStore.setTime(c.getString(c.getColumnIndex(Constant.Time)));
                            modelStore.setChannel(c.getString(c.getColumnIndex(Constant.Channel)));

                            storeArrayList.add(modelStore);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return storeArrayList;
    }


    //GET STORE VISIT
    public ArrayList<ModelStore> getArrayStoreVisit(String CallPlanID) {
        ArrayList<ModelStore> storeArrayList = new ArrayList<ModelStore>();
        ModelStore modelStore = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT  b.CustomerName,a.Sequence, a.isStart, a.isFinish, a.isDeliver,a.VisitID, \n" +
                    "a.CustomerID,b.CustomerAddress,a.Time,d.Value,a.Latitude, a.Longitude\n" +
                    "FROM VisitDetail a \n" +
                    "INNER JOIN Customer b \n" +
                    "on a.CustomerID = b.CustomerID\n" +
                    "LEFT JOIN  Reason d on d.ReasonID = a.ReasonID \n" +
                    "where a.VisitID = '"+CallPlanID+"' ORDER BY cast(a.Sequence as Sequence)  ASC", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelStore = new ModelStore();
                            modelStore.setCustomerName(c.getString(c.getColumnIndex(Constant.CustomerName)));
                            modelStore.setSequence(c.getString(c.getColumnIndex(Constant.Sequence)));
                            modelStore.setIsStart(c.getString(c.getColumnIndex(Constant.isStart)));
                            modelStore.setIsFinish(c.getString(c.getColumnIndex(Constant.isFinish)));
                            modelStore.setIsDeliver(c.getString(c.getColumnIndex(Constant.isDeliver)));
                            modelStore.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelStore.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            modelStore.setCustomerAddress(c.getString(c.getColumnIndex(Constant.CustomerAddress)));
                            modelStore.setTime(c.getString(c.getColumnIndex(Constant.Time)));
                            modelStore.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            modelStore.setLatitude(c.getString(c.getColumnIndex(Constant.Latitude)));
                            modelStore.setLongitude(c.getString(c.getColumnIndex(Constant.Longitude)));


                            storeArrayList.add(modelStore);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return storeArrayList;
    }



    //GET STORE VISIT
    public ArrayList<ModelStore> getArrayStoreVisitFinish(String CallPlanID) {
        ArrayList<ModelStore> storeArrayList = new ArrayList<ModelStore>();
        ModelStore modelStore = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT  b.CustomerName,a.Sequence, a.isStart, a.isFinish, a.isDeliver,a.VisitID, \n" +
                    "a.CustomerID,b.CustomerAddress,a.Time,a.Latitude, a.Longitude\n" +
                    "FROM VisitDetail a \n" +
                    "INNER JOIN Customer b\n" +
                    "on a.CustomerID = b.CustomerID\n" +
                    "where a.VisitID = '"+CallPlanID+"' and (a.isFinish = 0 OR a.isDeliver = 2) ORDER BY cast(a.Sequence as Sequence)  ASC", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelStore = new ModelStore();
                            modelStore.setCustomerName(c.getString(c.getColumnIndex(Constant.CustomerName)));
                            modelStore.setSequence(c.getString(c.getColumnIndex(Constant.Sequence)));
                            modelStore.setIsStart(c.getString(c.getColumnIndex(Constant.isStart)));
                            modelStore.setIsFinish(c.getString(c.getColumnIndex(Constant.isFinish)));
                            modelStore.setIsDeliver(c.getString(c.getColumnIndex(Constant.isDeliver)));
                            modelStore.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelStore.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            modelStore.setCustomerAddress(c.getString(c.getColumnIndex(Constant.CustomerAddress)));
                            modelStore.setTime(c.getString(c.getColumnIndex(Constant.Time)));
                            modelStore.setLatitude(c.getString(c.getColumnIndex(Constant.Latitude)));
                            modelStore.setLongitude(c.getString(c.getColumnIndex(Constant.Longitude)));

                            storeArrayList.add(modelStore);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return storeArrayList;
    }


    //GET PRODAK
    public ArrayList<ModelProduct> getArrayModelProduct() {
        ArrayList<ModelProduct> modelProductArrayList = new ArrayList<ModelProduct>();
        ModelProduct modelProduct = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT ProductID,ProductName,ProductType,ProductGroup,ProductWeight,UOM,ArticleNumber FROM Product", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelProduct = new ModelProduct();
                            modelProduct.setProductID(c.getString(c.getColumnIndex(Constant.ProductID)));
                            modelProduct.setProductName(c.getString(c.getColumnIndex(Constant.ProductName)));
                            modelProduct.setProductType(c.getString(c.getColumnIndex(Constant.ProductType)));
                            modelProduct.setProductGroup(c.getString(c.getColumnIndex(Constant.ProductGroup)));
                            modelProduct.setProductWeight(c.getString(c.getColumnIndex(Constant.ProductWeight)));
                            modelProduct.setUOM((c.getString(c.getColumnIndex(Constant.UOM))));
                            modelProduct.setArticleNumber(c.getString(c.getColumnIndex(Constant.ArticleNumber)));
                            modelProductArrayList.add(modelProduct);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelProductArrayList;
    }


    //GET SisaStockType
    public ArrayList<ModuleSisaStockType> getArrayModuleSisaStockType() {
        ArrayList<ModuleSisaStockType> modelProductArrayList = new ArrayList<ModuleSisaStockType>();
        ModuleSisaStockType moduleSisaStockType = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT SisaStockTypeID,Description,Category,Seq from SisaStockType", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleSisaStockType = new ModuleSisaStockType();
                            moduleSisaStockType.setSisaStockTypeID(c.getString(c.getColumnIndex(Constant.SisaStockTypeID)));
                            moduleSisaStockType.setDescription(c.getString(c.getColumnIndex(Constant.Description)));
                            moduleSisaStockType.setCategory(c.getString(c.getColumnIndex(Constant.Category)));
                            moduleSisaStockType.setSeq(c.getString(c.getColumnIndex(Constant.Seq)));

                            modelProductArrayList.add(moduleSisaStockType);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelProductArrayList;
    }



    //GET CompetitorActivityType
    public ArrayList<ModelCompetitorActivityType> getArrayListtCompetitorActivityType() {
        ArrayList<ModelCompetitorActivityType> competitorActivityTypeArrayList = new ArrayList<ModelCompetitorActivityType>();
        ModelCompetitorActivityType modelCompetitorActivityType = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CompetitorActivityTypeID,Description,Category,Seq from CompetitorActivityType", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelCompetitorActivityType = new ModelCompetitorActivityType();
                            modelCompetitorActivityType.setCompetitorActivityTypeID(c.getString(c.getColumnIndex(Constant.CompetitorActivityTypeID)));
                            modelCompetitorActivityType.setDescription(c.getString(c.getColumnIndex(Constant.Description)));
                            modelCompetitorActivityType.setCategory(c.getString(c.getColumnIndex(Constant.Category)));
                            modelCompetitorActivityType.setSeq(c.getString(c.getColumnIndex(Constant.Seq)));

                            competitorActivityTypeArrayList.add(modelCompetitorActivityType);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return competitorActivityTypeArrayList;
    }



    //GET VISIT
    public ArrayList<ModelVisit> getArrayListVisit() {
        ArrayList<ModelVisit> reasonArrayList = new ArrayList<ModelVisit>();
        ModelVisit modelVisit = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT VisitID,VehicleNumber,VisitDate,KMStart,KMFinish,isStart,isFinish,StartDate,FinishDate,EmployeeID FROM Visit", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelVisit = new ModelVisit();
                            modelVisit.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelVisit.setVehicleNumber(c.getString(c.getColumnIndex(Constant.VehicleNumber)));
                            modelVisit.setVisitDate(c.getString(c.getColumnIndex(Constant.VisitDate)));
                            modelVisit.setKMStart(c.getString(c.getColumnIndex(Constant.KMStart)));
                            modelVisit.setIsStart(c.getString(c.getColumnIndex(Constant.isStart)));
                            modelVisit.setIsFinish(c.getString(c.getColumnIndex(Constant.isFinish)));
                            modelVisit.setStartDate(c.getString(c.getColumnIndex(Constant.StartDate)));
                            modelVisit.setFinishDate(c.getString(c.getColumnIndex(Constant.FinishDate)));
                            modelVisit.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            modelVisit.setKMFinish(c.getString(c.getColumnIndex(Constant.KMFinish)));


                            reasonArrayList.add(modelVisit);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET Competitor
    public ArrayList<ModelCompetitor> getArrayListCompetitor() {
        ArrayList<ModelCompetitor> modelCompetitorArrayList = new ArrayList<ModelCompetitor>();
        ModelCompetitor modelCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CompetitorID,CompetitorName FROM Competitor", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelCompetitor = new ModelCompetitor();
                            modelCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            modelCompetitor.setCompetitorName(c.getString(c.getColumnIndex(Constant.CompetitorName)));

                            modelCompetitorArrayList.add(modelCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelCompetitorArrayList;
    }


    //GET VISIT DITAIL
    public ArrayList<ModelVisitDetail> getArrayListVisitDetail() {
        ArrayList<ModelVisitDetail> reasonArrayList = new ArrayList<ModelVisitDetail>();
        ModelVisitDetail modelVisitDetail = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT a.*,b.EmployeeID from VisitDetail a inner join Visit b on a.VisitID = b.VisitID", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelVisitDetail = new ModelVisitDetail();
                            modelVisitDetail.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            modelVisitDetail.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            modelVisitDetail.setIsStart(c.getString(c.getColumnIndex(Constant.isStart)));
                            modelVisitDetail.setIsFinish(c.getString(c.getColumnIndex(Constant.isFinish)));
                            modelVisitDetail.setStartDate(c.getString(c.getColumnIndex(Constant.StartDate)));
                            modelVisitDetail.setFinishDate(c.getString(c.getColumnIndex(Constant.FinishDate)));
                            modelVisitDetail.setLatitude(c.getString(c.getColumnIndex(Constant.Latitude)));
                            modelVisitDetail.setLongitude(c.getString(c.getColumnIndex(Constant.Longitude)));
                            modelVisitDetail.setLatitudeCheckOut(c.getString(c.getColumnIndex(Constant.LatitudeCheckOut)));
                            modelVisitDetail.setLongitudeCheckOut(c.getString(c.getColumnIndex(Constant.LongitudeCheckOut)));
                            modelVisitDetail.setIsDeliver(c.getString(c.getColumnIndex(Constant.isDeliver)));
                            modelVisitDetail.setReasonID(c.getString(c.getColumnIndex(Constant.ReasonID)));
                            modelVisitDetail.setReasonDescription(c.getString(c.getColumnIndex(Constant.ReasonDescription)));
                            modelVisitDetail.setWarehouseID(c.getString(c.getColumnIndex(Constant.WarehouseID)));
                            modelVisitDetail.setIsInRangeCheckin(c.getString(c.getColumnIndex(Constant.isInRangeCheckin)));
                            modelVisitDetail.setIsInRangeCheckout(c.getString(c.getColumnIndex(Constant.isInRangeCheckout)));
                            modelVisitDetail.setDistance(c.getString(c.getColumnIndex(Constant.Distance)));
                            modelVisitDetail.setSequence(c.getString(c.getColumnIndex(Constant.Sequence)));
                            modelVisitDetail.setDistanceCheckout(c.getString(c.getColumnIndex(Constant.DistanceCheckout)));
                            modelVisitDetail.setIsVisit(c.getString(c.getColumnIndex(Constant.isVisit)));
                            modelVisitDetail.setTime(c.getString(c.getColumnIndex(Constant.Time)));
                            modelVisitDetail.setReasonSequence(c.getString(c.getColumnIndex(Constant.ReasonSequence)));
                            modelVisitDetail.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            modelVisitDetail.setDuration(c.getString(c.getColumnIndex(Constant.Duration)));


                            reasonArrayList.add(modelVisitDetail);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return reasonArrayList;
    }


    //GET COMPETITOR
    public ArrayList<ModuleCompetitor> getArrayListCompetitorActivity() {
        ArrayList<ModuleCompetitor> moduleCompetitorArrayList = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CompetitorID,CompetitorName from Competitor", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCompetitorName(c.getString(c.getColumnIndex(Constant.CompetitorName)));


                            moduleCompetitorArrayList.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return moduleCompetitorArrayList;
    }



    //GET COMPETITOR VISIT
    public ArrayList<ModuleCompetitor> getArrayListCompetitorActivityVisit(String isSave) {
        ArrayList<ModuleCompetitor> moduleCompetitorArrayList = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT VisitID,\n" +
                    "EmployeeID,\n" +
                    "CompetitorID,\n" +
                    "CustomerID,\n" +
                    "CompetitorProductID,\n" +
                    "CompetitorActivityTypeID,\n" +
                    "Notes,\n" +
                    "Value,\n" +
                    "CreatedDate,\n" +
                    "Notes FROM CompetitorActivityVisit WHERE isSave = '"+isSave+"';", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {

                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCompetitor.setEmployeeID(c.getString(c.getColumnIndex(Constant.EmployeeID)));
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCompetitor.setCompetitorProductID(c.getString(c.getColumnIndex(Constant.CompetitorProductID)));
                            moduleCompetitor.setCreatedDate(c.getString(c.getColumnIndex(Constant.CreatedDate)));
                            moduleCompetitor.setNotes(c.getString(c.getColumnIndex(Constant.Notes)));
                            moduleCompetitor.setCompetitorActivityTypeID(c.getString(c.getColumnIndex(Constant.CompetitorActivityTypeID)));
                            moduleCompetitor.setNotes(c.getString(c.getColumnIndex(Constant.Notes)));
                            moduleCompetitor.setValue(c.getString(c.getColumnIndex(Constant.Value)));
                            moduleCompetitorArrayList.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return moduleCompetitorArrayList;
    }




    //GET CompetitorActivityImage
    public ArrayList<ModuleCompetitor> getArrayListCompetitorActivityImage() {
        ArrayList<ModuleCompetitor> moduleCompetitorArrayList = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CompetitorActivityImageID,\n" +
                    "VisitID,\n" +
                    "CompetitorID,\n" +
                    "CompetitorProductID,\n" +
                    "CustomerID,\n" +
                    "Image,\n" +
                    "CreatedDate from CompetitorActivityImage", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setCompetitorActivityImageID(c.getString(c.getColumnIndex(Constant.CompetitorActivityImageID)));
                            moduleCompetitor.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCompetitorProductID(c.getString(c.getColumnIndex(Constant.CompetitorProductID)));
                            moduleCompetitor.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCompetitor.setImage(c.getString(c.getColumnIndex(Constant.Image)));
                            moduleCompetitor.setCreatedDate(c.getString(c.getColumnIndex(Constant.CreatedDate)));
                            moduleCompetitorArrayList.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return moduleCompetitorArrayList;
    }



    //GET PromoCompetitor
    public ArrayList<ModuleCompetitor> getArrayListPromoCompetitor(String isStatus) {
        ArrayList<ModuleCompetitor> moduleCompetitorArrayList = new ArrayList<ModuleCompetitor>();
        ModuleCompetitor moduleCompetitor = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT VisitID,CompetitorID,CustomerID,PromoID,CompetitorProductID from PromoCompetitor WHERE isStatus = '"+isStatus+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleCompetitor = new ModuleCompetitor();
                            moduleCompetitor.setVisitID(c.getString(c.getColumnIndex(Constant.VisitID)));
                            moduleCompetitor.setCompetitorID(c.getString(c.getColumnIndex(Constant.CompetitorID)));
                            moduleCompetitor.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            moduleCompetitor.setPromoID(c.getString(c.getColumnIndex(Constant.PromoID)));
                            moduleCompetitor.setCompetitorProductID(c.getString(c.getColumnIndex(Constant.CompetitorProductID)));
                            moduleCompetitorArrayList.add(moduleCompetitor);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return moduleCompetitorArrayList;
    }


    //GET NAMA CustomerName AND  CustomerAddress
    public ModelStore getModelStore_CustomerName(String CustomerID) {
        ModelStore modelStore = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT  a.CustomerName,a.CustomerAddress FROM Customer a\n" +
                    "INNER JOIN VisitDetail b\n" +
                    "on a.CustomerID = b.CustomerID\n" +
                    "where a.CustomerID = '"+CustomerID+"' ", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelStore = new ModelStore();
                            modelStore.setCustomerName(c.getString(c.getColumnIndex(Constant.CustomerName)));
                            modelStore.setCustomerAddress(c.getString(c.getColumnIndex(Constant.CustomerAddress)));
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return modelStore;
    }


    //CEK SISA STOK
    public boolean getCekModuleSisaStockVisit(String VisitID, String CustomerID) {

        boolean isSuccess = false;

        ModuleSisaStockVisit moduleSisaStockVisit = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT IsStok from SisaStockVisit WHERE VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleSisaStockVisit = new ModuleSisaStockVisit();
                            moduleSisaStockVisit.setIsStok(c.getString(c.getColumnIndex(Constant.IsStok)));
                            isSuccess =true;
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return isSuccess;
    }


    //CEK SISA STOK
    public boolean getResultSurveyCanvasser(String VisitID, String CustomerID) {

        boolean isSuccess = false;

        ModuleSisaStockVisit moduleSisaStockVisit = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT IsStok from SisaStockVisit WHERE VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            moduleSisaStockVisit = new ModuleSisaStockVisit();
                            moduleSisaStockVisit.setIsStok(c.getString(c.getColumnIndex(Constant.IsStok)));
                            isSuccess =true;
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return isSuccess;
    }



    //CEK SISA STOK
    public String  getCekModuleSisaStockVisitFinish(String VisitID, String CustomerID,String Status) {

        String value = null;
        boolean isValue = false;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();

            Cursor c = sql.rawQuery("SELECT  Status from SisaStockVisit WHERE \n" +
                    "VisitID = '"+VisitID+"'\n" +
                    "and CustomerID = '"+CustomerID+"'\n" +
                    "and Status ='"+Status+"' LIMIT 1", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            value = c.getString(c.getColumnIndex(Constant.Status));
                            isValue = true;
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }else {
                    isValue = true;
                }
            }else {
                isValue = true;
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return value;
    }


    public String getVisitCustomer(){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT CustomerID from VisitDetail where isStart=1 and isfinish=0", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("CustomerID"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETisFinish", e.getMessage());
        }
        return value;
    }



    public String getPOSMStockVisit(String VisitID,String CustomerID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT count(*) as VisitID FROM POSMStockVisit WHERE VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("VisitID"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETisFinish", e.getMessage());
        }
        return value;
    }



    //check tabel CompetitorActivityVisit
    public String getCheckCompetitorActivityVisit(String VisitID, String CustomerID,String EmployeeID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT count(*) as VisitID from CompetitorActivityVisit WHERE VisitID = '"+VisitID+"' and EmployeeID = '"+EmployeeID+"' and CustomerID = '"+CustomerID+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("VisitID"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETisFinish", e.getMessage());
        }
        return value;
    }


    public String getMobilFlag(String ModuleID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from MobileFlag where ModuleID= '"+ModuleID+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("Value"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETValue", e.getMessage());
        }
        return value;
    }


    //update MobileFlag----
    public void updateMobileFlag(String ModuleID ,String Value) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("Value", Value);
            sqliteMobileFlag.update("MobileFlag", values, "ModuleID = '" + ModuleID + "'", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {

        }
    }


    //update MobileFlag----
    public void updateModuleButton(String Module_Id ,String Module_type,String Value) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.Value, Value);
            sqliteMobileFlag.update(Constant.ModuleButton, values, "Module_Id = '" + Module_Id + "' and  Module_type = '" + Module_type + "' ", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {

        }
    }

    // -------update VisitDetail isFinish----
    public void updateVisitTunjaKunjungan(String CustomerID,String isDeliver,String FinishDate,String VisitID,String isFinish) {

        SQLiteDatabase sql = this.getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put(Constant.isDeliver, isDeliver);
            values.put(Constant.FinishDate, FinishDate);
            values.put(Constant.isFinish, isFinish);

            sql.update(Constant.VisitDetail, values, "CustomerID = '" + CustomerID + "' and VisitID = '"+VisitID+"'", null);
            sql.close();

        } catch (SQLiteException ex) {

        }
    }

    // -------update HistoryDownloadUpdate DOWNLOAD----
    public void updateDownloadUpload(String ID ,String Date) {

        SQLiteDatabase sqliteVEHICLEID = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("Date", Date);
            sqliteVEHICLEID.update("HistoryDownloadUpdate", values, "ID = '" + ID + "'", null);
            sqliteVEHICLEID.close();

        } catch (SQLiteException ex) {

        }
    }

    // update VisitDetail IsStart----
    public void updateVisitDetailStart(String VisitID,
                                  String CustomerID ,
                                  String isStart,
                                  String StartDate,
                                  String Langitude,
                                  String Longitude,
                                  String isVisit,
                                       String ReasonSequence) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.isStart, isStart);
            values.put(Constant.isVisit, isVisit);
            values.put(Constant.StartDate, StartDate);
            values.put(Constant.Latitude, Langitude);
            values.put(Constant.Longitude, Longitude);
            values.put(Constant.ReasonSequence, ReasonSequence);
            values.put(Constant.isFinish, "0");
            values.put(Constant.isDeliver, "0");
            sqliteMobileFlag.update(Constant.VisitDetail, values, "CustomerID = '" + CustomerID + "' and VisitID = '"+VisitID+"'", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {
            Log.i("DBHelper", "Gagal update start visit.");
        }
    }


    // update VisitDetail FINSIH----
    public void updateVisitDetailFinsih(String VisitID,
                                       String CustomerID ,
                                       String isFinish,
                                       String isDeliver,
                                       String LatitudeCheckOut,
                                       String LongitudeCheckOut,
                                       String ReasonID,
                                       String ReasonDescription,String FinishDate) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.isFinish, isFinish);
            values.put(Constant.isDeliver, isDeliver);
            values.put(Constant.LatitudeCheckOut, LatitudeCheckOut);
            values.put(Constant.LongitudeCheckOut, LongitudeCheckOut);
            values.put(Constant.ReasonID, ReasonID);
            values.put(Constant.ReasonDescription, ReasonDescription);
            values.put(Constant.FinishDate, FinishDate);

            sqliteMobileFlag.update(Constant.VisitDetail, values, "CustomerID = '" + CustomerID + "' and VisitID = '"+VisitID+"'", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {
            Log.i("DBHelper", "Gagal update start visit.");
        }
    }


    // update VisitDetail FINSIH----
    public void updateVisitDetailFinsihEnd(String VisitID,
                                        String CustomerID ,
                                        String isFinish,
                                        String isDeliver,
                                        String ReasonID,
                                        String ReasonDescription,String FinishDate,String StartDate) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.isFinish, isFinish);
            values.put(Constant.isDeliver, isDeliver);
            values.put(Constant.ReasonID, ReasonID);
            values.put(Constant.ReasonDescription, ReasonDescription);
            values.put(Constant.StartDate, StartDate);
            values.put(Constant.FinishDate, FinishDate);

            sqliteMobileFlag.update(Constant.VisitDetail, values, "CustomerID = '" + CustomerID + "' and VisitID = '"+VisitID+"'", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {
            Log.i("DBHelper", "Gagal update start visit.");
        }
    }



    public void iupdateVisitDetailFinsihEndTundaKunjungan(String reasonId, String customerId, String visitId, String description){
        SQLiteDatabase sql  = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("ReasonID", reasonId);
            values.put("ReasonDescription", description);
            values.put("isDeliver", "0");
            values.put("isFinish", "1");
            sql.update("VisitDetail", values, ("CustomerID = '" + customerId + "' AND VisitID = '" + visitId + "'"), null);

        } catch (SQLiteException ex) {
            Log.e("DBHelper", "Can't insert reason to VisitDetail : " + ex.getMessage());
        }
    }


    public void updateCompetitorProductID(String VisitID, String CompetitorID, String CustomerID, String PromoID,String CompetitorProductID,String isStatus ){
        SQLiteDatabase sql  = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.isStatus, isStatus);
            sql.update(Constant.CompetitorProductID, values, ("VisitID = '" + VisitID + "' AND CompetitorID = '" + CompetitorID + "' and CustomerID = '"+CustomerID+"' and PromoID = '"+PromoID+"' and CompetitorProductID = '"+CompetitorProductID+"'"), null);

        } catch (SQLiteException ex) {
            Log.e("DBHelper", "Can't insert reason to VisitDetail : " + ex.getMessage());
        }
    }


    public void updateVisitIsFinish(String VisitID,  String finishDate) {

        SQLiteDatabase sql = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("isStart", "1");
            values.put("isFinish", "1");
            values.put("FinishDate", finishDate);
            sql.update("Visit", values, "VisitID = '" + VisitID + "'", null);
            sql.close();

        } catch (SQLiteException ex) {

        }
    }


    public void updateCompetitorActivityVisit(String VisitID,
                                              String EmployeeID,
                                              String CompetitorID,
                                              String CustomerID,
                                              String CompetitorProductID,
                                              String Value,
                                              String Notes,
                                              String isSave,
                                              String CompetitorActivityTypeID) {

        SQLiteDatabase sql = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.Value, Value);
            values.put(Constant.Notes, Notes);
            values.put(Constant.isSave, isSave);
            sql.update(Constant.CompetitorActivityVisit, values, " VisitID = '" + VisitID + "' and CompetitorID = '"+CompetitorID+"' and CustomerID = '"+CustomerID+"' and  CompetitorProductID = '"+CompetitorProductID+"' and EmployeeID = '"+EmployeeID+"' and CompetitorActivityTypeID = '"+CompetitorActivityTypeID+"'", null);
            sql.close();

        } catch (SQLiteException ex) {

        }
    }


    //update MobileFlag----
    public void updateModuleButtonRole(String Module_Id ,String Module_type,String IsActive ,String IsRole,String Title) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            if (IsActive.equals("True")){
                IsActive = "1";
            }else {
                IsActive = "0";
            }
            values.put(Constant.IsActive, IsActive);
            values.put(Constant.IsRole, IsRole);
            values.put(Constant.Title, Title);
            sqliteMobileFlag.update(Constant.ModuleButton, values, "Module_Id = '" + Module_Id + "' and  Module_type = '" + Module_type + "' ", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {

        }
    }


    //update POSMStock----
    public void updatePOSMStock(String VisitID ,String POSMID,String Quantity,String createdDate,String createdBy,String QuantityOut) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.Quantity, Quantity);
            values.put(Constant.QuantityOut, QuantityOut);
            values.put(Constant.CreatedDate, createdDate);
            values.put(Constant.CreatedBy, createdBy);
            sqliteMobileFlag.update(Constant.POSMStock, values, "VisitID = '" + VisitID + "' and  POSMID = '" + POSMID + "' ", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {

        }
    }

    //update POSMStock
    public void updatePOSMStockQuantityOut(String VisitID ,String POSMID,String QuantityOut) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.QuantityOut, QuantityOut);
            sqliteMobileFlag.update(Constant.POSMStock, values, "VisitID = '" + VisitID + "' and  POSMID = '" + POSMID + "' ", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {

        }
    }


    //update Sisa Stock Visit value dan status
    public void updateSisaStockVisit(String VisitID ,String ProductID,String CustomerID, String SisaStockTypeID,String Value,String Status,String IsStok) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.Value, Value);
            values.put(Constant.Status, Status);
            values.put(Constant.IsStok, IsStok);
            sqliteMobileFlag.update(Constant.SisaStockVisit, values, "VisitID = '" + VisitID + "' and  ProductID = '" + ProductID + "' and CustomerID = '" + CustomerID + "' and SisaStockTypeID = '"+SisaStockTypeID+"'", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {

        }
    }


    //update Sisa Stock Visit Notes
    public void updateSisaStockVisitNotes(String VisitID ,String ProductID,String CustomerID,String Notes) {

        SQLiteDatabase sqliteMobileFlag = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constant.Notes, Notes);
            sqliteMobileFlag.update(Constant.SisaStockVisit, values, "VisitID = '" + VisitID + "' and  ProductID = '" + ProductID + "' and CustomerID = '" + CustomerID + "'", null);
            sqliteMobileFlag.close();

        } catch (SQLiteException ex) {

        }
    }

    public String getVisitIsStartCheckVersion(){

        String visitId = null;
        SQLiteDatabase sql  = this.getWritableDatabase();

        try {
            Cursor c = sql.rawQuery("SELECT isStart FROM Visit", null);

            if (c != null) {
                if (c.getCount()>0){
                    if (c.moveToFirst()) {
                        do {
                            visitId = c.getString(c.getColumnIndex("isStart"));
                        } while (c.moveToNext());
                    }
                }

            }
            else
                return null;
            c.close();
            sql.close();

        } catch (SQLiteException ex) {
            Log.e("DBHelper", "Can't get active VisitID : "+ex.getMessage());
        }

        return visitId;
    }

    public String getVersionApp(){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from MobileConfig where ID= 'APP_VERSION'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("Value"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETAPP_VERSION", e.getMessage());
        }
        return value;
    }

    public String getModuleButton(String Module_Id ,String Module_type){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from ModuleButton where Module_Id= '"+Module_Id+"' and Module_type = '"+Module_type+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex(Constant.Value));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETAPP_VERSION", e.getMessage());
        }
        return value;
    }

    public Boolean isDownloadIsCompleted(){
        Boolean result = false;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from ModuleButton where Module_Id= 'DOWNLOAD' and Module_type = 'DOWNLOAD_UPLOAD' and Value = '1'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                String value = cursor.getString(cursor.getColumnIndex(Constant.Value));
                return value.equals("1")? true : false;
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETAPP_VERSION", e.getMessage());
        }
        return result;
    }

    public String getMobilFlag(String ModuleID,String ModuleType){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from MobileFlag where ModuleID= '"+ModuleID+"' and ModuleType = '"+ModuleType+"'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("Value"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETValue", e.getMessage());
        }
        return value;
    }

    public String getBranchID(){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from MobileConfig where ID= 'BRANCHID'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("Value"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETDRIVERID", e.getMessage());
        }
        return value;
    }

    public String getEmployeeID(){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Value from MobileConfig where ID= 'DRIVERID'", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("Value"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETDRIVERID", e.getMessage());
        }
        return value;
    }

    public String getVehicleNumber(String EmployeeID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT VehicleNumber from CallPlan WHERE EmployeeID= '"+EmployeeID+"' ", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("VehicleNumber"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETVehicleNumber", e.getMessage());
        }
        return value;
    }

    public String getVisitID(String EmployeeID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT VisitID from Visit WHERE EmployeeID = '"+EmployeeID+"' ", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex(Constant.VisitID));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GetVisitID", e.getMessage());
        }
        return value;
    }

    public String getCallPlanID(String EmployeeID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT CallPlanID from CallPlan WHERE EmployeeID= '"+EmployeeID+"' ", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex(Constant.CallPlanID));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GetCallPlanID", e.getMessage());
        }
        return value;
    }

    public String getCallPlanDate(String EmployeeID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT Date from CallPlan WHERE EmployeeID= '"+EmployeeID+"' ", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex(Constant.Date));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GetCallPlanID", e.getMessage());
        }
        return value;
    }

    public String getCheckIn(){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("select count(*) as FlagCheckIn from VisitDetail where isStart=1 and isFinish=0", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("FlagCheckIn"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETDRIVERID", e.getMessage());
        }
        return value;
    }

    public String getIsStart(){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT isStart FROM Visit", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("isStart"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETDRIVERID", e.getMessage());
        }
        return value;
    }

    public String getIsFinish(){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT isFinish FROM Visit", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("isFinish"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETDRIVERID", e.getMessage());
        }
        return value;
    }

    public String getIsFinishCustomer(String CustomerID){
        String value=null;
        try{
            SQLiteDatabase sql  = this.getWritableDatabase();
            Cursor cursor = sql.rawQuery("SELECT isFinish from VisitDetail WHERE CustomerID = '"+CustomerID+"';", null);
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                cursor.moveToPosition(0);
                value = cursor.getString(cursor.getColumnIndex("isFinish"));
            }
            cursor.close();
            sql.close();
        } catch (Exception e){
            Log.i("GETDRIVERID", e.getMessage());
        }
        return value;
    }

    //get custemer id dari tabel CallPlanDetail
    public ArrayList<ModelCallPlanDetail> getCallPlanDetail(String CallPlanId) {
        ArrayList<ModelCallPlanDetail> reasonArrayList = new ArrayList<ModelCallPlanDetail>();
        ModelCallPlanDetail modelCallPlanDetail = null;
        try {
            SQLiteDatabase sql = this.getWritableDatabase();
            Cursor c = sql.rawQuery("SELECT CallPlanID, CustomerID,WarehouseID,Sequence,Time FROM CallPlanDetail where  CallPlanID  = '"+CallPlanId +"'", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        try {
                            modelCallPlanDetail = new ModelCallPlanDetail();
                            modelCallPlanDetail.setCallPlanID(c.getString(c.getColumnIndex(Constant.CallPlanID)));
                            modelCallPlanDetail.setCustomerID(c.getString(c.getColumnIndex(Constant.CustomerID)));
                            modelCallPlanDetail.setWarehouseID(c.getString(c.getColumnIndex(Constant.WarehouseID)));
                            modelCallPlanDetail.setSequence(c.getString(c.getColumnIndex(Constant.Sequence)));
                            modelCallPlanDetail.setTime(c.getString(c.getColumnIndex(Constant.Time)));

                            reasonArrayList.add(modelCallPlanDetail);
                        } catch (NullPointerException ex) {

                        } catch (Exception e) {

                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
            sql.close();
        } catch (Exception e) {
            Log.i("GETREASONLIST", e.getMessage());
        }
        return reasonArrayList;
    }

    //------Delet Tabel  sisa stock kalo tunda kunjungan atu tidak mengisi stock------------------------
    public void deleteSisaStockVisitTundaKunjungan(String VisitID,String CustomerID) {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("SisaStockVisit where VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"'", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  RegisterNewCustomer ketika isSave 0
    public void deleteRegisterNewCustomerIsSave(String IsSave) {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete( Constant.RegisterNewCustomer+" where IsSave = '"+IsSave+"'", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  RegisterNewCustomer ketika isSave 0
    public void deleteRegisterNewCustomerDetailIsSave(String IsSave) {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete( Constant.RegisterNewCustomerDetail+" where IsSave = '"+IsSave+"'", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }


    //------Delet Tabel  RegisterNewCustomer ketika employee melakukan save atau back ke page sebelumnya RegisterID
    public void deleteRegisterNewCustomerDetailRegisterID(String RegisterID) {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete( Constant.RegisterNewCustomerDetail+" where RegisterID = '"+RegisterID+"'", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  RegisterNewCustomer ketika employee melakukan save atau back ke page sebelumnya RegisterID
    public void deleteRegisterNewCustomerDetailRegisterIDandQuestionID(String RegisterID,String QuestionID ) {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete( Constant.RegisterNewCustomerDetail+" where RegisterID = '"+RegisterID+"' and QuestionID = '"+QuestionID+"'", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  RegisterNewCustomer ketika employee melakukan save atau back ke page sebelumnya RegisterID
    public void deleteRegisterNewCustomerRegisterID(String RegisterID) {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete( Constant.RegisterNewCustomer+" where RegisterID = '"+RegisterID+"'", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }




    public void deletePromoVisitTundaKunjungan(String VisitID,String CustomerID) {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("PromoVisit where VisitID = '"+VisitID+"' and CustomerID = '"+CustomerID+"'", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }



    //------Delet ProductUOM------------------------
    public void deleteProductUOM() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("ProductUOM", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }


    //------Delet MasterVehicle------------------------
    public void deleteMasterVehicle() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("MasterVehicle", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }


    //------Delet Tabel  Visit------------------------
    public void deleteWarehouse() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("Warehouse", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  LogVisit------------------------
    public void deleteLogVisit() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("Warehouse", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteCompetitorActivity() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("CompetitorActivity", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteCompetitor() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("Competitor", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteCompetitorProduct() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("CompetitorProduct", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletePOSMProduct() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.POSMProduct, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletePOSMStock() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.POSMStock, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteSisaStockType() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.SisaStockType, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteSisaStockVisit() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.SisaStockVisit, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletePromoVisit() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.PromoVisit, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletePromo() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.Promo, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteCompetitorActivityType() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.CompetitorActivityType, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletePromoCompetitor() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.PromoCompetitor, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteCompetitorActivityVisit() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.CompetitorActivityVisit, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteCompetitorActivityImage() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.CompetitorActivityImage, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletePOSMStockVisit() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.POSMStockVisit, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    // delete
    public void deleteResultSurvey() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.RegisterNewCustomer, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteResultSurveyDetail() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.RegisterNewCustomerDetail, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletems_answer() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.ms_answer, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletems_answer_type() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.ms_answer_type, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletems_question() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.ms_question, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deletems_question_set() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.ms_question_set, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    public void deleteQuestion_Category() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.Question_Category, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  Visit------------------------
    public void deleteVisit() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("Visit", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  Visit------------------------
    public void deleteVisitDetail() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("VisitDetail", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  CustomerType------------------------
    public void deleteCustomer() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete(Constant.Customer, null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  Customer------------------------
    public void deleteCustomerType() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("CustomerType", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  DeliveryOrder------------------------
    public void deleteDeliveryOrder() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("DeliveryOrder", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  DeliveryOrder Detail------------------------
    public void deleteDeliveryOrderDetail() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("DeliveryOrderDetail", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  CallPlan------------------------
    public void deleteCallPlan() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("CallPlan", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  CallPlanDetail------------------------
    public void deleteCallPlanDetail() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("CallPlanDetail", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  Product------------------------
    public void deleteProduct() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("Product", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  ReturOrder------------------------
    public void deleteReturOrder() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("ReturOrder", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  ReturOrderDetail------------------------
    public void deleteReturOrderDetail() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("ReturOrderDetail", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  CustomerImage------------------------
    public void deleteCustomerImage() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("CustomerImage", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  DailyCost------------------------
    public void deleteDailyCost() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("DailyCost", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  DailyMessage------------------------
    public void deleteDailyMessage() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("DailyMessage", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  Visit------------------------
    public void deleteHistoryLocation() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("LiveTracking", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  Reason------------------------
    public void deleteReason() {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("Reason", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }

    //------Delet Tabel  CallPlanDetail------------------------
    public void deleteRatio() {


        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.delete("Ratio", null, null);
            db.close();
        }catch (SQLiteException ex){

        }

    }


    //insert CallPlanDetail
    public void InsertCallPlanDetail(String CallPlanID, String CustomerID, String Sequence, String WarehouseID,String Time ){

        SQLiteDatabase CallPlanDetail = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CallPlanID,CallPlanID == null ? "" : CallPlanID);
            values.put(Constant.CustomerID,CustomerID == null ? "" : CustomerID);
            values.put(Constant.Sequence,Sequence == null ? "" : Sequence);
            values.put(Constant.WarehouseID,WarehouseID == null ? "" : WarehouseID);
            values.put(Constant.Time,Time == null ? "" : Time);
            CallPlanDetail.insert(Constant.CallPlanDetail, null, values);
            CallPlanDetail.close();

        }catch (SQLiteException ex){

        }
    }

    public void InitKunjunganType(){

        String IsRole = this.getMobileConfig(Constant.Role);
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            /**
             * hardcoded string is for rebel \o/
             */
            ContentValues values = new ContentValues();
            values.put("Module_Id", "KUNJUNGAN_DALAM_RUTE");
            values.put("Module_type", "KUNJUNGAN_TYPE");
            values.put("Value", "1");
            values.put("IsActive", "1");
            values.put("IsRole", IsRole);
            values.put("Title", "Sesuai Job Desc");
            values.put("IsSequence", "1");
            db.insert(Constant.ModuleButton, null, values);
            ContentValues values2 = new ContentValues();
            values2.put("Module_Id", "KUNJUNGAN_LUAR_RUTE");
            values2.put("Module_type", "KUNJUNGAN_TYPE");
            values2.put("Value", "1");
            values2.put("IsActive", "1");
            values2.put("IsRole", IsRole);
            values2.put("Title", "Diluar Job Desc");
            values2.put("IsSequence", "2");
            db.insert(Constant.ModuleButton, null, values2);
            db.close();

        }catch (SQLiteException ex){

        }
    }

    //insert RegisterNewCustomer
    public void insertRegisterNewCustomer(
            String RegisterID,
            String EmployeeID,
            String VisitID,
            String Date,
            String Latitude,
            String Longitude,
            String IsSave,
            String IsUpload,
            String CustomerID,
            String QuestionCategoryID){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.RegisterID,RegisterID == null ? "" : RegisterID);
            values.put(Constant.EmployeeID,EmployeeID == null ? "" : EmployeeID);
            values.put(Constant.VisitID,VisitID == null ? "" : VisitID);
            values.put(Constant.Date,Date == null ? "" : Date);
            values.put(Constant.Latitude,Latitude == null ? "" : Latitude);
            values.put(Constant.Longitude,Longitude == null ? "" : Longitude);
            values.put(Constant.IsSave,IsSave == null ? "" : IsSave);
            values.put(Constant.IsUpload,IsUpload == null ? "" : IsUpload);
            values.put(Constant.CustomerID,CustomerID == null ? "" : CustomerID);
            values.put(Constant.QuestionCategoryID,QuestionCategoryID == null ? "" : QuestionCategoryID);


            sqLiteDatabase.insert(Constant.RegisterNewCustomer, null, values);
            sqLiteDatabase.close();

        }catch (SQLiteException ex){

        }
    }

    //insert RegisterNewCustomerDetail
    public void insertRegisterNewCustomerDetail(
            String RegisterID,
            String QuestionID,
            String AnswerID,
            String Value,
            String IsSave,
            String IsUpload){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.RegisterID,RegisterID == null ? "" : RegisterID);
            values.put(Constant.QuestionID,QuestionID == null ? "" : QuestionID);
            values.put(Constant.AnswerID,AnswerID == null ? "" : AnswerID);
            values.put(Constant.Value,Value == null ? "" : Value);
            values.put(Constant.IsSave,IsSave == null ? "" : IsSave);
            values.put(Constant.IsUpload,IsUpload == null ? "" : IsUpload);
            sqLiteDatabase.insert(Constant.RegisterNewCustomerDetail, null, values);
            sqLiteDatabase.close();

        }catch (SQLiteException ex){

        }
    }



    //------insert CustomerImage------------------------
    public void insertCustomerImage(String ImageID, String CustomerID, String DocNumber, String VisitID, String ImageDate, String ImageBase64, String ImageType,String WarehouseID,String Latitude,String Longitude){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.ImageID,ImageID);
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.DocNumber,DocNumber);
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.ImageDate,ImageDate);
            values.put(Constant.ImageBase64,ImageBase64);
            values.put(Constant.ImageType,ImageType);
            values.put(Constant.WarehouseID,WarehouseID);
            values.put(Constant.Latitude,Latitude);
            values.put(Constant.Longitude,Longitude);

            SqlCustomerImage.insert(Constant.CustomerImage, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }


    //------insert CompetitorActivityImage------------------------
    public void insertCompetitorActivityImage(String CompetitorActivityImageID,
                                              String VisitID,
                                              String CompetitorID,
                                              String CompetitorProductID,
                                              String CustomerID,
                                              String Image,
                                              String CreatedDate){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CompetitorActivityImageID,CompetitorActivityImageID);
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.CompetitorID,CompetitorID);
            values.put(Constant.CompetitorProductID,CompetitorProductID);
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.Image,Image);
            values.put(Constant.CreatedDate,CreatedDate);

            SqlCustomerImage.insert(Constant.CompetitorActivityImage, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }


    //------insert POSMStockVisit------------------------
    public void insertPOSMStockVisit(String VisitID,
                                              String POSMID,
                                              String CustomerID,
                                              String Quantity,
                                              String CreatedDate,
                                              String CreatedBy,
                                              String IsSave){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.POSMID,POSMID);
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.Quantity,Quantity);
            values.put(Constant.CreatedDate,CreatedDate);
            values.put(Constant.CreatedBy,CreatedBy);
            values.put(Constant.IsSave,IsSave);

            SqlCustomerImage.insert(Constant.POSMStockVisit, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }




    //------insert CompetitorActivityVisit------------------------
    public void insertCompetitorActivityVisit(String VisitID,
                                        String EmployeeID,
                                        String CompetitorID,
                                        String CustomerID,
                                        String CompetitorProductID,
                                        String CreateDate,
                                        String Notes,
                                        String isSave,String CompetitorActivityTypeID, String Value){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.EmployeeID,EmployeeID);
            values.put(Constant.CompetitorID,CompetitorID);
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.CompetitorProductID,CompetitorProductID);
            values.put(Constant.CreatedDate,CreateDate);
            values.put(Constant.Notes,Notes);
            values.put(Constant.isSave,isSave);
            values.put(Constant.CompetitorActivityTypeID,CompetitorActivityTypeID);
            values.put(Constant.Value,Value);

            SqlCustomerImage.insert(Constant.CompetitorActivityVisit, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }


    //------insert CompetitorActivityVisit------------------------
    public void insertCompetitorActivityPromoVisit(String VisitID,
                                        String CompetitorID,
                                        String CustomerID,
                                        String PromoID,
                                        String isStatus,String CompetitorProductID){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.CompetitorID,CompetitorID);
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.PromoID,PromoID);
            values.put(Constant.isStatus,isStatus);
            values.put(Constant.CompetitorProductID,CompetitorProductID);


            SqlCustomerImage.insert(Constant.PromoCompetitor, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }

    //------insert CompetitorActivityType------------------------
    public void insertCompetitorActivityType(String CompetitorActivityTypeID,
                                                   String Description,
                                                   String Category,
                                                   String Seq){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CompetitorActivityTypeID,CompetitorActivityTypeID);
            values.put(Constant.Description,Description);
            values.put(Constant.Category,Category);
            values.put(Constant.Seq,Seq);


            SqlCustomerImage.insert(Constant.CompetitorActivityType, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }

    //------insert ms_question------------------------
    public void insertms_question(String Section,
                                             String QuestionID,
                                             String QuestionText,
                                             String AnswerTypeID,
                                  boolean IsSubQuestion,
                                  int Sequence,
                                  String QuestionSetID,
                                  String QuestionCategoryID,
                                  String AnswerID,
                                  boolean IsActive,
                                  boolean isMandatory){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.Section,Section);
            values.put(Constant.QuestionID,QuestionID);
            values.put(Constant.QuestionText,QuestionText);
            values.put(Constant.AnswerTypeID,AnswerTypeID);
            if (IsSubQuestion == true){
                values.put(Constant.IsSubQuestion,"1");
            }else {
                values.put(Constant.IsSubQuestion,"0");
            }
            values.put(Constant.Sequence,String.valueOf(Sequence));
            values.put(Constant.QuestionSetID,QuestionSetID);
            values.put(Constant.AnswerID,AnswerID);
            if (IsActive == true){
                values.put(Constant.IsActive,"1");
            }else {
                values.put(Constant.IsActive,"0");
            }

            if (isMandatory == true){
                values.put(Constant.Mandatory,"1");
            }else {
                values.put(Constant.Mandatory,"0");
            }

            values.put(Constant.QuestionCategoryID,QuestionCategoryID);

            SqlCustomerImage.insert(Constant.ms_question, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }


    //------insert ms_answer------------------------
    public void insertms_answer(String AnswerID,
                                  String AnswerText,
                                  String QuestionID,
                                  boolean SubQuestion,
                                  boolean  IsSubQuestion,
                                  int Sequence,
                                  String  No,
                                  boolean IsActive){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.AnswerID,AnswerID);
            values.put(Constant.AnswerText,AnswerText);
            values.put(Constant.QuestionID,QuestionID);
            if (SubQuestion == true){
                values.put(Constant.SubQuestion,"1");
            }else {
                values.put(Constant.SubQuestion,"0");
            }

            if (IsSubQuestion == true){
                values.put(Constant.IsSubQuestion,"1");
            }else {
                values.put(Constant.IsSubQuestion,"0");
            }

            values.put(Constant.Sequence,String.valueOf(Sequence));
            values.put(Constant.No,No);
            values.put(Constant.AnswerID,AnswerID);
            if (IsActive == true){
                values.put(Constant.IsActive,"1");
            }else {
                values.put(Constant.IsActive,"0");
            }


            SqlCustomerImage.insert(Constant.ms_answer, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }



    //------insert ms_answer_type------------------------
    public void insertms_answer_type(String AnswerTypeID,
                                String AnswerTypeText){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.AnswerTypeID,AnswerTypeID);
            values.put(Constant.AnswerTypeText,AnswerTypeText);


            SqlCustomerImage.insert(Constant.ms_answer_type, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }

    //------insert ms_question_set------------------------
    public void insertms_question_set(String QuestionSetID,
                                     String QuestionSetText){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.QuestionSetID,QuestionSetID);
            values.put(Constant.QuestionSetText,QuestionSetText);


            SqlCustomerImage.insert(Constant.ms_question_set, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }

    //------insert Question_Category------------------------
    public void insertQuestion_Category(String QuestionCategoryID,
                                     String QuestionCategoryText){

        SQLiteDatabase SqlCustomerImage = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.QuestionCategoryID,QuestionCategoryID);
            values.put(Constant.QuestionCategoryText,QuestionCategoryText);


            SqlCustomerImage.insert(Constant.Question_Category, null, values);
            SqlCustomerImage.close();

        }catch (SQLiteException ex){

        }
    }


    //insert POSMID
    public void InsertPOSMID(String VisitID, String POSMID, String Quantity, String CreatedDate,String CreatedBy,String QuantityOut ){

        SQLiteDatabase CallPlanDetail = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID == null ? "" : VisitID);
            values.put(Constant.POSMID,POSMID == null ? "" : POSMID);
            values.put(Constant.Quantity,Quantity == null ? "" : Quantity);
            values.put(Constant.CreatedDate,CreatedDate == null ? "" : CreatedDate);
            values.put(Constant.CreatedBy,CreatedBy == null ? "" : CreatedBy);
            values.put(Constant.QuantityOut,QuantityOut == null ? "" : QuantityOut);


            CallPlanDetail.insert(Constant.POSMStock, null, values);
            CallPlanDetail.close();

        }catch (SQLiteException ex){

        }
    }

    //insert POSMID
    public void InsertPromo(String PromoID, String PromoName, String PromoCategory){

        SQLiteDatabase CallPlanDetail = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.PromoID,PromoID == null ? "" : PromoID);
            values.put(Constant.PromoName,PromoName == null ? "" : PromoName);
            values.put(Constant.PromoCategory,PromoCategory == null ? "" : PromoCategory);
            CallPlanDetail.insert(Constant.Promo, null, values);
            CallPlanDetail.close();

        }catch (SQLiteException ex){

        }
    }

    //insert PromoVisit
    public void insertPromoVisit(String VisitID, String ProductID, String CustomerID,String PromoID){

        SQLiteDatabase CallPlanDetail = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID );
            values.put(Constant.ProductID,ProductID );
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.PromoID,PromoID);

            CallPlanDetail.insert(Constant.PromoVisit, null, values);
            CallPlanDetail.close();

        }catch (SQLiteException ex){

        }
    }


    //insert Product
    public void insertProduct(String ProductID,
                              String ProductName,
                              String ProductType,
                              String ProductGroup,
                              String ProductWeight,
                              String UOM,
                              String ArticleNumber){

        SQLiteDatabase Product = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.ProductID,ProductID);
            values.put(Constant.ProductName,ProductName);
            values.put(Constant.ProductType,ProductType);
            values.put(Constant.ProductGroup,ProductGroup);
            values.put(Constant.ProductWeight,ProductWeight);
            values.put(Constant.UOM,UOM);
            values.put(Constant.ArticleNumber,ArticleNumber);
            Product.insert(Constant.Product, null, values);
            Product.close();

        }catch (SQLiteException ex){

        }
    }

    // insert Sisa Stock Visit
    public void insertSisaStockVisit(String VisitID,
                                    String ProductID,
                                    String CustomerID,
                                     String UOM,
                                    String SisaStockTypeID,
                                    String Value,
                                     String Status,
                                     String Notes,
                                     String CreatedDate,
                                     String CreatedBy,
                                     String IsStok){

        SQLiteDatabase Product = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.ProductID,ProductID);
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.UOM,UOM);
            values.put(Constant.SisaStockTypeID,SisaStockTypeID);
            values.put(Constant.Value,Value);
            values.put(Constant.Status,Status);
            values.put(Constant.Notes,Notes);
            values.put(Constant.CreatedDate,CreatedDate);
            values.put(Constant.CreatedBy,CreatedBy);
            values.put(Constant.IsStok,IsStok);
            Cursor c = Product.query(Constant.SisaStockVisit, null, "VisitID = ? AND ProductID = ? AND CustomerID = ?",
                    new String[]{ VisitID, ProductID, CustomerID }, null, null, null);
            if(c!=null){
                if(c.getCount() > 0){
                    Product.update(Constant.SisaStockVisit, values, "VisitID = ? AND ProductID = ? AND CustomerID = ?", new String[]{ VisitID, ProductID, CustomerID });
                } else {
                    Product.insert(Constant.SisaStockVisit, null, values);
                }
            } else {
                Product.insert(Constant.SisaStockVisit, null, values);
            }
            Product.close();

        }catch (SQLiteException ex){

        }
    }




    //------insert Tabel Visit------------------------
    public void insertVisit(
            String VisitID,
            String VehicleNumber,
            String VisitDate,
            String KMStart,
            String KMFinish,
            String isStart,
            String isFinish,
            String StartDate,
            String FinishDate,
            String EmployeeID,
            String isUpload){

        SQLiteDatabase SqlVisit = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.VehicleNumber,VehicleNumber);
            values.put(Constant.VisitDate,VisitDate);
            values.put(Constant.KMStart,KMStart);
            values.put(Constant.isStart,isStart);
            values.put(Constant.KMFinish,KMFinish);
            values.put(Constant.isFinish,isFinish);
            values.put(Constant.StartDate,StartDate);
            values.put(Constant.FinishDate,FinishDate);
            values.put(Constant.EmployeeID,EmployeeID);
            values.put(Constant.isUpload,isUpload);
            SqlVisit.insert(Constant.Visit, null, values);
            SqlVisit.close();

        }catch (SQLiteException ex){

        }
    }

    //------insert Tabel VisitDetail------------------------
    public void InsertVisitDetail(String VisitID,
                                  String CustomerID,
                                  String isStart,
                                  String isFinish ,
                                  String StartDate,
                                  String FinishDate,
                                  String Latitude,
                                  String Longitude,
                                  String isDeliver,
                                  String ReasonID ,
                                  String ReasonDescription,String WarehouseID,
                                  String isInRangeCheckin,String isInRangeCheckout,
                                  String Distance,String Sequence,
                                  String Duration,
                                  String DistanceCheckout,
                                  String Flag_IS_DO_CUSTOMER_SAVE,
                                  String LatitudeCheckOut,
                                  String LongitudeCheckOut,
                                  String isUpload,
                                  String isVisit,
                                  String Time,
                                  String ReasonSequence){

        SQLiteDatabase SqlVisit = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.VisitID,VisitID);
            values.put(Constant.CustomerID,CustomerID);
            values.put(Constant.isStart,isStart);
            values.put(Constant.isFinish,isFinish);
            values.put(Constant.StartDate,StartDate);
            values.put(Constant.FinishDate,FinishDate);
            values.put(Constant.Latitude,Latitude);
            values.put(Constant.Longitude,Longitude);
            values.put(Constant.isDeliver,isDeliver);
            values.put(Constant.ReasonID,ReasonID);
            values.put(Constant.ReasonDescription,ReasonDescription);
            values.put(Constant.WarehouseID,WarehouseID);
            values.put(Constant.isInRangeCheckin,isInRangeCheckin);
            values.put(Constant.isInRangeCheckout,isInRangeCheckout);
            values.put(Constant.Distance,Distance);
            values.put(Constant.Sequence,Sequence);
            values.put(Constant.Duration,Duration);
            values.put(Constant.DistanceCheckout,DistanceCheckout);
            values.put(Constant.Flag_IS_DO_CUSTOMER_SAVE,Flag_IS_DO_CUSTOMER_SAVE);
            values.put(Constant.LatitudeCheckOut,LatitudeCheckOut);
            values.put(Constant.LongitudeCheckOut,LongitudeCheckOut);
            values.put(Constant.isUpload,isUpload);
            values.put(Constant.isVisit,isVisit);
            values.put(Constant.Time,Time);
            values.put(Constant.ReasonSequence,ReasonSequence);
            SqlVisit.insert(Constant.VisitDetail, null, values);
            SqlVisit.close();

        }catch (SQLiteException ex){

        }
    }

    public void insertVisitDetail(String customerID, String visitID, Etc.GenericCallback cb) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor c = sqLiteDatabase.rawQuery("SELECT MAX(cast(Sequence as number)) AS MAX FROM VisitDetail", null);
        c.moveToFirst();
        Integer max = c.getInt(c.getColumnIndex("MAX"));
        c.close();

        String defaultDate = "1910-01-01 00:00:00";


        ContentValues values = new ContentValues();
        values.put(Constant.VisitID, visitID);
        values.put(Constant.CustomerID, customerID);
        values.put(Constant.isStart, 0);
        values.put(Constant.isFinish, 0);
        values.put(Constant.StartDate, defaultDate);
        values.put(Constant.FinishDate, defaultDate);
        values.put(Constant.Latitude, 0);
        values.put(Constant.Longitude, 0);
        values.put(Constant.LatitudeCheckOut, 0);
        values.put(Constant.LongitudeCheckOut, 0);
        values.put(Constant.isDeliver, 0);
        values.put(Constant.ReasonID, "");
        values.put(Constant.ReasonDescription, "");
        values.put(Constant.WarehouseID, "");
        values.put(Constant.isInRangeCheckin, 0);
        values.put(Constant.isInRangeCheckout, 0);
        values.put(Constant.Distance, 0);
        values.put(Constant.Sequence, max + 1);
        values.put(Constant.Duration, 0);
        values.put(Constant.DistanceCheckout, 0);
        values.put(Constant.Flag_IS_DO_CUSTOMER_SAVE, 0);
        values.put(Constant.IsUpload, 0);
        values.put(Constant.isVisit, 0);
        values.put(Constant.Time, "");
        values.put(Constant.ReasonSequence, "");
        //values.put(Constant.isRoute, 1);
        Long inserted = sqLiteDatabase.insert(Constant.VisitDetail, null, values);
        if(cb!=null) cb.run(inserted);
    }


    //insert POSMProduct
    public void InsertPOSMIDPOSMProduct( String POSMID, String POSMName  ){

        SQLiteDatabase CallPlanDetail = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.POSMID,POSMID == null ? "" : POSMID);
            values.put(Constant.POSMName,POSMName == null ? "" : POSMName);

            CallPlanDetail.insert(Constant.POSMProduct, null, values);
            CallPlanDetail.close();

        }catch (SQLiteException ex){

        }
    }


    //insert SisaStockType
    public void InsertSisaStockType(String SisaStockTypeID, String Description,String Category,String Seq  ){

        SQLiteDatabase CallPlanDetail = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.SisaStockTypeID,SisaStockTypeID == null ? "" : SisaStockTypeID);
            values.put(Constant.Description,Description == null ? "" : Description);
            values.put(Constant.Category,Category == null ? "" : Category);
            values.put(Constant.Seq,Seq == null ? "" : Seq);

            CallPlanDetail.insert(Constant.SisaStockType, null, values);
            CallPlanDetail.close();

        }catch (SQLiteException ex){

        }
    }

    //------insert DailyMessage------------------------
    public void inserDailyMessage(String MessageID, String MessageName,String MessageDesc,String MessageImg,String Date,String CreatedBy){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.MessageID,MessageID);
            values.put(Constant.MessageName,MessageName);
            values.put(Constant.MessageDesc,MessageDesc);
            values.put(Constant.MessageImg,MessageImg);
            values.put(Constant.DATE,Date);
            values.put(Constant.CreatedBy,CreatedBy);
            sqLiteDatabase.insert(Constant.DailyMessage, null, values);
            sqLiteDatabase.close();

        }catch (SQLiteException ex){

        }
    }


    //insert CallPlan
    public void InsertCallPlan(String CallPlanID,
                               String Date,
                               String VehicleNumber,
                               String EmployeeID,
                               String Helper1,
                               String Helper2,
                               String KMAkhir){

        SQLiteDatabase SqlCustomer = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CallPlanID,CallPlanID == null ? "" : CallPlanID);
            values.put(Constant.Date,Date == null ? "" : Date);
            values.put(Constant.VehicleNumber,VehicleNumber  == null ? "" : VehicleNumber);
            values.put(Constant.EmployeeID,EmployeeID  == null ? "" : EmployeeID);
            values.put(Constant.Helper1,Helper1  == null ? "" : Helper1);
            values.put(Constant.Helper2,Helper2 == null ? "" : Helper2);
            values.put(Constant.KMAkhir,KMAkhir == null ? "0" : KMAkhir);
            SqlCustomer.insert(Constant.CallPlan, null, values);
            SqlCustomer.close();

        }catch (SQLiteException ex){

        }
    }

    //------insert Master Reason------------------------
    public void InserReason(String ReasonID, String ReasonType,String Value){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.ReasonID,ReasonID  == null ? "" : ReasonID);
            values.put(Constant.ReasonType,ReasonType  == null ? "" : ReasonType);
            values.put(Constant.Value,Value  == null ? "" : Value);
            sqLiteDatabase.insert("Reason", null, values);
            sqLiteDatabase.close();

        }catch (SQLiteException ex){

        }
    }

    //insert CustomerType
    public void InsertCustomerType(String CustomerTypeID, String CustomerTypeName, String Description ){

        SQLiteDatabase SqlCustomer = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CustomerTypeID,CustomerTypeID == null ? "" : CustomerTypeID);
            values.put(Constant.CustomerTypeName,CustomerTypeName == null ? "" : CustomerTypeName);
            values.put(Constant.Description,Description == null ? "" : Description);

            SqlCustomer.insert(Constant.CustomerType, null, values);
            SqlCustomer.close();

        }catch (SQLiteException ex){

        }
    }

    //insert
    public void insetCompetitor(String CompetitorID, String CompetitorName ){

        SQLiteDatabase SqlCustomer = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CompetitorID,CompetitorID  == null ? "" : CompetitorID);
            values.put(Constant.CompetitorName,CompetitorName  == null ? "" : CompetitorName);

            SqlCustomer.insert(Constant.Competitor, null, values);
            SqlCustomer.close();

        }catch (SQLiteException ex){

        }
    }

    //insert CompetitorActivity
    public void insetCompetitorActivity(String ActivityID, String ActivityName,String CompetitorID ){

        SQLiteDatabase SqlCustomer = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.ActivityID,ActivityID == null ? "" : ActivityID);
            values.put(Constant.ActivityName,ActivityName == null ? "" : ActivityName);
            values.put(Constant.CompetitorID,CompetitorID == null ? "" : CompetitorID);


            SqlCustomer.insert(Constant.CompetitorActivity, null, values);
            SqlCustomer.close();

        }catch (SQLiteException ex){

        }
    }

    //insert CompetitorProduct
    public void insetCompetitorProduct(String CompetitorID, String CompetitorProductID,String CompetitorProductName ){

        SQLiteDatabase SqlCustomer = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CompetitorID,CompetitorID == null ? "" : CompetitorID);
            values.put(Constant.CompetitorProductID,CompetitorProductID == null ? "" : CompetitorProductID);
            values.put(Constant.CompetitorProductName,CompetitorProductName == null ? "" : CompetitorProductName);

            SqlCustomer.insert(Constant.CompetitorProduct, null, values);
            SqlCustomer.close();

        }catch (SQLiteException ex){

        }
    }


    //insert Customer
    public void insetCustomer(String CustomerID,
                              String CustomerName,
                              String PIC,
                              String CustomerAddress,
                              String Phone,
                              String CustomerTypeID,
                              String Radius,
                              String Latitude,
                              String Longitude,
                              String City,
                              String BranchID,
                              String Account,
                              String CountryRegionCode,
                              String Distributor,
                              String Channel){

        SQLiteDatabase SqlCustomer = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constant.CustomerID,CustomerID == null ? "" : CustomerID);
            values.put(Constant.CustomerName,CustomerName == null ? "" : CustomerName);
            values.put(Constant.PIC,PIC == null ? "" : PIC);
            values.put(Constant.CustomerAddress,CustomerAddress == null ? "" : CustomerAddress);
            values.put(Constant.Phone,Phone == null ? "" : Phone);
            values.put(Constant.CustomerTypeID,CustomerTypeID == null ? "" : CustomerTypeID);
            values.put(Constant.Radius,Radius == null ? "" : Radius);
            values.put(Constant.Latitude,Latitude == null ? "0" : Latitude);
            values.put(Constant.Longitude,Longitude == null ? "0" : Longitude);
            values.put(Constant.City,City == null ? "" : City);
            values.put(Constant.BranchID,BranchID == null ? "" : BranchID);
            values.put(Constant.Account,Account == null ? "" : Account);
            values.put(Constant.CountryRegionCode,CountryRegionCode == null ? "" : CountryRegionCode);
            values.put(Constant.Distributor,Distributor == null ? "" : Distributor);
            values.put(Constant.Channel,Channel == null ? "" : Channel);


            SqlCustomer.insert(Constant.Customer, null, values);
            SqlCustomer.close();

        }catch (SQLiteException ex){

        }
    }

    public boolean insertResultSurvey(ResultSurvey model) {
        try{
            SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(ResultSurvey.Constant.ResultSurveyID.name(),model.getResultSurveyID() == null ? "" : model.getResultSurveyID());
            values.put(ResultSurvey.Constant.EmployeeID.name(),model.getEmployeeID() == null ? "" : model.getEmployeeID());
            values.put(ResultSurvey.Constant.CustomerID.name(),model.getCustomerID()== null ? "" : model.getCustomerID());
            values.put(ResultSurvey.Constant.QuestionCategoryID.name(),model.getQuestionCategoryID() == null ? "" : model.getQuestionCategoryID());
            values.put(ResultSurvey.Constant.Date.name(),model.getDate() == null ? "" : model.getDate());
            values.put(ResultSurvey.Constant.VisitID.name(),model.getVisitID() == null ? "" : model.getVisitID());
            values.put(ResultSurvey.Constant.BranchID.name(),model.getBranchID() == null ? "" : model.getBranchID());
            values.put(ResultSurvey.Constant.IsSave.name(),model.getIsSave() == null ? "" : model.getIsSave());
            values.put(ResultSurvey.Constant.Latitude.name(),model.getLatitude() == null ? "" : model.getLatitude());
            values.put(ResultSurvey.Constant.Longitude.name(),model.getLongitude() == null ? "" : model.getLongitude());

            values.put(ResultSurvey.Constant.IsUpload.name(),model.getIsUpload() == null ? "" : model.getIsUpload());
            values.put(ResultSurvey.Constant.StartDate.name(), model.getStartDate() == null ? "" : model.getStartDate());
            values.put(ResultSurvey.Constant.EndDate.name(), model.getEndDate() == null ? "" : model.getEndDate());

            sqLiteDatabase.insert(ResultSurvey.TABLE_NAME, null, values);
        }catch (SQLiteException ex){
            return false;
        }
        return true;
    }

    public boolean insertResultSurveyDetail(ArrayList<ResultSurveyDetail> models) {
        try {
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
            for (int i = 0; i < models.size(); i++) {
                ContentValues values = new ContentValues();
                values.put(ResultSurveyDetail.Constant.ResultSurveyID.name(), models.get(i).getResultSurveyID() == null ? "" : models.get(i).getResultSurveyID());
                values.put(ResultSurveyDetail.Constant.AnswerID.name(), models.get(i).getAnswerID() == null ? "" : models.get(i).getAnswerID());
                values.put(ResultSurveyDetail.Constant.QuestionID.name(), models.get(i).getQuestionID() == null ? "" : models.get(i).getQuestionID());
                values.put(ResultSurveyDetail.Constant.QuestionSetID.name(), models.get(i).getQuestionSetID() == null ? "" : models.get(i).getQuestionSetID());
                values.put(ResultSurveyDetail.Constant.Value.name(), models.get(i).getValue() == null ? "" : models.get(i).getValue());
                values.put(ResultSurveyDetail.Constant.IsSave.name(), models.get(i).getIsSave() == null ? "" : models.get(i).getIsSave());


                sqLiteDatabase.insert(ResultSurveyDetail.TableName, null, values);

            }

        } catch (SQLiteException ex) {
            Log.i("assist", ex.getMessage());
            return false;
        }
        return true;
    }

    public String getResultSurveyID(String CustomerID, String VisitID, String QuestionCategoryID,String QuestionSetID) {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor c = sqLiteDatabase.rawQuery("SELECT a.ResultSurveyID FROM ResultSurvey a INNER JOIN ResultSurveyDetail b ON b.ResultSurveyID = a.ResultSurveyID  WHERE  a.CustomerID = ? AND a.VisitID = ? AND  a.QuestionCategoryID = ?  AND b.QuestionSetID = ? ", new String[] {CustomerID,VisitID, QuestionCategoryID,QuestionSetID});

        if(c!=null){
            if (c.moveToFirst()) {
                return c.getString(c.getColumnIndex(ResultSurvey.Constant.ResultSurveyID.name()));
            }
        }

        c.close();
        return null;
    }

    public String getResultSurveyIDWithoutCategory(String CustomerID, String VisitID,String QuestionSetID) {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor c = sqLiteDatabase.rawQuery("SELECT a.ResultSurveyID FROM ResultSurvey a INNER JOIN ResultSurveyDetail b ON b.ResultSurveyID = a.ResultSurveyID  WHERE  a.CustomerID = ? AND a.VisitID = ? AND b.QuestionSetID = ? ", new String[] {CustomerID,VisitID,QuestionSetID});

        if(c!=null){
            if (c.moveToFirst()) {
                return c.getString(c.getColumnIndex("ResultSurveyID"));
            }
        }

        c.close();
        return null;
    }

    public ArrayList<ModelQuestion> getArrayListResultSurveyDetail(String ResultSurveyID,
                                                                   String QuestionCategoryID,
                                                                   String QuestionSetID) {
        ArrayList<ModelQuestion> moduleQuestionArrayList = new ArrayList<>();
        try {
            SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
            Cursor c = sqLiteDatabase.rawQuery("SELECT a.AnswerID, b.QuestionID, b.QuestionText,b.AnswerTypeID,b.IsSubQuestion,b.Sequence,b.Mandatory, a.Value, b.Length  \n" +
                    "from ResultSurveyDetail a \n" +
                    "INNER JOIN  ms_question b on b.QuestionID = a.QuestionID  \n" +
                    "WHERE a.ResultSurveyID = '" + ResultSurveyID + "'\n" +
                    "AND b.QuestionCategoryID = '" + QuestionCategoryID + "' \n" +
                    "AND b.QuestionSetID = '" + QuestionSetID + "' \n", null);
            if (c != null && c.moveToFirst()) {
                do {
                    try {
                        ModelQuestion moduleQuestion = new ModelQuestion();
                        moduleQuestion.setQuestionID(c.getString(c.getColumnIndex("QuestionID")));
                        moduleQuestion.setQuestionText(c.getString(c.getColumnIndex("QuestionText")));
                        moduleQuestion.setAnswerTypeID(c.getString(c.getColumnIndex("AnswerTypeID")));
                        moduleQuestion.setIsSubQuestion(c.getString(c.getColumnIndex("IsSubQuestion")));
                        moduleQuestion.setMandatory(c.getString(c.getColumnIndex("Mandatory")));
                        moduleQuestion.setValue(c.getString(c.getColumnIndex("Value")));
                        moduleQuestion.setSequence(c.getString(c.getColumnIndex("Sequence")));
                        moduleQuestion.setAnswerID(c.getString(c.getColumnIndex("AnswerID")));
                        moduleQuestionArrayList.add(moduleQuestion);
                    } catch (NullPointerException ex) {

                    } catch (Exception e) {

                    }
                } while (c.moveToNext());
                c.close();
            }
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return moduleQuestionArrayList;
    }

    public ArrayList<ModelQuestion> getArrayListResultSurveyDetailWithoutCategory(String ResultSurveyID) {
        ArrayList<ModelQuestion> moduleQuestionArrayList = new ArrayList<>();
        try {
            SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
            Cursor c = sqLiteDatabase.rawQuery("SELECT a.AnswerID, b.QuestionID, b.QuestionText,b.AnswerTypeID,b.IsSubQuestion,b.Sequence,b.Mandatory, a.Value, b.Length  \n" +
                    "from ResultSurveyDetail a \n" +
                    "INNER JOIN  ms_question b on b.QuestionID = a.QuestionID  \n" +
                    "WHERE a.ResultSurveyID = '" + ResultSurveyID + "'\n", null);
            if (c != null && c.moveToFirst()) {
                do {
                    try {
                        ModelQuestion moduleQuestion = new ModelQuestion();
                        moduleQuestion.setQuestionID(c.getString(c.getColumnIndex("QuestionID")));
                        moduleQuestion.setQuestionText(c.getString(c.getColumnIndex("QuestionText")));
                        moduleQuestion.setAnswerTypeID(c.getString(c.getColumnIndex("AnswerTypeID")));
                        moduleQuestion.setIsSubQuestion(c.getString(c.getColumnIndex("IsSubQuestion")));
                        moduleQuestion.setMandatory(c.getString(c.getColumnIndex("Mandatory")));
                        moduleQuestion.setValue(c.getString(c.getColumnIndex("Value")));
                        moduleQuestion.setSequence(c.getString(c.getColumnIndex("Sequence")));
                        moduleQuestion.setAnswerID(c.getString(c.getColumnIndex("AnswerID")));
                        moduleQuestionArrayList.add(moduleQuestion);
                    } catch (NullPointerException ex) {

                    } catch (Exception e) {

                    }
                } while (c.moveToNext());
                c.close();
            }
        } catch (Exception e) {
            Log.i("GETModuleButton", e.getMessage());
        }
        return moduleQuestionArrayList;
    }

    /**
     * Fetch Question sets with the answers count from SQLite
     * @return Question sets with the answers count from SQLite
     */
    public List<QuestionSet> getQuestionSets(String customerId, String employeeId) {
        ArrayList<QuestionSet> items = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT QuestionSetID, QuestionSetText, (SELECT COUNT(QuestionID) from ms_question where QuestionSetID = ms_question_set.QuestionSetID) as QuestionCount, (SELECT COUNT(*) FROM ResultSurvey rs INNER JOIN ResultSurveyDetail rsd ON rs.ResultSurveyID = rsd.ResultSurveyID WHERE rsd.QuestionSetID = ms_question_set.QuestionSetID AND rs.CustomerID = ? AND rs.EmployeeID = ?) AS AnswersCount FROM ms_question_set WHERE QuestionCount > 0", new String[] { customerId, employeeId });
        if (cursor.moveToFirst()) {
            do {
                QuestionSet item = new QuestionSet();
                item.setId(cursor.getString(cursor.getColumnIndex("QuestionSetID")));
                item.setText(cursor.getString(cursor.getColumnIndex("QuestionSetText")));
                item.setAnswersCount(cursor.getInt(cursor.getColumnIndex("AnswersCount")));
                items.add(item);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return items;
    }
    /**
     * Update IsUpload field on ResultSurvey table where ResultSurveyID matches
     *
     * @param item     ResultSurvey to be updated
     * @param isUpload value of IsUpload
     */
    public void updateIsUploadResultSurvey(ResultSurvey item, String isUpload) {
        try {
            SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(ResultSurvey.Constant.IsUpload.name(), isUpload);
            sqLiteDatabase.update(ResultSurvey.TABLE_NAME, values, "ResultSurveyID = ?", new String[]{item.getResultSurveyID()});
        } catch (SQLiteException ignored) {
        }
    }

    /**
     * Update IsUpload field on ResultSurveyDetail table where ResultSurveyID, QuestionID, QuestionSetID, and AnswerID matches
     *
     * @param item     ResultSurveyDetail to be updated
     * @param isUpload value of IsUpload
     */
    public void updateIsUploadResultSurveyDetail(ResultSurveyDetail item, String isUpload) {
        try {
            SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(ResultSurvey.Constant.IsUpload.name(), isUpload);
            sqLiteDatabase.update(ResultSurveyDetail.TableName, values, "ResultSurveyID = ? AND QuestionID = ? AND QuestionSetID = ? AND AnswerID = ?", new String[]{
                    item.getResultSurveyID(), item.getQuestionID(), item.getQuestionSetID(), item.getAnswerID()});
        } catch (SQLiteException ignored) {
        }
    }

    public boolean insertResultSurveyImage(ArrayList<AdapterListFoto.Model> imageList, String VisitID, String CustomerID, String surveyID) {
        try {
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
            for (int i = 0; i < imageList.size(); i++) {
                String imageID = "SURVEY_" + VisitID + "_" + CustomerID + "_" + surveyID + "_" + imageList.get(i).getTime().replace(" ", "_");
                ContentValues values = new ContentValues();
                values.put(Constant.ImageID,imageID);
                values.put(Constant.CustomerID,CustomerID);
                values.put(Constant.DocNumber,surveyID);
                values.put(Constant.VisitID,VisitID);
                values.put(Constant.ImageDate,imageList.get(i).getTime());
                values.put(Constant.ImageBase64,imageList.get(i).getFilePath());
                values.put(Constant.ImageType,"checklist");
                values.put(Constant.WarehouseID,"");
                values.put(Constant.Latitude,imageList.get(i).getLatitude());
                values.put(Constant.Longitude,imageList.get(i).getLongitude());

                sqLiteDatabase.insert(Constant.CustomerImage, null, values);

            }

        } catch (SQLiteException ex) {
            Log.i("assist", ex.getMessage());
            return false;
        }
        return true;
    }

    public ArrayList<AdapterListFoto.Model> getArraySurveyImage(String visitID, String customerID, String questionSetID) {
        ArrayList<AdapterListFoto.Model> imageList = new ArrayList<>();

        try{
            SQLiteDatabase sql  = this.getReadableDatabase();
            Cursor cursor = sql.query(Constant.CustomerImage, null,
                    "CustomerID=? AND VisitID=? AND DocNumber=?",
                    new String[]{ customerID, visitID, questionSetID }, null, null, null);
            if(cursor!=null){
                while (cursor.moveToNext()){
                    AdapterListFoto.Model img = new AdapterListFoto.Model();
                    img.setFilePath(cursor.getString(cursor.getColumnIndex("ImageBase64")));
                    imageList.add(img);
                }
                cursor.close();
            }

            sql.close();
        } catch (Exception e){
            Log.i("getimgsurvey", e.getMessage());
        }

        return imageList;
    }

    public int updateSurveyImageIsUpload(String questionSetID) {
        SQLiteDatabase sql  = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("IsUpload", "1");
        int updated = sql.update(
                Constant.CustomerImage,
                contentValues,
                "DocNumber=?",
                new String[]{ questionSetID });
        return updated;
    }
}