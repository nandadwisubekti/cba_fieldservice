package invent.cba.Helper;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Created by kahfi on 11/16/2015.
 */
public class MyLocationListener implements LocationListener {

    public void onLocationChanged(Location location) {
        String message = String.format(
                "New Location \n Longitude: %1$s \n Latitude: %2$s",
                location.getLongitude(), location.getLatitude()
        );

        /*System.out.println("MyLocationListener getLongitude " + location.getLongitude());
        System.out.println("MyLocationListener getLatitude" + location.getLatitude());*/


    }

    public void onStatusChanged(String s, int i, Bundle b) {

    }

    public void onProviderDisabled(String s) {

    }

    public void onProviderEnabled(String s) {

    }

}

