package invent.cba.Helper.data;

import android.content.Context;
import android.content.SharedPreferences;

import invent.cba.Helper.Constant;
import invent.cba.Interface.AbstractCompetitorProductID;
import invent.cba.Interface.AbstractCustomerID;
import invent.cba.Interface.AbstractEmployeeID;
import invent.cba.Interface.AbstractEnableModulePersipanTools;
import invent.cba.Interface.AbstractVisitID;

public class SharedPreferenceHelper implements AbstractVisitID,
        AbstractCustomerID,AbstractEmployeeID,
        AbstractEnableModulePersipanTools,
        AbstractCompetitorProductID {

    Context ctx;

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    private SharedPreferences sharedPreferences;

    public SharedPreferenceHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(Constant.PREFS_CFC, Context.MODE_PRIVATE);
        this.ctx = context;
    }

    @Override
    public String getCustomerID() {
        return sharedPreferences.getString(Constant.PREFS_TAG_CUSTOMER_ID, null);
    }

    @Override
    public String getEmployeeID() {
        return sharedPreferences.getString(Constant.PREFS_TAG_EMPLOYEE_ID, null);


    }

    @Override
    public String getVisitID() {


        return sharedPreferences.getString(Constant.PREFS_TAG_VISIT_ID, null);
    }

    @Override
    public String getCompetitorProductID() {
        return sharedPreferences.getString(Constant.PREFS_TAG_COMPETITIOR_PRODUCT_ID, null);
    }

    @Override
    public void setCompetitorProductID(Context context, String competitorProductID) {
        SharedPreferences prefs = context.getSharedPreferences(Constant.PREFS_CFC, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.PREFS_TAG_COMPETITIOR_PRODUCT_ID, competitorProductID);
        editor.apply();
    }

    public String getCompetitorID() {
        return sharedPreferences.getString(Constant.PREFS_TAG_COMPETITIOR_ID, null);
    }

    public void setCompetitorID(String competitorID) {
        SharedPreferences prefs = ctx.getSharedPreferences(Constant.PREFS_CFC, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.PREFS_TAG_COMPETITIOR_ID, competitorID);
        editor.apply();
    }

    @Override
    public String getEnableModulePersipanTools() {
        return sharedPreferences.getString(Constant.PREFS_TAG_ENABLE_MODULE_PERSIAPAN_TOOLS, null);
    }


    public static void saveCallPlanId(Context context, String  visitid){
        SharedPreferences prefs = context.getSharedPreferences(Constant.PREFS_CFC, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.PREFS_TAG_VISIT_ID, visitid);
        editor.apply();
    }

    public static void saveCustomerID(Context context, String  customerID){
        SharedPreferences prefs = context.getSharedPreferences(Constant.PREFS_CFC, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.PREFS_TAG_CUSTOMER_ID, customerID);
        editor.apply();
    }

    public static void saveEnableModulePersipanTools(Context context, String  EnableModulePersipanTools){
        SharedPreferences prefs = context.getSharedPreferences(Constant.PREFS_CFC, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.PREFS_TAG_ENABLE_MODULE_PERSIAPAN_TOOLS, EnableModulePersipanTools);
        editor.apply();
    }

    public static void saveCompetitorProductID(Context context, String  CompetitorProductID){
        SharedPreferences prefs = context.getSharedPreferences(Constant.PREFS_CFC, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.PREFS_TAG_COMPETITIOR_PRODUCT_ID, CompetitorProductID);
        editor.apply();
    }



}
