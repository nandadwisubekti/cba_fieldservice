package invent.cba.Helper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.SuperscriptSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * Created by kahfizain on 15/07/2017.
 */

public class TextHelper {


    public enum FontFamily {
        OPENSANS_BOLD("OpenSans-Bold.ttf"),
        VAG_ROUNDED_BOLD("HelveticaNeueLTPro-Roman.otf"),
        OPENSANS_SEMIBOLD("HelveticaNeueLTPro-Roman.otf"),
        OPENSANS_REGULAR("OpenSans-Regular.ttf"),
        ROBOTOSLAB_REGULAR("RobotoSlab-Regular.ttf"),
        ROBOTOSLAB_LIGHT("RobotoSlab-Light.ttf"),
        ROBOTOSLAB_BOLD("RobotoSlab-Bold.ttf"),
        ROBOTOSLAB_THIN("RobotoSlab-Thin.ttf");

        private String fontFamily;

        FontFamily(String fontFamily) {
            this.fontFamily = fontFamily;
        }

        public String getFontFamily() {
            return fontFamily;
        }
    }

    public static void setFont(Context context, View view, String fontFamily) {
        String font = "fonts/" + fontFamily;
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), font);
        if (view instanceof EditText) {
            EditText editText = (EditText) view;
            editText.setTypeface(typeface);
        } else if (view instanceof TextView) {
            TextView textView = (TextView) view;
            textView.setTypeface(typeface);
        } else if (view instanceof Button) {
            Button button = (Button) view;
            button.setTypeface(typeface);
        }
    }

    public static Spannable mandatoryFormSpan(String word){
        int wordLength = word.length();
        Spannable wordtoSpan = new SpannableString(word);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#E5131D")), wordLength - 1, wordLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new RelativeSizeSpan(0.8f), wordLength - 1, wordLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new SuperscriptSpan(), wordLength - 1, wordLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return wordtoSpan;
    }

    public static void styleEditText(Context context, EditText et, String hint, Boolean mandatory){
        setFont(context, et, FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        if (mandatory)
            et.setHint(mandatoryFormSpan(hint));
        else
            et.setHint(hint);
    }



}
