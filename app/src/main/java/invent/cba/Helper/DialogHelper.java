package invent.cba.Helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import cn.refactor.lib.colordialog.PromptDialog;
import invent.cba.BuildConfig;
import invent.cba.Helper.data.SharedPreferenceHelper;
import invent.cba.Model.ModelGetLocation;
import invent.cba.R;
import invent.cba.Service.DownloadVersionApp;
import invent.cba.View.Activity.Kunjungan.KunjunganActivity;
import invent.cba.View.Activity.Kunjungan.KunjunganStartActivity;
import invent.cba.View.Activity.Login.LoginActivity;

/**
 * Created by kahfizain on 06/07/2017.
 */

public class DialogHelper {

    Context context;

    public DialogHelper(Context context) {
        this.context = context;
    }

    public static void dialogMessage(Context context,String TitleText, String ContentTex,String dialogType){

        if (dialogType.equals(Constant.DIALOG_SUCCESS)){
            new PromptDialog(context)
                    .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                    .setAnimationEnable(true)
                    .setTitleText(TitleText)
                    .setContentText(ContentTex)
                    .setPositiveListener(context.getString(R.string.titel_dialog_ok), new PromptDialog.OnPositiveListener() {
                        @Override
                        public void onClick(PromptDialog dialog) {
                            dialog.dismiss();


                        }
                    })
                    .show();
        }
        if (dialogType.equals(Constant.DIALOG_WRONG)){
            new PromptDialog(context)
                    .setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                    .setAnimationEnable(true)
                    .setTitleText(TitleText)
                    .setContentText(ContentTex)
                    .setPositiveListener(context.getString(R.string.titel_dialog_ok), new PromptDialog.OnPositiveListener() {
                        @Override
                        public void onClick(PromptDialog dialog) {
                            dialog.dismiss();


                        }
                    })
                    .show();
        }

        if (dialogType.equals(Constant.DIALOG_ERROR)){
            new PromptDialog(context)
                    .setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                    .setAnimationEnable(true)
                    .setTitleText(TitleText)
                    .setContentText(ContentTex)
                    .setPositiveListener(context.getString(R.string.titel_dialog_ok), new PromptDialog.OnPositiveListener() {
                        @Override
                        public void onClick(PromptDialog dialog) {
                            dialog.dismiss();


                        }
                    })

                    .show();
        }




    }

    public static void dialogScreenBrightness(final Context context) {

        SeekBar seekbar;
        final TextView tvprogress;
        int Brightness;

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.dialog_screenbrightness, null);
        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(context);
        alert.setTitle(R.string.action_screen_brightness);
        alert.setView(promptView);


        seekbar = (SeekBar)promptView.findViewById(R.id.seekBar1);
        tvprogress = (TextView)promptView.findViewById(R.id.textView1);


        //Getting Current screen brightness.
        Brightness = Settings.System.getInt(context.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,0);

        //Setting up current screen brightness to seekbar;
        seekbar.setProgress(Brightness);

        //Setting up current screen brightness to TextView;
        tvprogress.setText("Screen Brightness : " + Brightness);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                tvprogress.setText("Screen Brightness : " + progress);

                //Changing Brightness on seekbar movement.

                Settings.System.putInt(context.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        alert.setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                        dialogBox.cancel();
                    }
                });



        android.support.v7.app.AlertDialog alertDialogAndroid = alert.create();
        alertDialogAndroid.show();
    }

    public static void dialogSettingPrinter(Context context) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.dialog_tracking, null);
        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(context);
        alert.setTitle("Setting printer");
        alert.setView(promptView);

        ArrayList<String> arrayPrinter;
        final ArrayList<String> arrayPrinterId;
        final DatabesHelper databesHelper = new DatabesHelper(context);
        final String[] statusPrinter = {null};

        final EditText etIntervalTracking = (EditText) promptView.findViewById(R.id.etIntervalTracking);
        final Spinner spPrinter = (Spinner) promptView.findViewById(R.id.spPrinter);
        etIntervalTracking.setVisibility(View.GONE);
        spPrinter.setVisibility(View.VISIBLE);




        ////////////////
        int flagPinter = 0;
        String printerMobilConfig = databesHelper.getMobileConfig(Constant.PRINTER);

        arrayPrinter = new ArrayList<String>();
        arrayPrinter.clear();
        arrayPrinterId = new ArrayList<String>();
        arrayPrinterId.clear();
        SQLiteDatabase db = databesHelper.getReadableDatabase();
        Cursor cursormsater = db.rawQuery("SELECT Id,Name FROM MasterPrinter", null);
        if (cursormsater != null) {
            if (cursormsater.moveToFirst()) {
                do {
                    try {
                        arrayPrinter.add(cursormsater.getString(cursormsater.getColumnIndex("Name")));
                        arrayPrinterId.add(cursormsater.getString(cursormsater.getColumnIndex("Id")));
                    } catch (NullPointerException ex) {

                    } catch (Exception e) {

                    }
                } while (cursormsater.moveToNext());
            }
        }
        cursormsater.close();
        db.close();
        //helper.close();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.spiner_item);
        for (int i = 0; i < arrayPrinter.size(); i++) {
            adapter.add(arrayPrinter.get(i));

        }

        spPrinter.setAdapter(adapter);
        spPrinter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                int x = spPrinter.getSelectedItemPosition();

                statusPrinter[0] = arrayPrinterId.get(x);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        for(int i = 0;i<arrayPrinterId.size();i++){
            if(arrayPrinterId.get(i).equals(printerMobilConfig)){
                flagPinter = i;
            }
        }

        spPrinter.setSelection(flagPinter);

        alert.setCancelable(false)
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                        databesHelper.updateMobileConfig(Constant.PRINTER, statusPrinter[0]);
                        dialogBox.cancel();
                    }
                })

                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog alertDialogAndroid = alert.create();
        alertDialogAndroid.show();
    }


    public static void dialogSettingIntervalTracking(Context context) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.dialog_tracking, null);
        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(context);
        alert.setTitle("Timer Tracking (Minute)");
        alert.setView(promptView);

        final DatabesHelper databesHelper = new DatabesHelper(context);

        final EditText etIntervalTracking = (EditText) promptView.findViewById(R.id.etIntervalTracking);
        final Spinner spPrinter = (Spinner) promptView.findViewById(R.id.spPrinter);
        etIntervalTracking.setVisibility(View.VISIBLE);
        etIntervalTracking.setEnabled(false);
        spPrinter.setVisibility(View.GONE);

        TextHelper.styleEditText(context, etIntervalTracking, "INTERVAL *", true);

        String timeTracking = databesHelper.getMobilFlag(Constant.TIMERTRACKING);
        if (timeTracking!=null)
            etIntervalTracking.setText(timeTracking);

        alert.setCancelable(false)
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {


                        if (etIntervalTracking.getText().length() >0){
                            if (etIntervalTracking.getText().toString().equalsIgnoreCase("0")){
                                databesHelper.updateMobileFlag(Constant.TIMERTRACKING, etIntervalTracking.getText().toString());
                                databesHelper.close();
                                dialogBox.cancel();
                            }
                        }

                    }
                })

                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog alertDialogAndroid = alert.create();
        alertDialogAndroid.show();
    }


    public static void dialogLogout(final Context context){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setMessage(R.string.dialog_ind_logOut)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                                Intent  intent = new Intent(context, LoginActivity.class);
                                context.startActivity(intent);
                                ((Activity) context).finish();
                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }


    public static void dialogExitAplikasi(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setMessage(R.string.dialog_ind_exit_App)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {


                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                                ((Activity) context).finish();
                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }


    //DIALOG KONFIRMASI VISIT START
    public static void dialogKonfirmasiVisitStart(final Context context, final String isStatus, final String visitId, final String CustomerId, final String ReasonSequence){

       final DatabesHelper databesHelper = new DatabesHelper(context);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setMessage(R.string.dialog_ind_kunjungan_toko)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface
                                                        dialog,
                                                int _id) {

                                String startDate = ValidationHelper.dateFormat();
                                final  ModelGetLocation mobileLocation;
                                ValidationHelper validationHelper = new ValidationHelper(context);
                                mobileLocation = validationHelper.getMobileLocation();
                                if (startDate !=null){

                                    if (isStatus.equals("IN")){
                                        //JIKA IisStart=1 and isfinish=0  MAKA LANJUTAKAN

                                    }else if (isStatus.equals("REASON_SEQUENCE")){
                                        //JIKA TIDAK SESUAI URUTAN TOKO,ATAU TIDAK SESUAI JAM TOKO
                                        databesHelper.updateVisitDetailStart(visitId,CustomerId,"1",startDate, mobileLocation.getLatitude(), mobileLocation.getLongitude(),"1",ReasonSequence);


                                    }else if (isStatus.equals("ON_TIME")){
                                        //JIKA TIDAK SESUAI URUTAN TOKO,ATAU TIDAK SESUAI JAM TOKO
                                        databesHelper.updateVisitDetailStart(visitId,CustomerId,"1",startDate, mobileLocation.getLatitude(), mobileLocation.getLongitude(),"1","");
                                    }


                                    SharedPreferenceHelper.saveCustomerID(context,CustomerId);
                                    //SharedPreferenceHelper.saveCallPlanId(context,visitId);

                                    Intent  intent = new Intent(context, KunjunganStartActivity.class);
                                    context.startActivity(intent);
                                    ((Activity) context).finish();

                                }else {
                                    Toast.makeText(context,  "Tidak mendapatkan tanggal,tekan kembali!", Toast.LENGTH_LONG).show();

                                }

                                dialog.dismiss();

                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }




    //DIALOG TOKO TIDAK SESUAI URUTAN ATU TIDAK SESAUI WAKTU
    public static void dialogReasonVisitStart(final Context context, final String pesanTitle, final String visitId, final String CustomerId){
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.dialog_kujungan_tidak_urut, null);
        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(context);
        alert.setTitle(pesanTitle);
        alert.setView(promptView);

        final EditText et_dialog_kujungan_tikda_urut = (EditText) promptView.findViewById(R.id.et_dialog_kujungan_tikda_urut);
       // TextHelper.styleEditText(context, et_dialog_kujungan_tikda_urut, "", true);

        alert.setCancelable(false)
                .setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {


                        if (et_dialog_kujungan_tikda_urut.getText().length() >0){

                            //UPDATE Reason Sequence di visit detail;
                            //berdasarkan customerid dan visitid
                            DialogHelper.dialogKonfirmasiVisitStart(context,"REASON_SEQUENCE",visitId,CustomerId,et_dialog_kujungan_tikda_urut.getText().toString());
                        }else {
                            Toast.makeText(context,  "Alasan harus di isi", Toast.LENGTH_LONG).show();

                        }

                        dialogBox.dismiss();

                    }
                })

                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog alertDialogAndroid = alert.create();
        alertDialogAndroid.show();

    }


    //DIALOG View Foto
    public static void dialogViewFoto(final Context context, final String pesanTitle, final String image64){
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.dialog_view_photo, null);
        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(context);
       // alert.setTitle(pesanTitle);
        alert.setView(promptView);
        alert.setCancelable(true);


        final ImageView imageView = (ImageView) promptView.findViewById(R.id.imageView);

        imageView.setImageBitmap(ImageBitmapBas64.decodeSampledBitmapFromResource(context.getResources(), image64, 100, 100));

      /*  alert.setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {


                        dialogBox.dismiss();

                    }
                });
*/


        android.support.v7.app.AlertDialog alertDialogAndroid = alert.create();
        alertDialogAndroid.show();

    }


    //TUNDA KUNJUNGAN ATAU SELESAI KUNJUNGAN
    public static void dialogKonfirmasiTundaKunjungan(final Context context){
        final DatabesHelper databesHelper = new DatabesHelper(context);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage(R.string.dialog_start_tunda_kujungan_new_counter)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_starta_TundaKunjungan,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                                //tunda kunjungan

                                SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
                                String  visitID = sharedPreferenceHelper.getVisitID();
                                String  customerId = sharedPreferenceHelper.getCustomerID();



                                //update tabel visit isDeliver jadi 2, isFinish jadi 1 dan date tunda kunjungan
                                databesHelper.updateVisitTunjaKunjungan(customerId,"2",ValidationHelper.dateFormat(),visitID,"1");
                                //delete tabel sisa stock
                                databesHelper.deleteSisaStockVisitTundaKunjungan(visitID,customerId);
                                //delete tabel Promo visit
                                databesHelper.deletePromoVisitTundaKunjungan(visitID,customerId);

                                Intent  intent = new Intent(context, KunjunganActivity.class);
                                context.startActivity(intent);
                                ((Activity) context).finish();

                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(R.string.dialog_starta_SelesaiKunjungan,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialogFinishReason(context,"Pilih alasan");
                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }

    //KONFIRMASI FINISH
    public  static void dialogKonfirmasiFinish(final  Context context){
        final DatabesHelper databesHelper = new DatabesHelper(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage(R.string.dialog_starta_finish)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                               /* //VALIDASI WAYHT
                                String visitID = modelGlobal.getVisitID;
                                String  customerId = modelGlobal.getCustomerID;
                                //boolean isValue = false;
                                boolean getSisaStock = false;
                                getSisaStock = databesHelper.getCekModuleSisaStockVisit(visitID,customerId);
                                if (getSisaStock == true){
                                   String  isValue = databesHelper.getCekModuleSisaStockVisitFinish(visitID,customerId,"");

                                    if (isValue !=null){
                                        if (isValue.equals("")){
                                            //tunda kunjungan atau lanjut(beri alasan)
                                            DialogHelper.dialogKonfirmasiTundaKunjungan(context);
                                        }
                                    }else {
                                        final  ModelGetLocation mobileLocation;
                                        ValidationHelper validationHelper = new ValidationHelper(context);
                                        mobileLocation = validationHelper.getMobileLocation();

                                        //finish update isFinish jadi 1, isDelivery jadi 1,date
                                        databesHelper.updateVisitDetailFinsih(visitID,customerId,"1","1",mobileLocation.getLatitude(),mobileLocation.getLongitude(),"","",ValidationHelper.dateFormat());
                                        Intent  intent = new Intent(context, KunjunganActivity.class);
                                        context.startActivity(intent);
                                        ((Activity) context).finish();
                                    }

                                }else {
                                    DialogHelper.dialogKonfirmasiTundaKunjungan(context);

                                }*/




                                //CFC VALIDASI
                                SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
                                String  visitID = sharedPreferenceHelper.getVisitID();
                                String  customerId = sharedPreferenceHelper.getCustomerID();


                                if (visitID !=null && customerId!=null){
                                    //jika canvasser tidak sama sekali di sisi
                                    //int isNowData = databesHelper.getLitResultSurveyVisitCustomerID("1",visitID,customerId,Constant.QuestionCategoryRegisterCustomer);
                                    // temporary bypass. todo: remove this once it's not in use
                                    int isNowData = 1;
                                    if (isNowData == 0){
                                        if (BuildConfig.HAS_POSTPONE_VISIT_FEATURE) {
                                            DialogHelper.dialogKonfirmasiTundaKunjungan(context);
                                        } else {
                                            DialogHelper.visitRequirementNotMet(context);
                                        }
                                    }else {
                                        final  ModelGetLocation mobileLocation;
                                        ValidationHelper validationHelper = new ValidationHelper(context);
                                        mobileLocation = validationHelper.getMobileLocation();

                                        //finish update isFinish jadi 1, isDelivery jadi 1,date
                                        databesHelper.updateVisitDetailFinsih(visitID,customerId,"1","1",mobileLocation.getLatitude(),mobileLocation.getLongitude(),"","",ValidationHelper.dateFormat());
                                        Intent  intent = new Intent(context, KunjunganActivity.class);
                                        context.startActivity(intent);
                                        ((Activity) context).finish();
                                    }
                                }else {
                                    DialogHelper.dialogMessage(context,"Informasi","Customer dan Visit tidak terbaca, coba tekan lagi, atu hubungi adamin!",Constant.DIALOG_ERROR);
                                }



                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Show an alert dialog when visit requirement has not fulfilled in order to finish a visit.
     * @param context Context of current activity or application
     */
    private static void visitRequirementNotMet(Context context) {
        new AlertDialog.Builder(context)
                .setTitle("New Counter Belum Terisi")
                .setMessage("Anda harus mengisi new counter sebelum dapat menyelesaikan kunjungan ini.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }


    //DIALOG FINISH REASON
    public static void dialogFinishReason(final Context context,String pesanTitle){
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.dialog_reason, null);
        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(context);
        alert.setTitle(pesanTitle);
        alert.setView(promptView);

        final DatabesHelper databesHelper = new DatabesHelper(context);

        final EditText et_reason = (EditText) promptView.findViewById(R.id.et_reason);
        final Spinner spPrinter = (Spinner)promptView.findViewById(R.id.spPrinter);
        TextHelper.setFont(context, et_reason, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


        final ArrayList<String> arrayReasonID;
        final ArrayList<String> arrayValue;
        final String[] ReasonID = {null};
        arrayReasonID = new ArrayList<String>();
        arrayReasonID.clear();
        arrayValue = new ArrayList<String>();
        arrayValue.clear();
        SQLiteDatabase db = databesHelper.getReadableDatabase();
        Cursor cursormsater = db.rawQuery("SELECT ReasonID,Value FROM Reason WHERE ReasonType = '"+Constant.delivery+"' ", null);
        if (cursormsater != null) {
            if (cursormsater.moveToFirst()) {
                do {
                    try {
                        arrayReasonID.add(cursormsater.getString(cursormsater.getColumnIndex("ReasonID")));
                        arrayValue.add(cursormsater.getString(cursormsater.getColumnIndex("Value")));
                    } catch (NullPointerException ex) {

                    } catch (Exception e) {

                    }
                } while (cursormsater.moveToNext());
            }
        }
        cursormsater.close();
        db.close();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.spiner_item);
        for (int i = 0; i < arrayValue.size(); i++) {
            adapter.add(arrayValue.get(i));
        }

        spPrinter.setAdapter(adapter);
        spPrinter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                int x = spPrinter.getSelectedItemPosition();

                ReasonID[0] = arrayReasonID.get(x);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        alert.setCancelable(false)
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {



                        final  ModelGetLocation mobileLocation;
                        ValidationHelper validationHelper = new ValidationHelper(context);
                        mobileLocation = validationHelper.getMobileLocation();

                        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
                        String  visitID = sharedPreferenceHelper.getVisitID();
                        String  customerId = sharedPreferenceHelper.getCustomerID();



                        String reasonDesc = "";
                        if (et_reason.getText().length()>0){
                            reasonDesc = et_reason.getText().toString();
                        }
                        //delete tabel sisa stock
                        databesHelper.deleteSisaStockVisitTundaKunjungan(visitID,customerId);
                        //delete tabel Promo visit
                        databesHelper.deletePromoVisitTundaKunjungan(visitID,customerId);
                        //update isFinish jadi 1 isDelivey jadi 0,ada reasonid, dan reasonDescription
                        databesHelper.updateVisitDetailFinsih(visitID,customerId,"1","0",mobileLocation.getLatitude(),mobileLocation.getLongitude(),ReasonID[0],reasonDesc,ValidationHelper.dateFormat());

                        Intent  intent = new Intent(context, KunjunganActivity.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        dialogBox.cancel();
                    }
                })

                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog alertDialogAndroid = alert.create();
        alertDialogAndroid.show();
    }


    ///////////DOWNLOAD APP NEW////////////////
    public static void dialogUpdateAppNews(final Context context , final String appNew, final String NameVersion){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage("Perubahan aplikasi")
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                                if (appNew.equalsIgnoreCase(Constant.DOWNLOAD_APP_VERSION_NEW)){
                                    new DownloadVersionApp(context,NameVersion);
                                }

                                dialog.dismiss();
                            }
                        });


        final AlertDialog alert = builder.create();
        alert.show();
    }


    public static void dialogRegisterError(final Context context, final String  Message){


        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Informasi");

        builder.setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                                dialog.dismiss();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();




    }


    public static void dialogKonfirmasiSurveyNotCompleted(Context context, int unansweredQuestion, final Etc.GenericCallback cb) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage("ada " + String.valueOf(unansweredQuestion) + " checklist yang belum Diisi. Lanjutkan?")
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        cb.run();
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(R.string.dialog_batal, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        final AlertDialog alert = builder.create();
        alert.show();
    }
}
