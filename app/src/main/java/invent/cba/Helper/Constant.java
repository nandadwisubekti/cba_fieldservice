package invent.cba.Helper;

/**
 * Created by kahfizain on 26/08/2017.
 */

public class Constant {

    public static final String PERMISSION_WRITE_EXTERNAL_STORAGE_CAMERA_RASSIONALE = "Access to camera, storage and phone state is needed";
    public static final int WRITE_EXTERNAL_STORAGE_CAMERA_REQUEST_CODE = 69;


    public final static String DB_NAME = "FieldServiceEasyPay.sqlite";
    public final static String APPVERSION = "APPVERSION";
    public final static String IPAlternatif = "http://35.240.192.212:8008";

    public final static String ic_launcher = "ic_launcher";
    public final static String QuestionCategoryID = "QuestionCategoryID";
    public final static String QuestionCategoryText = "QuestionCategoryText";

    public final static String Mandatory = "Mandatory";
    public final static String PREFS_CFC = "prefs_cfc";
    public final static String PREFS_TAG_CUSTOMER_ID = "prefs_tag_customer_id"; //key
    public final static String PREFS_TAG_EMPLOYEE_ID = "prefs_tag_employee_id"; //key
    public final static String PREFS_TAG_VISIT_ID = "prefs_tag_visit_id"; //key
    public final static String PREFS_TAG_ENABLE_MODULE_PERSIAPAN_TOOLS = "prefs_tag_Enable_Module_Persipan_Tools"; //key

    public final static String PREFS_TAG_COMPETITIOR_PRODUCT_ID = "prefs_tag_Competitor_ProductID"; //key
    public final static String PREFS_TAG_COMPETITIOR_ID = "prefs_tag_CompetitorID"; //key




    //MODULE BUTTON
    public final static String Module_Id = "Module_Id";
    public final static String Module_type = "Module_type";
    public final static String Value = "Value";
    public final static String IsActive = "IsActive";
    public final static String IsRole = "IsRole";
    public final static String Title = "Title";
    public final static String SETTING = "SETTING";
    public final static String HOME = "HOME";
    public final static String PERSIAPAN = "PERSIAPAN";
    public final static String MULAIPERJALANAN = "MULAIPERJALANAN";
    public final static String KUNJUNGAN = "KUNJUNGAN";
    public final static String ISSU = "ISSUEHOME";
    public final static String AKHIRIKEGIATAN = "AKHIRIKEGIATAN";
    public final static String REPORT = "REPORT";
    public final static String DOWNLOAD_UPLOAD = "DOWNLOAD_UPLOAD";
    public final static String UTILITAS = "UTILITAS";
    public final static String ID = "ID";
    public static final String KUNJUNGAN_TYPE = "KUNJUNGAN_TYPE";
    public final static String TIMETOLERANCE = "TIMETOLERANCE";
    public final static String DOWNLOAD_APP_VERSION_NEW = "DOWNLOAD_APP_VERSION_NEW";
    public final static String NAME_VERSION = "NAME_VERSION";
    public final static String Version = "Version";
    public final static String ITEMGOODS = "ITEMGOODS";
    public static final String SURVEY = "SURVEY";

    public final static String NEWCOUNTER = "NEWCUSTOMER";



    public final static String UPLOAD = "UPLOAD";

    //MODULE ID
    public final static String PROFILE = "PROFILE";
    public final static String KATASANDI = "KATASANDI";
    public final static String WIFI = "WIFI";
    public final static String BIRGHTNESS = "BIRGHTNESS";
    public final static String ID_DEVICE = "ID_DEVICE";
    public final static String PRINTER = "PRINTER";
    public final static String TRACKING = "TRACKING";
    public final static String UPDATE_APP = "UPDATE_APP";
    public final static String UPDATE_CONFIGURATION = "UPDATE_CONFIGURATION";
    public final static String DOWNLOD_CPANEL = "DOWNLOD_CPANEL";
    public final static String RUTEKUNJUNGAN = "RUTEKUNJUNGAN";
    public final static String TOOLS = "TOOLS";
    public final static String NEWS = "NEWS";
    public final static String STOCKSTORE = "STOCKSTORE";
    public final static String POSM = "POSM";
    public final static String ISSUE = "ISSUEKUNJUNGAN";
    public final static String KOMPETITOR = "KOMPETITOR";
    public final static String DATAPEMBELIAN = "DATAPEMBELIAN";
    public final static String FOTODISPLAY = "FOTODISPLAY";
    public final static String CANVASSER = "CANVASSER";
    public final static String NEWCUSTOMER = "NEWCUSTOMER";



    //IP DAN PORT
    public final static String IPCONFIG = "IPCONFIG";
    public final static String PORT = "PORT";
    public final static String IPSTATION = "IPSTATION";
    public final static String PORTSTATION = "PORTSTATION";
    public final static String IPALTERNATIF = "IPALTERNATIF";
    public final static String PORTALTERNATIF = "PORTALTERNATIF";
    public final static String IPUSER = "IPUSER";
    public final static String PORTUSER = "PORTUSER";

    //USER CONFIG
    public final static String DRIVERID = "DRIVERID";
    public final static String BRANCHID = "BRANCHID";
    public final static String BRANCHNAME = "BRANCHNAME";
    public final static String DRIVER_NAME = "DRIVER_NAME";
    public final static String DOWNLOAD_USER_CONFIG = "DOWNLOAD_USER_CONFIG";
    public final static String PASSWORD = "PASSWORD";
    public final static String PASSWORDRESET = "PASSWORDRESET";
    public final static String PASSWORDSETTING = "PASSWORDSETTING";
    public final static String VEHICLEID = "VEHICLEID";
    public final static String VEHICLENUMBER = "VEHICLENUMBER";
    public final static String CODRIVER = "CODRIVER";

    //MobileFlag
    public final static String DOWNLOAD = "DOWNLOAD";
    public final static String TESTKONEKSI = "TESTKONEKSI";
    public final static String IS_TOKEN = "IS_TOKEN";
    public static final String CHECKLIST = "CHECKLIST";
    public static final String TIMERTRACKING = "TIMERTRACKING";



    //dialog type

    public final static String token = "token";
    public final static String deviceID = "deviceID";
    public final static String DIALOG_SUCCESS = "SUCCESS";
    public final static String DIALOG_WRONG = "WRONG";
    public final static String DIALOG_ERROR = "ERROR";
    public final static String TRACKINGID = "TRACKINGID";
    //DOWNLOAD DATA PERJALANAN
    public final static String mdlJson = "mdlJson";
    public final static String Result = "Result";
    public final static String CallPlanList = "CallPlanList";
    public final static String CallPlanDetailList = "CallPlanDetailList";
    public final static String CompetitorActivityList = "CompetitorActivityList";
    public final static String CompetitorList = "CompetitorList";
    public final static String CompetitorProductList = "CompetitorProductList";
    public final static String CustomerTypeList = "CustomerTypeList";
    public final static String ReasonList = "ReasonList";
    public final static String CustomerList = "CustomerList";
    public final static String CPDetailID = "CPDetailID";
    public final static String CallPlanID = "CallPlanID";
    public final static String CUSTOMERID = "CustomerID";
    public final static String SEQUENCE = "Sequence";
    public final static String WAREHOUSEID = "WarehouseID";
    public final static String EmployeeID = "EmployeeID";
    public final static String VehicleID = "VehicleID";
    public final static String SurveyID = "SurveyID";


    public final static String Helper1 = "Helper1";
    public final static String Helper2 = "Helper2";
    public final static String KMAkhir = "KMAkhir";
    public final static String Date = "Date";
    public final static String ActivityID = "ActivityID";
    public final static String ActivityName = "ActivityName";
    public final static String CompetitorID = "CompetitorID";
    public final static String CompetitorName = "CompetitorName";
    public final static String CompetitorProductID = "CompetitorProductID";
    public final static String CompetitorProductName = "CompetitorProductName";
    public final static String CustomerTypeID = "CustomerTypeID";
    public final static String CustomerTypeName = "CustomerTypeName";
    public final static String Description = "Description";
    public final static String ReasonID = "ReasonID";
    public final static String ReasonType = "ReasonType";
    public final static String Role = "Role";
    public final static String Competitor = "Competitor";
    public final static String CompetitorActivity = "CompetitorActivity";
    public final static String CompetitorProduct = "CompetitorProduct";
    public final static String CustomerType = "CustomerType";
    public final static String CallPlan = "CallPlan";
    public final static String Sequence = "Sequence";
    public final static String No = "No";


    public final static String Customer = "Customer";
    public final static String CustomerID = "CustomerID";
    public final static String CustomerName = "CustomerName";
    public final static String PIC = "PIC";
    public final static String CustomerAddress = "CustomerAddress";
    public final static String Phone = "Phone";
    public final static String Radius = "Radius";
    public final static String Latitude = "Latitude";
    public final static String Longitude = "Longitude";
    public final static String City = "City";
    public final static String BranchID = "BranchID";
    public final static String Account = "Account";
    public final static String WarehouseID = "WarehouseID";
    public final static String CallPlanDetail = "CallPlanDetail";
    public final static String VehicleNumber = "VehicleNumber";
    public final static String CountryRegionCode = "CountryRegionCode";
    public final static String Distributor = "Distributor";
    public final static String VisitDate = "VisitDate";
    public final static String KMStart = "KMStart";
    public final static String isStart = "isStart";
    public final static String KMFinish = "KMFinish";
    public final static String StartDate = "StartDate";
    public final static String FinishDate = "FinishDate";
    public final static String isUpload = "isUpload";
    public final static String Visit = "Visit";
    public final static String isDeliver = "isDeliver";
    public final static String ReasonDescription = "ReasonDescription";
    public final static String isInRangeCheckin = "isInRangeCheckin";
    public final static String isInRangeCheckout = "isInRangeCheckout";
    public final static String Distance = "Distance";
    public final static String Duration = "Duration";
    public final static String DistanceCheckout = "DistanceCheckout";
    public final static String Flag_IS_DO_CUSTOMER_SAVE = "Flag_IS_DO_CUSTOMER_SAVE";
    public final static String LatitudeCheckOut = "LatitudeCheckOut";
    public final static String LongitudeCheckOut = "LongitudeCheckOut";
    public final static String isVisit = "isVisit";
    public final static String VisitDetail = "VisitDetail";
    public final static String Time = "Time";
    public final static String Channel = "Channel";
    public final static String ModuleButton = "ModuleButton";
    public final static String StatusCode = "StatusCode";
    public final static String StatusMessage = "StatusMessage";
    public final static String ModuleID = "ModuleID";
    public final static String ModuleType = "ModuleType";
    public final static String POSMProductList = "POSMProductList";
    public final static String POSMStock = "POSMStock";
    public final static String VisitID = "VisitID";
    public final static String Quantity = "Quantity";
    public final static String CreatedDate = "CreatedDate";
    public final static String CreatedBy = "CreatedBy";
    public final static String POSMID = "POSMID";
    public final static String POSMProduct = "POSMProduct";
    public final static String POSMName = "POSMName";
    public final static String CallPlanId = "CallPlanId";
    public final static String ReasonSequence = "ReasonSequence";
    public final static String ProductID = "ProductID";
    public final static String ProductName = "ProductName";
    public final static String ProductType = "ProductType";
    public final static String ProductGroup = "ProductGroup";
    public final static String ProductWeight = "ProductWeight";
    public final static String UOM = "UOM";
    public final static String ArticleNumber = "ArticleNumber";
    public final static String Product = "Product";
    public final static String ProductList = "ProductList";
    public final static String SisaStockVisit = "SisaStockVisit";
    public final static String PromoPrice = "PromoPrice";
    public final static String Price = "Price";
    public final static String enam = "6";
    public final static String enam_sembilan = "6-9";
    public final static String sembilan_dua_belas = "9-12";
    public final static String lebih_dua_belas = ">12";
    public final static String IsStok = "IsStok";
    public final static String NotListed = "NotListed";
    public final static String Notes = "Notes";
    public final static String Promo = "Promo";
    public final static String PromoID = "PromoID";
    public final static String PromoName = "PromoName";
    public final static String PromoList = "PromoList";
    public final static String MobileConfigList = "MobileConfigList";
    public final static String SisaStockType = "SisaStockType";
    public final static String SisaStockTypeList = "SisaStockTypeList";
    public final static String NormalPrice = "NormalPrice";
    public final static String Stock = "Stock";
    public final static String PromoCategory = "PromoCategory";
    public final static String PromoVisit = "PromoVisit";
    public final static String SisaStock = "SISASTOCK";
    public final static String Category = "Category";
    public final static String SisaStockTypeID = "SisaStockTypeID";
    public final static String Seq = "Seq";
    public final static String Status = "Status";
    public final static String ImageID = "ImageID";
    public final static String DocNumber = "DocNumber";
    public final static String ImageDate = "ImageDate";
    public final static String ImageBase64 = "ImageBase64";
    public final static String ImageType = "ImageType";
    public final static String CustomerImage = "CustomerImage";
    public final static String delivery = "delivery";
    public final static String visit = "visit";
    public final static String EndDate = "EndDate";
    public final static String isFinish = "isFinish";
    public final static String isInRange = "isInRange";
    public final static String Image = "Image";
    public final static String COMPETITOR = "COMPETITOR";
    public final static String CompetitorActivityImageID = "CompetitorActivityImageID";
    public final static String QuantityIn = "QuantityIn";
    public final static String QuantityOut = "QuantityOut";
    public final static String IsSave = "IsSave";
    public final static String RegisterNewCustomer = "ResultSurvey";
    public final static String RegisterNewCustomerDetail = "ResultSurveyDetail";
    public final static String IsUpload = "IsUpload";
    public final static String IsStatus = "IsStatus";

    public final static String ms_answer = "ms_answer";
    public final static String ms_answer_type = "ms_answer_type";
    public final static String ms_question = "ms_question";
    public final static String ms_question_set = "ms_question_set";
    public final static String Question_Category = "Question_Category";


    public final static String QuestionList = "QuestionList";
    public final static String AnswerList = "AnswerList";
    public final static String AnswerTypeList = "AnswerTypeList";
    public final static String QuestionSetList = "QuestionSetList";
    public final static String QuestionCategoryList = "QuestionCategoryList";



    public final static String DailyMessageList = "DailyMessageList";
    public final static String MessageID = "MessageID";
    public final static String MessageName = "MessageName";
    public final static String MessageDesc = "MessageDesc";
    public final static String MessageImg = "MessageImg";
    public final static String DATE = "DATE";
    public final static String DailyMessage = "DailyMessage";
    public final static String CompetitorActivityVisit = "CompetitorActivityVisit";
    public final static String isSave = "isSave";
    public final static String PromoCompetitor = "PromoCompetitor";
    public final static String isStatus = "isStatus";

    public final static String CompetitorActivityType = "CompetitorActivityType";
    public final static String CompetitorActivityTypeID = "CompetitorActivityTypeID";
    public final static String CompetitorActivityTypeList = "CompetitorActivityTypeList";
    public final static String CompetitorActivityImage = "CompetitorActivityImage";
    public final static String POSMStockVisit = "POSMStockVisit";

    public final static String ShortAnswer = "ATY-0001";
    public final static String Paragraph = "ATY-0002";
    public final static String CheckBox= "ATY-0003";
    public final static String MultipleChoice= "ATY-0004";
    public final static String Numeric= "ATY-0005";
    public final static String NumericDecimal= "ATY-0006";
    public final static String DateType= "ATY-0007";
    public final static String PhoneNumber= "ATY-0008";
    public final static String Email= "ATY-0009";
    public final static String TIME= "ATY-0010";


    public final static String PAGE_1= "QST-001";
    public final static String PAGE_2= "QST-002";
    public final static String PAGE_3= "QST-003";
    public final static String PAGE_4= "QST-004";
    public final static String PAGE_5= "QST-005";
    public final static String CANVASSER_PAGE_1= "QST-006";



    public final static String RegisterID= "RegisterID";
    public final static String QuestionID= "QuestionID";
    public final static String QuestionText= "QuestionText";
    public final static String AnswerTypeID= "AnswerTypeID";
    public final static String IsSubQuestion= "IsSubQuestion";
    public final static String QuestionSetID= "QuestionSetID";
    public final static String Section= "Section";
    public final static String AnswerTypeText= "AnswerTypeText";
    public final static String QuestionSetText= "QuestionSetText";


    public final static String QuestionVisit= "QCD-0001";
    public final static String QuestionCategoryRegisterCustomer= "QCD-0004";
    public final static String QuestionCategoryCanvasser= "QCD-0005";



    // public final static String QuestionCategoryRegisterCustomer= "QCD-0004";


    public final static String AnswerID= "AnswerID";
    public final static String SubQuestion= "SubQuestion";
    public final static String AnswerText= "AnswerText";

    public final static String PREFS_EASY_PAY = "prefs_easy_pay";
    public final static String PREFS_TAG_NEW_REGISTER_CUSTOMER_ID= "prefs_tag_new_register_customer_id"; //key
    public static String IsMandatory = "IsMandatory";
}
