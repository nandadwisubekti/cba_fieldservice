package invent.cba.Helper;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by kahfi on 11/16/2015.
 */
public class Database {
    private final Context mCtx;
    private DatabesHelper mDbHelper;
    protected static final String TAG = Database.class.getName();

    public Database(Context ctx) {
        this.mCtx = ctx;
        mDbHelper = new DatabesHelper(mCtx);
    }

    public Database createDatabase() throws SQLException {
        try {
            mDbHelper.createDataBase();
            Log.v(TAG, "database created");
        } catch (IOException ioe) {
            Log.v(TAG, ioe.toString() + " Unable to create database.");
            throw new Error("Unable to create database");
        }
        return this;
    }
    public Database open() throws SQLException {
        // Create and open Database
        try {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDbHelper.getReadableDatabase();
        } catch (SQLException sqle) {
            Log.v(TAG, sqle.toString());
            throw sqle;
        }
        return this;
    }
    public void close() {
        mDbHelper.close();
    }






}
