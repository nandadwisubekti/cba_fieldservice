package invent.cba.Helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.Settings;

import java.lang.reflect.Method;

/**
 * Created by kahfi on 12/13/2016.
 */

public class ViewMobiledata {
    Context context;
    public ViewMobiledata(Context context) {
        this.context = context;
    }


    public boolean viewMobailData(){
        boolean mobileDataEnabled = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean)method.invoke(cm);
        } catch (Exception e) {
            // Some problem accessible private API

        }

        return mobileDataEnabled;
    }



    public boolean isAirplaneModeOn(){

        return Settings.System.getInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;

    }
    /*private static boolean isAirplaneModeOn(Context context) {

        return Settings.System.getInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;

    }*/
}
