package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModelRegisterNewCustomer;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class NewCustomerAdapter extends RecyclerView.Adapter<NewCustomerAdapter.MyViewHolder> {

    private ArrayList<ModelRegisterNewCustomer> newCustomerArrayList;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;
    boolean isEnabled = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mrSettingItem;
        TextView tvSettingNameModule;
        ImageView ivSettingIcon;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mrSettingItem = (MaterialRippleLayout)view.findViewById(R.id.mr_setting_item);
            tvSettingNameModule = (TextView)view.findViewById(R.id.tv_setting_name_module);
            ivSettingIcon = (ImageView)view.findViewById(R.id.iv_setting_icon);

        }
    }


    public NewCustomerAdapter(Context context, ArrayList<ModelRegisterNewCustomer> newCustomerArrayList) {
        this.newCustomerArrayList = newCustomerArrayList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_setting, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModelRegisterNewCustomer registerNewCustomer = newCustomerArrayList.get(position);

        holder.tvSettingNameModule.setText(registerNewCustomer.getRegisterID());
        TextHelper.setFont(context, holder.tvSettingNameModule, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_driver_info));


        holder.mrSettingItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });

      /*  holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mItemLongClickListener != null)
                    mItemLongClickListener.onLongClick(v, position);
                return false;
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return newCustomerArrayList.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
