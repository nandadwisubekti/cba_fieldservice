package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.cba.Helper.Constant;
import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModelModuleButton;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class PersiapanAdapter extends RecyclerView.Adapter<PersiapanAdapter.MyViewHolder> {

    private ArrayList<ModelModuleButton> modelModuleButtons;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_button_item;
        TextView tv_button_name_module;
        ImageView iv_button_icon;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_button_item = (MaterialRippleLayout)view.findViewById(R.id.mr_button_item);
            tv_button_name_module = (TextView)view.findViewById(R.id.tv_button_name_module);
            iv_button_icon = (ImageView)view.findViewById(R.id.iv_button_icon);

        }
    }


    public PersiapanAdapter(Context context, ArrayList<ModelModuleButton> modelModuleButtons) {
        this.modelModuleButtons = modelModuleButtons;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_button_persiapan, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModelModuleButton modelModuleButton = modelModuleButtons.get(position);

        holder.tv_button_name_module.setText(modelModuleButton.getTitle());
        TextHelper.setFont(context, holder.tv_button_name_module, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        String module_Id = modelModuleButton.getModule_Id();
        String value = modelModuleButton.getValue();
        String isActive = modelModuleButton.getIsActive();

        if (module_Id.equals(Constant.RUTEKUNJUNGAN)){
            holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_rute_kunjungan));

            if (isActive.equals("1")){
               /* if (value.equals("1")){
                    holder.mrPersiapanItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));
                    holder.mrPersiapanItem.setEnabled(false);

                }else {
                    holder.mrPersiapanItem.setEnabled(true);
                    holder.mrPersiapanItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));

                }*/
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }

        if (module_Id.equals(Constant.NEWS)){
            holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_news));

            if (isActive.equals("1")){
               /* if (value.equals("1")){
                    holder.mrPersiapanItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));
                    holder.mrPersiapanItem.setEnabled(false);

                }else {
                    holder.mrPersiapanItem.setEnabled(true);
                    holder.mrPersiapanItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));

                }*/
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }


        if (module_Id.equals(Constant.TOOLS)){
            holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_tools));
            if (isActive.equals("1")){
               /* if (value.equals("1")){
                    holder.mr_button_item.setEnabled(true);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));

                }else {
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));
                    holder.mr_button_item.setEnabled(false);

                }*/
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }
        }


        holder.mr_button_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });

      /*  holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mItemLongClickListener != null)
                    mItemLongClickListener.onLongClick(v, position);
                return false;
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return modelModuleButtons.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
