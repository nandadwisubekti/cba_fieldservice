package invent.cba.Adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Interface.OnCheckedChangeListener;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModelAnswer;
import invent.cba.Model.ModelQuestion;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.MyViewHolder> {

    private ArrayList<ModelQuestion> modelQuestionArrayList;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;
    DatabesHelper databaseHelper;
    boolean isEnabled ;
    boolean isEnabledEdite ;



    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        RelativeLayout ryRegisterAnswerDate,
                rySpRegisterAnswer;
        TextView tvRegisterQuestionTitle,
                tvRegisterAnswerDate;
        RecyclerView recyclerViewRegisterAnswer;
        EditText etRegisterAnswer;
        Spinner spRegisterAnswer;

        public MyViewHolder(View view) {
            super(view);
            mView = view;
            ryRegisterAnswerDate = (RelativeLayout) view.findViewById(R.id.ry_register_answer_date);
            tvRegisterQuestionTitle = (TextView)view.findViewById(R.id.tv_register_question_title);
            etRegisterAnswer = (EditText) view.findViewById(R.id.et_register_answer);
            rySpRegisterAnswer = (RelativeLayout)view.findViewById(R.id.ry_sp_register_answer);
            recyclerViewRegisterAnswer = (RecyclerView)view.findViewById(R.id.recyclerView_register_answer);
            spRegisterAnswer = (Spinner)view.findViewById(R.id.sp_register_answer);
            tvRegisterAnswerDate = (TextView)view.findViewById(R.id.tv_register_answer_date);


        }
    }


    public QuestionAdapter(Context context, ArrayList<ModelQuestion> modelQuestionArrayList, boolean isEnabled,boolean isEnabledEdite) {
        this.modelQuestionArrayList = modelQuestionArrayList;
        this.context = context;
        this.isEnabled = isEnabled;
        this.isEnabledEdite = isEnabledEdite;


        databaseHelper = new DatabesHelper(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_input_register, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelQuestion modelQuestion = modelQuestionArrayList.get(position);


        if (isEnabledEdite == true){
            holder.etRegisterAnswer.setEnabled(isEnabledEdite);
        }else {
            holder.etRegisterAnswer.setEnabled(isEnabled);

        }
        //holder.recyclerViewRegisterAnswer.setEnabled(isEnabled);
        holder.ryRegisterAnswerDate.setEnabled(isEnabled);
        //holder.rySpRegisterAnswer.setEnabled(isEnabled);
        holder.spRegisterAnswer.setEnabled(isEnabled);


        holder.etRegisterAnswer.setVisibility(View.GONE);
        holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
        holder.ryRegisterAnswerDate.setVisibility(View.GONE);
        holder.rySpRegisterAnswer.setVisibility(View.GONE);


        holder.tvRegisterQuestionTitle.setText(modelQuestion.getQuestionText());

        //type ShortAnswer
        ////////////////////////////////////////////////////////////////////////////////////////////
        if (modelQuestion.getAnswerTypeID().equals(Constant.ShortAnswer)){

            if (isEnabled == false){
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
            }

            holder.etRegisterAnswer.setVisibility(View.VISIBLE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);

            holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_TEXT);
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(100);
            holder.etRegisterAnswer.setFilters(FilterArray);

            holder.etRegisterAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    modelQuestion.setValue(s.toString());
                }
            });

            //type Paragraph
            // //////////////////////////////////////////////////////////////////////////////////////////
        }else if (modelQuestion.getAnswerTypeID().equals(Constant.Paragraph)){
            if (isEnabled == false){
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
            }

            holder.etRegisterAnswer.setVisibility(View.VISIBLE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);

            holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            holder.etRegisterAnswer.setSingleLine(false);
            holder.etRegisterAnswer.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);

            holder.etRegisterAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    modelQuestion.setValue(s.toString());

                }
            });

            //type Numeric Decimal
            ////////////////////////////////////////////////////////////////////////////////////////////
        } else if (modelQuestion.getAnswerTypeID().equals(Constant.NumericDecimal)){
            if (isEnabled == false){
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
                // holder.etRegisterAnswer.setText( ValidationHelper.formatCurrency(context, modelQuestion.getValue()));
            }
            holder.etRegisterAnswer.setVisibility(View.VISIBLE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);

            holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(15);
            holder.etRegisterAnswer.setFilters(FilterArray);

            holder.etRegisterAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    holder.etRegisterAnswer.removeTextChangedListener(this);

                    if (s.length()>0) {
                        //btn_next.setVisibility(View.VISIBLE);
                        holder.etRegisterAnswer.setError(null);
                        if (s.toString().contains("0") && s.length()>1){
                            String et = s.toString();
                            if (!et.substring(0,1).matches("[1-9]")){
                                holder.etRegisterAnswer.setText("0");
                                modelQuestion.setValue("0");
                            }
                        }

                    }else {
                        holder.etRegisterAnswer.setText("0");
                        modelQuestion.setValue("0");
                    }

                    String givenstring = s.toString();
                    try {
                        Long longval;
                        if (givenstring.contains(","))
                            givenstring = givenstring.replaceAll(",", "");
                        else if (givenstring.contains("."))
                            givenstring = givenstring.replaceAll("\\.", "");

                        longval = Long.parseLong(givenstring);

                        modelQuestion.setValue(String.valueOf(longval));
                        holder.etRegisterAnswer.setText(ValidationHelper.formatCurrency(context, String.valueOf(longval)));

                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    holder.etRegisterAnswer.setSelection( holder.etRegisterAnswer.getText().length());
                    holder.etRegisterAnswer.addTextChangedListener(this);
                }
            });


            //type Numeric
            ////////////////////////////////////////////////////////////////////////////////////////////
        }else if (modelQuestion.getAnswerTypeID().equals(Constant.Numeric)){
            if (isEnabled == false){
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
            }
            holder.etRegisterAnswer.setVisibility(View.VISIBLE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);

            holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(15);
            holder.etRegisterAnswer.setFilters(FilterArray);

            holder.etRegisterAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    modelQuestion.setValue(s.toString());
                }
            });

            //type MultipleChoice == spinner
            ////////////////////////////////////////////////////////////////////////////////////////////
        }else  if (modelQuestion.getAnswerTypeID().equals(Constant.MultipleChoice)) {

            holder.rySpRegisterAnswer.setVisibility(View.VISIBLE);
            holder.etRegisterAnswer.setVisibility(View.GONE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);

            ArrayList<ModelAnswer> modelAnswerArrayList = new ArrayList<>();
            modelAnswerArrayList.clear();
            modelAnswerArrayList = databaseHelper.getArrayListModelAnswer(modelQuestion.getQuestionID(),"1",Constant.MultipleChoice);


            ArrayAdapter<String> adapterValue = new ArrayAdapter<String>(context.getApplicationContext(), R.layout.spiner_item);

            for (int i = 0; i < modelAnswerArrayList.size(); i++) {
                adapterValue.add(modelAnswerArrayList.get(i).getAnswerText());
            }

            holder.spRegisterAnswer.setAdapter(adapterValue);


            final ArrayList<ModelAnswer> finalModelAnswerArrayList = modelAnswerArrayList;
            holder.spRegisterAnswer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    int x =  holder.spRegisterAnswer.getSelectedItemPosition();

                    modelQuestion.setValue(finalModelAnswerArrayList.get(x).getAnswerText().toString());
                    modelQuestion.setAnswerID(finalModelAnswerArrayList.get(x).getAnswerID());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            if (isEnabled == false){
                int flagPinter = 0;
                for(int i = 0;i<modelAnswerArrayList.size();i++){
                    if(modelAnswerArrayList.get(i).getAnswerID().equals(modelQuestion.getAnswerID())){
                        flagPinter = i;
                    }
                }
                holder.spRegisterAnswer.setSelection(flagPinter);
            }

            //type DateType
            ////////////////////////////////////////////////////////////////////////////////////////////
        }else if (modelQuestion.getAnswerTypeID().equals(Constant.DateType)){

            if (isEnabled == false){
                holder.tvRegisterAnswerDate.setText(modelQuestion.getValue());
            }

            holder.ryRegisterAnswerDate.setVisibility(View.VISIBLE);
            holder.etRegisterAnswer.setVisibility(View.GONE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);

            holder.tvRegisterAnswerDate.setText(modelQuestion.getValue());

            holder.ryRegisterAnswerDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogDate(holder,false,position);
                }
            });


            //type CheckBox == list array
            ////////////////////////////////////////////////////////////////////////////////////////////
        }else if (modelQuestion.getAnswerTypeID().equals(Constant.CheckBox)){
            holder.recyclerViewRegisterAnswer.setVisibility(View.VISIBLE);
            holder.etRegisterAnswer.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);

            ArrayList<ModelAnswer> modelAnswerArrayList = new ArrayList<>();
            modelAnswerArrayList.clear();

            modelAnswerArrayList = databaseHelper.getArrayListModelAnswer(modelQuestion.getQuestionID(),"1",Constant.CheckBox);

            final Gson gson = new Gson();
            if (modelAnswerArrayList.size()>0){
                int numberOfColumns = 2;
                holder.recyclerViewRegisterAnswer.setLayoutManager(new GridLayoutManager(context, numberOfColumns));
                ArrayList<ModelAnswer> listMA = gson.fromJson(modelQuestion.getValue(),new TypeToken<List<ModelAnswer>>(){}.getType());

                RegisterCheckBoxAdapter registerCheckBoxAdapter = new RegisterCheckBoxAdapter(context,isEnabled,modelAnswerArrayList,listMA);

                holder.recyclerViewRegisterAnswer.setAdapter(registerCheckBoxAdapter);
                holder.recyclerViewRegisterAnswer.setNestedScrollingEnabled(false);
                final ArrayList<ModelAnswer> modelForGson = modelAnswerArrayList;
                registerCheckBoxAdapter.setOnItemCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked, int position) {
                        if (isChecked){
                            modelForGson.get(position).setStatus(true);
                        }else {
                            modelForGson.get(position).setStatus(false);
                        }

                        modelQuestion.setValue(gson.toJson(modelForGson));
                    }
                });




                registerCheckBoxAdapter.notifyDataSetChanged();

                modelQuestion.setValue(gson.toJson(modelForGson));




            }

            //type PhoneNumber
            ////////////////////////////////////////////////////////////////////////////////////////////
        }else if (modelQuestion.getAnswerTypeID().equals(Constant.PhoneNumber)) {

            if (isEnabled == false){
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
            }

            holder.etRegisterAnswer.setVisibility(View.VISIBLE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);

            holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_PHONE);
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(18);
            holder.etRegisterAnswer.setFilters(FilterArray);

            holder.etRegisterAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    modelQuestion.setValue(s.toString());
                }
            });

            //type Email
            ////////////////////////////////////////////////////////////////////////////////////////////
        }else if (modelQuestion.getAnswerTypeID().equals(Constant.Email)) {

            if (isEnabled == false){
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
            }

            holder.etRegisterAnswer.setVisibility(View.VISIBLE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.ryRegisterAnswerDate.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);

            holder.etRegisterAnswer.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT
                    |InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                    | InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS  );
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(18);
            holder.etRegisterAnswer.setFilters(FilterArray);

            holder.etRegisterAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    modelQuestion.setValue(s.toString());
                }
            });
        }else if (modelQuestion.getAnswerTypeID().equals(Constant.TIME)){

            if (isEnabled == false){
                holder.tvRegisterAnswerDate.setText(modelQuestion.getValue());
            }else {
                holder.tvRegisterAnswerDate.setText("-- : --");
            }

            holder.ryRegisterAnswerDate.setVisibility(View.VISIBLE);
            holder.etRegisterAnswer.setVisibility(View.GONE);
            holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
            holder.rySpRegisterAnswer.setVisibility(View.GONE);


            holder.ryRegisterAnswerDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogTime(holder,position);
                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return modelQuestionArrayList.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

    //DIALOG DATE
    private void dialogDate(final MyViewHolder holder, boolean isMinDate, final int position){

        final ModelQuestion modelQuestion = modelQuestionArrayList.get(position);



        int mYear, mMonth, mDay;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        android.app.DatePickerDialog datePickerDialog = new android.app.DatePickerDialog(context,
                new android.app.DatePickerDialog.OnDateSetListener(){

                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                        //modelQuestion.setAnswerTypeID(date);
                        modelQuestion.setValue(date);
                        holder.tvRegisterAnswerDate.setText(date);
                    }
                }, mYear, mMonth, mDay);
        if (isMinDate == true){
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //MIN DATE
        }
        datePickerDialog.show();

    }


    private void dialogTime(final MyViewHolder holder, final int position){
        final ModelQuestion modelQuestion = modelQuestionArrayList.get(position);

        final String[] time = new String[1];
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int  mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        time[0] = hourOfDay + ":" + minute;
                        modelQuestion.setValue( time[0]);
                        holder.tvRegisterAnswerDate.setText(time[0]);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();



    }

}
