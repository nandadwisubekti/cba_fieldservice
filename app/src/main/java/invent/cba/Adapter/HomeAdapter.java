package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModelModuleButton;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private ArrayList<ModelModuleButton> modelModuleButtons;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    DatabesHelper databesHelper;
    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_button_item;
        TextView tv_button_name_module;
        ImageView iv_button_icon;
        RelativeLayout ryList;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            tv_button_name_module = (TextView)view.findViewById(R.id.tv_button_name_module);
            mr_button_item =(MaterialRippleLayout)view.findViewById(R.id.mr_button_item);
            iv_button_icon = (ImageView)view.findViewById(R.id.iv_button_icon);
            ryList = (RelativeLayout)view.findViewById(R.id.ryList);



        }
    }


    public HomeAdapter(Context context, ArrayList<ModelModuleButton> modelModuleButtons) {
        this.modelModuleButtons = modelModuleButtons;
        this.context = context;
        databesHelper = new DatabesHelper(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_button, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModelModuleButton modelModuleButton = modelModuleButtons.get(position);

        holder.tv_button_name_module.setText(modelModuleButton.getTitle());
        TextHelper.setFont(context, holder.tv_button_name_module, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        String module_Id = modelModuleButton.getModule_Id();
        String value = modelModuleButton.getValue();
        String isActive = modelModuleButton.getIsActive();



        if ((module_Id.equals(Constant.PERSIAPAN))){

            if (isActive.equals("1")){
                if (value.equals("1")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));
                    holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_persiapan));
                   // holder.mr_button_item.setVisibility(View.VISIBLE);
                    holder.mr_button_item.setEnabled(true);
                }else {
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));
                    holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_persiapan));
                    holder.mr_button_item.setEnabled(false);
                   // holder.mr_button_item.setVisibility(View.VISIBLE);

                }

            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }


        }

        if ((module_Id.equals(Constant.MULAIPERJALANAN))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_mulai_perjalanan));
                if (value.equals("1")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }


        }
        if ((module_Id.equals(Constant.KUNJUNGAN))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_kunjungan));
                if (value.equals("1")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }


        }
        if ((module_Id.equals(Constant.ISSU))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_issue
                ));
                if (value.equals("1")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }
        if ((module_Id.equals(Constant.AKHIRIKEGIATAN))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_akhir_kegiatan));
                if (value.equals("1")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }

        //module new customer
        if ((module_Id.equals(Constant.NEWCOUNTER))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_create_customer));
                if (value.equals("1")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }

        if ((module_Id.equals(Constant.REPORT))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_report));
                if (value.equals("1")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_enable));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }








        holder.mr_button_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });

      /*  holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mItemLongClickListener != null)
                    mItemLongClickListener.onLongClick(v, position);
                return false;
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return modelModuleButtons.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
