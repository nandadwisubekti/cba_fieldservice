package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModelPOSMStock;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class KunjunganPosmAdapter extends RecyclerView.Adapter<KunjunganPosmAdapter.MyViewHolder> {

    private ArrayList<ModelPOSMStock> modelPOSMStocks;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_stock_item;
        TextView tv_qty_productName,
                tv_posm_qty_in,
                tv_posm_qty_sisa;
        EditText et_posm_qty_out;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_stock_item = (MaterialRippleLayout)view.findViewById(R.id.mr_stock_item);
            tv_qty_productName = (TextView)view.findViewById(R.id.tv_qty_productName);
            tv_posm_qty_in = (TextView)view.findViewById(R.id.tv_posm_qty_in);
            et_posm_qty_out = (EditText) view.findViewById(R.id.et_posm_qty_out);
            tv_posm_qty_sisa = (TextView)view.findViewById(R.id.tv_posm_qty_sisa);



        }
    }


    public KunjunganPosmAdapter(Context context, ArrayList<ModelPOSMStock> modelPOSMStocks1) {
        this.modelPOSMStocks = modelPOSMStocks1;
        this.context = context;

        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_posm, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelPOSMStock modelPOSMStock = modelPOSMStocks.get(position);

        holder.tv_qty_productName.setText(modelPOSMStock.getPOSMName());
        holder.tv_posm_qty_in.setText(formatter.format(Integer.valueOf(modelPOSMStock.getQuantity())));
        holder.et_posm_qty_out.setText(formatter.format(Integer.valueOf(modelPOSMStock.getQuantityOut())));
        holder.tv_posm_qty_sisa.setText(formatter.format(Integer.valueOf(modelPOSMStock.getQuantitySisa())));


        TextHelper.setFont(context, holder.tv_qty_productName, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_posm_qty_in, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.et_posm_qty_out, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_posm_qty_sisa, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());


        holder.tv_posm_qty_in.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                holder.tv_posm_qty_in.removeTextChangedListener(this);

                if (editable.length()>0) {
                    holder.tv_posm_qty_in.setError(null);
                    if (editable.toString().contains("0") && editable.length()>1){
                        String s = editable.toString();
                        if (!s.substring(0,1).matches("[1-9]")){
                            holder.tv_posm_qty_in.setText("0");
                            modelPOSMStock.setQuantity("0");
                        }
                    }
                }else {
                    holder.tv_posm_qty_in.setText("0");
                    modelPOSMStock.setQuantity("0");
                }


                String givenstring = editable.toString();
                try {
                    Long longval;
                    if (givenstring.contains(","))
                        givenstring = givenstring.replaceAll(",", "");
                    else if (givenstring.contains("."))
                        givenstring = givenstring.replaceAll("\\.", "");
                    longval = Long.parseLong(givenstring);

                    String formattedString = formatter.format(longval);
                    holder.tv_posm_qty_in.setText(formattedString);
                    modelPOSMStock.setQuantity(String.valueOf(longval));


                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.tv_posm_qty_in.addTextChangedListener(this);

            }
        });


        holder.tv_posm_qty_sisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                holder.tv_posm_qty_sisa.removeTextChangedListener(this);

                if (editable.length()>0) {
                    holder.tv_posm_qty_sisa.setError(null);
                    if (editable.toString().contains("0") && editable.length()>1){
                        String s = editable.toString();
                        if (!s.substring(0,1).matches("[1-9]")){
                            holder.tv_posm_qty_sisa.setText("0");
                            modelPOSMStock.setQuantitySisa("0");
                        }
                    }
                }else {
                    holder.tv_posm_qty_sisa.setText("0");
                    modelPOSMStock.setQuantitySisa("0");
                }


                String givenstring = editable.toString();
                try {
                    Long longval;
                    if (givenstring.contains(","))
                        givenstring = givenstring.replaceAll(",", "");
                    else if (givenstring.contains("."))
                        givenstring = givenstring.replaceAll("\\.", "");
                    longval = Long.parseLong(givenstring);

                    String formattedString = formatter.format(longval);
                    holder.tv_posm_qty_sisa.setText(formattedString);
                    modelPOSMStock.setQuantitySisa(String.valueOf(longval));


                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.tv_posm_qty_sisa.addTextChangedListener(this);

            }
        });


        holder.et_posm_qty_out.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                holder.et_posm_qty_out.removeTextChangedListener(this);

                if (editable.length()>0) {
                    holder.et_posm_qty_out.setError(null);
                    if (editable.toString().contains("0") && editable.length()>1){
                        String s = editable.toString();
                        if (!s.substring(0,1).matches("[1-9]")){
                            holder.et_posm_qty_out.setText("0");
                            modelPOSMStock.setQuantityOut("0");
                            holder.tv_posm_qty_sisa.setText(formatter.format(Integer.valueOf(modelPOSMStock.getQuantity())));

                        }
                    }
                }else {
                    holder.et_posm_qty_out.setText("0");
                    modelPOSMStock.setQuantityOut("0");
                    holder.tv_posm_qty_sisa.setText(formatter.format(Integer.valueOf(modelPOSMStock.getQuantity())));

                }


                String givenstring = editable.toString();
                Long qtyInValue = Long.valueOf(modelPOSMStock.getQuantity());

                try {
                    Long longval;
                    if (givenstring.contains(","))
                        givenstring = givenstring.replaceAll(",", "");
                    else if (givenstring.contains("."))
                        givenstring = givenstring.replaceAll("\\.", "");

                    longval = Long.parseLong(givenstring);
                    //kalkulasi qtyIn - QtyOut,Jika qtyOut lebih dari qtyIn, maka tidak bisa kalkulasib

                    Long SisaQty = (qtyInValue-longval);
                    String formatteRp = formatter.format(SisaQty);
                    holder.tv_posm_qty_sisa.setText(formatteRp);
                    modelPOSMStock.setQuantitySisa(String.valueOf(SisaQty));


                    String formattedString = formatter.format(longval);
                    holder.et_posm_qty_out.setText(formattedString);
                    modelPOSMStock.setQuantityOut(String.valueOf(longval));

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.et_posm_qty_out.setSelection(holder.et_posm_qty_out.getText().length());
                holder.et_posm_qty_out.addTextChangedListener(this);


            }
        });


        holder.mr_stock_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return modelPOSMStocks.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
