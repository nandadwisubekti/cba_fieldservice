package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModelStore;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class PersiapanRuteKunjunganAdapter extends RecyclerView.Adapter<PersiapanRuteKunjunganAdapter.MyViewHolder> {

    private ArrayList<ModelStore> modelStores;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_store_item;
        TextView tv_store_no,
                tv_store_name,
                tv_store_address,
                tv_store_time;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_store_item = (MaterialRippleLayout)view.findViewById(R.id.mr_store_item);
            tv_store_no = (TextView)view.findViewById(R.id.tv_store_no);
            tv_store_name = (TextView)view.findViewById(R.id.tv_store_name);
            tv_store_address = (TextView)view.findViewById(R.id.tv_store_address);
            tv_store_time = (TextView)view.findViewById(R.id.tv_store_time);

        }
    }


    public PersiapanRuteKunjunganAdapter(Context context, ArrayList<ModelStore> modelStores) {
        this.modelStores = modelStores;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_store, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModelStore modelStore = modelStores.get(position);

        holder.tv_store_time.setVisibility(View.VISIBLE);
        holder.tv_store_no.setText(String.valueOf(position+1));
        holder.tv_store_name.setText(modelStore.getCustomerName());
        holder.tv_store_time.setText(modelStore.getTime());

        TextHelper.setFont(context, holder.tv_store_name, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context,holder.tv_store_no, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context,holder.tv_store_address, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context,holder.tv_store_time, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());



        holder.tv_store_address.setText(modelStore.getCustomerAddress());

        holder.mr_store_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return modelStores.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
