package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModuleCustomerImage;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class FotoDisplayAdapter extends RecyclerView.Adapter<FotoDisplayAdapter.MyViewHolder> {

    private ArrayList<ModuleCustomerImage> moduleCustomerImages;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_foto_display_item;
        TextView tvImageType,
                tvImageDate,
                tv_image_klik_title;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_foto_display_item = (MaterialRippleLayout)view.findViewById(R.id.mr_foto_display_item);
            tvImageType = (TextView)view.findViewById(R.id.tvImageType);
            tvImageDate = (TextView)view.findViewById(R.id.tvImageDate);
            tv_image_klik_title = (TextView)view.findViewById(R.id.tv_image_klik_title);


        }
    }


    public FotoDisplayAdapter(Context context, ArrayList<ModuleCustomerImage> moduleCustomerImages1) {
        this.moduleCustomerImages = moduleCustomerImages1;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_image, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModuleCustomerImage moduleCustomerImage = moduleCustomerImages.get(position);

        holder.tvImageType.setText(moduleCustomerImage.getImageType());
        holder.tvImageDate.setText(moduleCustomerImage.getImageDate());

        TextHelper.setFont(context,  holder.tvImageType, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(context,  holder.tvImageDate, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(context,  holder.tv_image_klik_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());




        holder.mr_foto_display_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return moduleCustomerImages.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
