package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.Model.ModuleCompetitor;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class CompetitorValueAdapter extends RecyclerView.Adapter<CompetitorValueAdapter.MyViewHolder> {

    private ArrayList<ModuleCompetitor> moduleCompetitorArrayList;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;


        TextView tvDec,
                tvRp;
        EditText et_competitor_value;

        public MyViewHolder(View view) {
            super(view);
            mView = view;
            tvDec = (TextView)view.findViewById(R.id.tvDec);
            tvRp = (TextView)view.findViewById(R.id.tvRp);


            et_competitor_value = (EditText) view.findViewById(R.id.et_competitor_value);



        }
    }


    public CompetitorValueAdapter(Context context, ArrayList<ModuleCompetitor> moduleCompetitorArrayList) {
        this.moduleCompetitorArrayList = moduleCompetitorArrayList;
        this.context = context;

        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_value, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModuleCompetitor modelCompetitor = moduleCompetitorArrayList.get(position);

        holder.tvDec.setText(modelCompetitor.getDescription());

        holder.et_competitor_value.setText(formatter.format(Integer.valueOf(modelCompetitor.getValue())));

        TextHelper.setFont(context, holder.tvDec, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.et_competitor_value, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tvRp, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());


        holder.et_competitor_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                holder.et_competitor_value.removeTextChangedListener(this);

                if (editable.length()>0) {
                    holder.et_competitor_value.setError(null);
                    if (editable.toString().contains("0") && editable.length()>1){
                        String s = editable.toString();
                        if (!s.substring(0,1).matches("[1-9]")){
                            holder.et_competitor_value.setText("0");
                            modelCompetitor.setValue("0");
                        }
                    }
                }else {
                    holder.et_competitor_value.setText("0");
                    modelCompetitor.setValue("0");
                }


                String givenstring = editable.toString();
                try {
                    Long longval;
                    if (givenstring.contains(","))
                        givenstring = givenstring.replaceAll(",", "");
                    else if (givenstring.contains("."))
                        givenstring = givenstring.replaceAll("\\.", "");

                    longval = Long.parseLong(givenstring);
                    String formattedString = formatter.format(longval);
                    holder.et_competitor_value.setText(formattedString);

                    modelCompetitor.setValue(String.valueOf(longval));

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.et_competitor_value.setSelection(holder.et_competitor_value.getText().length());
                holder.et_competitor_value.addTextChangedListener(this);

            }
        });


    }

    @Override
    public int getItemCount() {
        return moduleCompetitorArrayList.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
