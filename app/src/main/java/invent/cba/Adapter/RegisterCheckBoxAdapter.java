package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;

import invent.cba.Interface.OnCheckedChangeListener;
import invent.cba.Model.ModelAnswer;
import invent.cba.R;


public class RegisterCheckBoxAdapter extends RecyclerView.Adapter<PromoStockOptCheckBoxViewHolder> {


    OnCheckedChangeListener mCheckedChangeListener;
    Context mContext;

    ArrayList<ModelAnswer> modelMasterAnswerArrayList;

    ArrayList<ModelAnswer> modelMasterAnswerArrayListInDB;

    boolean isEnable;

    public RegisterCheckBoxAdapter(Context mContext, boolean isEnable, ArrayList<ModelAnswer> modelMasterAnswers, ArrayList<ModelAnswer> modelMasterAnswerArrayListInDB) {
        this.modelMasterAnswerArrayList = modelMasterAnswers;
        this.modelMasterAnswerArrayListInDB = modelMasterAnswerArrayListInDB;
        this.mContext = mContext;
        this.isEnable = isEnable;

    }

    @Override
    public PromoStockOptCheckBoxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_checkboox, parent, false);
        PromoStockOptCheckBoxViewHolder viewHolder = new PromoStockOptCheckBoxViewHolder(layoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PromoStockOptCheckBoxViewHolder holder, final int position) {

        final ModelAnswer modelMasterAnswer = modelMasterAnswerArrayList.get(position);

        holder.mCbFoodOpt.setText(modelMasterAnswer.getAnswerText());
        holder.mCbFoodOpt.setOnCheckedChangeListener(null);

        holder.mCbFoodOpt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckedChangeListener != null)
                    mCheckedChangeListener.onCheckedChanged(buttonView, isChecked, position);
            }
        });


        if(modelMasterAnswerArrayListInDB!=null){
            if(modelMasterAnswerArrayListInDB.size() == modelMasterAnswerArrayList.size()){
                if (modelMasterAnswerArrayListInDB.get(position).isStatus() == true){
                    holder.mCbFoodOpt.setChecked(true);
                }else {
                    holder.mCbFoodOpt.setChecked(false);
                }
            }
        }
        holder.mCbFoodOpt.setEnabled(isEnable);







    }

    @Override
    public int getItemCount() {
        return modelMasterAnswerArrayList.size();
    }

    public void setOnItemCheckedChangeListener(final OnCheckedChangeListener mCheckedChangeListener) {
        this.mCheckedChangeListener = mCheckedChangeListener;
    }

    // Clean all elements of the recycler
    public void clear() {
        modelMasterAnswerArrayList.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    /*public void addAll(List<HashMap<String, String>> list) {
        mDataList.addAll(list);
        notifyDataSetChanged();
    }*/

}
