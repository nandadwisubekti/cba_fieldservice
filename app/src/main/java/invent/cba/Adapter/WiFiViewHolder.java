package invent.cba.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import invent.cba.R;


/**
 * Created by FEBRIANSYAH on 3/29/2016.
 */
public class WiFiViewHolder extends RecyclerView.ViewHolder{

    View mView;
    TextView mTvString, mTvStatus;
    ImageView mIvSignal;

    public WiFiViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mTvString = (TextView) itemView.findViewById(R.id.wifi_item_ssid);
        mTvStatus = (TextView) itemView.findViewById(R.id.wifi_item_status);
        mIvSignal = (ImageView) itemView.findViewById(R.id.wifi_item_signal);
    }
}
