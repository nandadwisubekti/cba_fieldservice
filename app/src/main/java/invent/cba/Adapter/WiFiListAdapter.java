package invent.cba.Adapter;

import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.R;


public class WiFiListAdapter extends RecyclerView.Adapter<WiFiViewHolder> {

    WifiManager wifiManager;
    WifiInfo wifiInfo;
    NetworkInfo netInfo;
    ScanResult scanResult;

    private List<ScanResult> mapWifiList;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    public WiFiListAdapter(WifiManager wifiManager, List<ScanResult> mapWifiList){
        this.mapWifiList = mapWifiList;

        this.wifiManager = wifiManager;
//        this.wifiInfo = wifiInfo;
    }

    public WiFiListAdapter(WifiManager wifiManager, NetworkInfo netInfo, List<ScanResult> mapWifiList){
        this.mapWifiList = mapWifiList;
        this.netInfo = netInfo;
        this.wifiManager = wifiManager;
//        this.wifiInfo = wifiInfo;
    }

    @Override
    public WiFiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_row_wifi, parent, false);
        WiFiViewHolder viewHolder = new WiFiViewHolder(layoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WiFiViewHolder holder, final int position) {

        scanResult = mapWifiList.get(position);
        wifiInfo = wifiManager.getConnectionInfo();

        holder.mTvString.setText(scanResult.SSID);

        if (scanResult.BSSID.equalsIgnoreCase(wifiInfo.getBSSID())) {
            holder.mTvStatus.setText(netInfo.getState().toString());
        } else {
            holder.mTvStatus.setText(scanResult.capabilities);
        }

        if (scanResult.level >-60 ) {
            holder.mIvSignal.setImageResource(R.mipmap.wifi_signal_strong);
        } else if (scanResult.level <= -60 && scanResult.level >- 70) {
            holder.mIvSignal.setImageResource(R.mipmap.wifi_signal_good);
        } else if (scanResult.level <= -70 && scanResult.level >- 80) {
            holder.mIvSignal.setImageResource(R.mipmap.wifi_signal_fair);
        } else {
            holder.mIvSignal.setImageResource(R.mipmap.wifi_signal_weak);
        }

//        holder.mIvImage.setImageResource(initializeIcon(menuImage[position]));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(v, position);
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mItemLongClickListener != null)
                    mItemLongClickListener.onLongClick(view, position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mapWifiList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener (final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }
}
