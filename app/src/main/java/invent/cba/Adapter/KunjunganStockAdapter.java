package invent.cba.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Model.ModuleSisaStockVisit;
import invent.cba.R;


/**
 * Created by kahfi on 2/10/2017.
 *
 * modified by Falih on 30/11/2018.
 */

public class KunjunganStockAdapter extends RecyclerView.Adapter<KunjunganStockAdapter.MyViewHolder> {

    private ArrayList<ModuleSisaStockVisit> moduleSisaStockVisits;
    Context context;
    OnItemClickListener mItemClickListener;

    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        View parent_view;
        AppCompatTextView productID, productName, qtyValue, qtyUOM, mandatorySign;
        MaterialRippleLayout cekStokBtn;
        LinearLayout stockInfo;

        public MyViewHolder(View view) {
            super(view);
            parent_view = view.findViewById(R.id.item_catat_stok_parent);
            productID = view.findViewById(R.id.item_catat_stok_productID);
            productName = view.findViewById(R.id.item_catat_stok_product_name);
            cekStokBtn = view.findViewById(R.id.item_catat_stok_check_btn);
            stockInfo = view.findViewById(R.id.item_catat_stok_qty);
            qtyValue = view.findViewById(R.id.item_catat_stok_qty_value);
            qtyUOM = view.findViewById(R.id.item_catat_stok_qty_uom);
            mandatorySign = view.findViewById(R.id.item_catat_stok_mandatory_sign);
        }
    }

    public void updateStok(int position, String qty){
        moduleSisaStockVisits.get(position).setStock(qty);
        moduleSisaStockVisits.get(position).setStatus("1");
        notifyDataSetChanged();
    }

    public ModuleSisaStockVisit getItem(int position){
        return moduleSisaStockVisits.get(position);
    }

    public KunjunganStockAdapter(Context context, ArrayList<ModuleSisaStockVisit> moduleSisaStockVisits1) {
        this.moduleSisaStockVisits = moduleSisaStockVisits1;
        this.context = context;

        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_catat_stock, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModuleSisaStockVisit moduleSisaStockVisit = moduleSisaStockVisits.get(position);

        holder.productID.setText(moduleSisaStockVisit.getProductID());
        holder.productName.setText(moduleSisaStockVisit.getProductName());

        if(moduleSisaStockVisit.getIsMandatory().equals("1")){
            holder.mandatorySign.setVisibility(View.VISIBLE);
        } else holder.mandatorySign.setVisibility(View.GONE);

        holder.qtyValue.setText(formatter.format(Integer.valueOf(moduleSisaStockVisit.getStock() == null? "0" : moduleSisaStockVisit.getStock())));
        holder.qtyUOM.setText(moduleSisaStockVisit.getUOM());

        TextHelper.setFont(context, holder.productID, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(context, holder.productName, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

        holder.parent_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });

        holder.cekStokBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return moduleSisaStockVisits.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}
