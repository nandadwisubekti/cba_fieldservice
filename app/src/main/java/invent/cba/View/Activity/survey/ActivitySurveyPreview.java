package invent.cba.View.Activity.survey;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import invent.cba.Adapter.AdapterListFoto;
import invent.cba.Adapter.PreviewQuestionAdapter;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.Etc;
import invent.cba.Helper.MRuntimePermission;
import invent.cba.Helper.MarshMallowPermission;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Model.ModelQuestion;
import invent.cba.Model.ResultSurvey;
import invent.cba.Model.ResultSurveyDetail;
import invent.cba.R;
import invent.cba.Service.ConnectionClient;
import invent.cba.Service.PostSurveyCallback;
import invent.cba.Service.PostSurveyDetailCallback;
import invent.cba.Service.Upload;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INVENT on 8/29/2018.
 */

public class ActivitySurveyPreview extends AppCompatActivity {


    ImageView image_right;
    RecyclerView recyclerView;
    PreviewQuestionAdapter questionAdapter;
    RecyclerView recyclerView_foto_display;
    AdapterListFoto adapterListFoto;
    AppCompatButton surveyAddImageBtn;

    DatabesHelper databaseHelper;
    RelativeLayout ry_toolbar_back,ry_toolbar_right;
    TextView tv_toolbar_name_module, tv_btn_title;
    MarshMallowPermission marshMallowPermission;
    MaterialRippleLayout btn_next;
    RelativeLayout ry_customer_detail;
    TextView tv_name_toko, tv_customerID;

    private MRuntimePermission mPermissionChecker;

    ResultSurvey resultSurvey;
    ArrayList<ResultSurveyDetail> resultSurveyDetails;
    ArrayList<ModelQuestion> moduleQuestionList = new ArrayList<>();
    ArrayList<AdapterListFoto.Model> imageList;
    String startTime, endTime;
    String Title ="Summary";
    private String questionSetID;
    private int totalQuestionSet;
    private int totalCompletedQuestionSet;

    public ActivitySurveyPreview() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_add);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            startTime = bundle.getString("startTime");
            questionSetID = bundle.getString("questionSetID");
            resultSurvey = bundle.getParcelable("resultSurvey");
            resultSurveyDetails = bundle.getParcelableArrayList("resultSurveyDetail");
            moduleQuestionList = bundle.getParcelableArrayList("questionList");
            imageList = bundle.getParcelableArrayList("resultSurveyImage");
            totalQuestionSet = bundle.getInt("totalQuestionSet");
            totalCompletedQuestionSet = bundle.getInt("totalCompletedQuestionSet");
        } else {
            /**
             * kalau gak ada data yg dilempar ya buat apa
             */
            finish();
        }

        databaseHelper = new DatabesHelper(ActivitySurveyPreview.this);
        marshMallowPermission = new MarshMallowPermission(ActivitySurveyPreview.this);
        mPermissionChecker = new MRuntimePermission(ActivitySurveyPreview.this);
        initView(getWindow().getDecorView());
        initEvent();

        /**
         * integrasiin question sama answer-nya
         */
        for(int i=0; i<moduleQuestionList.size(); i++){
            moduleQuestionList.get(i).setValue(resultSurveyDetails.get(i).getValue());
        }

        questionAdapter = new PreviewQuestionAdapter(moduleQuestionList);
        setupRecyclerView(recyclerView, questionAdapter);
        questionAdapter.notifyDataSetChanged();
    }



    private void initView(View view) {

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        ry_toolbar_right = (RelativeLayout) view.findViewById(R.id.ry_toolbar_right);
        recyclerView_foto_display = view.findViewById(R.id.survey_image_recyclerView);
        ry_toolbar_right.setVisibility(View.GONE);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_btn_title = view.findViewById(R.id.tv_btn_title);
        surveyAddImageBtn = view.findViewById(R.id.survey_add_image);
        surveyAddImageBtn.setVisibility(View.GONE);
        tv_btn_title.setText("SIMPAN");
        btn_next = (MaterialRippleLayout)view.findViewById(R.id.btn_next);
        btn_next.setVisibility(View.VISIBLE);

        tv_toolbar_name_module.setText(Title);
        image_right = (ImageView) view.findViewById(R.id.image_right);
        image_right.setVisibility(View.GONE);

        setupRecyclerViewPhoto(recyclerView_foto_display, imageList);
    }

    private void setupRecyclerView(RecyclerView recyclerView, PreviewQuestionAdapter questionAdapter) {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivitySurveyPreview.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }

    private void setupRecyclerViewPhoto(@NonNull final RecyclerView recyclerView, final ArrayList<AdapterListFoto.Model> models) {

        adapterListFoto = new AdapterListFoto(this,models);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(this);
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(MyLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterListFoto);
        adapterListFoto.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

    }


    private void initEvent(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurveyPreview.this);
                builder.setTitle(R.string.dialog_confirmation);
                builder.setMessage("yakin akan menyimpan survey?")
                        .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveSurveyAnswer();
                            }
                        })
                        .setNegativeButton("Batal", null).show();
            }
        });



    }


    /*private void getData(final boolean isEnable, final CallbackFinish callbackFinish) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                moduleQuestionList.clear();
                moduleQuestionList = databaseHelper.getArrayListResultSurveyDetailWithoutCategory(ResultSurveyID);

                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (moduleQuestionList.size() > 0) {
                    questionAdapter = new QuestionAdapter(ActivitySurveyPreview.this, moduleQuestionList, false, false);
                    setupRecyclerView(recyclerView, questionAdapter);
                    questionAdapter.notifyDataSetChanged();
                }


                if (callbackFinish != null) {
                    callbackFinish.finish();
                }

            }
        }.execute();

    }*/

    private void saveSurveyAnswer(){
        resultSurvey.setStartDate(startTime);
        endTime = ValidationHelper.dateFormat().substring(11, 19);
        resultSurvey.setEndDate(endTime);

        DatabesHelper helper = new DatabesHelper(this);
        resultSurvey.setBranchID(helper.getBranchID());

        boolean insertResultSurvey = databaseHelper.insertResultSurvey(resultSurvey);
        boolean insertResultSurveyDetail = databaseHelper.insertResultSurveyDetail(resultSurveyDetails);
        if(insertResultSurvey && insertResultSurveyDetail ){
            /**
             * simpen imagenya
             */
            boolean insertResultSurveyImage = databaseHelper.insertResultSurveyImage(imageList, resultSurvey.getVisitID(), resultSurvey.getCustomerID(), questionSetID);
            if(insertResultSurveyImage){
                setResult(RESULT_OK);
                finish();
            }
            Toast.makeText(ActivitySurveyPreview.this, "Survey saved. Now uploading...", Toast.LENGTH_SHORT).show();
            postSurveyAnswerToServer(resultSurvey, resultSurveyDetails);
        }
    }

    private void postSurveyAnswerToServer(ResultSurvey survey, final List<ResultSurveyDetail> answers) {
        final ConnectionClient connection = new ConnectionClient(this);
        // tambahin persentasi survey completed.
        Double ratio = ((double)totalCompletedQuestionSet + 1d) / totalQuestionSet;
        survey.CompletedPercentage =  ratio * 100d;
        survey.CompletedPercentage = Math.floor(survey.CompletedPercentage * 10) / 10;
        //String stringifyPercentage = String.valueOf(survey.CompletedPercentage);
        connection.getApiService().postInsertResultSurvey(survey.toParams()).enqueue(new PostSurveyCallback(this, survey) {
            @Override
            public void onSuccess(Response<JsonObject> response) {
                postAnswerToServer(answers, connection);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ActivitySurveyPreview.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void postAnswerToServer(List<ResultSurveyDetail> answers, final ConnectionClient withClient) {
        JsonArray array = new JsonArray();
        for (ResultSurveyDetail answer : answers) {
            array.add(answer.toParams());
        }
        withClient.getApiService().postInsertResultSurveyDetail(array).enqueue(new PostSurveyDetailCallback(this, answers) {
            @Override
            public void onSuccess(Response<JsonObject> response) {
                // upload the image if any
                if(adapterListFoto.list.size() > 0){
                    Toast.makeText(ActivitySurveyPreview.this, "uploading image(s)..", Toast.LENGTH_SHORT).show();
                    postSurveyImage(withClient, questionSetID);
                } else {
                    Toast.makeText(ActivitySurveyPreview.this, "Survey uploaded.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ActivitySurveyPreview.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void postSurveyImage(ConnectionClient withClient, final String questionSetID) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        for(AdapterListFoto.Model photoFilePath : adapterListFoto.list){
            File photoImg = new File(photoFilePath.getFilePath());
            builder.addFormDataPart("files", photoImg.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), photoImg));
        }
        MultipartBody requestBody = builder.build();

        withClient.getApiService().postUploadSurveyImages(requestBody).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject respBody = response.body();
                if(respBody.get("Result").getAsString().equals("SUCCESS")){
                    //tandain isUpload = 1 di sqlite
                    int updated = databaseHelper.updateSurveyImageIsUpload(questionSetID);
                    if(updated!=-1){
                        //todo: upload sqlite customerImage
                        UploadCustomerImageTask u = new UploadCustomerImageTask(ActivitySurveyPreview.this, new Etc.GenericCallback() {
                            @Override
                            public void run(Object... o) {
                                Boolean success = (Boolean) o[0];
                                if(success) Toast.makeText(ActivitySurveyPreview.this,  "image(s) uploaded successfully", Toast.LENGTH_LONG).show();
                                else Toast.makeText(ActivitySurveyPreview.this,  "image(s) failed to upload", Toast.LENGTH_LONG).show();
                                Toast.makeText(ActivitySurveyPreview.this, "Survey uploaded.", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                        u.execute();
                    } else {

                    }
                } else {
                    Toast.makeText(ActivitySurveyPreview.this, "Survey Image Upload Failed (5xx)", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ActivitySurveyPreview.this, "Survey Image Upload Failed (4xx)", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent (MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view
                .getClass().getName().startsWith("android.webkit."))
        {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
            {
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    class UploadCustomerImageTask extends AsyncTask<Void, Void, Boolean> {

        Context context;
        Etc.GenericCallback cb;

        public UploadCustomerImageTask(Context context, Etc.GenericCallback... cb){
            this.context = context;
            if(cb.length>0) this.cb = cb[0];
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Upload u = new Upload(context, false);
            Boolean uploaded = u.openTabelCustomerImage();
            if(uploaded){
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(cb!=null) cb.run(result);
        }
    }
}
