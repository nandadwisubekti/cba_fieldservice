package invent.cba.View.Fragmentt.Setting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import invent.cba.Adapter.SettingAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.MarshMallowPermission;
import invent.cba.Helper.TextHelper;
import invent.cba.Interface.ConnectionInterface;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Model.ModelModuleButton;
import invent.cba.R;
import invent.cba.Service.ConnectionClient;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class SettingFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewSettingList;
    SettingAdapter settingAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;
    MarshMallowPermission marshMallowPermission;
    ConnectionClient connectionClient;
    ConnectionInterface apiService;

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting, container, false);

        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        styleLayout();
        assignListener();
        getModuleList();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerViewSettingList = (RecyclerView)view.findViewById(R.id.recyclerView_setting_list);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_setting);
    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


    }


    private void getModuleList(){
        ArrayList<ModelModuleButton> arrayListModuleButton = new ArrayList<>();
        arrayListModuleButton.clear();

         databesHelper = new DatabesHelper(getActivity());
        String IsRole = databesHelper.getMobileConfig(Constant.Role);

        arrayListModuleButton = databesHelper.getArralyModuleButton(Constant.SETTING,IsRole,"1");
        if (arrayListModuleButton.size()>0){
            setupRecyclerView(recyclerViewSettingList,arrayListModuleButton);
        }else {
            arrayListModuleButton = databesHelper.getArralyModuleButton(Constant.SETTING,"0","1");
            if (arrayListModuleButton.size()>0){
                setupRecyclerView(recyclerViewSettingList,arrayListModuleButton);
            }
        }
      //  databesHelper.close();
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelModuleButton> arrayListModuleButton) {

        settingAdapter = new SettingAdapter(getActivity(),arrayListModuleButton);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(settingAdapter);
        settingAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);


        settingAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String Module_Id = arrayListModuleButton.get(position).getModule_Id();

                //PROFILE
                if ((Module_Id.equals(Constant.PROFILE))){

                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("mapheader", mapArrayList.get(position));
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    SettingProfileFragment settingProfileFragment = new SettingProfileFragment();
                    settingProfileFragment.setArguments(bundle);
                    transaction.replace(R.id.fragment, settingProfileFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();


                }


                //KATASANDI
                if (Module_Id.equals(Constant.KATASANDI)){

                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("mapheader", mapArrayList.get(position));
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    SettingpChangPasswordFragment settingpChangPasswordFragment = new SettingpChangPasswordFragment();
                    settingpChangPasswordFragment.setArguments(bundle);
                    transaction.replace(R.id.fragment, settingpChangPasswordFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }

                //Wifi
                if (Module_Id.equals(Constant.WIFI)){

                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("mapheader", mapArrayList.get(position));
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    SettingIPFragment settingIPFragment1 = new SettingIPFragment();
                    settingIPFragment1.setArguments(bundle);
                    transaction.replace(R.id.fragment, settingIPFragment1);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

                //BIRGHTNESS
                if (Module_Id.equals(Constant.BIRGHTNESS)){

                   /* if (Settings.System.canWrite(getActivity()))
                    {
                        // perform your actions
                        if (!marshMallowPermission.checkPermissionWriteSetting()) {
                            marshMallowPermission.requestPermissionWriteSetting();
                        }
                    }
                    else
                    {
                        DialogHelper.dialogScreenBrightness(getActivity());
                    }
*/

                }

                //ID_DEVICE
                if (Module_Id.equals(Constant.ID_DEVICE)){

                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("mapheader", mapArrayList.get(position));
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    SettingDeviceFragment settingDeviceFragment1 = new SettingDeviceFragment();
                    settingDeviceFragment1.setArguments(bundle);
                    transaction.replace(R.id.fragment, settingDeviceFragment1);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

                //PRINTER
                if (Module_Id.equals(Constant.PRINTER)){
                    DialogHelper.dialogSettingPrinter(getActivity());
                }

                //PRINTER
                if (Module_Id.equals(Constant.TRACKING)){
                    DialogHelper.dialogSettingIntervalTracking(getActivity());
                }

                //UPDATE_APP
                if (Module_Id.equals(Constant.UPDATE_APP)){

                    connectionClient.downloadCekVersionApk();
                }

                //UPDATE_APP
                if (Module_Id.equals(Constant.UPDATE_CONFIGURATION)){
                    connectionClient.downloadConfiguration();

                    //connectionClient.uploadFileSqlite(getActivity());
                }


            }
        });



    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
