package invent.cba.View.Fragmentt.Setting;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import invent.cba.Helper.CheckConnection;
import invent.cba.Service.ConnectionClient;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.IPAddressKeyListener;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Interface.ConnectionInterface;
import invent.cba.R;
import invent.cba.Service.TestConnection;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class SettingIPFragment extends Fragment  {


    TextView tv_setting_wifi_titel_ippublic,
            tv_setting_wifi_titel_iplokal,
            tv_setting_wifi_titel_ipalternatif;
    EditText et_setting_wifi_ip_public,
            et_setting_wifi_port_public,
            et_setting_wifi_ip_lokal,
            et_setting_wifi_port_lokal,
            et_setting_wifi_ip_alternatif,
            et_setting_wifi_port_alternatif;

    MaterialRippleLayout mr_btn_setting_wifi_save,
            mr_btn_setting_wifi_test,
            mr_btn_setting_wifi;

    HeaderHelper headerHelper;
    DatabesHelper databesHelper;
    ConnectionInterface apiService;
    ConnectionClient connectionClient;
    CheckConnection checkConnection;

    private String errorMsg = "";

    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;
    public SettingIPFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        checkConnection = new CheckConnection(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting_ip_port, container, false);

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

       /* Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.modul_title_setting_ip);
        // ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/

        tv_setting_wifi_titel_ippublic = (TextView)view.findViewById(R.id.tv_setting_wifi_titel_ippublic);
        tv_setting_wifi_titel_iplokal = (TextView)view.findViewById(R.id.tv_setting_wifi_titel_iplokal);
        tv_setting_wifi_titel_ipalternatif = (TextView)view.findViewById(R.id.tv_setting_wifi_titel_ipalternatif) ;

        et_setting_wifi_ip_public = (EditText)view.findViewById(R.id.et_setting_wifi_ip_public);
        et_setting_wifi_port_public = (EditText) view.findViewById(R.id.et_setting_wifi_port_public);
        et_setting_wifi_ip_lokal = (EditText)view.findViewById(R.id.et_setting_wifi_ip_lokal);
        et_setting_wifi_port_lokal = (EditText) view.findViewById(R.id.et_setting_wifi_port_lokal);
        et_setting_wifi_ip_alternatif = (EditText) view.findViewById(R.id.et_setting_wifi_ip_alternatif);
        et_setting_wifi_port_alternatif = (EditText) view.findViewById(R.id.et_setting_wifi_port_alternatif);

        mr_btn_setting_wifi_save = (MaterialRippleLayout) view.findViewById(R.id.mr_btn_setting_wifi_save);
        mr_btn_setting_wifi_test = (MaterialRippleLayout) view.findViewById(R.id.mr_btn_setting_wifi_test);
        mr_btn_setting_wifi = (MaterialRippleLayout) view.findViewById(R.id.mr_btn_setting_wifi);


        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_setting_ip);



    }

    private void styleLayout() {

        TextHelper.styleEditText(getActivity(), et_setting_wifi_ip_public, "IP PUBLIC *", true);
        TextHelper.styleEditText(getActivity(), et_setting_wifi_port_public,"PORT PUBLIC *", true);
        TextHelper.styleEditText(getActivity(), et_setting_wifi_ip_lokal,  "IP LOKAL *", true);
        TextHelper.styleEditText(getActivity(), et_setting_wifi_port_lokal, "PORT LOKAL *", true);
        TextHelper.styleEditText(getActivity(), et_setting_wifi_ip_alternatif, "IP ALTERNATIF *", true);
        TextHelper.styleEditText(getActivity(), et_setting_wifi_port_alternatif, "PORT ALTERNATIF *", true);


        TextHelper.setFont(getActivity(), tv_setting_wifi_titel_ippublic, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_wifi_titel_iplokal, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_wifi_titel_ipalternatif, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

        TextHelper.setFont(getActivity(), mr_btn_setting_wifi_save, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), mr_btn_setting_wifi_test, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), mr_btn_setting_wifi, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }


    private void assignListener(){

        String ipPublc = databesHelper.getMobileConfig(Constant.IPCONFIG);
        String portPublic = databesHelper.getMobileConfig(Constant.PORT);
        String ipLokal = databesHelper.getMobileConfig(Constant.IPSTATION);
        String portLokal = databesHelper.getMobileConfig(Constant.PORTSTATION);
        String ipAlternatif = databesHelper.getMobileConfig(Constant.IPALTERNATIF);
        String portAlternatif = databesHelper.getMobileConfig(Constant.PORTALTERNATIF);
        databesHelper.close();

        et_setting_wifi_ip_public.setText(ipPublc);
        et_setting_wifi_port_public.setText(portPublic);
        et_setting_wifi_ip_lokal.setText(ipLokal);
        et_setting_wifi_port_lokal.setText(portLokal);
        et_setting_wifi_ip_alternatif.setText(ipAlternatif);
        et_setting_wifi_port_alternatif.setText(portAlternatif);

        InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(et_setting_wifi_ip_public, InputMethodManager.SHOW_FORCED);
        mgr.showSoftInput(et_setting_wifi_ip_lokal, InputMethodManager.SHOW_FORCED);
        mgr.showSoftInput(et_setting_wifi_ip_alternatif, InputMethodManager.SHOW_FORCED);

        //etIpConfig.setInputType(InputType.TYPE_CLASS_TEXT); // hidden
        et_setting_wifi_ip_public.setKeyListener(IPAddressKeyListener.getInstance());
        et_setting_wifi_ip_lokal.setKeyListener(IPAddressKeyListener.getInstance());
        et_setting_wifi_ip_alternatif.setKeyListener(IPAddressKeyListener.getInstance());

        mr_btn_setting_wifi_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()){
                    saveData();
                }else {
                    DialogHelper.dialogMessage(getActivity(),"Warning",errorMsg,Constant.DIALOG_WRONG);
                }
            }
        });

        mr_btn_setting_wifi_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkConnection.Connect() == false) {
                    DialogHelper.dialogMessage(getActivity(),"Error","Tidak ada koneksi intrenet,cek kembali IP dan Port!",Constant.DIALOG_ERROR);

                } else {
                    new TestConnection(getActivity());

                }
            }
        });
        mr_btn_setting_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Bundle bundle = new Bundle();
                //bundle.putSerializable("mapheader", mapArrayList.get(position));
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                SettingWifiFragment settingWifiFragment = new SettingWifiFragment();
                settingWifiFragment.setArguments(bundle);
                transaction.replace(R.id.fragment, settingWifiFragment);
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

    }


    private Boolean validateForm() {
        if (et_setting_wifi_ip_public.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi IP public";
            return false;
        }
        if (!ValidationHelper.validateIP(et_setting_wifi_ip_public.getText().toString())){
            errorMsg = "IP public tidak valid! cek kembali IP public yang anda masukkan.";
            return false;
        }

        if (et_setting_wifi_port_public.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi nama PORT public";
            return false;
        }
        if (et_setting_wifi_port_public.getText().length() < 4 || et_setting_wifi_port_public.getText().length() > 5) {
            errorMsg = "Format PORT public yang Anda masukan salah";
            return false;
        }
        if (et_setting_wifi_ip_lokal.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi IP lokal";
            return false;

        }

        if (!ValidationHelper.validateIP(et_setting_wifi_ip_lokal.getText().toString())){
            errorMsg = "IP lokal tidak valid! cek kembali IP lokal yang anda masukkan.";
            return false;
        }
        if (et_setting_wifi_port_lokal.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi PORL lokal";
            return false;
        }
        if (et_setting_wifi_port_lokal.getText().length() < 4 || et_setting_wifi_port_lokal.getText().length() > 5) {
            errorMsg = "Format PORT lokal yang Anda masukan salah";
            return false;
        }
        if (et_setting_wifi_ip_alternatif.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi IP alternatif";
            return false;
        }
        if (!ValidationHelper.validateIP(et_setting_wifi_ip_alternatif.getText().toString())){
            errorMsg = "IP alternatif tidak valid! cek kembali IP alternatif yang anda masukkan.";
            return false;
        }
        if (et_setting_wifi_port_alternatif.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi PORT alternatif";
            return false;
        }
        if (et_setting_wifi_port_alternatif.getText().length() < 4 || et_setting_wifi_port_alternatif.getText().length() > 5) {
            errorMsg = "Format PORT alternatif yang Anda masukan salah";
            return false;
        }
        return true;
    }

    private void saveData(){

        //IP CLOUD
        databesHelper.updateMobileConfig(Constant.IPCONFIG, et_setting_wifi_ip_public.getText().toString());
        databesHelper.updateMobileConfig(Constant.PORT, et_setting_wifi_port_public.getText().toString());

        //IP LOKAL
        databesHelper.updateMobileConfig(Constant.IPSTATION,et_setting_wifi_ip_lokal.getText().toString());
        databesHelper.updateMobileConfig(Constant.PORTSTATION,et_setting_wifi_port_lokal.getText().toString());

        //IP ALTERNATIF
        databesHelper.updateMobileConfig(Constant.IPALTERNATIF,et_setting_wifi_ip_alternatif.getText().toString());
        databesHelper.updateMobileConfig(Constant.PORTALTERNATIF,et_setting_wifi_port_alternatif.getText().toString());

       // Toast.makeText(getActivity(), "Save berhasil", Toast.LENGTH_SHORT).show();
        DialogHelper.dialogMessage(getActivity(),"Success","Save Success",Constant.DIALOG_SUCCESS);

    }


    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        checkConnection = new CheckConnection(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


}
