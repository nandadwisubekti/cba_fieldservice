package invent.cba.View.Fragmentt.CompetitorActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.cba.Adapter.CompetitorActivityCheckBoxAdapter;
import invent.cba.Adapter.CompetitorAdapter;
import invent.cba.Adapter.CompetitorValueAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.MRuntimePermission;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.data.SharedPreferenceHelper;
import invent.cba.Interface.OnCheckedChangeListener;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Model.ModuleCompetitor;
import invent.cba.Model.ModulePromo;
import invent.cba.R;
import invent.cba.View.Activity.FotoDisplay.KunjunganCompetitirActivityFoto;
import invent.cba.View.Activity.Kunjungan.KunjunganStartActivity;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class CompetitorActivityFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerView_competitor;
    CompetitorAdapter competitorAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_title_data,
            tv_stock_save,
            tv_competitor_item_title,
            tv_competitor_normal_price_title,
            tv_competitor_promo_price_title;
    MaterialRippleLayout mr_item_competitor_save;
    ProgressBar progressBar;
    String customerId,visitID,EmployeeID;
    Spinner spCompetitor;
    ArrayList<ModuleCompetitor> moduleCompetitorArrayList = new ArrayList<>();

    Dialog dialogCompetitorActivity;

    String gelobalCompetitorID;


    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    String imgBase64;
    private MRuntimePermission mPermissionChecker;

    RecyclerView recyclerView_competitor_image;

    ArrayList<ModuleCompetitor> moduleCompetitorArrayListImage = new ArrayList<>();


    public CompetitorActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());


        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        customerId = sharedPreferenceHelper.getCustomerID();
        visitID = sharedPreferenceHelper.getVisitID();

        EmployeeID = databesHelper.getEmployeeID();
        mPermissionChecker = new MRuntimePermission(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kunjungan_competitor_activity, container, false);



        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getNameCompetitor();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerView_competitor = (RecyclerView)view.findViewById(R.id.recyclerView_competitor);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_stock_save = (TextView)view.findViewById(R.id.tv_stock_save);
        tv_competitor_item_title = (TextView)view.findViewById(R.id.tv_competitor_item_title);
        tv_competitor_normal_price_title = (TextView)view.findViewById(R.id.tv_competitor_normal_price_title);
        tv_competitor_promo_price_title = (TextView)view.findViewById(R.id.tv_competitor_promo_price_title);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);

        mr_item_competitor_save = (MaterialRippleLayout)view.findViewById(R.id.mr_item_competitor_save);
        spCompetitor = (Spinner)view.findViewById(R.id.spCompetitor);
        tv_toolbar_name_module.setText(R.string.module_titiel_competitor_activity);




    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_title_data, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_stock_save, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_competitor_item_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_competitor_normal_price_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_competitor_promo_price_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


    }

    private void assignListener(){



        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent  intent = new Intent(getActivity(),KunjunganStartActivity.class);
                startActivity(intent);
                getActivity().finish();
                //modelGlobal.getCompetitorID = "";
                SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
                sharedPreferenceHelper.setCompetitorID("");
            }
        });


        //Provinsi
        spCompetitor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int x = spCompetitor.getSelectedItemPosition();

                gelobalCompetitorID = moduleCompetitorArrayList.get(x).getCompetitorID();
                //get server city
                getItemCompetitor(String.valueOf(moduleCompetitorArrayList.get(x).getCompetitorID()));
                //Provinsi = arrayMasterProvince.get(x).getProvinceID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





    }

    private void getNameCompetitor(){

        moduleCompetitorArrayList.clear();
        moduleCompetitorArrayList = databesHelper.getArrayListCompetitorActivity();

        if (moduleCompetitorArrayList.size()>0){

            ArrayAdapter<String> adapterCityByProvince = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.spiner_item);
            for (int i = 0; i < moduleCompetitorArrayList.size(); i++) {
                adapterCityByProvince.add(moduleCompetitorArrayList.get(i).getCompetitorName());

            }
            spCompetitor.setAdapter(adapterCityByProvince);
        }


        int flagCompetitor=0;
        //String competitor =modelGlobal.getCompetitorID;
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        String competitor =sharedPreferenceHelper.getCompetitorID();
        if (!competitor.equals("")){
            for(int i = 0;i<moduleCompetitorArrayList.size();i++){
                if(moduleCompetitorArrayList.get(i).getCompetitorID().equals(competitor)){
                    flagCompetitor = i;
                }
            }

            spCompetitor.setSelection(flagCompetitor);
            //getItemCompetitor(competitor);
        }

    }

    private void getItemCompetitor(String CompetitorID){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModuleCompetitor> moduleCompetitorArrayList = new ArrayList<>();
        moduleCompetitorArrayList.clear();

        databesHelper = new DatabesHelper(getActivity());

        moduleCompetitorArrayList = databesHelper.getArrayListCompetitorActivityVisitInput(visitID,EmployeeID,customerId,CompetitorID);

        if (moduleCompetitorArrayList.size()>0){
            setupRecyclerView(recyclerView_competitor,moduleCompetitorArrayList);
            progressBar.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);
        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModuleCompetitor> moduleCompetitorArrayList) {

        competitorAdapter = new CompetitorAdapter(getActivity(),moduleCompetitorArrayList);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(competitorAdapter);
        competitorAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        competitorAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String VisitID = moduleCompetitorArrayList.get(position).getVisitID();
                String EmployeeID = moduleCompetitorArrayList.get(position).getEmployeeID();
                String CompetitorID = moduleCompetitorArrayList.get(position).getCompetitorID();
                String CompetitorProductID = moduleCompetitorArrayList.get(position).getCompetitorProductID();
                String Notes = moduleCompetitorArrayList.get(position).getNotes();
                String competitorProductName = moduleCompetitorArrayList.get(position).getCompetitorProductName();
                String CustomerID = moduleCompetitorArrayList.get(position).getCustomerID();

                dialogCompetitorActivityInput(VisitID,
                        EmployeeID,
                        CustomerID,
                        CompetitorID,
                        CompetitorProductID,
                        Notes,
                        competitorProductName);

            }
        });



        mr_item_competitor_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(),KunjunganStartActivity.class);
                startActivity(intent);
                //modelGlobal.getCompetitorID = "";
                SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
                sharedPreferenceHelper.setCompetitorID("");
                getActivity().finish();
            }
        });

    }

    private void dialogCompetitorActivityInput(final String VisitID,
                                               final String EmployeeID,
                                               final String CustomerID,
                                               final String CompetitorID,
                                               final String CompetitorProductID,
                                               String Notes,
                                               final String competitorProductName){

        dialogCompetitorActivity = new Dialog(getActivity());
        dialogCompetitorActivity.setContentView(R.layout.dialog_competitor_activity);
        dialogCompetitorActivity.setCancelable(true);
        dialogCompetitorActivity.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final TextView tv_competitor_item = (TextView)dialogCompetitorActivity.findViewById(R.id.tv_competitor_item);
        final EditText et_competitor_reason_input_notes = (EditText)dialogCompetitorActivity.findViewById(R.id.et_competitor_reason_input_notes);
        final RecyclerView recyclerView_competitor_activity = (RecyclerView) dialogCompetitorActivity.findViewById(R.id.recyclerView_competitor_activity);
        final MaterialRippleLayout mr_item_competitor_input_save = (MaterialRippleLayout)dialogCompetitorActivity.findViewById(R.id.mr_item_competitor_input_save);
        final AppCompatCheckBox checkboxCompetitorActivity = (AppCompatCheckBox)dialogCompetitorActivity.findViewById(R.id.checkboxCompetitorActivity);
        final RecyclerView recyclerView_competitor_value = (RecyclerView)dialogCompetitorActivity.findViewById(R.id.recyclerView_competitor_value);
        final TextView tv_stock_next = (TextView)dialogCompetitorActivity.findViewById(R.id.tv_stock_next);




        TextHelper.setFont(getActivity(), tv_competitor_item, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_stock_next, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), et_competitor_reason_input_notes, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


        tv_competitor_item.setText(competitorProductName);
        et_competitor_reason_input_notes.setText(Notes);

        /////LIST PROMO///////////////////////////////////////////////////////////////////////////
        databesHelper = new DatabesHelper(getActivity());
        ArrayList<ModulePromo> moduleArrayListModulePromo = new ArrayList<>();
        moduleArrayListModulePromo.clear();
        moduleArrayListModulePromo = databesHelper.getArrayListPromoCompetitorActivity(
                VisitID,
                CompetitorID,
                CustomerID,
                CompetitorProductID,
                Constant.COMPETITOR,
                false);
        final ArrayList<ModulePromo> finalModuleArrayListModulePromo = moduleArrayListModulePromo;
        if (finalModuleArrayListModulePromo.size()>0){
            setupRecyclerViewCheckBooxPromo(recyclerView_competitor_activity,
                    finalModuleArrayListModulePromo,
                    false);
        }

        checkboxCompetitorActivity.setText("Check All ");
        checkboxCompetitorActivity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true){
                    checkboxCompetitorActivity.setText("Uncheck All");
                }else {
                    checkboxCompetitorActivity.setText("Check All");

                }

                setupRecyclerViewCheckBooxPromo(recyclerView_competitor_activity,
                        finalModuleArrayListModulePromo,
                        b);

            }
        });

        //////LIST VALUE///////////////////////////////////////////////////////////////////////////
        ArrayList<ModuleCompetitor> moduleArrayListCompetitorValue = new ArrayList<>();
        moduleArrayListCompetitorValue.clear();
        moduleArrayListCompetitorValue = databesHelper.getArrayListPromoCompetitorActivityValue(VisitID,CompetitorID,CustomerID,CompetitorProductID);
        if (moduleArrayListCompetitorValue.size()>0){
            setupRecyclerViewCompettiorVlue(recyclerView_competitor_value,moduleArrayListCompetitorValue);
        }

        final ArrayList<ModuleCompetitor> finalModuleArrayListCompetitorValue = moduleArrayListCompetitorValue;
        mr_item_competitor_input_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPhoto = false;

                for (int i = 0; i< finalModuleArrayListCompetitorValue.size(); i++){
                    if ( finalModuleArrayListCompetitorValue.get(i).getCompetitorActivityTypeID().equals("PromoPrice")){
                        if (!finalModuleArrayListCompetitorValue.get(i).getValue().equals("0")){
                            isPhoto = true;
                        }
                    }
                        //Update tabel Competitor Activity Visit
                    databesHelper.updateCompetitorActivityVisit(
                            finalModuleArrayListCompetitorValue.get(i).getVisitID(),
                            finalModuleArrayListCompetitorValue.get(i).getEmployeeID(),
                            finalModuleArrayListCompetitorValue.get(i).getCompetitorID(),
                            finalModuleArrayListCompetitorValue.get(i).getCustomerID(),
                            finalModuleArrayListCompetitorValue.get(i).getCompetitorProductID(),
                            finalModuleArrayListCompetitorValue.get(i).getValue(),
                            et_competitor_reason_input_notes.getText().toString(),
                            "1",
                            finalModuleArrayListCompetitorValue.get(i).getCompetitorActivityTypeID());

                }

                //insert CompetitorActivityVisit
                for (int promo =0; promo<finalModuleArrayListModulePromo.size(); promo++){
                    databesHelper.insertCompetitorActivityPromoVisit(
                            VisitID,
                            CompetitorID,
                            CustomerID,
                            finalModuleArrayListModulePromo.get(promo).getPromoID(),
                            finalModuleArrayListModulePromo.get(promo).getIsStatus(),
                            CompetitorProductID);

                    //update
                    databesHelper.updateCompetitorProductID(
                            VisitID,
                            CompetitorID,
                            CustomerID,
                            finalModuleArrayListModulePromo.get(promo).getPromoID(),
                            CompetitorProductID,
                            finalModuleArrayListModulePromo.get(promo).getIsStatus());

                }


                if (isPhoto == true){
                    Toast.makeText(getActivity(), "Wajib photo!", Toast.LENGTH_LONG).show();
                    SharedPreferenceHelper.saveCompetitorProductID(getActivity(),CompetitorProductID);
                    //modelGlobal.getCompetitorID = CompetitorID;
                    SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
                    sharedPreferenceHelper.setCompetitorID(CompetitorID);
                    Intent intent = new Intent(getActivity(), KunjunganCompetitirActivityFoto.class);
                    startActivity(intent);
                    getActivity().finish();
                }else {
                    getItemCompetitor(gelobalCompetitorID);
                }

                dialogCompetitorActivity.dismiss();
            }
        });



        dialogCompetitorActivity.show();
    }


    private void setupRecyclerViewCheckBooxPromo(RecyclerView recyclerView_competitor_activity,
                                                 final ArrayList<ModulePromo> moduleArrayListModulePromo,
                                                 boolean isStatus){
        final DecimalFormat formatter;
        DecimalFormatSymbols symbols;
        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);


        int numberOfColumns = 2;
        recyclerView_competitor_activity.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        CompetitorActivityCheckBoxAdapter  competitorActivityCheckBoxAdapter = new CompetitorActivityCheckBoxAdapter(getActivity(),moduleArrayListModulePromo,isStatus);
        recyclerView_competitor_activity.setAdapter(competitorActivityCheckBoxAdapter);
        recyclerView_competitor_activity.setNestedScrollingEnabled(false);
        competitorActivityCheckBoxAdapter.setOnItemCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked, int position) {
                if (isChecked){
                    moduleArrayListModulePromo.get(position).setIsStatus("true");
                }else {
                    moduleArrayListModulePromo.get(position).setIsStatus("false");
                }


            }
        });
        competitorActivityCheckBoxAdapter.notifyDataSetChanged();



    }


    private void setupRecyclerViewCompettiorVlue(RecyclerView recyclerView_competitor_activity,
                                                 final ArrayList<ModuleCompetitor> moduleArrayListModulePromo){

        int numberOfColumns = 1;
        recyclerView_competitor_activity.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        CompetitorValueAdapter competitorValueAdapter = new CompetitorValueAdapter(getActivity(),moduleArrayListModulePromo);
        recyclerView_competitor_activity.setAdapter(competitorValueAdapter);
        recyclerView_competitor_activity.setNestedScrollingEnabled(false);
        competitorValueAdapter.notifyDataSetChanged();

    }






    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }



}
