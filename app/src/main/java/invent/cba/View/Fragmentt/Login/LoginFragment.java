package invent.cba.View.Fragmentt.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
//import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;


import invent.cba.Helper.ValidationHelper;
import invent.cba.Service.ConnectionClient;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Interface.ConnectionInterface;
import invent.cba.R;
import invent.cba.View.Activity.Notif.NotifActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class LoginFragment extends Fragment{


    TextView tvLoginTitelLogin,
            tvLoginSetting;
    EditText etLoginUsername,
            etLoginPassword;
    MaterialRippleLayout mrBtnLoginSubmit,
            mrLoginSetting;
    ImageView iv_login_icon;

    HeaderHelper headerHelper;
    DatabesHelper databesHelper;

    ConnectionClient connectionClient;
    ConnectionInterface apiService;

    Intent intent;

    Context context;
    private static final String TAG = "LoginFragment";




    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());
        styleLayout();
        assignListener();
        // todo: untuk debug, di-comment
        downloadConfiguration();
        return v;
    }




    private void initializeLayout(View view) {

        tvLoginTitelLogin = (TextView)view.findViewById(R.id.tv_login_titel_login);
        tvLoginSetting = (TextView)view.findViewById(R.id.tv_login_setting) ;

        etLoginUsername = (EditText)view.findViewById(R.id.et_login_username);
        etLoginPassword = (EditText) view.findViewById(R.id.et_login_password);

        mrBtnLoginSubmit = (MaterialRippleLayout) view.findViewById(R.id.mr_btn_login_submit);
        mrLoginSetting = (MaterialRippleLayout) view.findViewById(R.id.mr_login_setting);

        iv_login_icon = (ImageView)view.findViewById(R.id.img_login_logo);

        ValidationHelper.getIcLauncher(getActivity(),iv_login_icon);

        etLoginUsername.requestFocus();
        etLoginUsername.setInputType(InputType.TYPE_NULL); // hidden
        etLoginUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.showSoftInput(etLoginUsername, InputMethodManager.SHOW_FORCED);

               //etLoginUsername.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        });

        etLoginPassword.requestFocus();
        etLoginPassword.setInputType(InputType.TYPE_NULL); // hidden
        etLoginPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.showSoftInput(etLoginPassword, InputMethodManager.SHOW_FORCED);
               // etLoginPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                etLoginPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);


            }
        });

    }

    private void styleLayout() {

        TextHelper.styleEditText(getActivity(), etLoginUsername, "USER NAME *", true);
        TextHelper.styleEditText(getActivity(), etLoginPassword, "PASSWORD *", true);

        TextHelper.setFont(getActivity(), tvLoginTitelLogin, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tvLoginSetting, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        TextHelper.setFont(getActivity(), mrBtnLoginSubmit, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), mrLoginSetting, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

        etLoginUsername.setEnabled(false);

        mrLoginSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                //bundle.putSerializable("mapheader", mapArrayList.get(position));
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                LoginSettingFragment fragment = new LoginSettingFragment();
                //CompetitorActivityFragment fragment = new  CompetitorActivityFragment();
                fragment.setArguments(bundle);
                transaction.replace(R.id.fragment, fragment);
                transaction.addToBackStack(null);
                transaction.commit();


              /*  intent = new Intent(getActivity(), RegisterCustomerActivity.class);
                startActivity(intent);
*/
            }
        });

        mrBtnLoginSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // todo: untuk debug, uncomment 2 baris code di bawah, kemudian comment baris berikutnya
                /*   intent = new Intent(getActivity(),HomeActivity.class);
                    startActivity(intent);*/
               validateLogin();

            }
        });


    }

    //DOWNLOAD KONFIGURASI
    private void downloadConfiguration() {

        //ANDROID DEVICEID
        String deViceID = ValidationHelper.getMacAddrss();
        //TOKEN FAIRBASE
       /* String refreshedToken = "";
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (refreshedToken != null) {
            databesHelper.updateMobileFlag(Constant.IS_TOKEN, refreshedToken);
        }
*/
        JsonObject jsonObjectParam;
        jsonObjectParam = new JsonObject();
        jsonObjectParam.addProperty(Constant.deviceID, deViceID);
        jsonObjectParam.addProperty(Constant.token, "");

        final JsonObject[] object = new JsonObject[1];
        final boolean[] isRegister = {false};
        final String[] versionServer = new String[1];
        final String[] versionMobail = new String[1];
        final String[] Title = {null};
        final String[] StatusCode = {null};
        final String[] StatusMessage = {null};
        final JsonObject[] objectValue = new JsonObject[1];

        final Call<JsonObject> mDownloadUserConfig = apiService.postUserConfig(jsonObjectParam);
        mDownloadUserConfig.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    object[0] = response.body();
                    Title[0] = object[0].get(Constant.Title).getAsString();
                    StatusCode[0] = object[0].get(Constant.StatusCode).getAsString();
                    StatusMessage[0] = object[0].get(Constant.StatusMessage).getAsString();
                    if (StatusCode[0].contains("01")) {
                        objectValue[0] = object[0].getAsJsonObject(Constant.Value);
                        if (!objectValue[0].get("EmployeeID").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.DRIVERID, objectValue[0].get("EmployeeID").getAsString());}
                        if (!objectValue[0].get("BranchID").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.BRANCHID, objectValue[0].get("BranchID").getAsString());}
                        if (!objectValue[0].get("BranchName").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.BRANCHNAME, objectValue[0].get("BranchName").getAsString());}
                        if (!objectValue[0].get("IpPublic").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.IPCONFIG, objectValue[0].get("IpPublic").getAsString());}
                        if (!objectValue[0].get("PortPublic").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PORT, objectValue[0].get("PortPublic").getAsString());}
                        if (!objectValue[0].get("IpLocal").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.IPSTATION, objectValue[0].get("IpLocal").getAsString());}
                        if (!objectValue[0].get("PortLocal").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PORTSTATION, objectValue[0].get("PortLocal").getAsString());}
                        if (!objectValue[0].get("IpAlternative").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.IPALTERNATIF, objectValue[0].get("IpAlternative").getAsString());}
                        if (!objectValue[0].get("PortAlternative").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PORTALTERNATIF, objectValue[0].get("PortAlternative").getAsString());}
                        if (!objectValue[0].get("EmployeeName").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.DRIVER_NAME, objectValue[0].get("EmployeeName").getAsString());}
                        if (!objectValue[0].get("Result").isJsonNull()) {
                            databesHelper.updateMobileFlag(Constant.DOWNLOAD_USER_CONFIG, objectValue[0].get("Result").getAsString());}
                        if (!objectValue[0].get("Password").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PASSWORD, objectValue[0].get("Password").getAsString());}
                        if (!objectValue[0].get("PasswordReset").isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.PASSWORDRESET, objectValue[0].get("PasswordReset").getAsString());}
                        if (!objectValue[0].get(Constant.Role).isJsonNull()) {
                            databesHelper.updateMobileConfig(Constant.Role, objectValue[0].get(Constant.Role).getAsString());}

                        String isStart = databesHelper.getVisitIsStartCheckVersion();
                        String isUpload = databesHelper.getMobilFlag(Constant.DOWNLOAD);
                        if (isStart != null) {
                            if (isStart.equals("1") && isUpload.equals("1")) {
                                isRegister[0] = false;
                            } else {
                                ////////////VERSION DARI SERVER/////////////////////
                                if (!objectValue[0].get("Version").isJsonNull()) {
                                    versionServer[0] = objectValue[0].get("Version").getAsString();
                                    String arrTempServer[] = versionServer[0].split("_");
                                    String version_server = arrTempServer[2].toString().replace(".apk", "");
                                    int mVersionServer = Integer.parseInt(version_server);

                                   /* ////////////VERSION DARI HP/////////////////////
                                    versionMobail[0] = databesHelper.getVersionApp();
                                    if (versionMobail[0] != null) {
                                        String arrTempMobail[] = versionMobail[0].split("_");
                                        String version_app = arrTempMobail[2].toString().replace(".apk", "");
                                        int mVersionApp = Integer.parseInt(version_app);

                                        ///////CHECK KONDISI VERSION////////////////////////
                                        if (mVersionServer > mVersionApp) {
                                            isRegister[0] = true;
                                        }


                                    }*/

                                    ////////////VERSION DARI HP/////////////////////
                                    versionMobail[0] = ValidationHelper.versionApp(getActivity());
                                    if (versionMobail[0] != null) {
                                        int mVersionApp = Integer.parseInt(versionMobail[0]);

                                        ///////CHECK KONDISI VERSION////////////////////////
                                        if (mVersionServer > mVersionApp) {
                                            isRegister[0] = true;
                                        }
                                    }

                                }
                            }
                        } else {
                            ////////////VERSION DARI SERVER/////////////////////
                            if (!objectValue[0].get("Version").isJsonNull()) {
                                versionServer[0] = objectValue[0].get("Version").getAsString();
                                String arrTempServer[] = versionServer[0].split("_");
                                String version_server = arrTempServer[2].toString().replace(".apk", "").trim();
                                int mVersionServer = Integer.parseInt(version_server);

                             /*   ////////////VERSION DARI HP/////////////////////
                                versionMobail[0] = databesHelper.getVersionApp();
                                if (versionMobail[0] != null) {
                                    String arrTempMobail[] = versionMobail[0].split("_");
                                    String version_app = arrTempMobail[2].toString().replace(".apk", "").trim();
                                    int mVersionApp = Integer.parseInt(version_app);

                                    ///////CHECK KONDISI VERSION////////////////////////
                                    if (mVersionServer > mVersionApp) {
                                        isRegister[0] = true;
                                    }
                                }
*/

                                ////////////VERSION DARI HP/////////////////////
                                if (context!=null){
                                    versionMobail[0] = ValidationHelper.versionApp(context);
                                    if (versionMobail[0] != null) {
                                        int mVersionApp = Integer.parseInt(versionMobail[0]);

                                        ///////CHECK KONDISI VERSION////////////////////////
                                        if (mVersionServer > mVersionApp) {
                                            isRegister[0] = true;
                                        }
                                    }
                                }


                            }

                        }

                        if (context!=null)
                        Toast.makeText(context,  StatusMessage[0], Toast.LENGTH_LONG).show();
                        if (etLoginUsername != null) {
                            if (!objectValue[0].get("EmployeeName").isJsonNull()) {
                                etLoginUsername.setText(objectValue[0].get("EmployeeName").getAsString());
                            }

                        }

                        uploadToken(objectValue[0].get("EmployeeID").getAsString(), objectValue[0].get("BranchID").getAsString());

                        if (isRegister[0] == true) {
                            //fungsi untuk download apk
                            Intent intent = new Intent(getActivity(), NotifActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(Constant.DOWNLOAD_APP_VERSION_NEW, Constant.DOWNLOAD_APP_VERSION_NEW);
                            if (versionServer[0] != null) {
                                intent.putExtra(Constant.NAME_VERSION, versionServer[0] = objectValue[0].get(Constant.Version).getAsString());
                            } else {
                                intent.putExtra(Constant.NAME_VERSION, "");
                            }
                            getActivity().startActivity(intent);

                            if (context!=null)
                            Toast.makeText(context, "fungsi download apk", Toast.LENGTH_LONG).show();

                        }

                    } else {
                        isRegister[0] = false;
                        if (context!=null)
                        Toast.makeText(context, StatusMessage[0], Toast.LENGTH_LONG).show();
                    }

                }else {
                    if (response!=null && context!=null){
                        Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();

                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (context!=null && t!=null)
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    //PARAMETER TOKEN
    private void uploadToken(final String EmployeeID, final String Branchid) {
        /**
         * belum dipake. mattin dulu sementara
         */
        /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String AndroidKey = instanceIdResult.getToken();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("EmployeeID", EmployeeID);
                jsonObject.addProperty("BranchID", Branchid);
                jsonObject.addProperty("AndroidKey", AndroidKey);
                uploadTokenServerFirbase(jsonObject);
            }
        });*/
    }

    //KRIM TOKEN FIREBASE KE SERVER KE TIKA LOGIN
    private void uploadTokenServerFirbase(final JsonObject jsonObject) {

        final JsonObject[] object = new JsonObject[1];
        final String[] result = {null};
        final Call<JsonObject> mUpload = apiService.postFirebaseToServer(jsonObject);

        mUpload.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    object[0] = response.body();
                    result[0] = object[0].get("Result").getAsString();
                    if (result[0].contains("1")) {
                        if (context!=null)
                        Toast.makeText(context, "Token berhasil", Toast.LENGTH_LONG).show();
                    } else {
                        if (context!=null)
                        Toast.makeText(context, "Token gagal", Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t!=null && context!=null){
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

                }


            }
        });
    }

    //VALIDATE LOGIN
    private void validateLogin(){


        if (etLoginPassword.getText().length()>0 && etLoginUsername.getText().length()>0){

            String passwordSetting = databesHelper.getMobileConfig(Constant.PASSWORDSETTING);
            if (passwordSetting !=null){
                if (etLoginPassword.getText().toString().toLowerCase().equals(passwordSetting.toLowerCase())){

                    getMenuMobileList();

                }else {
                    DialogHelper.dialogMessage(getActivity(),"Informasi","Kesalahan Password",Constant.DIALOG_WRONG);
                }

            }else {
                DialogHelper.dialogMessage(getActivity(),"Informasi","Databes belum terbuat",Constant.DIALOG_WRONG);

            }

        }else {
            DialogHelper.dialogMessage(getActivity(),"Informasi","Mohon isi Password",Constant.DIALOG_WRONG);

        }
        databesHelper.close();

    }

    private void getMenuMobileList(){

        //ANDROID DEVICE ID
        String driverId = databesHelper.getMobileConfig(Constant.DRIVERID);
        String branchId = databesHelper.getMobileConfig(Constant.BRANCHID);
        String Role = databesHelper.getMobileConfig(Constant.Role);

        if ( driverId !=null && branchId!=null && Role!=null){

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("EmployeeID",driverId);
            jsonObject.addProperty("BranchID", branchId);
            jsonObject.addProperty("Role", Role);

            connectionClient.getButtonList(jsonObject);

        }else {
            DialogHelper.dialogMessage(getActivity(),"Informasi","parameter ada yang null",Constant.DIALOG_WRONG);

        }



    }

    @Override
    public void onResume(){
        super.onResume();

    }


    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }




}
