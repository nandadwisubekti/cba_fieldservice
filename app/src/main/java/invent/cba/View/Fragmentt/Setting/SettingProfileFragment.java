package invent.cba.View.Fragmentt.Setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class SettingProfileFragment extends Fragment  {


    TextView tv_setting_profile_titel_info_kendaraan,
            tv_setting_profile_titel_info_pengemudi,
            tv_setting_setting_info_cabang;
    EditText et_setting_profile_id_kendaraan,
            et_setting_profile_no_kendaraan,
            et_setting_profile_id_pengemudi,
            et_setting_profile_id_helper,
            et_setting_profile_id_cabang,
            et_setting_profile_nama_cabang;

    MaterialRippleLayout mr_btn_setting_profile_save;

    HeaderHelper headerHelper;
    DatabesHelper databesHelper;

    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;

    private String errorMsg = "";
    public SettingProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting_profile, container, false);

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

        /*Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.modul_title_profile);
        // ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/

        tv_setting_profile_titel_info_kendaraan = (TextView)view.findViewById(R.id.tv_setting_profile_titel_info_kendaraan);
        tv_setting_profile_titel_info_pengemudi = (TextView)view.findViewById(R.id.tv_setting_profile_titel_info_pengemudi);
        tv_setting_setting_info_cabang = (TextView)view.findViewById(R.id.tv_setting_setting_info_cabang) ;

        et_setting_profile_id_kendaraan = (EditText)view.findViewById(R.id.et_setting_profile_id_kendaraan);
        et_setting_profile_no_kendaraan = (EditText) view.findViewById(R.id.et_setting_profile_no_kendaraan);
        et_setting_profile_id_pengemudi = (EditText)view.findViewById(R.id.et_setting_profile_id_pengemudi);
        et_setting_profile_id_helper = (EditText) view.findViewById(R.id.et_setting_profile_id_helper);
        et_setting_profile_id_cabang = (EditText) view.findViewById(R.id.et_setting_profile_id_cabang);
        et_setting_profile_nama_cabang = (EditText) view.findViewById(R.id.et_setting_profile_nama_cabang);

        mr_btn_setting_profile_save = (MaterialRippleLayout) view.findViewById(R.id.mr_btn_setting_profile_save);

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_profile);


    }

    private void styleLayout() {

        TextHelper.styleEditText(getActivity(), et_setting_profile_id_kendaraan, "ID KENDARAAN *", true);
        TextHelper.styleEditText(getActivity(), et_setting_profile_no_kendaraan,"NOMER KENDARAAN *", true);
        TextHelper.styleEditText(getActivity(), et_setting_profile_id_pengemudi,  "ID PENGEMEUDI *", true);
        TextHelper.styleEditText(getActivity(), et_setting_profile_id_helper, "ID HELPER *", true);
        TextHelper.styleEditText(getActivity(), et_setting_profile_id_cabang, "ID CABANG *", true);
        TextHelper.styleEditText(getActivity(), et_setting_profile_nama_cabang, "NAMA CABANG *", true);


        TextHelper.setFont(getActivity(), tv_setting_profile_titel_info_kendaraan, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_profile_titel_info_pengemudi, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_setting_info_cabang, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

        TextHelper.setFont(getActivity(), mr_btn_setting_profile_save, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


    }


    private void assignListener(){

        String idKendaraan = databesHelper.getMobileConfig(Constant.VEHICLEID);
        String nomerKendaraan = databesHelper.getMobileConfig(Constant.VEHICLENUMBER);
        String idPengemudi = databesHelper.getMobileConfig(Constant.DRIVERID);
        String idHelper = databesHelper.getMobileConfig(Constant.CODRIVER);
        String idCabang = databesHelper.getMobileConfig(Constant.BRANCHID);
        String namaCabang = databesHelper.getMobileConfig(Constant.BRANCHNAME);
        databesHelper.close();

        et_setting_profile_id_kendaraan.setText(idKendaraan);
        et_setting_profile_no_kendaraan.setText(nomerKendaraan);
        et_setting_profile_id_pengemudi.setText(idPengemudi);
        et_setting_profile_id_helper.setText(idHelper);
        et_setting_profile_id_cabang.setText(idCabang);
        et_setting_profile_nama_cabang.setText(namaCabang);


        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mr_btn_setting_profile_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()){
                    saveData();
                }else {
                    DialogHelper.dialogMessage(getActivity(),"Warning",errorMsg,Constant.DIALOG_WRONG);
                }
            }
        });



    }


    private Boolean validateForm() {
        if (et_setting_profile_id_kendaraan.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi id kendaraan";
            return false;
        }


        if (et_setting_profile_no_kendaraan.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi nomer kendaraan";
            return false;
        }

        if (et_setting_profile_id_pengemudi.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi id pengemudi";
            return false;

        }

        if (et_setting_profile_id_helper.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi id helper";
            return false;
        }

        if (et_setting_profile_id_cabang.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi id cabang";
            return false;
        }

        if (et_setting_profile_nama_cabang.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi nama cabang";
            return false;
        }






        return true;
    }

    private void saveData(){

        //INFO KENDARAAN
        databesHelper.updateMobileConfig(Constant.VEHICLEID, et_setting_profile_id_kendaraan.getText().toString());
        databesHelper.updateMobileConfig(Constant.VEHICLENUMBER, et_setting_profile_no_kendaraan.getText().toString());

        //INFO PENGEMUDI
        databesHelper.updateMobileConfig(Constant.DRIVERID,et_setting_profile_id_pengemudi.getText().toString());
        databesHelper.updateMobileConfig(Constant.CODRIVER,et_setting_profile_id_helper.getText().toString());

        //INFO CABANG
        databesHelper.updateMobileConfig(Constant.BRANCHID,et_setting_profile_id_cabang.getText().toString());
        databesHelper.updateMobileConfig(Constant.BRANCHNAME,et_setting_profile_nama_cabang.getText().toString());

       // Toast.makeText(getActivity(), "Save berhasil", Toast.LENGTH_SHORT).show();
        DialogHelper.dialogMessage(getActivity(),"Success","Save Success",Constant.DIALOG_SUCCESS);

    }


    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


}
