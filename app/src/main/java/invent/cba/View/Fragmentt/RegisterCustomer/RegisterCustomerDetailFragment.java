package invent.cba.View.Fragmentt.RegisterCustomer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import invent.cba.Adapter.QuestionAdapter;
import invent.cba.Adapter.SettingAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.MarshMallowPermission;
import invent.cba.Helper.TextHelper;
import invent.cba.Interface.ConnectionInterface;
import invent.cba.Model.ModelQuestion;
import invent.cba.R;
import invent.cba.Service.ConnectionClient;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class RegisterCustomerDetailFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerview_customer_data_pribadi,
            recyclerview_data_lokasi,
            recyclerview_data_keuangan,
            recyclerview_data_EmergencyContact,
            recyclerview_data_KerapianKaryawan;
    SettingAdapter settingAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back,
            ry_toolbar_right;
    TextView tv_toolbar_name_module;
    MarshMallowPermission marshMallowPermission;
    ConnectionClient connectionClient;
    ConnectionInterface apiService;
    ArrayList<ModelQuestion> modelCustomerDetailPribadiArrayList = new ArrayList<ModelQuestion>();
    ArrayList<ModelQuestion> modelCustomerDetailLokasiArrayList = new ArrayList<ModelQuestion>();
    ArrayList<ModelQuestion> modelCustomerDetailKeuanganArrayList = new ArrayList<ModelQuestion>();
    ArrayList<ModelQuestion> modelCustomerDetailEmergencyContactArrayList = new ArrayList<ModelQuestion>();
    ArrayList<ModelQuestion> modelCustomerDetailKerapianKaryawanArrayList = new ArrayList<ModelQuestion>();

    String RegisterID;
    public RegisterCustomerDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(Constant.RegisterID)){
            RegisterID = getArguments().getString(Constant.RegisterID);
        }

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_customer_detail, container, false);

        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        styleLayout();
        assignListener();

        getCustomerDataPribadi();
        getCustomerDataLokasi();
        getCustomerDataKeuangan();
        getCustomerDataEmergencyContact();
        getDataKerapianKaryawan();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerview_customer_data_pribadi = (RecyclerView)view.findViewById(R.id.recyclerview_customer_data_pribadi);
        recyclerview_data_lokasi = (RecyclerView)view.findViewById(R.id.recyclerview_data_lokasi);
        recyclerview_data_keuangan = (RecyclerView)view.findViewById(R.id.recyclerview_data_keuangan);
        recyclerview_data_EmergencyContact = (RecyclerView)view.findViewById(R.id.recyclerview_data_EmergencyContact);
        recyclerview_data_KerapianKaryawan = (RecyclerView)view.findViewById(R.id.recyclerview_data_KerapianKaryawan);


        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_list_register_customer_detail);
    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

    }

    //DATA PRIBADI
    private void getCustomerDataPribadi(){

        modelCustomerDetailPribadiArrayList.clear();
        modelCustomerDetailPribadiArrayList = databesHelper.getArrayLitRegisterCutomerDetail(RegisterID,Constant.PAGE_1);
        if (modelCustomerDetailPribadiArrayList.size()>0){
            setupRecyclerViewDataPribadi(recyclerview_customer_data_pribadi,modelCustomerDetailPribadiArrayList);
        }
    }

    private void setupRecyclerViewDataPribadi(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList) {

        QuestionAdapter questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,false,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }



    //DATA LOKASI
    private void getCustomerDataLokasi(){

        modelCustomerDetailLokasiArrayList.clear();
        modelCustomerDetailLokasiArrayList = databesHelper.getArrayLitRegisterCutomerDetail(RegisterID,Constant.PAGE_2);
        if (modelCustomerDetailLokasiArrayList.size()>0){
            setupRecyclerViewDataLokasi(recyclerview_data_lokasi,modelCustomerDetailLokasiArrayList);
        }
    }

    private void setupRecyclerViewDataLokasi(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList) {

        QuestionAdapter questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,false,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }



    //DATA KEUANGAN
    private void getCustomerDataKeuangan(){

        modelCustomerDetailKeuanganArrayList.clear();
        modelCustomerDetailKeuanganArrayList = databesHelper.getArrayLitRegisterCutomerDetail(RegisterID,Constant.PAGE_3);
        if (modelCustomerDetailKeuanganArrayList.size()>0){
            setupRecyclerViewDataKeuangan(recyclerview_data_keuangan,modelCustomerDetailKeuanganArrayList);
        }
    }

    private void setupRecyclerViewDataKeuangan(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList) {

        QuestionAdapter questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,false,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }



    //DATA EMERGENCY CONTACT
    private void getCustomerDataEmergencyContact(){

        modelCustomerDetailEmergencyContactArrayList.clear();
        modelCustomerDetailEmergencyContactArrayList = databesHelper.getArrayLitRegisterCutomerDetail(RegisterID,Constant.PAGE_4);
        if (modelCustomerDetailEmergencyContactArrayList.size()>0){
            setupRecyclerViewDataEmergencyContact(recyclerview_data_EmergencyContact,modelCustomerDetailEmergencyContactArrayList);
        }
    }

    private void setupRecyclerViewDataEmergencyContact(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList) {

        QuestionAdapter questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,false,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }

    private void getDataKerapianKaryawan(){

        modelCustomerDetailKerapianKaryawanArrayList.clear();
        modelCustomerDetailKerapianKaryawanArrayList = databesHelper.getArrayLitRegisterCutomerDetail(RegisterID,Constant.PAGE_5);
        if (modelCustomerDetailKerapianKaryawanArrayList.size()>0){
            setupRecyclerViewDataKerapianKaryawan(recyclerview_data_KerapianKaryawan,modelCustomerDetailKerapianKaryawanArrayList);
        }
    }

    private void setupRecyclerViewDataKerapianKaryawan(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList) {

        QuestionAdapter questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,false,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }







    @Override
    public void onResume(){
        super.onResume();

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

}
