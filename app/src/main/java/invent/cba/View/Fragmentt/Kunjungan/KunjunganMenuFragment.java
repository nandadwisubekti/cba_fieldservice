package invent.cba.View.Fragmentt.Kunjungan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import invent.cba.Adapter.AdapterMenu;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Interface.CallbackOnClickItemListener;
import invent.cba.Model.ModelModuleButton;
import invent.cba.R;
import invent.cba.View.Activity.Kunjungan.KunjunganActivity;
import invent.cba.View.Activity.Kunjungan.NonVisitCustomerListActivity;

/**
 * Created by kahfizain on 25/08/2017.
 */

public class KunjunganMenuFragment extends Fragment {

    HeaderHelper headerHelper;
    RecyclerView recyclerViewPersiapan;

    AdapterMenu adapterMenu;
    DatabesHelper databaseGetHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_title_data;
    ProgressBar progressBar;

    public KunjunganMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databaseGetHelper = new DatabesHelper(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_module_list, container, false);

        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());
        styleLayout();
        assignListener();




        return v;
    }

    private void initializeLayout(View view) {

        recyclerViewPersiapan = (RecyclerView)view.findViewById(R.id.recyclerView);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_toolbar_name_module.setText("Kunjungan");

    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (getActivity()).finish();
            }
        });
    }
    ArrayList<ModelModuleButton> arrayListModuleButton;
    private void getModuleList(){
        progressBar.setVisibility(View.VISIBLE);
        arrayListModuleButton = new ArrayList<>();


        try {
            databaseGetHelper.openDataBase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String IsRole = databaseGetHelper.getMobileConfig(Constant.Role);
        arrayListModuleButton = databaseGetHelper.getArralyModuleButton(Constant.KUNJUNGAN_TYPE,IsRole,"1");
        databaseGetHelper.close();

        List<AdapterMenu.Model> list = new ArrayList<>();

        for(ModelModuleButton module : arrayListModuleButton){
            AdapterMenu.Model m = new AdapterMenu.Model();
            m.setId(module.getModule_Id());
            m.setTvA(module.getTitle());
            m.setType(AdapterMenu.Model.Type.TYPE_B);


            switch (module.getModule_Id()){
                case "KUNJUNGAN_DALAM_RUTE":
                    m.setResIdIconEnable(R.drawable.ic_kunjungan_dalam_rute);
                    m.setResIdIconDisable(R.drawable.ic_kunjungan_dalam_rute);
                    break;

                case "KUNJUNGAN_LUAR_RUTE":
                    m.setResIdIconEnable(R.drawable.ic_kunjungan_luar_rute);
                    m.setResIdIconDisable(R.drawable.ic_kunjungan_luar_rute);
                    break;
            }


            if(module.getIsActive().contentEquals("1"))
                m.setVisibility(true);
            else
                m.setVisibility(false);


            if(module.getValue().contentEquals("1"))
                m.setEnability(true);
            else
                m.setEnability(false);

            list.add(m);
        }
        if (arrayListModuleButton.size()>0){
            setupRecyclerView(recyclerViewPersiapan,list);
            tv_title_data.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.GONE);
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final List<AdapterMenu.Model> list) {

        adapterMenu = new AdapterMenu(getActivity(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterMenu);
        adapterMenu.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        adapterMenu.setOnClickItemListener(new CallbackOnClickItemListener() {
            @Override
            public void onClick(int position) {

                Intent intent;
                switch (list.get(position).getId()){
                    case "KUNJUNGAN_DALAM_RUTE":
                        intent = new Intent(getActivity(), KunjunganActivity.class);
                        startActivity(intent);
                        break;
                    case "KUNJUNGAN_LUAR_RUTE":
                        intent = new Intent(getActivity(), NonVisitCustomerListActivity.class);
                        startActivity(intent);
                        break;

                }
            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        getModuleList();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();

    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


}
