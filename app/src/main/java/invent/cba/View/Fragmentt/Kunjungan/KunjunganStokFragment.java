package invent.cba.View.Fragmentt.Kunjungan;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.cba.Adapter.KunjunganStockAdapter;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.Etc;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Helper.data.SharedPreferenceHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Model.ModelStore;
import invent.cba.Model.ModuleSisaStockVisit;
import invent.cba.R;
import invent.cba.Service.Upload;
import invent.cba.dialogs.EditProductStockDialog;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class KunjunganStokFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerView_stock;
    KunjunganStockAdapter kunjunganStockAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_title_data,
            tv_stock_save,
            tv_name_toko;
    MaterialRippleLayout mr_item_stock_save;
    ProgressBar progressBar;
    String customerId,visitID;

    Dialog dialog,dialogReasonPromo;

    public KunjunganStokFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        customerId = sharedPreferenceHelper.getCustomerID();
        visitID= sharedPreferenceHelper.getVisitID();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kunjungan_stock, container, false);



        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getStoreList();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerView_stock = (RecyclerView)view.findViewById(R.id.recyclerView_catat_stock);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);

        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_name_toko = (TextView)view.findViewById(R.id.tv_name_toko);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_stock_save = (TextView)view.findViewById(R.id.tv_stock_save);

        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        mr_item_stock_save = (MaterialRippleLayout)view.findViewById(R.id.mr_item_stock_save);

        tv_name_toko.setVisibility(View.VISIBLE);
        tv_toolbar_name_module.setText(R.string.titel_stock_store);

    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_title_data, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_name_toko, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_stock_save, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

        ModelStore modelStore;
        modelStore = databesHelper.getModelStore_CustomerName(customerId);
        tv_name_toko.setText(modelStore.getCustomerName());
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });



    }


    private void getStoreList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModuleSisaStockVisit> moduleSisaStockVisitArrayList = new ArrayList<>();
        moduleSisaStockVisitArrayList.clear();

        databesHelper = new DatabesHelper(getActivity());

        moduleSisaStockVisitArrayList = databesHelper.getArraySisaStockVisit(customerId,visitID);

        if (moduleSisaStockVisitArrayList.size()>0){
            setupRecyclerView(recyclerView_stock, moduleSisaStockVisitArrayList);
            progressBar.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);

        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModuleSisaStockVisit> moduleSisaStockVisits) {

        kunjunganStockAdapter = new KunjunganStockAdapter(getActivity(),moduleSisaStockVisits);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(kunjunganStockAdapter);
        //kunjunganStockAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        //recyclerView.setNestedScrollingEnabled(false);


        int lalala = kunjunganStockAdapter.getItemCount();
        kunjunganStockAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ModuleSisaStockVisit m = kunjunganStockAdapter.getItem(position);
                EditProductStockDialog editProductStockDialog;
                if(m.getStock() == null){
                    editProductStockDialog = new EditProductStockDialog(getActivity(), m.getProductID(), m.getProductName(), position);
                } else {
                    editProductStockDialog = new EditProductStockDialog(getActivity(), m.getProductID(), m.getProductName(), position, Integer.valueOf(m.getStock()));
                }
                editProductStockDialog.setQtyInputListener(new EditProductStockDialog.QtyInputListener() {
                    @Override
                    public void onQtySubmitted(int pos, String qty) {
                        kunjunganStockAdapter.updateStok(pos, qty);
                    }
                });
                editProductStockDialog.show();
                editProductStockDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });



        mr_item_stock_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isCheckedAll = true;
                String uncheckedProduct = "";
                for (int i=0; i<moduleSisaStockVisits.size(); i++){
                    Boolean isChecked = moduleSisaStockVisits.get(i).getStatus().equals("1") ? true : false;
                    Boolean isMandatory = moduleSisaStockVisits.get(i).getIsMandatory().equals("1") ? true : false;
                    if (isMandatory && !isChecked){
                        isCheckedAll = false;
                        uncheckedProduct = moduleSisaStockVisits.get(i).getProductID() + " (" + moduleSisaStockVisits.get(i).getProductName() + ")";
                        break;
                    }

                }

                if (!isCheckedAll){
                    Toast.makeText(getActivity(),  "cek stock "+ uncheckedProduct +" terlebih dahulu!", Toast.LENGTH_LONG).show();

                }else {
                    for (int i=0; i<moduleSisaStockVisits.size(); i++){
                        if(moduleSisaStockVisits.get(i).getStock() == null) moduleSisaStockVisits.get(i).setStock("0");
                        databesHelper.insertSisaStockVisit(visitID, moduleSisaStockVisits.get(i).getProductID(), customerId, moduleSisaStockVisits.get(i).getUOM(), "", moduleSisaStockVisits.get(i).getStock(), "", "", ValidationHelper.dateFormat(), databesHelper.getEmployeeID(), "");
                    }
                    mr_item_stock_save.setEnabled(false);
                    mr_item_stock_save.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_button_diseble));
                    tv_stock_save.setText("Saving...");
                    Toast.makeText(getActivity(),  "uploading stock visit...", Toast.LENGTH_LONG).show();
                    UploadSisaStockVisitTask u = new UploadSisaStockVisitTask(getActivity(), new Etc.GenericCallback() {
                        @Override
                        public void run(Object... o) {
                            Boolean success = (Boolean) o[0];
                            if(success) Toast.makeText(getActivity(),  "stock visit uploaded", Toast.LENGTH_LONG).show();
                            else Toast.makeText(getActivity(),  "stock visit saved but failed to upload", Toast.LENGTH_LONG).show();
                            getActivity().onBackPressed();
                        }
                    });
                    u.execute();
                }
            }
        });
    }


    class UploadSisaStockVisitTask extends AsyncTask<Void, Void, Boolean>{

        Context context;
        Etc.GenericCallback cb;

        public UploadSisaStockVisitTask(Context context, Etc.GenericCallback... cb){
            this.context = context;
            if(cb.length>0) this.cb = cb[0];
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Upload u = new Upload(context, false);
            Boolean uploaded = u.openTabelSisaStockVisit();
            if(uploaded){
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(cb!=null) cb.run(result);
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
