package invent.cba.View.Fragmentt.Setting;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import invent.cba.Adapter.WiFiListAdapter;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Interface.OnLongClickListener;
import invent.cba.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class SettingWifiFragment extends Fragment  {


    HeaderHelper headerHelper;

    ConnectivityManager connectivityManager;
    NetworkInfo mNetInfo;
    WifiManager wifiManager;
    WifiInfo wifiInfo;
    WiFiListAdapter wifiListAdapter;
    ArrayList<HashMap<String, String>> mapWifiList;
    List<ScanResult> wifiList;
    List<WifiConfiguration> configuredWifiList;
    WifiConfiguration wifiConfiguration;


    ToggleButton mToggleWifi;
    RecyclerView mRvWifi;
    TextView mTvSsid;
    LinearLayoutManager linearLayoutManager;
    AlertDialog.Builder builder;
    private ProgressDialog pDialog;

    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;

    public SettingWifiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting_wifi, container, false);

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

      /*  Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.titel_setting_ip_wifisetting);
       // ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        mToggleWifi = (ToggleButton)view.findViewById(R.id.toggle_wifi);
        mTvSsid = (TextView)view.findViewById(R.id.txt_wifi);
        mRvWifi = (RecyclerView)view.findViewById(R.id.wifi_list_rv);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRvWifi.setLayoutManager(linearLayoutManager);

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.titel_setting_ip_wifisetting);
    }

    private void assignListener(){
        mToggleWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wifiManager.isWifiEnabled()) {
                    wifiManager.setWifiEnabled(false);
                    wifiList = new ArrayList<ScanResult>();

                } else {
                    wifiManager.setWifiEnabled(true);
                }

                if (wifiList != null)
                    setupRecyclerView(mRvWifi);
            }
        });


        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        connectivityManager = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        mNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        configuredWifiList = wifiManager.getConfiguredNetworks();

        getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (wifiManager.isWifiEnabled())
                    wifiList = wifiManager.getScanResults();
                else
                    wifiList = new ArrayList<ScanResult>();

                if (wifiList != null) {

                    setupRecyclerView(mRvWifi);

                    wifiInfo = wifiManager.getConnectionInfo();

                }
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));


        getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                mNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                if (wifiManager.isWifiEnabled() && wifiList != null) {
                    setupRecyclerView(mRvWifi);
                }

            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));


        //getting saved network
        configuredWifiList = wifiManager.getConfiguredNetworks();
    }


    private void setupRecyclerView(@NonNull final RecyclerView recyclerView) {
        //set the adapter
        mNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        wifiListAdapter = new WiFiListAdapter(wifiManager, mNetInfo, wifiList);


        wifiListAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (wifiList.get(position).BSSID.equalsIgnoreCase(wifiManager.getConnectionInfo().getBSSID())) {
//                    Toast.makeText(getBaseContext(), "Disconnect", Toast.LENGTH_SHORT).show();
                    builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Pengaturan WiFi")
                            .setMessage("Apakah anda yakin ingin memutuskan sambungan Wi-Fi " + wifiList.get(position).SSID)
                            .setPositiveButton("YA",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            dialog.dismiss();
                                            wifiManager.disableNetwork(wifiManager.getConnectionInfo().getNetworkId());
                                            wifiManager.startScan();
                                        }
                                    })
                            .setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.dismiss();
                                }
                            });

                    final AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    boolean isExist = false;

                    //getting saved network
                    configuredWifiList = wifiManager.getConfiguredNetworks();

                    for (WifiConfiguration conf : configuredWifiList) { //check is network exist
                        if (conf.SSID != null && conf.SSID.equals("\"" + wifiList.get(position).SSID + "\"")) {
                            new ConnectWifi(conf).execute();
                            isExist = true;
                            break;
                        }
                    }

                    if (!isExist) {
                        //show dialog to connect
                        if (wifiList.get(position).capabilities.contains("WPA") || wifiList.get(position).capabilities.contains("WPA2") || wifiList.get(position).capabilities.contains("WEP"))
                            dialogConnectWifi(wifiList.get(position).SSID, wifiList.get(position).capabilities);
                        else
                            new ConnectWifi(wifiList.get(position).SSID, "", "").execute();
                    }
//                    Toast.makeText(getBaseContext(), "Connect to "+wifiList.get(position).SSID, Toast.LENGTH_SHORT).show();
                }
            }
        });


        wifiListAdapter.setOnItemLongClickListener(new OnLongClickListener() {
            @Override
            public void onLongClick(View view, final int position) {
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Pengaturan WiFi")
                        .setMessage("Apa anda yakin ingin menghapus sambungan " + wifiList.get(position).SSID)
                        .setPositiveButton("YA",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.dismiss();
                                        removeParticularWifi(wifiList.get(position).SSID);
                                    }
                                })
                        .setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.dismiss();
                            }
                        });

                final AlertDialog alert = builder.create();
                alert.show();
            }
        });

        recyclerView.setAdapter(wifiListAdapter);
    }

    private void dialogConnectWifi(final String ssId, final String type) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_wifi_connect);
        dialog.setTitle("Sambungan WiFi");

        ((TextView) dialog.findViewById(R.id.dialog_wifi_ssid)).setText(ssId);

        // set the custom dialog components - text, image and button
        final EditText mEtPass = (EditText) dialog.findViewById(R.id.dialog_wifi_password);

        ((ImageView) dialog.findViewById(R.id.dialog_wifi_iv_hide_see_pass)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //switch hide show password
            }
        });

        mEtPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((getActivity().getWindow().getDecorView().getApplicationWindowToken()), 0);
                    return true;
                }
                return false;
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_wifi_btn_connect);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEtPass.length() > 0) {
                    dialog.dismiss();
                    new ConnectWifi(ssId, mEtPass.getText().toString(), type).execute();
                } else {
                    mEtPass.setError("Harus diisi!");
                }
            }
        });

        ((Button) dialog.findViewById(R.id.dialog_wifi_btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }



    public void removeParticularWifi(String wifiSSID) {//remove saved wifi

        configuredWifiList = wifiManager.getConfiguredNetworks();

        if(configuredWifiList != null && configuredWifiList.size() > 0) {
            for(WifiConfiguration wc : configuredWifiList) {
                if(wc.SSID!= null&& wc.SSID.equals("\""+ wifiSSID + "\"")) {
                    wifiManager.removeNetwork(wc.networkId);
                }
            }

        }

    }


    private class ConnectWifi extends AsyncTask<Void, Void, Void> {//Connecting to specific wifi

        String ssId, pass, secType;
        WifiConfiguration tempWifiConf;
        JsonObject mJsonObject;
        boolean isConnectionSuccess = false;

        public ConnectWifi(String ssId, String pass, String secType) {
            this.ssId = ssId;
            this.pass = pass;
            this.secType = secType;
        }

        public ConnectWifi(WifiConfiguration tempWifiConf) {
            this.tempWifiConf = tempWifiConf;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Menyambungkan ...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            if (tempWifiConf != null) {// handle saved network

                ssId = tempWifiConf.SSID;

                wifiManager.disconnect();
                isConnectionSuccess = wifiManager.enableNetwork(tempWifiConf.networkId, true);
                if (isConnectionSuccess) // for handle there was unsuccessfull connection
                    isConnectionSuccess = wifiManager.reconnect();

            } else { // new network
                wifiConfiguration = new WifiConfiguration();
                wifiConfiguration.SSID = "\"" + ssId + "\"";
                if (secType.contains("WPA") || secType.contains("WPA2")) {
                    wifiConfiguration.preSharedKey = "\"" + pass + "\"";
                } else if (secType.contains("WEP")) {//WEP

                    wifiConfiguration.wepKeys[0] = "\"" + pass + "\"";
                    wifiConfiguration.wepTxKeyIndex = 0;
                    wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                    wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                } else {
                    wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                }

//            if ( wifiManager.addNetwork(wifiConfiguration) != -1)// to check is success adding network

//                wifiManager.removeNetwork(wifiConfiguration.networkId);
                try {

                    int result = wifiManager.addNetwork(wifiConfiguration);
                    if (result != -1) {

                        configuredWifiList = wifiManager.getConfiguredNetworks();

                        for (WifiConfiguration conf : configuredWifiList) {
                            if (conf.SSID != null && conf.SSID.equals("\"" + ssId + "\"")) {
                                wifiManager.disconnect();
                                isConnectionSuccess = wifiManager.enableNetwork(conf.networkId, true);
                                if (isConnectionSuccess) // for handle there was unsuccessfull connection
                                    isConnectionSuccess = wifiManager.reconnect();
                                break;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            for (int x=0; x<=20000; x++) {
                if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting())
                    break;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (mNetInfo.getState().equals(NetworkInfo.State.DISCONNECTED) || mNetInfo.getState().equals(NetworkInfo.State.DISCONNECTING)) {
                isConnectionSuccess = wifiManager.reconnect();
            }

            mNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting() && isConnectionSuccess/*|| mNetInfo.getState().equals(NetworkInfo.State.CONNECTING)*/) {
                wifiManager.startScan();
            } else
                /*if (mNetInfo.getState().equals(NetworkInfo.State.DISCONNECTED) || mNetInfo.getState().equals(NetworkInfo.State.SUSPENDED) || mNetInfo.getState().equals(NetworkInfo.State.UNKNOWN))*/ {
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Pengaturan WiFi")
                        .setMessage("Gagal menyambungkan dengan " + ssId + ".")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.dismiss();
                                    }
                                });

                final AlertDialog alert = builder.create();
                alert.show();
            }


        }
    }



    @Override
    public void onResume(){
        if (wifiManager.isWifiEnabled()) {
            mToggleWifi.setChecked(true);
        } else {
            mToggleWifi.setChecked(false);
        }

        wifiManager.startScan();

        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
