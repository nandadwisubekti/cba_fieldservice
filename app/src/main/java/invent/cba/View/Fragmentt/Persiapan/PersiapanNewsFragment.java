package invent.cba.View.Fragmentt.Persiapan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import java.util.ArrayList;

import invent.cba.Adapter.PersiapanNewsAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Model.ModuleDailyMessage;
import invent.cba.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class PersiapanNewsFragment extends Fragment  {


    HeaderHelper headerHelper;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;
    ViewPager mPager;
    PageIndicator mIndicator;
    PersiapanNewsAdapter persiapanNewsAdapter;

    public PersiapanNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan_news, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());
        styleLayout();
        assignListener();
        getNews();
        return v;
    }


    private void initializeLayout(View view) {

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        mPager = (ViewPager)view.findViewById(R.id.pager);
        mIndicator = (CirclePageIndicator)view.findViewById(R.id.indicator);

        tv_toolbar_name_module.setText(R.string.modul_title_persipan_news);
    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }


    private void getNews(){
        ArrayList<ModuleDailyMessage> moduleDailyMessageArrayList = new ArrayList<>();
        moduleDailyMessageArrayList.clear();

         databesHelper = new DatabesHelper(getActivity());
        String IsRole = databesHelper.getMobileConfig(Constant.Role);
        moduleDailyMessageArrayList = databesHelper.getArrayDailyMessageList();

        if (moduleDailyMessageArrayList.size()>0){
            setupViewPager(moduleDailyMessageArrayList);
        }
      //  databesHelper.close();
    }

    private void setupViewPager(final ArrayList<ModuleDailyMessage> moduleDailyMessageArrayList) {

        persiapanNewsAdapter = new PersiapanNewsAdapter(getActivity(), moduleDailyMessageArrayList);
        mPager.setAdapter(persiapanNewsAdapter);
        mIndicator.setViewPager(mPager);


    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
