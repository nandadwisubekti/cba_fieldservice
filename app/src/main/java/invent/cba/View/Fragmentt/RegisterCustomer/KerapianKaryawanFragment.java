package invent.cba.View.Fragmentt.RegisterCustomer;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import invent.cba.Adapter.QuestionAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.MarshMallowPermission;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Interface.ConnectionInterface;
import invent.cba.Model.ModelQuestion;
import invent.cba.R;
import invent.cba.Service.ConnectionClient;
import invent.cba.View.Activity.RegisterCustomer.RegisterCustomerActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class KerapianKaryawanFragment extends Fragment {

    HeaderHelper headerHelper;
    RecyclerView recyclerViewSettingList;
    QuestionAdapter questionAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,tv_btn_title;
    MarshMallowPermission marshMallowPermission;
    ConnectionClient connectionClient;
    ConnectionInterface apiService;
    MaterialRippleLayout btn_next;
    ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<>();
    SharedPreferences mPrefs;
    String RegisterID;
    public KerapianKaryawanFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

        mPrefs = getActivity().getSharedPreferences(Constant.PREFS_EASY_PAY, Context.MODE_PRIVATE);
        RegisterID = mPrefs.getString(Constant.PREFS_TAG_NEW_REGISTER_CUSTOMER_ID, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register_customer_data_pribadi, container, false);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setSteps(getActivity(),v,4);
        styleLayout();
        assignListener();
        getQuestion();
        return v;
    }

    private void initializeLayout(View view) {

        recyclerViewSettingList = (RecyclerView)view.findViewById(R.id.recyclerView_setting_list);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        btn_next = (MaterialRippleLayout)view.findViewById(R.id.btn_next);
        tv_toolbar_name_module.setText(R.string.modul_title_data_kerapian);
        tv_btn_title = (TextView)view.findViewById(R.id.tv_btn_title);
        tv_btn_title.setText("FINISH");
    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //delete register id ketika employee back page register customer(cancel registter Customer)
                databesHelper.deleteRegisterNewCustomerDetailRegisterID(RegisterID);
                databesHelper.deleteRegisterNewCustomerRegisterID(RegisterID);

                getActivity().onBackPressed();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogKonfirmasiRegisterCustomer();

            }
        });
    }


    private void getQuestion(){



        modelQuestionArrayList.clear();
        modelQuestionArrayList = databesHelper.getArrayListQuestion(Constant.PAGE_5,"1",Constant.QuestionVisit);
        if (modelQuestionArrayList.size()>0){
            setupRecyclerView(recyclerViewSettingList,modelQuestionArrayList);
        }
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList) {

        questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,true,true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


    private void validateBtnNext(){
        boolean isSuccess = ValidationHelper.validateRegister(modelQuestionArrayList,getActivity());
        if (isSuccess == true){
            if (modelQuestionArrayList.size()>0){

                Gson gson;
                JsonObject jsonObject = new JsonObject();;
                JsonElement jsonElement;
                JsonArray jsonArray;
                //save table
                for (int i=0; i<modelQuestionArrayList.size(); i++){

                    if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.CheckBox)) {
                        gson = new Gson();
                        jsonElement =  gson.fromJson(modelQuestionArrayList.get(i).getValue().toString(), JsonElement.class);
                        jsonArray  = jsonElement.getAsJsonArray();
                        if (jsonArray.size()>0){
                            for (int j=0; j<jsonArray.size(); j++){
                                jsonObject = jsonArray.get(j).getAsJsonObject();

                                if (jsonObject.get(Constant.IsStatus).getAsBoolean() == true){
                                    databesHelper.insertRegisterNewCustomerDetail(
                                            RegisterID,
                                            modelQuestionArrayList.get(i).getQuestionID().toString(),
                                            modelQuestionArrayList.get(i).getAnswerID().toString(),
                                            modelQuestionArrayList.get(i).getValue().toString(),
                                            "0",
                                            "0");
                                }
                            }

                        }
                    }else {
                        databesHelper.insertRegisterNewCustomerDetail(
                                RegisterID,
                                modelQuestionArrayList.get(i).getQuestionID().toString(),
                                modelQuestionArrayList.get(i).getAnswerID().toString(),
                                modelQuestionArrayList.get(i).getValue().toString(),
                                "0",
                                "0");
                    }


                }


                databesHelper.updateRegisterNewCustomerIsSave(RegisterID,"1");
                databesHelper.updateRegisterNewCustomerDetailIsSave(RegisterID,"1");
                Intent intent = new Intent(getActivity(),RegisterCustomerActivity.class);
                startActivity(intent);
                getActivity().finishAffinity();

            }


        }

    }

    public  void dialogKonfirmasiRegisterCustomer(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage(R.string.dialog_home_register_customer)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {


                                validateBtnNext();

                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }

}
