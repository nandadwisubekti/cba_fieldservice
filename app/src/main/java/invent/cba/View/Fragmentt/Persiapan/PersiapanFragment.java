package invent.cba.View.Fragmentt.Persiapan;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import invent.cba.Adapter.PersiapanAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Model.ModelModuleButton;
import invent.cba.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class PersiapanFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewPersiapan;
    PersiapanAdapter persiapanAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_title_data;
    ProgressBar progressBar;
    public PersiapanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());
        styleLayout();
        assignListener();
        getModuleList();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerViewPersiapan = (RecyclerView)view.findViewById(R.id.recyclerView_persiapan);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_toolbar_name_module.setText(R.string.modul_title_persipan);

    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){



        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }


    private void getModuleList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModelModuleButton> arrayListModuleButton = new ArrayList<>();
        arrayListModuleButton.clear();

         databesHelper = new DatabesHelper(getActivity());
        String IsRole = databesHelper.getMobileConfig(Constant.Role);
        arrayListModuleButton = databesHelper.getArralyModuleButton(Constant.PERSIAPAN,IsRole,"1");

        if (arrayListModuleButton.size()>0){
            setupRecyclerView(recyclerViewPersiapan,arrayListModuleButton);
            tv_title_data.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        databesHelper.close();
        progressBar.setVisibility(View.GONE);
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelModuleButton> arrayListModuleButton) {

        persiapanAdapter = new PersiapanAdapter(getActivity(),arrayListModuleButton);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(persiapanAdapter);
        persiapanAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        persiapanAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String Module_Id = arrayListModuleButton.get(position).getModule_Id();


                if (Module_Id.equals(Constant.NEWS)){

                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("mapheader", mapArrayList.get(position));
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    PersiapanNewsFragment persiapanNewsFragment = new PersiapanNewsFragment();
                    persiapanNewsFragment.setArguments(bundle);
                    transaction.replace(R.id.fragment, persiapanNewsFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

                //PROFILE
                if ((Module_Id.equals(Constant.RUTEKUNJUNGAN))){

                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("mapheader", mapArrayList.get(position));
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    PersiapanRuteKunjunganFragment persiapanRuteKunjunganFragment = new PersiapanRuteKunjunganFragment();
                    persiapanRuteKunjunganFragment.setArguments(bundle);
                    transaction.replace(R.id.fragment, persiapanRuteKunjunganFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();


                }


                //KATASANDI
                if (Module_Id.equals(Constant.TOOLS)){


                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("mapheader", mapArrayList.get(position));
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    PersiapanToolsFragment persiapanToolsFragment = new PersiapanToolsFragment();
                    persiapanToolsFragment.setArguments(bundle);
                    transaction.replace(R.id.fragment, persiapanToolsFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }



            }
        });



    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
