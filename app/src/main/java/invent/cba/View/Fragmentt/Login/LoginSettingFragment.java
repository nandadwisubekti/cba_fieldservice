package invent.cba.View.Fragmentt.Login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.R;
import invent.cba.View.Fragmentt.Setting.SettingFragment;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class LoginSettingFragment extends Fragment  {


    TextView tvSettingLoginTitelLogin;

    EditText etSettingLoginPassword;
    MaterialRippleLayout mrBtnSettingLoginSubmit;
    HeaderHelper headerHelper;
    ImageView ivSettingLoginLogo;

    private static final String TAG = "LoginSettingFragment";

    DatabesHelper databesHelper;
    public LoginSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting_login, container, false);

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());
        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

        tvSettingLoginTitelLogin = (TextView)view.findViewById(R.id.tv_setting_login_titel_login);

        etSettingLoginPassword = (EditText) view.findViewById(R.id.et_setting_login_password);
        mrBtnSettingLoginSubmit = (MaterialRippleLayout) view.findViewById(R.id.mr_btn_setting_login_submit);
        ivSettingLoginLogo = (ImageView)view.findViewById(R.id.img_setting_login_logo);

        ValidationHelper.getIcLauncher(getActivity(),ivSettingLoginLogo);

    }

    private void styleLayout() {

        TextHelper.styleEditText(getActivity(), etSettingLoginPassword, "PASSWORD *", true);

        TextHelper.setFont(getActivity(), tvSettingLoginTitelLogin, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        TextHelper.setFont(getActivity(), mrBtnSettingLoginSubmit, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
    }

    private void assignListener(){
        tvSettingLoginTitelLogin.setText(getActivity().getResources().getString(R.string.titel_login_setting));
/*
        String versionMobail = databesHelper.getMobileConfig("APP_VERSION");
        tvSettingLoginCopyrighVersion.setText(versionMobail);
        tvSettingLoginCopyrighVersion.setVisibility(View.GONE);*/
        mrBtnSettingLoginSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (etSettingLoginPassword.getText().length()>0){

                    String passwordSetting = databesHelper.getMobileConfig(Constant.PASSWORDSETTING);
                    if (passwordSetting !=null){
                        if (etSettingLoginPassword.getText().toString().toLowerCase().equals(passwordSetting.toLowerCase())){


                            Bundle bundle = new Bundle();
                            //bundle.putSerializable("mapheader", mapArrayList.get(position));
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            SettingFragment fragment = new SettingFragment();
                            fragment.setArguments(bundle);
                            transaction.replace(R.id.fragment, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                           /* Intent intent = new Intent(getActivity(), SettingActivity.class);
                            startActivity(intent);*/


                        }else {
                            DialogHelper.dialogMessage(getActivity(),"Informasi","Kesalahan Password",Constant.DIALOG_WRONG);
                        }

                    }else {
                        DialogHelper.dialogMessage(getActivity(),"Informasi","Databes belum terbuat",Constant.DIALOG_WRONG);

                    }

                }else {
                    DialogHelper.dialogMessage(getActivity(),"Informasi","Mohon isi Password",Constant.DIALOG_WRONG);

                }
                databesHelper.close();

            }
        });

    }


    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }



}
