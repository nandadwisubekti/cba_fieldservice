package invent.cba.View.Fragmentt.Kunjungan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

import invent.cba.Adapter.KunjunganAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Interface.OnItemClickListener;
import invent.cba.Model.ModelStore;
import invent.cba.R;
import invent.cba.View.Activity.Home.HomeActivity;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class KunjunganFragment extends Fragment  {

    HeaderHelper headerHelper;
    RecyclerView recyclerViewPersiapan;
    KunjunganAdapter kunjunganAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back,
            ryPersiapanScanBarcode;
    TextView tv_toolbar_name_module,
            tv_persiapan_name_title,
            tv_title_data;
    ProgressBar progressBar;
    EditText etScanBarcode;
    LinearLayout lyPersiapanBarcode;

    public KunjunganFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getStoreList();
        return v;
    }

    private void initializeLayout(View view) {

        recyclerViewPersiapan = (RecyclerView)view.findViewById(R.id.recyclerView_persiapan);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_persiapan_name_title = (TextView)view.findViewById(R.id.tv_persiapan_name_title);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        ryPersiapanScanBarcode = (RelativeLayout)view.findViewById(R.id.ry_persiapan_scan_barcode);
        etScanBarcode = (EditText)view.findViewById(R.id.et_scan_barcode);
        lyPersiapanBarcode = (LinearLayout)view.findViewById(R.id.ly_persiapan_barcode);
        lyPersiapanBarcode.setVisibility(View.VISIBLE);
        tv_persiapan_name_title.setVisibility(View.VISIBLE);
        tv_toolbar_name_module.setText(R.string.modul_title_persipan_ruete_kunjungan);
        tv_persiapan_name_title.setText(R.string.titel_persian_store_list);
    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_persiapan_name_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), etScanBarcode, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


    }

    private void assignListener(){



        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // getActivity().onBackPressed();

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
            }
        });

        ryPersiapanScanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator.forSupportFragment(KunjunganFragment.this).initiateScan();

            }
        });

        etScanBarcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                kunjunganAdapter.getFilter().filter(s);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getStoreList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModelStore> arrayListmoduleStore = new ArrayList<>();
        arrayListmoduleStore.clear();

        databesHelper = new DatabesHelper(getActivity());
        String EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);
        String VisitID = databesHelper.getVisitID(EmployeeID);
        arrayListmoduleStore = databesHelper.getArrayStoreVisit(VisitID);

        if (arrayListmoduleStore.size()>0){
            setupRecyclerView(recyclerViewPersiapan,arrayListmoduleStore);
            progressBar.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);

        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelStore> arrayListmoduleStore) {

        kunjunganAdapter = new KunjunganAdapter(getActivity(),arrayListmoduleStore);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(kunjunganAdapter);
        kunjunganAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        kunjunganAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String customerId = arrayListmoduleStore.get(position).getCustomerID();
                String visitId = arrayListmoduleStore.get(position).getVisitID();
                String time = arrayListmoduleStore.get(position).getTime();
                String isFinish = arrayListmoduleStore.get(position).getIsFinish();
                String customerIdVisit = databesHelper.getVisitCustomer();

                if (customerIdVisit != null) {
                    //JIKA IisStart=1 and isfinish=0  MAKA LANJUTAKAN
                    if (customerId.equals(customerIdVisit)) {
                        DialogHelper.dialogKonfirmasiVisitStart(getActivity(), "IN", visitId, customerId, "");
                       /* Intent intent = new Intent(getActivity(), KunjunganStartActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();*/
                    } else {
                        String customerName = databesHelper.getVisitCustomerName(customerIdVisit);
                        if (customerName != null) {

                            DialogHelper.dialogMessage(getActivity(), "Info", "Selesaikan aktifitas " + customerName + " telebih dahulu!", Constant.DIALOG_WRONG);

                        } else {
                            DialogHelper.dialogMessage(getActivity(), "Info", "Selesaikan aktifitas sebelumnya!", Constant.DIALOG_WRONG);
                        }
                    }
                } else {

                    //CEK URUTAN TOKO
                    if (isFinish.equals("0")) {
                       // String sequence = String.valueOf(arrayListmoduleStore.get(position).getSequence());
                        if (position == 0) {
                            //JIKA SEQ -1 MAKA JADI TOKO KE 1
                            cekTimeToko(visitId, customerId, time);
                        } else {
                          /*  String sequenceBefore = null;
                            String sequence = String.valueOf(arrayListmoduleStore.get(position).getSequence());*/
                            int positionGet = position - 1;
                            String customerId_position = arrayListmoduleStore.get(positionGet).getCustomerID();
                            String isFinish_sequence = databesHelper.getIsFinishCustomer(customerId_position);
                            if (isFinish_sequence != null) {
                                String customerId_sequence = arrayListmoduleStore.get(position).getCustomerID();
                                String visitId_sequence = arrayListmoduleStore.get(position).getVisitID();
                                String time_sequence = arrayListmoduleStore.get(position).getTime();

                                if (isFinish_sequence.equals("1")) {
                                    cekTimeToko(visitId_sequence, customerId_sequence, time_sequence);
                                } else {
                                    //ALASAN TIDAK SESUI URUTAN
                                    DialogHelper.dialogReasonVisitStart(getActivity(), "Beri alasan kenapa tidak sesuai urutan?", visitId_sequence, customerId_sequence);
                                }

                            } else {
                                DialogHelper.dialogMessage(getActivity(), "Info", "Tekan kembali aktifitas", Constant.DIALOG_WRONG);

                            }
                        }

                    } else {
                        //TUNDA KUNJUNGAN
                        // KETIKA SETATUS IS DELIVERY 2
                        String isDelivery = arrayListmoduleStore.get(position).getIsDeliver();
                        if (isDelivery.equals("2")) {
                            Toast.makeText(getActivity(), "TUNDA AKTIFITAS", Toast.LENGTH_LONG).show();
                            //ALASAN TUNDA KUNJUGAN
                            DialogHelper.dialogReasonVisitStart(getActivity(), "Beri alasan kenapa tunda aktifitas?", visitId, customerId);

                        }
                    }

                }
            }

        });

    }

    //CEK TIME TOKO KUJUNGAN
    private void cekTimeToko(final String visitId,final String CustomerId,final String timeToko){

        boolean checkTimeToko = ValidationHelper.checkTimeToko(getActivity(),timeToko);

        if (checkTimeToko == true){
           // Toast.makeText(getActivity(),  "Sesuai dengan waktu toko + toleransi", Toast.LENGTH_LONG).show();
            DialogHelper.dialogKonfirmasiVisitStart(getActivity(),"ON_TIME",visitId,CustomerId,"");

        }else {
            DialogHelper.dialogReasonVisitStart(getActivity(),"Beri alasan kenapa tidak sesuai jam?",visitId,CustomerId);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                //Toast.makeText(getActivity(), "Scan Cancelled", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getActivity(), "Result : " + result.getContents(), Toast.LENGTH_LONG).show();
                etScanBarcode.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

}
