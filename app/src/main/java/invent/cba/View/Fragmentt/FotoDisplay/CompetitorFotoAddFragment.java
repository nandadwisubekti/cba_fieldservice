package invent.cba.View.Fragmentt.FotoDisplay;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.MRuntimePermission;
import invent.cba.Helper.TakeImage;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Helper.data.SharedPreferenceHelper;
import invent.cba.R;
import invent.cba.View.Activity.FotoDisplay.KunjunganCompetitirActivityFoto;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class CompetitorFotoAddFragment extends Fragment  {


    HeaderHelper headerHelper;

    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;
    MaterialRippleLayout mr_item_foto_display_save,
            mr_foto_display_takCamer;
    ImageView ivViewPhoto;

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    String imgBase64;
    private MRuntimePermission mPermissionChecker;
    Intent intent;
    public CompetitorFotoAddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        mPermissionChecker = new MRuntimePermission(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_foto_display_add, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        mr_item_foto_display_save = (MaterialRippleLayout)view.findViewById(R.id.mr_item_foto_display_save);
        ivViewPhoto = (ImageView)view.findViewById(R.id.ivViewPhoto);
        mr_foto_display_takCamer = (MaterialRippleLayout)view.findViewById(R.id.mr_foto_display_takCamer);
        tv_toolbar_name_module.setText(R.string.modul_title_persipan_ruete_photo_display);
    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getActivity(),KunjunganCompetitirActivityFoto.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        mr_foto_display_takCamer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] perms = {
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                };
                checkPermissions(perms);
            }
        });


        mr_item_foto_display_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imgBase64 == null || imgBase64.equalsIgnoreCase("")){
                    DialogHelper.dialogMessage(getActivity(),"Informasi","Lakukan pengambilan foto terlebih dahulu!",Constant.DIALOG_ERROR);

                }else{
                    dialogSave();
                }
            }
        });
    }


    //dialog save
    public void dialogSave(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirmation);
        builder.setMessage(R.string.dialog_ind_save)
                .setCancelable(false)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {


                                SimpleDateFormat id = new SimpleDateFormat("yyyMMddHHmmss");
                                String Dateimage = id.format(new Date());

                                SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
                                String CustomerID = sharedPreferenceHelper.getCustomerID();
                                String VisitID = sharedPreferenceHelper.getVisitID();
                                String CompetitorProductID = sharedPreferenceHelper.getCompetitorProductID();

                                //String CompetitorID = modelGlobal.getCompetitorID;
                                String CompetitorID = new SharedPreferenceHelper(getActivity()).getCompetitorID();


                                if (CustomerID!=null && VisitID !=null){
                                    String IdImage = "IMG_"+VisitID+"_"+CompetitorID+"_"+CompetitorProductID+"_"+Dateimage;

                                    databesHelper.insertCompetitorActivityImage(
                                            IdImage,
                                            VisitID,
                                            CompetitorID,
                                            CompetitorProductID,
                                            CustomerID,
                                            imgBase64,
                                            ValidationHelper.dateFormat());
                                    databesHelper.close();

                                    // getActivity().onBackPressed();

                                    intent = new Intent(getActivity(),KunjunganCompetitirActivityFoto.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }else {
                                    Toast.makeText(getActivity(), "Customer ID atau Visit ID null!",
                                            Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }



                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }


    //Take camer
    //region PERMISSION CHECK
    private void checkPermissions(String[] perm) {
        if (ContextCompat.checkSelfPermission(getActivity(), perm[0]) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getActivity(), perm[1]) == PackageManager.PERMISSION_GRANTED) {
                if (mPermissionChecker.checkFragmentCameraPermission(CompetitorFotoAddFragment.this)) {
                    //CAMERA
                    takeImageCamera();
                }
            } else {
                requestPermissions(perm,0);
            }
        } else {
            requestPermissions(perm,0);
        }
    }


    private void takeImageCamera() {
        if (TakeImage.isIntentAvailable(getContext(), MediaStore.ACTION_IMAGE_CAPTURE)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePictureIntent, REQUEST_CAMERA);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data,ivViewPhoto);
        }
    }



    private void onCaptureImageResult(Intent data, ImageView ivViewPhoto) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

       /* File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        //compress IMAGE TO Base64
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        byte[] b = bs.toByteArray();
        //imgBase64 = Base64.encodeToString(b, Base64.NO_WRAP);
        imgBase64 = Base64.encodeToString(b, Base64.DEFAULT);
        //compress IMAGE TO Base64

        ivViewPhoto.setImageBitmap(thumbnail);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MRuntimePermission.PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    int grantResult = grantResults[i];

                    if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            //takeImagGallery();
                            break;
                        } else {
                            mPermissionChecker.checkFragmentWriteExternalStoragePermission(this);
                            break;
                        }

                    }
                }
                break;
            case MRuntimePermission.PERMISSIONS_REQUEST_CAMERA:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    int grantResult = grantResults[i];

                    if (permission.equals(Manifest.permission.CAMERA)) {

                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            takeImageCamera();
                            break;
                        } else {
                            mPermissionChecker.checkFragmentCameraPermission(this);
                            break;
                        }

                    }
                }
                break;
        }

    }



    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
