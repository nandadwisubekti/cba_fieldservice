package invent.cba.View.Fragmentt.Kunjungan;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anton46.stepsview.StepsView;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import invent.cba.Adapter.QuestionAdapter;
import invent.cba.Helper.Constant;
import invent.cba.Helper.DatabesHelper;
import invent.cba.Helper.DialogHelper;
import invent.cba.Helper.HeaderHelper;
import invent.cba.Helper.MarshMallowPermission;
import invent.cba.Helper.TextHelper;
import invent.cba.Helper.ValidationHelper;
import invent.cba.Helper.data.SharedPreferenceHelper;
import invent.cba.Interface.ConnectionInterface;
import invent.cba.Model.ModelGetLocation;
import invent.cba.Model.ModelQuestion;
import invent.cba.R;
import invent.cba.Service.ConnectionClient;

public class KunjunganCanvasserFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewSettingList;
    StepsView stepsView;
    QuestionAdapter questionAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_btn_title;
    MarshMallowPermission marshMallowPermission;
    ConnectionClient connectionClient;
    ConnectionInterface apiService;
    MaterialRippleLayout btn_next;
    ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<>();
    SharedPreferences mPrefs;
    String RegisterID;
    String msgError ="";
    String customerId,visitID;
    boolean isNowData ;
    public KunjunganCanvasserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        customerId = sharedPreferenceHelper.getCustomerID();
        visitID= sharedPreferenceHelper.getVisitID();



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register_customer_data_pribadi, container, false);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
       // headerHelper.setSteps(getActivity(),v,0);
        styleLayout();
        assignListener();
        checkSudahAdaDataAtauBelum(customerId,visitID);
        return v;
    }


    private void initializeLayout(View view) {

        recyclerViewSettingList = (RecyclerView)view.findViewById(R.id.recyclerView_setting_list);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_btn_title = (TextView)view.findViewById(R.id.tv_btn_title);
        btn_next = (MaterialRippleLayout)view.findViewById(R.id.btn_next);
        tv_toolbar_name_module.setText(R.string.modul_title_data_canvasser);
        stepsView = (StepsView)view.findViewById(R.id.stepsView);
        stepsView.setVisibility(View.GONE);
        tv_btn_title.setText("Simpan");

    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                getActivity().onBackPressed();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateBtnNext();

            }
        });
    }


    private void getQuestion(){



        modelQuestionArrayList.clear();
        modelQuestionArrayList = databesHelper.getArrayListQuestion(Constant.CANVASSER_PAGE_1,"1",Constant.QuestionCategoryCanvasser);
        if (modelQuestionArrayList.size()>0){
            setupRecyclerView(recyclerViewSettingList,modelQuestionArrayList,true,true);
        }
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList, boolean isEnabled, boolean isEnabledEdite) {

        questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,isEnabled,isEnabledEdite);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


    private void validateBtnNext(){
        boolean isSuccess = ValidationHelper.validateRegister(modelQuestionArrayList,getActivity());
        if (isSuccess == true){

            if (isNowData == true){
                //edite
                editeCanvasser();
            }else {
                //save
                saveCanvasser();
            }


        }

    }


    private boolean createCanvasser(){
        boolean isSave = false;

        if (customerId!=null){
            final ModelGetLocation mobileLocation;
            ValidationHelper validationHelper = new ValidationHelper(getActivity());
            mobileLocation = validationHelper.getMobileLocation();

            if(mobileLocation.getLatitude()!=null && mobileLocation.getLongitude() !=null){

                String EmployeeID = databesHelper.getEmployeeID();
                String VisitID = databesHelper.getVisitID(EmployeeID);
                SimpleDateFormat srcDf = new SimpleDateFormat("yyyMMdd");
                String date = srcDf.format(new Date());
                RegisterID = "CVR-"+date;
                String StartDate = ValidationHelper.dateFormat();

                //insert RegisterNewCustomer
                databesHelper.insertRegisterNewCustomer(
                        RegisterID,
                        EmployeeID,
                        VisitID,
                        StartDate,
                        mobileLocation.getLatitude(),
                        mobileLocation.getLongitude(),
                        "0",
                        "0",
                        customerId,
                        Constant.QuestionCategoryCanvasser);

                isSave = true;
            }else {

                isSave = false;
                msgError = "Pencatatan Canvasser membutuhkan GPS dan koneksi internet,hidupkan terlebih dahulu GPS  dan koneksi internet anda!";
            }

        }else {
            isSave = false;
            msgError = "Customer ID tidak diketahui, coba tekan kembali!";
        }



        return  isSave;
    }

    private void saveCanvasser(){
        boolean isCreateCanvasser = createCanvasser();
        if (isCreateCanvasser == true){
            if (modelQuestionArrayList.size()>0){

                Gson gson;
                JsonObject jsonObject = new JsonObject();;
                JsonElement jsonElement;
                JsonArray jsonArray;
                //save table RegisterNewCustomerDetail
                for (int i=0; i<modelQuestionArrayList.size(); i++){

                    if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.CheckBox)) {
                        gson = new Gson();
                        jsonElement =  gson.fromJson(modelQuestionArrayList.get(i).getValue().toString(), JsonElement.class);
                        jsonArray  = jsonElement.getAsJsonArray();
                        if (jsonArray.size()>0){
                            for (int j=0; j<jsonArray.size(); j++){
                                jsonObject = jsonArray.get(j).getAsJsonObject();

                                if (jsonObject.get(Constant.IsStatus).getAsBoolean() == true){

                                    databesHelper.insertRegisterNewCustomerDetail(
                                            RegisterID,
                                            modelQuestionArrayList.get(i).getQuestionID().toString(),
                                            modelQuestionArrayList.get(i).getAnswerID().toString(),
                                            modelQuestionArrayList.get(i).getValue().toString(),
                                            "0",
                                            "0");
                                }
                            }

                        }
                    }else {

                        databesHelper.insertRegisterNewCustomerDetail(
                                RegisterID,
                                modelQuestionArrayList.get(i).getQuestionID().toString(),
                                modelQuestionArrayList.get(i).getAnswerID().toString(),
                                modelQuestionArrayList.get(i).getValue().toString(),
                                "0",
                                "0");
                    }
                }

                databesHelper.updateRegisterNewCustomerIsSave(RegisterID,"1");
                databesHelper.updateRegisterNewCustomerDetailIsSave(RegisterID,"1");
                getActivity().onBackPressed();

            }
        }else {
            DialogHelper.dialogMessage(getActivity(), "Info", msgError, Constant.DIALOG_WRONG);

        }
    }

    private void editeCanvasser(){
        if (modelQuestionArrayList.size()>0){
            Gson gson;
            JsonObject jsonObject = new JsonObject();;
            JsonElement jsonElement;
            JsonArray jsonArray;
            String value = null;
            //save table RegisterNewCustomerDetail
            for (int i=0; i<modelQuestionArrayList.size(); i++){

                if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.CheckBox)) {
                    gson = new Gson();
                    jsonElement =  gson.fromJson(modelQuestionArrayList.get(i).getValue().toString(), JsonElement.class);
                    jsonArray  = jsonElement.getAsJsonArray();
                    if (jsonArray.size()>0){
                        for (int j=0; j<jsonArray.size(); j++){
                            jsonObject = jsonArray.get(j).getAsJsonObject();

                            if (jsonObject.get(Constant.IsStatus).getAsBoolean() == true){

                                if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.NumericDecimal)) {
                                    if (modelQuestionArrayList.get(i).getValue().toString().contains(","))
                                        value = modelQuestionArrayList.get(i).getValue().toString().replaceAll(",", "");
                                    else if ( modelQuestionArrayList.get(i).getValue().toString().contains("."))
                                        value = modelQuestionArrayList.get(i).getValue().toString().replaceAll("\\.", "");

                                }else {
                                    value = modelQuestionArrayList.get(i).getValue().toString();
                                }
                                
                                databesHelper.updateRegisterNewCustomerDetailVisit(modelQuestionArrayList.get(i).getRegisterID().toString(),
                                        modelQuestionArrayList.get(i).getQuestionID().toString(),
                                        value);
                            }
                        }

                    }
                }else {

                    if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.NumericDecimal)) {
                        if (modelQuestionArrayList.get(i).getValue().toString().contains(","))
                            value = modelQuestionArrayList.get(i).getValue().toString().replaceAll(",", "");
                        else if ( modelQuestionArrayList.get(i).getValue().toString().contains("."))
                            value = modelQuestionArrayList.get(i).getValue().toString().replaceAll("\\.", "");

                    }else {
                        value = modelQuestionArrayList.get(i).getValue().toString();
                    }
                    
                    databesHelper.updateRegisterNewCustomerDetailVisit(modelQuestionArrayList.get(i).getRegisterID().toString(),
                            modelQuestionArrayList.get(i).getQuestionID().toString(),
                            value);

                }
            }

            getActivity().onBackPressed();

        }
    }

    private void checkSudahAdaDataAtauBelum(String customerId,String visitID){
         isNowData = databesHelper.getArrayLitResultSurveyVisitCustomerID("1",visitID,customerId,Constant.QuestionCategoryCanvasser);

         if (isNowData == true){
             //sudah ada
             getQuestionSudahDiisi(visitID,customerId);

         }else {
             //belum ada
             getQuestion();
         }
    }

    private void getQuestionSudahDiisi(String visitID ,String customerId) {

        modelQuestionArrayList.clear();
        modelQuestionArrayList = databesHelper.getArrayListQuestionSudahDiisi(visitID,customerId,Constant.CANVASSER_PAGE_1,"1",Constant.QuestionCategoryCanvasser);
        if (modelQuestionArrayList.size()>0){
            setupRecyclerView(recyclerViewSettingList,modelQuestionArrayList,false,true);
        }

    }



}
