package invent.cba.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;

import invent.cba.R;

/**
 * Created by Falih on 11/30/2018.
 */
public class EditProductStockDialog extends Dialog {

    //todo: buat tampilan dialog untuk input qty stock product
    private QtyInputListener qtyInputListener;
    private String productID, productName;
    private int position, qty;
    AppCompatTextView productIDTextview, productNameTextview;
    AppCompatEditText qtyEditText;
    AppCompatButton confirmBtn, cancelBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_edit_product_stock);

        initComponents();
    }

    private void initComponents() {
        productIDTextview = findViewById(R.id.dialog_edit_stock_productID);
        productNameTextview = findViewById(R.id.dialog_edit_stock_product_name);
        qtyEditText = findViewById(R.id.dialog_edit_stock_qty);
        confirmBtn = findViewById(R.id.dialog_edit_stock_confirm_btn);
        cancelBtn = findViewById(R.id.dialog_edit_stock_cancel_btn);

        productIDTextview.setText(productID);
        productNameTextview.setText(productName);
        qtyEditText.setText(qty == 0 ? "" : String.valueOf(qty));
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(qtyInputListener!=null){
                    String qty = qtyEditText.getText().toString().equals("") ? "0" : qtyEditText.getText().toString();
                    qtyInputListener.onQtySubmitted(position, qty);
                    dismiss();
                }
            }
        });

        new Handler().postDelayed(new Runnable() {

            public void run() {
                qtyEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN , 0, 0, 0));
                qtyEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP , 0, 0, 0));
            }
        }, 200);
    }

    public EditProductStockDialog(@NonNull Context context, String productID, String productName, int position) {
        super(context);
        this.productID = productID;
        this.productName = productName;
        this.position = position;
        this.qty = 0;
    }

    public EditProductStockDialog(@NonNull Context context, String productID, String productName, int position, int qty) {
        super(context);
        this.productID = productID;
        this.productName = productName;
        this.position = position;
        this.qty = qty;
    }

    public void setQtyInputListener(QtyInputListener qtyInputListener){
        this.qtyInputListener = qtyInputListener;
    }

    public interface QtyInputListener {
        void onQtySubmitted(int position, String qty);
    }


}
