package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModuleTools;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class PersiapanToolsAdapter extends RecyclerView.Adapter<PersiapanToolsAdapter.MyViewHolder> {

    private ArrayList<ModuleTools> moduleToolses;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    DatabesHelper databesHelper;
    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_item;
        TextView tv_item_name;
        EditText et_item_qty;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_item = (MaterialRippleLayout)view.findViewById(R.id.mr_item);
            tv_item_name = (TextView)view.findViewById(R.id.tv_item_name);
            et_item_qty = (EditText) view.findViewById(R.id.et_item_qty);

        }
    }


    public PersiapanToolsAdapter(Context context, ArrayList<ModuleTools> moduleToolses) {
        this.moduleToolses = moduleToolses;
        this.context = context;

        formatter = new DecimalFormat("#,###,###");
        databesHelper = new DatabesHelper(context);
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
//                                symbols.setDecimalSeparator(',');
//                                symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_tools, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModuleTools moduleTools = moduleToolses.get(position);

        holder.tv_item_name.setText(moduleTools.getPOSMName());
        holder.et_item_qty.setText(moduleTools.getQuantity());


        String value = databesHelper.getMobilFlag(Constant.ITEMGOODS,Constant.CHECKLIST);
        if (value.equals("1")){
            holder.et_item_qty.setEnabled(false);

        }

        TextHelper.setFont(context, holder.tv_item_name, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.et_item_qty, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        holder.et_item_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                holder.et_item_qty.removeTextChangedListener(this);

                if (editable.length()>0) {
                    holder.et_item_qty.setError(null);
                    if (editable.toString().contains("0") && editable.length()>1){
                        String s = editable.toString();
                        if (!s.substring(0,1).matches("[1-9]")){
                            holder.et_item_qty.setText("0");
                        }
                    }
                }else {
                    holder.et_item_qty.setText("0");
                }


                String givenstring = editable.toString();
                try {
                    Long longval;
                    if (givenstring.contains(","))
                        givenstring = givenstring.replaceAll(",", "");
                    else if (givenstring.contains("."))
                        givenstring = givenstring.replaceAll("\\.", "");

                    longval = Long.parseLong(givenstring);
                    //formatter = new DecimalFormat("#,###,###");
//                         NumberFormat formatter = new DecimalFormat("##,##,##,###");
                    String formattedString = formatter.format(longval);
                    holder.et_item_qty.setText(formattedString);

                    moduleTools.setQuantity(String.valueOf(longval));

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.et_item_qty.setSelection(holder.et_item_qty.getText().length());
                holder.et_item_qty.addTextChangedListener(this);

            }
        });

       /* holder.et_item_qty.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (holder.et_item_qty.length() == 0)
                        moduleTools.setQuantity("0");

                        holder.et_item_qty.setText("0");

                    return true;
                }
                return false;
            }
        });*/




    }

    @Override
    public int getItemCount() {
        return moduleToolses.size();
    }


    public void setOnItemQtyClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }



}
