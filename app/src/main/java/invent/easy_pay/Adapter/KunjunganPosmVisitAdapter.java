package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModelPOSMStock;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class KunjunganPosmVisitAdapter extends RecyclerView.Adapter<KunjunganPosmVisitAdapter.MyViewHolder> {

    private ArrayList<ModelPOSMStock> modelPOSMStocks;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_stock_item;
        TextView tv_qty_productName,
                tv_posm_qty_in,
                tv_posm_qty_sisa;
        EditText et_posm_qty_out;
        TextInputLayout textInput;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_stock_item = (MaterialRippleLayout)view.findViewById(R.id.mr_stock_item);
            tv_qty_productName = (TextView)view.findViewById(R.id.tv_qty_productName);
            tv_posm_qty_in = (TextView)view.findViewById(R.id.tv_posm_qty_in);
            et_posm_qty_out = (EditText) view.findViewById(R.id.et_posm_qty_out);
            tv_posm_qty_sisa = (TextView)view.findViewById(R.id.tv_posm_qty_sisa);
            textInput = (TextInputLayout)view.findViewById(R.id.textInput);



        }
    }


    public KunjunganPosmVisitAdapter(Context context, ArrayList<ModelPOSMStock> modelPOSMStocks1) {
        this.modelPOSMStocks = modelPOSMStocks1;
        this.context = context;

        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_posm, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelPOSMStock modelPOSMStock = modelPOSMStocks.get(position);

        holder.tv_qty_productName.setText(modelPOSMStock.getPOSMName());
        holder.tv_posm_qty_in.setText(formatter.format(Integer.valueOf(modelPOSMStock.getQuantity())));
        holder.et_posm_qty_out.setVisibility(View.GONE);
        holder.tv_posm_qty_sisa.setVisibility(View.GONE);
        holder.textInput.setVisibility(View.GONE);

        TextHelper.setFont(context, holder.tv_qty_productName, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_posm_qty_in, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());


        holder.mr_stock_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return modelPOSMStocks.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
