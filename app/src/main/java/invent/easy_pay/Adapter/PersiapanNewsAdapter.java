package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import invent.easy_pay.Helper.ImageBitmapBas64;
import invent.easy_pay.Model.ModuleDailyMessage;
import invent.easy_pay.R;


/**
 * Created by kahfi on 12/16/2015.
 */
public class PersiapanNewsAdapter extends PagerAdapter {

    Context context;
    private ArrayList<ModuleDailyMessage> moduleNewses;



    public PersiapanNewsAdapter(Context context, ArrayList<ModuleDailyMessage> arrayList) {
        this.context = context;
        moduleNewses = arrayList;
    }

    @Override
    public int getCount() {
        return moduleNewses.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final LayoutInflater inflater = (LayoutInflater) container.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View convertView = inflater.inflate(R.layout.fragment_persiapan_news_detail, null);

        ModuleDailyMessage moduleNews = moduleNewses.get(position);

        TextView tvMessageName = (TextView) convertView.findViewById(R.id.tvMessageName);
        TextView tvMessageDesc = (TextView) convertView.findViewById(R.id.tvMessageDesc);
        ImageView iv_persiapan_news = (ImageView)convertView.findViewById(R.id.iv_persiapan_news);

        tvMessageName.setText(moduleNews.getMessageName());
        tvMessageDesc.setText(moduleNews.getMessageDesc());

        String messageImage = moduleNews.getMessageImg().toString();
        if ( messageImage !=null){
             iv_persiapan_news.setImageBitmap(ImageBitmapBas64.decodeSampledBitmapFromResource(context.getResources(), messageImage, 100, 100));

        }

        ((ViewPager) container).addView(convertView, 0);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        return convertView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ViewGroup) object);
    }
}
