package invent.easy_pay.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Model.QuestionSet;
import invent.easy_pay.R;

public class QuestionSetAdapter extends RecyclerView.Adapter<QuestionSetAdapter.ViewHolder> {

    private List<QuestionSet> items;

    private QuestionSetAdapterOnClickListener onClickListener;

    public QuestionSetAdapter(Context context, String customerId, String employeeId) {
        DatabesHelper databesHelper = new DatabesHelper(context);
        items = databesHelper.getQuestionSets(customerId, employeeId);
        databesHelper.close();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_menu_a, parent, false));
    }

    public int getCount(){
        return items.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int finalPosition = position;
        final QuestionSet item = items.get(position);
        holder.textView.setText(item.getText());
        if (item.getAnswersCount() > 0) {
            holder.setColor(Color.GREEN);
        } else {
            holder.setColor(Color.RED);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onClickListener != null) {
                    onClickListener.onQuestionSetItemClick(item, finalPosition, view);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public QuestionSetAdapterOnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(QuestionSetAdapterOnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public int getCompletedCount() {
        int counter = 0;
        for (QuestionSet item: items) {
            if(item.getAnswersCount()>0) counter++;
        }
        return counter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        private ImageView indicatorView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_a);
            indicatorView = itemView.findViewById(R.id.lyItem);
        }

        private void setColor(int color) {
            indicatorView.setBackgroundColor(color);
        }
    }

    public interface QuestionSetAdapterOnClickListener {
        void onQuestionSetItemClick(QuestionSet questionSet, int position, View view);
    }

}
