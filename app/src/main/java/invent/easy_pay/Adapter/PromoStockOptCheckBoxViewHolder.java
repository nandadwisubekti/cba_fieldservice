package invent.easy_pay.Adapter;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import invent.easy_pay.R;


/**
 * Created by Kodelokus-Dell on 3/29/2016.
 */
public class PromoStockOptCheckBoxViewHolder extends RecyclerView.ViewHolder{

    View mView;
    AppCompatCheckBox mCbFoodOpt;

    public PromoStockOptCheckBoxViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mCbFoodOpt = (AppCompatCheckBox) itemView.findViewById(R.id.item_check_boox);
    }
}
