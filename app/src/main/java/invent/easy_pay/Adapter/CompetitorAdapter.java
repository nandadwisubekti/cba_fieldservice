package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModuleCompetitor;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class CompetitorAdapter extends RecyclerView.Adapter<CompetitorAdapter.MyViewHolder> {

    private ArrayList<ModuleCompetitor> moduleCompetitorArrayList;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;

        MaterialRippleLayout mr_competitor_item;

        TextView tv_competitor_item,
                tv_item_competitor_normal_price,
                tv_item_competitor_normal_promo;

        LinearLayout lyStatus;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_competitor_item = (MaterialRippleLayout)view.findViewById(R.id.mr_competitor_item);
            tv_competitor_item = (TextView)view.findViewById(R.id.tv_competitor_item);
            tv_item_competitor_normal_price = (TextView)view.findViewById(R.id.tv_item_competitor_normal_price);
            tv_item_competitor_normal_promo = (TextView)view.findViewById(R.id.tv_item_competitor_normal_promo);



        }
    }


    public CompetitorAdapter(Context context, ArrayList<ModuleCompetitor> moduleCompetitorArrayList) {
        this.moduleCompetitorArrayList = moduleCompetitorArrayList;
        this.context = context;

        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_competitor, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModuleCompetitor modelCompetitor = moduleCompetitorArrayList.get(position);

        holder.tv_competitor_item.setText(modelCompetitor.getCompetitorProductName());

        holder.tv_item_competitor_normal_price.setText("Rp."+formatter.format(Integer.valueOf(modelCompetitor.getNormalPrice())));
        holder.tv_item_competitor_normal_promo.setText("Rp."+formatter.format(Integer.valueOf(modelCompetitor.getPromoPrice())));

        TextHelper.setFont(context, holder.tv_competitor_item, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_item_competitor_normal_price, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_item_competitor_normal_promo, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        holder.mr_competitor_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return moduleCompetitorArrayList.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
