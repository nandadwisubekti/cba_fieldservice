package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import invent.easy_pay.Interface.CallbackOnClickItemListener;
import invent.easy_pay.Interface.CallbackOnLongClickItemListener;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.R;

/**
 * Created by kahfi on 2/10/2017.
 */

public class AdapterMenu extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    CallbackOnClickItemListener callbackOnClickItemListener;
    CallbackOnLongClickItemListener callbackOnLongClickItemListener;


    List<Model> list;

    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;


    public class MyViewHolderA extends RecyclerView.ViewHolder {
        View mView;
        TextView tvA;
        ImageView ivIcon;
        ImageView lyItem;

        public MyViewHolderA(View view) {
            super(view);
            mView = view;
            tvA = (TextView)view.findViewById(R.id.tv_a);
            ivIcon = (ImageView) view.findViewById(R.id.iv_icon);
            lyItem = (ImageView)view.findViewById(R.id.lyItem);

        }
    }
    public class MyViewHolderB extends RecyclerView.ViewHolder {
        View mView;
        TextView tvA;
        ImageView ivIcon;
        public MyViewHolderB(View view) {
            super(view);
            mView = view;
            tvA = (TextView)view.findViewById(R.id.tv_a);
            ivIcon = (ImageView) view.findViewById(R.id.iv_icon);
        }
    }




    public AdapterMenu(Context context, List<Model> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType){
            case 1:
                View itemViewA = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_menu_a, parent, false);
                return new MyViewHolderA(itemViewA);

            case 2:
                View itemViewB = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_menu_b, parent, false);
                return new MyViewHolderB(itemViewB);

        }

        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (list.get(position).getType()){
            case TYPE_A:
                MyViewHolderA viewHolderA = (MyViewHolderA) holder;
                viewHolderA.tvA.setText(list.get(position).getTvA());

                if(list.get(position).isVisibility())
                    viewHolderA.itemView.setVisibility(View.VISIBLE);
                else
                    viewHolderA.itemView.setVisibility(View.GONE);


                if(list.get(position).isEnability()) {
                    viewHolderA.ivIcon.setImageDrawable(context.getResources().getDrawable(list.get(position).getResIdIconEnable()));
                    viewHolderA.lyItem.setBackgroundColor(context.getResources().getColor(R.color.button_enable));
                } else  {
                    if(list.get(position).getResIdIconDisable() != -1) viewHolderA.ivIcon.setImageDrawable(context.getResources().getDrawable(list.get(position).getResIdIconDisable()));
                    viewHolderA.lyItem.setBackgroundColor(context.getResources().getColor(R.color.button_diseble));

                }

                viewHolderA.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(callbackOnClickItemListener!=null){
                            if(list.get(position).isEnability()) callbackOnClickItemListener.onClick(position);
                        }
                    }
                });

                viewHolderA.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if(callbackOnLongClickItemListener!=null){
                            callbackOnLongClickItemListener.onLongClick(position);
                        }
                        return false;
                    }
                });

                break;
            case TYPE_B:
                MyViewHolderB viewHolderB = (MyViewHolderB) holder;
                viewHolderB.tvA.setText(list.get(position).getTvA());

                if(list.get(position).getResIdIconEnable()!= -1) viewHolderB.ivIcon.setImageDrawable(context.getResources().getDrawable(list.get(position).getResIdIconEnable()));

                viewHolderB.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(callbackOnClickItemListener!=null){
                            callbackOnClickItemListener.onClick(position);
                        }
                    }
                });

                viewHolderB.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if(callbackOnLongClickItemListener!=null){
                            callbackOnLongClickItemListener.onLongClick(position);
                        }
                        return false;
                    }
                });

                break;
                

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }


    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getType()){
            case TYPE_A:
                return 1;
            case TYPE_B:
                return 2;
        }
        return -1;
    }


    public static class Model{
        private String id;
        private boolean visibility;
        private boolean enability;

        int resIdIconEnable = -1;
        int resIdIconDisable = -1;
        private String tvA,tvB;
        private Type type = Type.TYPE_A;
        public enum Type{
            TYPE_A,TYPE_B
        }


        public boolean isVisibility() {
            return visibility;
        }

        public void setVisibility(boolean visibility) {
            this.visibility = visibility;
        }

        public boolean isEnability() {
            return enability;
        }

        public void setEnability(boolean enability) {
            this.enability = enability;
        }

        public int getResIdIconEnable() {
            return resIdIconEnable;
        }

        public void setResIdIconEnable(int resIdIcon) {
            this.resIdIconEnable = resIdIcon;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public String getTvA() {
            return tvA;
        }

        public void setTvA(String tvA) {
            this.tvA = tvA;
        }

        public String getTvB() {
            return tvB;
        }

        public void setTvB(String tvB) {
            this.tvB = tvB;
        }


        public int getResIdIconDisable() {
            return resIdIconDisable;
        }

        public void setResIdIconDisable(int resIdIconDisable) {
            this.resIdIconDisable = resIdIconDisable;
        }
    }


    public void setOnClickItemListener(CallbackOnClickItemListener callbackOnClickItemListener){
        this.callbackOnClickItemListener = callbackOnClickItemListener;
    }
    public void setOnLongClickItemListener(CallbackOnLongClickItemListener callbackOnLongClickItemListener){
        this.callbackOnLongClickItemListener = callbackOnLongClickItemListener;
    }
}
