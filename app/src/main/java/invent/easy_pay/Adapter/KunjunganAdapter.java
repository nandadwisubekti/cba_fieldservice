package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModelStore;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class KunjunganAdapter extends RecyclerView.Adapter<KunjunganAdapter.MyViewHolder> implements Filterable {

    private ArrayList<ModelStore> modelStores;
    private ArrayList<ModelStore> contactList;


    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;



    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_store_item;
        TextView tv_store_no,
                tv_store_name,
                tv_store_address,
                tv_store_time,
                tv_store_status,
                tv_store_reson;
        RelativeLayout ryStatus;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_store_item = (MaterialRippleLayout)view.findViewById(R.id.mr_store_item);
            tv_store_no = (TextView)view.findViewById(R.id.tv_store_no);
            tv_store_name = (TextView)view.findViewById(R.id.tv_store_name);
            tv_store_address = (TextView)view.findViewById(R.id.tv_store_address);
            tv_store_time = (TextView)view.findViewById(R.id.tv_store_time);
            tv_store_status = (TextView)view.findViewById(R.id.tv_store_status);
            tv_store_reson = (TextView)view.findViewById(R.id.tv_store_reson);
            ryStatus = (RelativeLayout)view.findViewById(R.id.ryStatus);


        }
    }


    public KunjunganAdapter(Context context, ArrayList<ModelStore> modelStores) {
        this.modelStores = modelStores;
        this.context = context;
        this.contactList = modelStores;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_store, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModelStore modelStore = modelStores.get(position);

        holder.tv_store_no.setText(String.valueOf(position+1));
        holder.tv_store_name.setText(modelStore.getCustomerName());
        holder.tv_store_address.setText(modelStore.getCustomerAddress());
        holder.tv_store_time.setText(modelStore.getTime());


        holder.tv_store_time.setVisibility(View.VISIBLE);
        holder.tv_store_status.setVisibility(View.VISIBLE);

        TextHelper.setFont(context, holder.tv_store_name, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_store_no, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_store_address, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_store_time, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_store_status, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.tv_store_reson, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());



        String isFinish = modelStore.getIsFinish();
        String isStart = modelStore.getIsStart();
        String isDeliver = modelStore.getIsDeliver();
        String statusReason = modelStore.getValue();

        if (statusReason != null){
            holder.tv_store_reson.setVisibility(View.VISIBLE);
            holder.tv_store_reson.setText("Alasan :"+statusReason);
        }else {
            holder.tv_store_reson.setText("");
        }

        //BELUM DI KUNJUNGI
        if (isStart.equalsIgnoreCase("0") && isFinish.equals("0")){
            holder.tv_store_status.setText("Belum" );
            holder.ryStatus.setBackgroundDrawable(context.getResources().getDrawable(R.color.colorwhite));

        }

        //BELUM DI KUNJUNGI SAMA SEKALI
        if (isStart.equalsIgnoreCase("0") && isFinish.equals("1")){
            holder.tv_store_status.setText("Gagal dikunjungi" );
            holder.ryStatus.setBackgroundDrawable(context.getResources().getDrawable(R.color.paleSubtitle));
          //  holder.mr_store_item.setEnabled(false);

        }

        //SELESAI KUNJUGI
        if(isFinish.equals("1") && isStart.equals("1")){
            if (isDeliver.equals("1")){
                holder.ryStatus.setBackgroundDrawable(context.getResources().getDrawable(R.color.colorSuccess));
                holder.tv_store_status.setText("Selesai");
              //  holder.mr_store_item.setEnabled(false);
            }
            //TUNDA KUNJUNGAN
            if(isDeliver.equals("2")){
                holder.ryStatus.setBackgroundDrawable(context.getResources().getDrawable(R.color.colorBackgroundTundaKunjungan));
                holder.tv_store_status.setText("Tunda kunjungan" );
            }
            //SELESAI DI KUNJUNGI,TAPI ADA ALASAN
            if(isDeliver.equals("0")){
                holder.ryStatus.setBackgroundDrawable(context.getResources().getDrawable(R.color.paleSubtitle));
                holder.tv_store_status.setText("Gagal dikunjungi" );
               // holder.mr_store_item.setEnabled(false);

            }

        }



        //CUTOMER DI KUNJUNGI TETAPI ERROR ATAU KELUAR APLIKASI
        if (isStart.equalsIgnoreCase("1") && isFinish.equals("0")){
            holder.ryStatus.setBackgroundDrawable(context.getResources().getDrawable(R.color.colorStatusIn));
            holder.tv_store_status.setText("In" );

        }




        holder.mr_store_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return modelStores.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    modelStores = contactList;
                } else {
                    ArrayList<ModelStore> filteredList = new ArrayList<>();
                    for (ModelStore row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCustomerID().toLowerCase().contains(charString.toLowerCase()) || row.getCustomerName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    modelStores = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = modelStores;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                modelStores = (ArrayList<ModelStore>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



}
