package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModelModuleButton;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class KunjunganStartAdapter extends RecyclerView.Adapter<KunjunganStartAdapter.MyViewHolder> {

    private ArrayList<ModelModuleButton> modelModuleButtons;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;

    DatabesHelper databesHelper;
    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_button_item;
        TextView tv_button_name_module;
        ImageView iv_button_icon;
        RelativeLayout ryList;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            tv_button_name_module = (TextView)view.findViewById(R.id.tv_button_name_module);
            mr_button_item =(MaterialRippleLayout)view.findViewById(R.id.mr_button_item);
            iv_button_icon = (ImageView)view.findViewById(R.id.iv_button_icon);
            ryList = (RelativeLayout)view.findViewById(R.id.ryList);



        }
    }


    public KunjunganStartAdapter(Context context, ArrayList<ModelModuleButton> modelModuleButtons) {
        this.modelModuleButtons = modelModuleButtons;
        this.context = context;
        databesHelper = new DatabesHelper(context);

        // hotfix: ngilangin new counter module button, kalau emg ada di db.
        removeNewCounterModuleButton();
    }

    private void removeNewCounterModuleButton() {
        for(int i=0; i<modelModuleButtons.size(); i++){
            if(modelModuleButtons.get(i).getModule_Id().equals(Constant.NEWCOUNTER)){
                modelModuleButtons.remove(i);
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_button, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModelModuleButton modelModuleButton = modelModuleButtons.get(position);

        holder.tv_button_name_module.setText(modelModuleButton.getTitle());
        TextHelper.setFont(context, holder.tv_button_name_module, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        holder.tv_button_name_module.setTextColor(context.getResources().getColor(R.color.colorBackgroundBtn));
        String module_Id = modelModuleButton.getModule_Id();
        String value = modelModuleButton.getValue();
        String isActive = modelModuleButton.getIsActive();



        if ((module_Id.equals(Constant.STOCKSTORE))){

            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_stock));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                   // holder.mr_button_item.setVisibility(View.VISIBLE);
                    holder.mr_button_item.setEnabled(true);
                }else {
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(false);
                   // holder.mr_button_item.setVisibility(View.VISIBLE);

                }

            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }


        }

        if ((module_Id.equals(Constant.POSM))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_posm));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }


        }
        if ((module_Id.equals(Constant.ISSUE))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_issue_kunjungan));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }


        }
        if ((module_Id.equals(Constant.KOMPETITOR))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_competitor));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }
        if ((module_Id.equals(Constant.DATAPEMBELIAN))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_database));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }

        if ((module_Id.equals(Constant.FOTODISPLAY))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_fotodisp));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }

        if ((module_Id.equals(Constant.CANVASSER))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_posm));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }


        if ((module_Id.equals(Constant.NEWCUSTOMER))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_posm));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }

        if ((module_Id.equals(Constant.SURVEY))){
            if (isActive.equals("1")){
                holder.iv_button_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_posm));
                if (value.equals("0")){
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));
                    holder.mr_button_item.setEnabled(true);

                }else {
                    holder.mr_button_item.setEnabled(false);
                    holder.mr_button_item.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_button_transparan));

                }
                holder.mr_button_item.setVisibility(View.VISIBLE);
            }else {
                holder.mr_button_item.setVisibility(View.GONE);
            }

        }

        holder.mr_button_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });

      /*  holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mItemLongClickListener != null)
                    mItemLongClickListener.onLongClick(v, position);
                return false;
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return modelModuleButtons.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
