package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModelQuestion;
import invent.easy_pay.R;

public class PreviewQuestionAdapter extends RecyclerView.Adapter<PreviewQuestionAdapter.MyViewHolder> {

    private ArrayList<ModelQuestion> modelQuestionArrayList;

    public PreviewQuestionAdapter(ArrayList<ModelQuestion> modelQuestionArrayList) {
        this.modelQuestionArrayList = modelQuestionArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_input_register_preview, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ModelQuestion modelQuestion = modelQuestionArrayList.get(position);
        holder.etRegisterAnswer.setVisibility(View.GONE);
        holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
        holder.ryRegisterAnswerDate.setVisibility(View.GONE);
        holder.rySpRegisterAnswer.setVisibility(View.GONE);

        holder.tvRegisterQuestionTitle.setText(modelQuestion.getQuestionText());

        switch (modelQuestion.getAnswerTypeID()) {
            case Constant.ShortAnswer: {
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
                holder.etRegisterAnswer.setVisibility(View.VISIBLE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);

                holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            }
            case Constant.Paragraph:
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
                holder.etRegisterAnswer.setVisibility(View.VISIBLE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);
                holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                holder.etRegisterAnswer.setSingleLine(false);
                holder.etRegisterAnswer.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                break;
            case Constant.NumericDecimal: {
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
                holder.etRegisterAnswer.setVisibility(View.VISIBLE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);
                holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            }
            case Constant.Numeric: {
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
                holder.etRegisterAnswer.setVisibility(View.VISIBLE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);
                holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            }
            case Constant.MultipleChoice: {
                holder.rySpRegisterAnswer.setVisibility(View.VISIBLE);
                holder.etRegisterAnswer.setVisibility(View.GONE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
            }
            case Constant.DateType:
                holder.tvRegisterAnswerDate.setText(modelQuestion.getValue());
                holder.ryRegisterAnswerDate.setVisibility(View.VISIBLE);
                holder.etRegisterAnswer.setVisibility(View.GONE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);

                holder.tvRegisterAnswerDate.setText(modelQuestion.getValue());
                break;
            case Constant.CheckBox: {
                holder.recyclerViewRegisterAnswer.setVisibility(View.VISIBLE);
                holder.etRegisterAnswer.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
                break;
            }
            case Constant.PhoneNumber: {
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
                holder.etRegisterAnswer.setVisibility(View.VISIBLE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);

                holder.etRegisterAnswer.setInputType(InputType.TYPE_CLASS_PHONE);
                break;
            }
            case Constant.Email: {
                holder.etRegisterAnswer.setText(modelQuestion.getValue());
                holder.etRegisterAnswer.setVisibility(View.VISIBLE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.ryRegisterAnswerDate.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);

                holder.etRegisterAnswer.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT
                        | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        | InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
                break;
            }
            case Constant.TIME:
                holder.tvRegisterAnswerDate.setText(modelQuestion.getValue());
                holder.ryRegisterAnswerDate.setVisibility(View.VISIBLE);
                holder.etRegisterAnswer.setVisibility(View.GONE);
                holder.recyclerViewRegisterAnswer.setVisibility(View.GONE);
                holder.rySpRegisterAnswer.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return modelQuestionArrayList.size();
    }

    public void setOnItemClickListener() {
    }

    public void setOnItemLongClickListener() {
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        View mView;
        RelativeLayout ryRegisterAnswerDate, rySpRegisterAnswer;
        TextView tvRegisterQuestionTitle, tvRegisterAnswerDate;
        RecyclerView recyclerViewRegisterAnswer;
        EditText etRegisterAnswer, spRegisterAnswer;

        public MyViewHolder(View view) {
            super(view);
            mView = view;
            ryRegisterAnswerDate = (RelativeLayout) view.findViewById(R.id.ry_register_answer_date);
            tvRegisterQuestionTitle = (TextView) view.findViewById(R.id.tv_register_question_title);
            etRegisterAnswer = (EditText) view.findViewById(R.id.et_register_answer);
            rySpRegisterAnswer = (RelativeLayout) view.findViewById(R.id.ry_sp_register_answer);
            recyclerViewRegisterAnswer = (RecyclerView) view.findViewById(R.id.recyclerView_register_answer);
            spRegisterAnswer = view.findViewById(R.id.sp_register_answer);
            tvRegisterAnswerDate = (TextView) view.findViewById(R.id.tv_register_answer_date);
        }
    }

}
