package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModelModuleButton;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.MyViewHolder> {

    private ArrayList<ModelModuleButton> modelModuleButtons;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mrSettingItem;
        TextView tvSettingNameModule;
        ImageView ivSettingIcon;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mrSettingItem = (MaterialRippleLayout)view.findViewById(R.id.mr_setting_item);
            tvSettingNameModule = (TextView)view.findViewById(R.id.tv_setting_name_module);
            ivSettingIcon = (ImageView)view.findViewById(R.id.iv_setting_icon);

        }
    }


    public SettingAdapter(Context context, ArrayList<ModelModuleButton> modelModuleButtons) {
        this.modelModuleButtons = modelModuleButtons;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_setting, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModelModuleButton modelModuleButton = modelModuleButtons.get(position);

        holder.tvSettingNameModule.setText(modelModuleButton.getTitle());
        TextHelper.setFont(context, holder.tvSettingNameModule, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        String module_Id = modelModuleButton.getModule_Id();
        if (module_Id.equals(Constant.PROFILE)){
            holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_driver_info));
        }
        if (module_Id.equals(Constant.KATASANDI)){
            holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_password));
        }
        if (module_Id.equals(Constant.WIFI)){
            holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_wifi));
        }
        if (module_Id.equals(Constant.BIRGHTNESS)){
            //holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_brightness));
            holder.mrSettingItem.setVisibility(View.GONE);
        }
        if (module_Id.equals(Constant.ID_DEVICE)){
            holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_device));
        }
        if (module_Id.equals(Constant.PRINTER)){
            holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_printer));
        }
        if (module_Id.equals(Constant.TRACKING)){
            holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_timer_tracking));
        }
        if (module_Id.equals(Constant.UPDATE_APP)
                || module_Id.equals(Constant.UPDATE_CONFIGURATION)
                || module_Id.equals(Constant.DOWNLOD_CPANEL)){
            holder.ivSettingIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_download_setting));
        }

        if (module_Id.equals(Constant.DOWNLOD_CPANEL)){
            holder.mrSettingItem.setVisibility(View.GONE);

        }

      /*  if (modelModuleButtons.equals(Constant.DOWNLOD_CPANEL)){
            holder.mrSettingItem.setVisibility(View.GONE);
            holder.mrSettingItem.setVisibility(View.GONE);
        }
*/

        holder.mrSettingItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });

      /*  holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mItemLongClickListener != null)
                    mItemLongClickListener.onLongClick(v, position);
                return false;
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return modelModuleButtons.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
