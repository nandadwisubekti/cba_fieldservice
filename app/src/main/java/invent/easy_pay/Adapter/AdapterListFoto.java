package invent.easy_pay.Adapter;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import invent.easy_pay.Interface.CallbackOnClickItemListener;
import invent.easy_pay.Interface.CallbackOnLongClickItemListener;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.R;

/**
 * Created by INVENT on 8/30/2018.
 */

public class AdapterListFoto extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    CallbackOnClickItemListener callbackOnClickItemListener;
    CallbackOnLongClickItemListener callbackOnLongClickItemListener;


    public ArrayList<Model> list;

    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;


    public class MyViewHolderA extends RecyclerView.ViewHolder {
        View mView;

        ImageView ivImage;
        ImageView lyItem;

        public MyViewHolderA(View view) {
            super(view);
            mView = view;

            ivImage = (ImageView) view.findViewById(R.id.iv_image);
            lyItem = (ImageView)view.findViewById(R.id.lyItem);

        }
    }




    public AdapterListFoto(Context context, ArrayList<Model> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType){
            case 1:
                View itemViewA = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_foto_a, parent, false);
                return new MyViewHolderA(itemViewA);

        }

        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (list.get(position).getType()){
            case TYPE_A:
                MyViewHolderA viewHolderA = (MyViewHolderA) holder;

                viewHolderA.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(callbackOnClickItemListener!=null){
                            callbackOnClickItemListener.onClick(position);
                        }
                    }
                });

                if(list.get(position).getFilePath()!=null){
                    Picasso.get().load(new File(list.get(position).getFilePath())).resize(0,200).into(viewHolderA.ivImage);
                }


                viewHolderA.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if(callbackOnLongClickItemListener!=null){
                            callbackOnLongClickItemListener.onLongClick(position);
                        }
                        return false;
                    }
                });

                break;



        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }


    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getType()){
            case TYPE_A:
                return 1;

        }
        return -1;
    }


    public static class Model implements Parcelable{
        private String id;
        private String time;
        private String latitude;
        private String longitude;
        private String filePath = null;
        private Type type = Type.TYPE_A;

        public Model(){};

        protected Model(Parcel in) {
            id = in.readString();
            filePath = in.readString();
            time = in.readString();
            latitude = in.readString();
            longitude = in.readString();
        }

        public static final Creator<Model> CREATOR = new Creator<Model>() {
            @Override
            public Model createFromParcel(Parcel in) {
                return new Model(in);
            }

            @Override
            public Model[] newArray(int size) {
                return new Model[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(id);
            parcel.writeString(filePath);
            parcel.writeString(time);
            parcel.writeString(latitude);
            parcel.writeString(longitude);
        }

        public enum Type{
            TYPE_A
        }




        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public void setTime(String time){ this.time = time; }

        public String getTime(){ return time; }

        public void setLatitude(String latitude){ this.latitude = latitude; }

        public String getLatitude(){ return latitude; }

        public void setLongitude(String longitude){ this.longitude= longitude; }

        public String getLongitude(){ return longitude; }
    }


    public void setOnClickItemListener(CallbackOnClickItemListener callbackOnClickItemListener){
        this.callbackOnClickItemListener = callbackOnClickItemListener;
    }
    public void setOnLongClickItemListener(CallbackOnLongClickItemListener callbackOnLongClickItemListener){
        this.callbackOnLongClickItemListener = callbackOnLongClickItemListener;
    }
}
