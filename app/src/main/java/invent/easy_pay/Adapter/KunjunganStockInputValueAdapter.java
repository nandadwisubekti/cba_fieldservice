package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModuleSisaStockVisit;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class KunjunganStockInputValueAdapter extends RecyclerView.Adapter<KunjunganStockInputValueAdapter.MyViewHolder> {

    private ArrayList<ModuleSisaStockVisit> moduleSisaStockVisits;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;
    int status;
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_item;
        TextView tv_item_name,
                tv_stock_rp;
        TextInputLayout input_stock_qty,
                input_stock_price;
        EditText et_stock_input_qty,
                et_stock_input_price;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_item = (MaterialRippleLayout)view.findViewById(R.id.mr_item);
            tv_item_name = (TextView)view.findViewById(R.id.tv_item_name);
            tv_stock_rp = (TextView)view.findViewById(R.id.tv_stock_rp);

            input_stock_qty = (TextInputLayout)view.findViewById(R.id.input_stock_qty);
            input_stock_price = (TextInputLayout)view.findViewById(R.id.input_stock_price);

            et_stock_input_qty =  (EditText)view.findViewById(R.id.et_stock_input_qty);
            et_stock_input_price = (EditText)view.findViewById(R.id.et_stock_input_price);




        }
    }


    public KunjunganStockInputValueAdapter(Context context, ArrayList<ModuleSisaStockVisit> moduleSisaStockVisits1,int status) {
        this.moduleSisaStockVisits = moduleSisaStockVisits1;
        this.status = status;
        this.context = context;

        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_stock_input, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModuleSisaStockVisit moduleSisaStockVisit = moduleSisaStockVisits.get(position);


        String SisaStockTypeID = moduleSisaStockVisit.getSisaStockTypeID();


        TextHelper.setFont(context,  holder.tv_item_name, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(context,  holder.et_stock_input_qty, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(context,  holder.et_stock_input_price, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());






        /*formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
*/
        holder.tv_item_name.setText(moduleSisaStockVisit.getDescription());
        holder.et_stock_input_qty.setText(moduleSisaStockVisit.getValue());

        if (status == 0){

            holder.et_stock_input_price.setText(formatter.format(Integer.valueOf(moduleSisaStockVisit.getValue())));
            holder.et_stock_input_qty.setText(formatter.format(Integer.valueOf(moduleSisaStockVisit.getValue())));

            holder.et_stock_input_price.setEnabled(true);
            holder.et_stock_input_price.setEnabled(true);
            holder.input_stock_price.setEnabled(true);
            holder.input_stock_qty.setEnabled(true);
        }
        if (status == 1){
            String isStok = moduleSisaStockVisit.getIsStok();
            if (!isStok.equals("1")){
                moduleSisaStockVisit.setValue("0");
            }
            holder.et_stock_input_price.setText(formatter.format(Integer.valueOf(moduleSisaStockVisit.getValue())));
            holder.et_stock_input_qty.setText(formatter.format(Integer.valueOf(moduleSisaStockVisit.getValue())));

            holder.input_stock_qty.setEnabled(false);
            if (SisaStockTypeID.equals("NormalPrice") || SisaStockTypeID.equals("PromoPrice")){
                holder.input_stock_price.setEnabled(true);
                holder.et_stock_input_price.setEnabled(true);

            }else {
                holder.input_stock_price.setEnabled(false);
                holder.et_stock_input_price.setEnabled(false);
            }
        }

        if (status == 2){
            moduleSisaStockVisit.setValue("0");
            holder.et_stock_input_price.setText(formatter.format(Integer.valueOf(moduleSisaStockVisit.getValue())));
            holder.et_stock_input_qty.setText(formatter.format(Integer.valueOf(moduleSisaStockVisit.getValue())));

            holder.et_stock_input_price.setEnabled(false);
            holder.et_stock_input_price.setEnabled(false);
            holder.input_stock_price.setEnabled(false);
            holder.input_stock_qty.setEnabled(false);

        }

        if (SisaStockTypeID.equals("0")
                ||SisaStockTypeID.equals("12")
                || SisaStockTypeID.equals("6")
                || SisaStockTypeID.equals("9")){


            holder.input_stock_qty.setVisibility(View.VISIBLE);
            holder.input_stock_price.setVisibility(View.GONE);
            holder.tv_stock_rp.setVisibility(View.GONE);


        }

        if (SisaStockTypeID.equals("NormalPrice") || SisaStockTypeID.equals("PromoPrice")){

            holder.input_stock_price.setVisibility(View.VISIBLE);
            holder.input_stock_qty.setVisibility(View.GONE);
            holder.tv_stock_rp.setVisibility(View.VISIBLE);

        }

        TextHelper.setFont(context, holder.tv_item_name, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.et_stock_input_price, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, holder.et_stock_input_qty, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());




      /*  holder.et_stock_input_qty.requestFocus();
        holder.et_stock_input_qty.setInputType(InputType.TYPE_NULL); // hidden
        holder.et_stock_input_qty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.showSoftInput( holder.et_stock_input_qty, InputMethodManager.SHOW_FORCED);
                holder.et_stock_input_qty.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);

            }
        });
*/
        holder.et_stock_input_price.requestFocus();
        holder.et_stock_input_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                holder.et_stock_input_price.removeTextChangedListener(this);

                if (editable.length()>0) {
                    holder.et_stock_input_price.setError(null);
                    if (editable.toString().contains("0") && editable.length()>1){
                        String s = editable.toString();
                        if (!s.substring(0,1).matches("[1-9]")){
                            holder.et_stock_input_price.setText("0");
                            moduleSisaStockVisit.setValue("0");
                        }
                    }
                }else {
                    holder.et_stock_input_price.setText("0");
                    moduleSisaStockVisit.setValue("0");
                }


                String givenstring = editable.toString();
                try {
                    Long longval;
                    if (givenstring.contains(","))
                        givenstring = givenstring.replaceAll(",", "");
                    else if (givenstring.contains("."))
                        givenstring = givenstring.replaceAll("\\.", "");

                    longval = Long.parseLong(givenstring);
                    String formattedString = formatter.format(longval);
                    holder.et_stock_input_price.setText(formattedString);

                    moduleSisaStockVisit.setValue(String.valueOf(longval));

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.et_stock_input_price.setSelection(holder.et_stock_input_price.getText().length());
                holder.et_stock_input_price.addTextChangedListener(this);

            }
        });


        holder.et_stock_input_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                holder.et_stock_input_qty.removeTextChangedListener(this);

                if (editable.length()>0) {
                    holder.et_stock_input_qty.setError(null);
                    if (editable.toString().contains("0") && editable.length()>1){
                        String s = editable.toString();
                        if (!s.substring(0,1).matches("[1-9]")){
                            holder.et_stock_input_qty.setText("0");
                            moduleSisaStockVisit.setValue("0");
                        }
                    }
                }else {
                    holder.et_stock_input_qty.setText("0");
                    moduleSisaStockVisit.setValue("0");
                }


                String givenstring = editable.toString();
                try {
                    Long longval;
                    if (givenstring.contains(","))
                        givenstring = givenstring.replaceAll(",", "");
                    else if (givenstring.contains("."))
                        givenstring = givenstring.replaceAll("\\.", "");

                    longval = Long.parseLong(givenstring);
                    String formattedString = formatter.format(longval);
                    holder.et_stock_input_qty.setText(formattedString);

                    moduleSisaStockVisit.setValue(String.valueOf(longval));

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.et_stock_input_qty.setSelection(holder.et_stock_input_qty.getText().length());
                holder.et_stock_input_qty.addTextChangedListener(this);


            }
        });


        holder.et_stock_input_qty.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (holder.et_stock_input_qty.length() == 0){
                            moduleSisaStockVisit.setValue("0");

                            holder.et_stock_input_qty.setText("0");
                        }

                        return true;
                    }
                    return false;
                }
            });

        holder.et_stock_input_price.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (holder.et_stock_input_price.length() == 0){
                        moduleSisaStockVisit.setValue("0");
                        holder.et_stock_input_price.setText("0");
                    }

                        return true;
                }
                return false;
            }
        });

        holder.mr_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return moduleSisaStockVisits.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
