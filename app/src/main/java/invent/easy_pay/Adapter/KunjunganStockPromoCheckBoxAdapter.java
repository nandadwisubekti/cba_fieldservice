package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;

import invent.easy_pay.Interface.OnCheckedChangeListener;
import invent.easy_pay.Model.ModulePromo;
import invent.easy_pay.R;


public class KunjunganStockPromoCheckBoxAdapter extends RecyclerView.Adapter<PromoStockOptCheckBoxViewHolder> {


    OnCheckedChangeListener mCheckedChangeListener;
    Context mContext;
    boolean isCheck;
    ArrayList<ModulePromo> modulePromos;



    public KunjunganStockPromoCheckBoxAdapter(Context mContext, ArrayList<ModulePromo> modulePromos, boolean isCheck) {
        this.modulePromos = modulePromos;
        this.mContext = mContext;
        this.isCheck = isCheck;
    }

    @Override
    public PromoStockOptCheckBoxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_checkboox, parent, false);
        PromoStockOptCheckBoxViewHolder viewHolder = new PromoStockOptCheckBoxViewHolder(layoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PromoStockOptCheckBoxViewHolder holder, final int position) {

        final ModulePromo modulePromo = modulePromos.get(position);

        holder.mCbFoodOpt.setText(modulePromo.getPromoName());
        holder.mCbFoodOpt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckedChangeListener != null)
                    mCheckedChangeListener.onCheckedChanged(buttonView, isChecked, position);



            }
        });

        if (isCheck == true){
            modulePromos.get(position).setIsStatus("true");
        }else {
            modulePromos.get(position).setIsStatus("false");
        }

        holder.mCbFoodOpt.setChecked(isCheck);

    }

    @Override
    public int getItemCount() {
        return modulePromos.size();
    }

    public void setOnItemCheckedChangeListener(final OnCheckedChangeListener mCheckedChangeListener) {
        this.mCheckedChangeListener = mCheckedChangeListener;
    }

    // Clean all elements of the recycler
    public void clear() {
        modulePromos.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    /*public void addAll(List<HashMap<String, String>> list) {
        mDataList.addAll(list);
        notifyDataSetChanged();
    }*/

}
