package invent.easy_pay.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Interface.OnLongClickListener;
import invent.easy_pay.Model.ModuleCompetitor;
import invent.easy_pay.R;


/**
 * Created by kahfi on 2/10/2017.
 */

public class CompetitorActivityFotoDisplayAdapter extends RecyclerView.Adapter<CompetitorActivityFotoDisplayAdapter.MyViewHolder> {

    private ArrayList<ModuleCompetitor> moduleCompetitorArrayList;
    Context context;
    OnItemClickListener mItemClickListener;
    OnLongClickListener mItemLongClickListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        View mView;
        MaterialRippleLayout mr_foto_display_item;
        TextView tvImageType,
                tvImageDate;
        public MyViewHolder(View view) {
            super(view);
            mView = view;
            mr_foto_display_item = (MaterialRippleLayout)view.findViewById(R.id.mr_foto_display_item);
            tvImageType = (TextView)view.findViewById(R.id.tvImageType);
            tvImageDate = (TextView)view.findViewById(R.id.tvImageDate);


        }
    }


    public CompetitorActivityFotoDisplayAdapter(Context context, ArrayList<ModuleCompetitor> moduleCompetitorArrayList) {
        this.moduleCompetitorArrayList = moduleCompetitorArrayList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_image, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ModuleCompetitor moduleCompetitor = moduleCompetitorArrayList.get(position);

        holder.tvImageType.setText(moduleCompetitor.getCompetitorProductID());
        holder.tvImageDate.setText(moduleCompetitor.getCreatedDate());

        holder.mr_foto_display_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return moduleCompetitorArrayList.size();
    }


    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListener(final OnLongClickListener mItemLongClickListener) {
        this.mItemLongClickListener = mItemLongClickListener;
    }

}
