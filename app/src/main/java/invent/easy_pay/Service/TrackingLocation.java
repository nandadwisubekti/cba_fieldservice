package invent.easy_pay.Service;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.LocationListener;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.MyLocationListener;
import invent.easy_pay.Interface.ConnectionInterface;
import invent.easy_pay.Model.ModelGetLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kahfizain on 14/09/2017.
 */

public class TrackingLocation extends BroadcastReceiver implements LocationListener {


    //----location
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
    LocationManager locationManager;

    Context context;

    DatabesHelper databesHelper;

    ConnectionClient connectionClient;
    ConnectionInterface apiService;

    @Override
    public void onReceive(final Context context, Intent intent) {

        this.context = context;
        databesHelper = new DatabesHelper(context);
        connectionClient = new ConnectionClient(context);
        apiService = connectionClient.getApiService();

        //----MyLocation
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                new MyLocationListener()
        );

        SimpleDateFormat srcDf = new SimpleDateFormat("yyyyMMddHHmmss");
        String dateId = srcDf.format(new Date());
        SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = fdate.format(new Date());

        String EmployeeID = databesHelper.getEmployeeID();
        String vehiclenId = databesHelper.getVehicleNumber(EmployeeID);
        String BranchID = databesHelper.getBranchID();
        String getflagCheckIn = databesHelper.getCheckIn();
        String isStart = databesHelper.getIsStart();
        String isFinish = databesHelper.getIsFinish();

        if (vehiclenId == null){
            vehiclenId = "";
        }
        String  HLid = EmployeeID+"_"+dateId+"";
        if (isStart !=null && isFinish !=null) {
            if (isStart.equals("1") && isFinish.equals("1")) {

            }else {

                ModelGetLocation mobileLocation = getMobileLocation();

                if (mobileLocation.getLatitude() !=null && mobileLocation.getLongitude() !=null){
                    if (mobileLocation.getLatitude().equals("0.0") && mobileLocation.getLongitude().equals("0,0")){

                    }else {

                        String flagCheckIn="False";
                        if (getflagCheckIn !=null){
                            if(!getflagCheckIn.equals("0")){
                                flagCheckIn="True";
                            }
                        }


                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("TrackingID",HLid);
                        jsonObject.addProperty("VehicleID", vehiclenId);
                        jsonObject.addProperty("EmployeeID", EmployeeID);
                        jsonObject.addProperty("Latitude", mobileLocation.getLatitude());
                        jsonObject.addProperty("Longitude", mobileLocation.getLongitude());
                        jsonObject.addProperty("BranchID", BranchID);
                        jsonObject.addProperty("TrackingDate", date);
                        jsonObject.addProperty("FlagCheckIn", flagCheckIn);

                        final JsonObject[] object = new JsonObject[1];

                        final Call<JsonObject> mUpload = apiService.postUploadLiveTrackingOnlien(jsonObject);
                        mUpload.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if(response.isSuccessful()) {
                                    object[0] = response.body();
                                    // array = object.getAsJsonArray("ResultList");

                                }
                            }


                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });
                    }
                }



            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public Location getLocation() {
        boolean isGPSEnabled = false, isNetworkEnabled = false;
        Location location = null;
        try {
          /*  locationManager = (LocationManager)getBaseContext()
                    .getSystemService(LOCATION_SERVICE);*/

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
//                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return location;
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MINIMUM_TIME_BETWEEN_UPDATES,
                            MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            return location;
                            /*latitude = location.getLatitude();
                            longitude = location.getLongitude();*/
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MINIMUM_TIME_BETWEEN_UPDATES,
                                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                return location;
                                /*latitude = location.getLatitude();
                                longitude = location.getLongitude();*/
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    //cek radius
    protected ModelGetLocation getMobileLocation() {

//        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location location = getLocation();
        ModelGetLocation mobileLocation = new ModelGetLocation();
        if (location == null)
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return mobileLocation;
            }location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (location != null) {
            String message = String.format(
                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );


            mobileLocation.setLongitude(String.valueOf(location.getLongitude()));
            mobileLocation.setLatitude(String.valueOf(location.getLatitude()));


        }
        return mobileLocation;
    }

}
