/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invent.easy_pay.Service;


/*import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;*/

@Deprecated
public class MyFirebaseMessagingService /*extends FirebaseMessagingService */{

   /* private static final String TAG = "MyFirebaseMsgService";
    DatabesHelper helper;
    String IdPicker,IdTransaction,NamePicker,Phone,Status,Barcode,ActualAmount,title,notice;


    *//**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     *//*
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        //Log.d(TAG, "From: " + remoteMessage.getFrom());
       // Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        String from = remoteMessage.getData().get("title");
        String notificationid = remoteMessage.getData().get("notificationid");
        //title = remoteMessage.getNotification().getTitle();
      //  notice = remoteMessage.getData().get("DAILYMESSAGE");

        if (from !=null && notificationid!=null){
            if (from.equalsIgnoreCase("DAILYMESSAGE")){
                // new DownloadNotificationDailyMessage(getApplicationContext());
            }

            if (from.equalsIgnoreCase("KONFIGURASIUSER")){
                // new DownloadNotifcationUserConfigServer(getApplicationContext(),from,notificationid);
            }

            if (from.equalsIgnoreCase("DOWNLOADALL")){
           *//* Intent intent = new Intent(this, ActivityLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("DOWNLOADALL",from);
            intent.putExtra("notificationid",notificationid);
            startActivity(intent);*//*
            }


            if (from.equalsIgnoreCase("CLEARDATA")){
            *//*Intent intent = new Intent(this, ActivityLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("CLEARDATA",from);
            intent.putExtra("notificationid",notificationid);
            startActivity(intent);*//*
            }


            //////////NON AKTIV KNOX//////////////////
            if (from.equals("NONACTIVATEKNOX")){
           *//* Intent intent = new Intent(this, ActivityLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("NONACTIVATEKNOX",from);
            intent.putExtra("notificationid",notificationid);
            startActivity(intent);*//*

            }


            if (from.equalsIgnoreCase("KONFIGURASIMOBILE")){
                //new DownloadNotifcationMobailConfigServer(getApplicationContext(),from,notificationid);
            }

            sendNotification(from,notificationid);

        }



    }

    // [END receive_message]

    *//**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     *//*
    private void sendNotification(String messageBody, String notificationid) {


        //////////DAILYMESSAGE//////////////////
        if (messageBody.equals("DAILYMESSAGE")){
          *//*  Intent intent = new Intent(this, ActivityHome.class);
            // intent.putExtra("DAILYMESSAGE",notice);
            new DownloadNotificationDailyMessage(getApplicationContext());
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//**//* Request code *//**//*, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("DAILY MESSAGE AVIAN")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 *//**//* ID of notification *//**//*, notificationBuilder.build());*//*

        }


        //////////KONFIGURASIUSER//////////////////
        if (messageBody.equals("KONFIGURASIUSER")){
           *//* Intent intent = new Intent(this, ActivityLogin.class);
            // intent.putExtra("DAILYMESSAGE",notice);
            new DownloadNotifcationUserConfigServer(getApplicationContext(),messageBody,notificationid);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0  *//**//*Request code *//**//*, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("KONFIGURASI USER CONFIG AVIAN")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 *//**//* ID of notification*//**//* , notificationBuilder.build());*//*

        }


        //////////DOWNLOADALL//////////////////
        if (messageBody.equals("DOWNLOADALL")){

          *//*  Intent intent = new Intent(this, ActivityLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("DOWNLOADALL",messageBody);
            intent.putExtra("notificationid",notificationid);
            startActivity(intent);*//*


        }



        //////////DOWNLOADALL//////////////////
        if (messageBody.equals("CLEARDATA")){

         *//*   Intent intent = new Intent(this, ActivityLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("CLEARDATA",messageBody);
            intent.putExtra("notificationid",notificationid);
            startActivity(intent);*//*


        }



        //////////NON AKTIV KNOX//////////////////
        if (messageBody.equals("NONACTIVATEKNOX")){

           *//* Intent intent = new Intent(this, ActivityLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("NONACTIVATEKNOX",messageBody);
            intent.putExtra("notificationid",notificationid);
            startActivity(intent);*//*


        }


        //////////KONFIGURASIMOBILE//////////////////
        if (messageBody.equals("KONFIGURASIMOBILE")){
          *//*  Intent intent = new Intent(this, ActivityLogin.class);
            // intent.putExtra("DAILYMESSAGE",notice);
            new DownloadNotifcationMobailConfigServer(getApplicationContext(),messageBody,notificationid);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0  *//**//*Request code *//**//*, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("KONFIGURASI MOBILE CONFIG AVIAN")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 *//**//* ID of notification*//**//* , notificationBuilder.build());*//*

        }




    }

*/



}
