package invent.easy_pay.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonObject;

/**
 * Created by INVENT on 8/1/2018.
 */

public class ResultSurveyDetail implements Parcelable {
    String ResultSurveyID;
    String QuestionID;
    String QuestionSetID;

    String AnswerID;
    String Value;
    String IsSave;


    public final static String TableName = "ResultSurveyDetail";

    public ResultSurveyDetail(){}

    protected ResultSurveyDetail(Parcel in) {
        ResultSurveyID = in.readString();
        QuestionID = in.readString();
        QuestionSetID = in.readString();
        AnswerID = in.readString();
        Value = in.readString();
        IsSave = in.readString();
    }

    public static final Creator<ResultSurveyDetail> CREATOR = new Creator<ResultSurveyDetail>() {
        @Override
        public ResultSurveyDetail createFromParcel(Parcel in) {
            return new ResultSurveyDetail(in);
        }

        @Override
        public ResultSurveyDetail[] newArray(int size) {
            return new ResultSurveyDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ResultSurveyID);
        parcel.writeString(QuestionID);
        parcel.writeString(QuestionSetID);
        parcel.writeString(AnswerID);
        parcel.writeString(Value);
        parcel.writeString(IsSave);
    }


    public enum Constant{
        ResultSurveyID,
        QuestionID,
        QuestionSetID,
        AnswerID,
        Value,
        IsSave
    }

    public String getQuestionSetID() {
        return QuestionSetID;
    }

    public void setQuestionSetID(String questionSetID) {
        QuestionSetID = questionSetID;
    }

    public String getResultSurveyID() {
        return ResultSurveyID;
    }

    public void setResultSurveyID(String resultSurveyID) {
        ResultSurveyID = resultSurveyID;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getIsSave() {
        return IsSave;
    }

    public void setIsSave(String isSave) {
        IsSave = isSave;
    }

    public JsonObject toParams() {
        JsonObject object = new JsonObject();
        object.addProperty("SurveyID", getResultSurveyID());
        object.addProperty("QuestionID", getQuestionID());
        object.addProperty("AnswerID", getAnswerID());
        object.addProperty("Value", getValue());
        return object;
    }

}
