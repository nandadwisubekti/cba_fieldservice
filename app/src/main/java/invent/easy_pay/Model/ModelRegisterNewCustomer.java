package invent.easy_pay.Model;

/**
 * Created by kahfizain on 23/03/2018.
 */

public class ModelRegisterNewCustomer {

    String RegisterID;
    String EmployeeID;
    String VisitID;
    String Date;
    String IsSave;
    String IsUpload;
    String Value;
    String QuestionID;
    String AnswerID;

    public String getQuestionCategoryID() {
        return QuestionCategoryID;
    }

    public void setQuestionCategoryID(String questionCategoryID) {
        QuestionCategoryID = questionCategoryID;
    }

    String QuestionCategoryID;

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    String CustomerID;

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    String Latitude;
    String Longitude;

    public String getRegisterID() {
        return RegisterID;
    }

    public void setRegisterID(String registerID) {
        RegisterID = registerID;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getIsSave() {
        return IsSave;
    }

    public void setIsSave(String isSave) {
        IsSave = isSave;
    }

    public String getIsUpload() {
        return IsUpload;
    }

    public void setIsUpload(String isUpload) {
        IsUpload = isUpload;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }


}
