package invent.easy_pay.Model;

/**
 * Created by kahfizain on 20/09/2017.
 */

public class ModelCallPlanDetail {

    public String CallPlanID;
    public String CustomerID;
    public String Sequence;
    public String WarehouseID;
    public String Time;

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }


    public String getCallPlanID() {
        return CallPlanID;
    }

    public void setCallPlanID(String callPlanID) {
        CallPlanID = callPlanID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public String getWarehouseID() {
        return WarehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        WarehouseID = warehouseID;
    }


}
