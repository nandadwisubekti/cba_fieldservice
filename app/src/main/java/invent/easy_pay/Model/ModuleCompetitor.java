package invent.easy_pay.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModuleCompetitor {

    private String CompetitorID;
    private String CompetitorName;
    private String ActivityID;
    private String ActivityName;
    private String CompetitorProductID;
    private String CompetitorProductName;
    private String NormalPrice;
    private String PromoPrice;
    private String VisitID;
    private String EmployeeID;
    private String CustomerID;
    private String CreatedDate;
    private String isSave;
    private String Notes;
    private String PromoID;
    private String isStatus;
    private String CompetitorActivityTypeID;
    private String CompetitorActivityImageID;
    private String Image;

    public String getCompetitorActivityImageID() {
        return CompetitorActivityImageID;
    }

    public void setCompetitorActivityImageID(String competitorActivityImageID) {
        CompetitorActivityImageID = competitorActivityImageID;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }


    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    private String Description;



    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    private String Value;


    public String getCompetitorActivityTypeID() {
        return CompetitorActivityTypeID;
    }

    public void setCompetitorActivityTypeID(String competitorActivityTypeID) {
        CompetitorActivityTypeID = competitorActivityTypeID;
    }


    public String getPromoID() {
        return PromoID;
    }

    public void setPromoID(String promoID) {
        PromoID = promoID;
    }

    public String getIsStatus() {
        return isStatus;
    }

    public void setIsStatus(String isStatus) {
        this.isStatus = isStatus;
    }


    public String getVisitID() {
        return VisitID;
    }

    public void setVisitID(String visitID) {
        VisitID = visitID;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }




    public String getIsSave() {
        return isSave;
    }

    public void setIsSave(String isSave) {
        this.isSave = isSave;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }






    public String getNormalPrice() {
        return NormalPrice;
    }

    public void setNormalPrice(String normalPrice) {
        NormalPrice = normalPrice;
    }

    public String getPromoPrice() {
        return PromoPrice;
    }

    public void setPromoPrice(String promoPrice) {
        PromoPrice = promoPrice;
    }

    public String getCompetitorID() {
        return CompetitorID;
    }

    public void setCompetitorID(String competitorID) {
        CompetitorID = competitorID;
    }

    public String getCompetitorName() {
        return CompetitorName;
    }

    public void setCompetitorName(String competitorName) {
        CompetitorName = competitorName;
    }

    public String getActivityID() {
        return ActivityID;
    }

    public void setActivityID(String activityID) {
        ActivityID = activityID;
    }

    public String getActivityName() {
        return ActivityName;
    }

    public void setActivityName(String activityName) {
        ActivityName = activityName;
    }

    public String getCompetitorProductID() {
        return CompetitorProductID;
    }

    public void setCompetitorProductID(String competitorProductID) {
        CompetitorProductID = competitorProductID;
    }

    public String getCompetitorProductName() {
        return CompetitorProductName;
    }

    public void setCompetitorProductName(String competitorProductName) {
        CompetitorProductName = competitorProductName;
    }






}
