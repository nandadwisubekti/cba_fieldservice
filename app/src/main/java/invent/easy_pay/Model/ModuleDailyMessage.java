package invent.easy_pay.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModuleDailyMessage {




    private String MessageID;
    private String MessageName;
    private String MessageDesc;
    private String MessageImg;
    private String DATE;
    private String CreatedBy;

    public String getMessageID() {
        return MessageID;
    }

    public void setMessageID(String messageID) {
        MessageID = messageID;
    }

    public String getMessageName() {
        return MessageName;
    }

    public void setMessageName(String messageName) {
        MessageName = messageName;
    }

    public String getMessageDesc() {
        return MessageDesc;
    }

    public void setMessageDesc(String messageDesc) {
        MessageDesc = messageDesc;
    }

    public String getMessageImg() {
        return MessageImg;
    }

    public void setMessageImg(String messageImg) {
        MessageImg = messageImg;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }













}
