package invent.easy_pay.Model;

/**
 * Created by kahfizain on 28/07/2017.
 */

public class ModelGetLocation {


    public String Latitude;
    public String Longitude;

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}
