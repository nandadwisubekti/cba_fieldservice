package invent.easy_pay.Model;

/**
 * Created by kahfizain on 22/03/2018.
 */

public class ModelAnswer {


    String AnswerID;
    String AnswerText;
    String QuestionID;
    String SubQuestion;
    String IsSubQuestion;
    String Sequence;
    String NoVarchar;
    String IsActive;

    public boolean isStatus() {
        return IsStatus;
    }

    public void setStatus(boolean status) {
        IsStatus = status;
    }

    boolean IsStatus;

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getAnswerText() {
        return AnswerText;
    }

    public void setAnswerText(String answerText) {
        AnswerText = answerText;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getSubQuestion() {
        return SubQuestion;
    }

    public void setSubQuestion(String subQuestion) {
        SubQuestion = subQuestion;
    }

    public String getIsSubQuestion() {
        return IsSubQuestion;
    }

    public void setIsSubQuestion(String isSubQuestion) {
        IsSubQuestion = isSubQuestion;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public String getNoVarchar() {
        return NoVarchar;
    }

    public void setNoVarchar(String noVarchar) {
        NoVarchar = noVarchar;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }





}
