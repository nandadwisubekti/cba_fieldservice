package invent.easy_pay.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kahfizain on 20/03/2018.
 */

public class ModelQuestion implements Parcelable {

    String Section;
    String QuestionID;
    String QuestionText;
    String AnswerTypeID;
    String IsSubQuestion;
    String Sequence;
    String QuestionSetID;
    String QuestionCategoryID;
    String AnswerID;
    String IsActive;

    public ModelQuestion(){}

    protected ModelQuestion(Parcel in) {
        Section = in.readString();
        QuestionID = in.readString();
        QuestionText = in.readString();
        AnswerTypeID = in.readString();
        IsSubQuestion = in.readString();
        Sequence = in.readString();
        QuestionSetID = in.readString();
        QuestionCategoryID = in.readString();
        AnswerID = in.readString();
        IsActive = in.readString();
        RegisterID = in.readString();
        Mandatory = in.readString();
        value = in.readString();
    }

    public static final Creator<ModelQuestion> CREATOR = new Creator<ModelQuestion>() {
        @Override
        public ModelQuestion createFromParcel(Parcel in) {
            return new ModelQuestion(in);
        }

        @Override
        public ModelQuestion[] newArray(int size) {
            return new ModelQuestion[size];
        }
    };

    public String getRegisterID() {
        return RegisterID;
    }

    public void setRegisterID(String registerID) {
        RegisterID = registerID;
    }

    String RegisterID;


    public String getMandatory() {
        return Mandatory;
    }

    public void setMandatory(String mandatory) {
        Mandatory = mandatory;
    }

    String Mandatory;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    String value;

    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getQuestionText() {
        return QuestionText;
    }

    public void setQuestionText(String questionText) {
        QuestionText = questionText;
    }

    public String getAnswerTypeID() {
        return AnswerTypeID;
    }

    public void setAnswerTypeID(String answerTypeID) {
        AnswerTypeID = answerTypeID;
    }

    public String getIsSubQuestion() {
        return IsSubQuestion;
    }

    public void setIsSubQuestion(String isSubQuestion) {
        IsSubQuestion = isSubQuestion;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public String getQuestionSetID() {
        return QuestionSetID;
    }

    public void setQuestionSetID(String questionSetID) {
        QuestionSetID = questionSetID;
    }

    public String getQuestionCategoryID() {
        return QuestionCategoryID;
    }

    public void setQuestionCategoryID(String questionCategoryID) {
        QuestionCategoryID = questionCategoryID;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Section);
        parcel.writeString(QuestionID);
        parcel.writeString(QuestionText);
        parcel.writeString(AnswerTypeID);
        parcel.writeString(IsSubQuestion);
        parcel.writeString(Sequence);
        parcel.writeString(QuestionSetID);
        parcel.writeString(QuestionCategoryID);
        parcel.writeString(AnswerID);
        parcel.writeString(IsActive);
        parcel.writeString(RegisterID);
        parcel.writeString(Mandatory);
        parcel.writeString(value);
    }
}
