package invent.easy_pay.Model;

/**
 * Created by kahfizain on 15/09/2017.
 */

public class ModelCompetitor {


    private String CompetitorID;
    private String CompetitorName;


    public String getCompetitorID() {
        return CompetitorID;
    }

    public void setCompetitorID(String competitorID) {
        CompetitorID = competitorID;
    }

    public String getCompetitorName() {
        return CompetitorName;
    }

    public void setCompetitorName(String competitorName) {
        CompetitorName = competitorName;
    }



}
