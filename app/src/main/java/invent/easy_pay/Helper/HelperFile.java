package invent.easy_pay.Helper;

import android.content.Context;

import java.io.File;
import java.io.IOException;

/**
 * Created by INVENT on 8/1/2018.
 */

public class HelperFile {
    public static File createFolderLocalCamera(Context context) throws IOException {
        File folderCacheImage = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/local_image/");
        File folderYear = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/local_image/"+HelperNamed.getYearString());
        File folderMonth = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/local_image/"+HelperNamed.getYearString()+"/"+HelperNamed.getMonthString());
        File folderDay = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/local_image/"+HelperNamed.getYearString()+"/"+HelperNamed.getMonthString()+"/"+HelperNamed.getDayString());
        if (!folderCacheImage.exists()) {
            folderCacheImage.mkdirs();
        }
        if (!folderYear.exists()) {
            folderYear.mkdirs();
        }
        if (!folderMonth.exists()) {
            folderMonth.mkdirs();
        }
        if (!folderDay.exists()) {
            folderDay.mkdirs();
        }
        return  new File(context.getExternalFilesDir(null).getAbsolutePath()+"/local_image/"+HelperNamed.getYearString()+"/"+HelperNamed.getMonthString()+"/"+HelperNamed.getDayString()+"/"+HelperNamed.getFileNameUniq()+".jpg/") ;
    }
    public static File createFolderCacheCameraJPG(Context context, String EmployeeID) throws IOException {
        File folderCacheImage = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/cache_image/");
        if (!folderCacheImage.exists()) {
            folderCacheImage.mkdirs();
        }

        File folderCacheImageCompress = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/cache_image_compress/");
        if (!folderCacheImageCompress.exists()) {
            folderCacheImageCompress.mkdirs();
        }
        return  new File(context.getExternalFilesDir(null).getAbsolutePath()+"/cache_image/"+EmployeeID+HelperNamed.getFileNameUniq()+".jpg/") ;
    }

    public static File createFileNameCacheCameraJPG(Context context, String imageName){
        return  new File(context.getExternalFilesDir(null).getAbsolutePath()+"/cache_image/"+imageName+".jpg/") ;
    }

    public static File createFileNameCacheCompressCameraJPG(Context context, String imageName){
        return  new File(context.getExternalFilesDir(null).getAbsolutePath()+"/cache_image_compress/"+imageName+".jpg/") ;
    }


    public static boolean deleteFolderCacheCamera(Context context){
        File folderCacheImage = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/cache_image/");
        return folderCacheImage.delete();
    }
}
