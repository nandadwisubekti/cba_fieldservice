package invent.easy_pay.Helper;

import android.content.Context;

/**
 * Created by kahfizain on 02/08/2017.
 */

public class GeneralDate {


    Context context;

    public GeneralDate(Context context){
        this.context = context;
    }


    public String monthReplacer(String month) {
        String replacedMonth = "";
        if (month.contains("0")) {
            replacedMonth = "Januari";
        } else if (month.contains("1")) {
            replacedMonth = "Februari";
        } else if (month.contains("2")) {
            replacedMonth = "Maret";
        } else if (month.contains("3")) {
            replacedMonth = "April";
        } else if (month.contains("4")) {
            replacedMonth = "Mei";
        } else if (month.contains("5")) {
            replacedMonth = "Juni";
        } else if (month.contains("6")) {
            replacedMonth = "Juli";
        } else if (month.contains("7")) {
            replacedMonth = "Agustus";
        } else if (month.contains("8")) {
            replacedMonth = "September";
        } else if (month.contains("9")) {
            replacedMonth = "Oktober";
        } else if (month.contains("10")) {
            replacedMonth = "November";
        } else if (month.contains("11")) {
            replacedMonth = "Desember";
        }
        return replacedMonth;
    }
}
