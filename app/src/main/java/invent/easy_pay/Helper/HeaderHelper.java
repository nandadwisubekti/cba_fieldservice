package invent.easy_pay.Helper;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DigitalClock;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.anton46.stepsview.StepsView;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.Calendar;

import invent.easy_pay.R;


/**
 * Created by kahfizain on 26/08/2017.
 */

public class HeaderHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback {

    private static TextView tvToolbartopGps;
    Context context;
    //---------------------
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 100;

    private static final String TAG = "HeaderHelper";


    public HeaderHelper(Context context){

        this.context = context;
        peringanGps(context);
    }


    public static void setToolbarTop(final View view, final Context context) {

        DigitalClock analog_clock;
        TextView tv_toolbartop_date;
        int year, month, day,dayOfWeek;
        int monthTest;
        String weekDay = null;

        //------Calendar Today--------
        Calendar calendar = Calendar.getInstance();
        //monthTest = calendar.getTime().getMonth();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        dayOfWeek  = calendar.get(Calendar.DAY_OF_WEEK);
        if (Calendar.MONDAY == dayOfWeek) {
            weekDay = "Senin";
        } else if (Calendar.TUESDAY == dayOfWeek) {
            weekDay = "Selasa";
        } else if (Calendar.WEDNESDAY == dayOfWeek) {
            weekDay = "Rabu";
        } else if (Calendar.THURSDAY == dayOfWeek) {
            weekDay = "Kamis";
        } else if (Calendar.FRIDAY == dayOfWeek) {
            weekDay = "Jumat";
        } else if (Calendar.SATURDAY == dayOfWeek) {
            weekDay = "Sabtu";
        } else if (Calendar.SUNDAY == dayOfWeek) {
            weekDay = "Minggu";
        }


        GeneralDate generalDate = new GeneralDate(context);

        analog_clock = (DigitalClock)view.findViewById(R.id.analogclok_toolbartop);
        analog_clock = new  DigitalClock(context);
        tvToolbartopGps = (TextView)view.findViewById(R.id.tv_toolbartop_Gps);
        tv_toolbartop_date = (TextView)view.findViewById(R.id.tv_toolbartop_date);

        TextHelper.setFont(context, tvToolbartopGps, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(context, tv_toolbartop_date, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        tv_toolbartop_date.setText(weekDay+", "+day + " " + generalDate.monthReplacer(String.valueOf(month)) + " " + year);


        CheckWifi(context,view);
        ViewBattery(context,view);
        ViewCekPaketData(context,view);
        ViewGPS(context);


    }


    public static void setToolbarHome(final View view, final Context context) {

        final MaterialRippleLayout mr_btn_home_logout,
                mr_btn_home_menu;
        mr_btn_home_logout = (MaterialRippleLayout)view.findViewById(R.id.mr_btn_home_logout);
        mr_btn_home_menu = (MaterialRippleLayout)view.findViewById(R.id.mr_btn_home_menu);




        mr_btn_home_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(context, mr_btn_home_menu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_main, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_settings:


                                return true;
                            case R.id.action_logout:
                                    DialogHelper.dialogExitAplikasi(context);
                                return true;

                          /*  case R.id.action_screen_brightness:
                                  //  DialogHelper.dialogScreenBrightness(context);
*/
                              //  return true;
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });

        mr_btn_home_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogHelper.dialogLogout(context);
            }
        });


    }

    public static void setTolbarBottomVersion(final View view, final  Context context){

        TextView tv_copyrigh_version = (TextView)view.findViewById(R.id.tv_copyrigh_version);

        TextHelper.setFont(context, tv_copyrigh_version, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());


        try {
            ////////////VERSION APP/////////////////////
            String app_ver = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;

            if (app_ver != null){

                tv_copyrigh_version.setText(context.getResources().getString(R.string.title_copyright)+"\n"+context.getResources().getString(R.string.titel_login_copyrigh_version)+" v."+app_ver);
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    public static void setSteps(final Context context,final View view, int position){

        StepsView stepsView = (StepsView)view.findViewById(R.id.stepsView);

        stepsView.setLabels(context.getResources().getStringArray(R.array.labels))
                .setLabels(context.getResources().getStringArray(R.array.labels))
                .setBarColorIndicator(context.getResources().getColor(R.color.etTextColor))
                .setProgressColorIndicator(context.getResources().getColor(R.color.colorAccent))
                .setLabelColorIndicator(context.getResources().getColor(R.color.tvTitleColor))
                .setCompletedPosition(position)
                .drawView();

    }



    //view Checkwifi
    public static void CheckWifi(Context context,View view){
         WifiInfo wifiInfo = null;       //The obtained Wifi information making
         WifiManager wifiManager = null; //The Wifi manager making
         int level;

        TextView tvToolbartopWifi = (TextView)view.findViewById(R.id.tv_toolbartop_Wifi);

        TextHelper.setFont(context, tvToolbartopWifi, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        wifiInfo = wifiManager.getConnectionInfo();
        //Received signal strength value making
        level = wifiInfo.getRssi();
        //According to the obtained signal intensity of transmitted information making


        if (level <= 0 && level >= -100) {
            tvToolbartopWifi.setVisibility(View.VISIBLE);

        } else if (level < -50 && level >= -70) {
            tvToolbartopWifi.setVisibility(View.VISIBLE);


        } else if (level < -70 && level >= -80) {
            tvToolbartopWifi.setVisibility(View.VISIBLE);


        } else if (level < -80 && level >= -100) {
            tvToolbartopWifi.setVisibility(View.GONE);

        } else {
            tvToolbartopWifi.setVisibility(View.GONE);



        }



    }


    //view Battery
    public static void ViewBattery(Context context, View view) {

        final ImageView iv_toolbartop_Battray = (ImageView)view.findViewById(R.id.iv_toolbartop_Battray);
        final TextView tvToolbartopBattray = (TextView)view.findViewById(R.id.tv_toolbartop_Battray);

        TextHelper.setFont(context, tvToolbartopBattray, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int currentLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int level = -1;
                if (currentLevel >= 0 && scale > 0) {
                    level = (currentLevel * 100) / scale;
                }

                if (level <= 10) {
                    iv_toolbartop_Battray.setImageResource(R.drawable.ic_battery_empty);
                } else if (level > 10 && level <= 30) {
                    iv_toolbartop_Battray.setImageResource(R.drawable.ic_battery_low);
                } else if (level > 30 && level <= 60) {
                    iv_toolbartop_Battray.setImageResource(R.drawable.ic_battery_medium);
                } else if (level > 60 && level <= 70) {
                    iv_toolbartop_Battray.setImageResource(R.drawable.ic_battery_medium_a);
                } else if (level > 60 && level <= 100) {
                    iv_toolbartop_Battray.setImageResource(R.drawable.ic_battery_full);
                }

                tvToolbartopBattray.setText(level + "%");
            }
        };
        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        context.registerReceiver(batteryLevelReceiver, batteryLevelFilter);
    }


    ///CEK PAKET DATA
    public static void ViewCekPaketData(Context context, View view){

        final TextView tvToolbartopKoneksiInternet = (TextView)view.findViewById(R.id.tv_toolbartop_KoneksiInternet);
        TextHelper.setFont(context, tvToolbartopKoneksiInternet, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        ViewMobiledata   viewMobiledata = new ViewMobiledata(context);
        boolean isKoneksiInternet = false;
        isKoneksiInternet = viewMobiledata.viewMobailData();
        if (isKoneksiInternet == true){
            tvToolbartopKoneksiInternet.setText("Data ");
        }

        boolean isAirplaneModeOn = false;
        isAirplaneModeOn = viewMobiledata.isAirplaneModeOn();
        if (isAirplaneModeOn == true){
            tvToolbartopKoneksiInternet.setText("Airplane mode ");
        }
    }


    //view Gps
    public static void ViewGPS(Context context) {
        LocationManager alm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


        if (alm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
            //  Toast.makeText(this, "GPS is already on", Toast.LENGTH_LONG).show();
            tvToolbartopGps.setVisibility(View.VISIBLE);

        } else {
            tvToolbartopGps.setVisibility(View.GONE);


        }

    }


    //PERIGATAN GPS
    public void peringanGps(Context context){
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
    }

    @Override
    public void onConnected(Bundle bundle) {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(Result result) {
        final Status status = result.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;
                tvToolbartopGps.setVisibility(View.VISIBLE);


                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult((Activity) context, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }






}
