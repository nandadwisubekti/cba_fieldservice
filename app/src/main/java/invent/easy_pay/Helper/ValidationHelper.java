package invent.easy_pay.Helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.location.LocationListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.IllegalFormatCodePointException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import invent.easy_pay.Model.ModelGetLocation;
import invent.easy_pay.Model.ModelQuestion;
import invent.easy_pay.R;

/**
 * Created by kahfizain on 27/08/2017.
 */

public class ValidationHelper implements LocationListener {


    Context context;
    //----location
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
    LocationManager locationManager;
    private static final String TAG = "ValidationHelper";


    public ValidationHelper(Context context){

        this.context = context;
        //----MyLocation
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                new MyLocationListener()
        );


    }

    //FORMAT CURRENCY
    public static String formatCurrency(Context context, String isValue){
        DecimalFormat formatter;
        DecimalFormatSymbols symbols;
        String value = null;

        formatter = new DecimalFormat("#,###,###");
        symbols = new DecimalFormatSymbols(new Locale("ind", "in"));
        formatter.setDecimalFormatSymbols(symbols);
        value = formatter.format(Long.valueOf(isValue));

        return value;
    }



    //validsi IP
    public static boolean validateIP(String ip){

        boolean isTrueIP = false;

        Matcher matcherIpConfig = IP_ADDRESS.matcher(ip);
        if (matcherIpConfig.matches()) {

            isTrueIP = true;
        } else {
            isTrueIP = false;


        }

        return isTrueIP;
    }

    private static final Pattern IP_ADDRESS = Pattern.compile(
            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9]))");


    //format date
    public static String dateFormat(){
        String date = null;

            SimpleDateFormat srcDf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
            date = srcDf.format(new Date());


        return date;
    }

    //format date
    public static String date(){
        String date = null;

        SimpleDateFormat srcDf = new SimpleDateFormat("yyy-MM-dd");
        date = srcDf.format(new Date());


        return date;
    }

    //format tiem
    public static String timeFormat(){
        String date = null;

        SimpleDateFormat srcDf = new SimpleDateFormat("HH:mm:ss");
        date = srcDf.format(new Date());

        return date;
    }

    //check time toko
    public static boolean checkTimeToko(Context context,String timeToko){
        boolean isSuccess = false;

        DatabesHelper databesHelper = new DatabesHelper(context);

        String timeMobileConfig =  databesHelper.getMobileConfig(Constant.TIMETOLERANCE);
        String timeNow = ValidationHelper.timeFormat();

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        Date dateToko = null;
        try {
            dateToko = dateFormat.parse(timeToko);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        Date dateNow = null;
        try {
            dateNow = dateFormat.parse(timeNow);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        long timeDiff = Math.abs(dateToko.getTime() - dateNow.getTime()) / 1000;

        int detikConfig = Integer.valueOf(timeMobileConfig) * 60;
        if(timeDiff>detikConfig)
        {
            //tidak sesuai waktu toko + toleransi
           // Toast.makeText(context,  "Tidak sesuai waktu toko + toleransi", Toast.LENGTH_LONG).show();
            isSuccess = false;

        }
        else
        {
            //sesuai dengan waktu toko + toleransi
            //Toast.makeText(context,  "Sesuai dengan waktu toko + toleransi", Toast.LENGTH_LONG).show();

            isSuccess = true;

        }

        return isSuccess;
    }

    //get DeviceID
    public static String getMacAddrss() {

        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }


    public static String versionApp(Context context){
        String app_ver = null;

        try {
            ////////////VERSION APP/////////////////////
             app_ver = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;

        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.e(TAG, e.getMessage());
        }
        return app_ver;
    }


    public static String replaceAllFormatRP(String Promo_price){

        if (Promo_price.contains(",")){
            Promo_price = Promo_price.replaceAll(",", "");
        }else if( Promo_price.contains(".")){
            Promo_price = Promo_price.replaceAll("\\.", "");
        }

        return  Promo_price;
    }


    //GET LAT LONG
    @Override
    public void onLocationChanged(Location location) {

    }

    public Location getLocation() {
        boolean isGPSEnabled = false, isNetworkEnabled = false;
        Location location = null;
        try {
          /*  locationManager = (LocationManager)getBaseContext()
                    .getSystemService(LOCATION_SERVICE);*/

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
//                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return location;
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MINIMUM_TIME_BETWEEN_UPDATES,
                            MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            return location;
                            /*latitude = location.getLatitude();
                            longitude = location.getLongitude();*/
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MINIMUM_TIME_BETWEEN_UPDATES,
                                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                return location;
                                /*latitude = location.getLatitude();
                                longitude = location.getLongitude();*/
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    //GET LAT LONG
    public ModelGetLocation getMobileLocation() {

         // Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location location = getLocation();
        ModelGetLocation mobileLocation = new ModelGetLocation();
        if (location == null)
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return mobileLocation;
            }location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (location != null) {
            String message = String.format(
                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );

            mobileLocation.setLatitude(String.valueOf(location.getLatitude()));
            mobileLocation.setLongitude(String.valueOf(location.getLongitude()));

        }

        return mobileLocation;
    }


    public static boolean createFolder(Context context) throws IOException {
        //File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "TollCulator");
        File folder = new File(context.getExternalFilesDir(null).getAbsolutePath()+"/"+ Constant.ic_launcher);
        boolean success = false;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            // Do something on success
            success =true;
        } else {
            // Do something else on failure
        }
        return success;
    }

    public static void getIcLauncher(Context context, ImageView imageView){

      //  String urlImage = context.getExternalFilesDir(null).getAbsolutePath() +"/"+ Constant.ic_launcher+"/icon.png";
        //String myPath = context.getExternalFilesDir(null).getAbsolutePath()+context.getPackageName()+"/"+ Constant.ic_launcher+"/icon.png";

        // File file = new File(Environment.getExternalStorageDirectory(), context.getPackageName()+"/files/"+ Constant.ic_launcher+"/icon.png");
        String myPath = context.getExternalFilesDir(null).getAbsolutePath()+"/"+Constant.ic_launcher+"/icon.png";

        Picasso.get()
                .load(new File(myPath))
                .placeholder(R.drawable.icon)
                .error(R.drawable.icon)
                .noFade()
                .into(imageView);
    }


    public static boolean validateRegister(ArrayList<ModelQuestion> modelQuestionArrayList, Context context){

        boolean isSuccess = true;
        Gson gson;
        String errorMsg = "";
        boolean isNext = false;
        JsonObject jsonObject = new JsonObject();;
        JsonElement jsonElement;
        JsonArray jsonArray;
        for (int i=0; i<modelQuestionArrayList.size(); i++){

            //validasi jika value string kosing
            if (modelQuestionArrayList.get(i).getValue().equals("")){

                if (modelQuestionArrayList.get(i).getMandatory().equals("1")){
                    errorMsg = "Mohon isi "+modelQuestionArrayList.get(i).getQuestionText();
                    DialogHelper.dialogRegisterError(context,errorMsg);
                    isSuccess =  false;
                    break;
                }

            }

            if (!modelQuestionArrayList.get(i).getValue().equals("")){

                //validasi phone number
                /*if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.PhoneNumber)){
                    if (modelQuestionArrayList.get(i).getValue().length() < 8 || modelQuestionArrayList.get(i).getValue().length() > 14) {
                        errorMsg = "Format nomor handphone yang Anda masukan salah, harus lebih dari 8 angka";
                        DialogHelper.dialogRegisterError(context,errorMsg);
                        isSuccess =  false;
                        break;
                    }else if (!modelQuestionArrayList.get(i).getValue().toString().substring(0, 2).equals("08")) {
                        errorMsg = "Nomor handphone harus dimulai dengan 08";
                        DialogHelper.dialogRegisterError(context,errorMsg);
                        isSuccess =   false;
                        break;
                    }
                    //validasi email
                }else if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.Email)){
                    if (!ValidationHelper.isEmailValid(modelQuestionArrayList.get(i).getValue().toString())) {
                        errorMsg = "Format email salah";
                        DialogHelper.dialogRegisterError(context,errorMsg);
                        isSuccess =   false;
                        break;
                    }
                }*/

                if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.Email)){
                    if (!ValidationHelper.isEmailValid(modelQuestionArrayList.get(i).getValue().toString())) {
                        errorMsg = "Format email salah";
                        DialogHelper.dialogRegisterError(context,errorMsg);
                        isSuccess =   false;
                        break;
                    }
                }


                //validate check box
                if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.CheckBox)) {

                    errorMsg = "salah satu " +modelQuestionArrayList.get(i).getQuestionText()+" harus di pilih !";
                    //json convert to array list json
                    gson = new Gson();
                    jsonElement =  gson.fromJson(modelQuestionArrayList.get(i).getValue().toString(), JsonElement.class);
                    jsonArray  = jsonElement.getAsJsonArray();
                    if (jsonArray.size()>0){
                        for (int j=0; j<jsonArray.size(); j++){
                            jsonObject = jsonArray.get(j).getAsJsonObject();

                            if (jsonObject.get(Constant.IsStatus).getAsBoolean() == true){
                                isNext = true;
                            }
                        }

                        if (isNext !=true){
                            DialogHelper.dialogRegisterError(context,errorMsg);
                            isSuccess =   false;
                            break;
                        }


                    }
                }

            }




        }


        return isSuccess;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    //check time toko
    public static boolean checkTimeTokoNew(Context context,String timeToko,String jamKunjungan){
        boolean isSuccess = false;

        DatabesHelper databesHelper = new DatabesHelper(context);

        String timeMobileConfig =  databesHelper.getMobileConfig(Constant.TIMETOLERANCE);
        String timeNow = ValidationHelper.timeFormat();

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        Date dateToko = null;
        try {
            dateToko = dateFormat.parse(timeToko);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        Date dateNow = null;
        try {
            dateNow = dateFormat.parse(jamKunjungan);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        long timeDiff = Math.abs(dateToko.getTime() - dateNow.getTime()) / 1000;


        long timeDiffJamkujungan = Math.abs(dateToko.getTime() - dateNow.getTime()) / 1000;

      //  int detikConfig = Integer.valueOf(timeMobileConfig) * 60;
        if(timeDiffJamkujungan>timeDiff)
        {
            //tidak sesuai waktu toko + toleransi
            // Toast.makeText(context,  "Tidak sesuai waktu toko + toleransi", Toast.LENGTH_LONG).show();
            isSuccess = false;

        }
        else
        {
            //sesuai dengan waktu toko + toleransi
            //Toast.makeText(context,  "Sesuai dengan waktu toko + toleransi", Toast.LENGTH_LONG).show();

            isSuccess = true;

        }

        return isSuccess;
    }


}
