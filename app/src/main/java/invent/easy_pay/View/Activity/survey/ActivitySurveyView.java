package invent.easy_pay.View.Activity.survey;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Adapter.AdapterListFoto;
import invent.easy_pay.Adapter.PreviewQuestionAdapter;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.MRuntimePermission;
import invent.easy_pay.Helper.MarshMallowPermission;
import invent.easy_pay.Interface.CallbackFinish;
import invent.easy_pay.Model.ModelQuestion;
import invent.easy_pay.R;

/**
 * Created by INVENT on 8/29/2018.
 */

public class ActivitySurveyView extends AppCompatActivity {


    ImageView image_right;
    RecyclerView recyclerView;
    PreviewQuestionAdapter questionAdapter;
    RecyclerView recyclerView_foto_display;
    AdapterListFoto adapterListFoto;
    AppCompatButton surveyAddImageBtn;

    DatabesHelper databaseHelper;
    RelativeLayout ry_toolbar_back,ry_toolbar_right;
    TextView tv_toolbar_name_module;
    MarshMallowPermission marshMallowPermission;
    MaterialRippleLayout btn_next;


    ArrayList<ModelQuestion> moduleQuestionList = new ArrayList<>();


    RelativeLayout ry_customer_detail;
    TextView tv_name_toko, tv_customerID;

    private MRuntimePermission mPermissionChecker;

    String ResultSurveyID = "";
    String QuestionSetID="";
    String QuestionCategoryID="";
    String Title ="";
    private String VisitID;
    private String CustomerID;

    public ActivitySurveyView() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_add);
//        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            ResultSurveyID = bundle.getString("ResultSurveyID");
            //QuestionCategoryID = bundle.getString("QuestionCategoryID");
            QuestionSetID = bundle.getString("QuestionSetID");
            VisitID = bundle.getString("visitID");
            CustomerID = bundle.getString("customerID");
            Title = bundle.getString("Title");
        }



        databaseHelper = new DatabesHelper(ActivitySurveyView.this);
        marshMallowPermission = new MarshMallowPermission(ActivitySurveyView.this);
        mPermissionChecker = new MRuntimePermission(ActivitySurveyView.this);
        initView(getWindow().getDecorView());
        initEvent();

        getData(false, new CallbackFinish() {
            @Override
            public void finish() {

            }
        });


    }



    private void initView(View view) {

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        ry_toolbar_right = (RelativeLayout) view.findViewById(R.id.ry_toolbar_right);
        recyclerView_foto_display = view.findViewById(R.id.survey_image_recyclerView);
        ry_toolbar_right.setVisibility(View.GONE);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);

        btn_next = (MaterialRippleLayout)view.findViewById(R.id.btn_next);
        btn_next.setVisibility(View.GONE);
        tv_toolbar_name_module.setText(Title);
        image_right = (ImageView) view.findViewById(R.id.image_right);
        image_right.setVisibility(View.GONE);
        surveyAddImageBtn = view.findViewById(R.id.survey_add_image);
        surveyAddImageBtn.setVisibility(View.GONE);

        ArrayList<AdapterListFoto.Model> imageList = databaseHelper.getArraySurveyImage(VisitID, CustomerID, QuestionSetID);

        setupRecyclerViewPhoto(recyclerView_foto_display, imageList);
    }

    private void setupRecyclerViewPhoto(@NonNull final RecyclerView recyclerView, final ArrayList<AdapterListFoto.Model> models) {

        adapterListFoto = new AdapterListFoto(this,models);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(this);
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(MyLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterListFoto);
        adapterListFoto.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

    }


    private void initEvent(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



    }


    private void getData(final boolean isEnable, final CallbackFinish callbackFinish) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                moduleQuestionList.clear();
                moduleQuestionList = databaseHelper.getArrayListResultSurveyDetailWithoutCategory(ResultSurveyID);

                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (moduleQuestionList.size() > 0) {
                    questionAdapter = new PreviewQuestionAdapter(moduleQuestionList);
                    setupRecyclerView(recyclerView, questionAdapter);
                    questionAdapter.notifyDataSetChanged();
                }


                if (callbackFinish != null) {
                    callbackFinish.finish();
                }

            }
        }.execute();

    }


    private void setupRecyclerView(RecyclerView recyclerView, PreviewQuestionAdapter questionAdapter) {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ActivitySurveyView.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }












    @Override
    public boolean dispatchTouchEvent (MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view
                .getClass().getName().startsWith("android.webkit."))
        {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
            {
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
            }
        }

        return super.dispatchTouchEvent(ev);
    }
}
