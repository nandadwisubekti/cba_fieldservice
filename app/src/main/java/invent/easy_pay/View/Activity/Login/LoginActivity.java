package invent.easy_pay.View.Activity.Login;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.Database;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.R;
import invent.easy_pay.View.Fragmentt.FotoDisplay.FotoDisplayAddFragment;
import invent.easy_pay.View.Fragmentt.Login.LoginFragment;
import invent.installupdate.DefaultUpdateWorker;

public class LoginActivity extends AppCompatActivity {

    //LOCKED NOTIFICATION BAR
    //  To keep track of activity's window focus
    boolean currentFocus;
    // To keep track of activity's foreground/background status
    boolean isPaused;
    Handler collapseNotificationHandler;
    //---databes
    DatabesHelper databesHelper;
    private static final String TAG = "LoginActivity";

    Database db;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);



        if (savedInstanceState == null) {

                Bundle bundle = new Bundle();
                //bundle.putSerializable("mapheader", mapArrayList.get(position));
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                LoginFragment fragment = new LoginFragment();
                fragment.setArguments(bundle);
                transaction.replace(R.id.fragment, fragment);
               // transaction.addToBackStack(null);
                transaction.commit();



            }



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:

               onBackPressed();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
         super.onBackPressed();

    }

    @Override
    public void onResume(){
        super.onResume();

        isPaused = false;

        /**
         * siapa tau butuh
         */

        /*DefaultUpdateWorker defaultUpdateWorker = new DefaultUpdateWorker(this, new AlertDialog.Builder(this), getSupportFragmentManager());
        defaultUpdateWorker.checkForUpdate(true, "http://35.240.192.212:8997/LogisticOnline/apk/logol-16-2.1.0.apk", "invent.easy_pay", "logol-16-2.1.0.apk", null);*/
    }



    //CEK VERSION APP
    private void cekVersionApp(){
        try {
            ////////////VERSION APP/////////////////////
            String app_ver = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;

            if (app_ver != null){

                ////////////VERSION DARI DATABES HP/////////////////////
                databesHelper = new DatabesHelper(getApplication());
                String versionMobile = databesHelper.getMobileConfig("APP_VERSION");
                if (versionMobile != null){

                    String arrTempServer[] = versionMobile.split("_");
                    String version_mobile= arrTempServer[2].toString().replace(".apk", "");

                    if (app_ver.equalsIgnoreCase(version_mobile)){
                    }else {
                        if (!checkPermissions()) {
                            requestPermissions();
                        } else {
                            //File file = new File("/sdcard/Android/data/"+this.getPackageName()+"/files/"+Constant.DB_NAME);
                            File file = new File(this.getExternalFilesDir(null).getAbsolutePath()+"/"+ Constant.DB_NAME);
                            boolean deleted = file.delete();
                            if(deleted == true){

                                if (!checkPermissions()) {
                                    requestPermissions();
                                }else {
                                    createDb();
                                    databesHelper = new DatabesHelper(getApplication());
                                }


                            }
                        }


                    }

                }else {
                    if (!checkPermissions()) {
                        requestPermissions();
                    }else {
                        //File file = new File("/sdcard/Android/data/"+this.getPackageName()+"/files/"+Constant.DB_NAME);
                        File file = new File(this.getExternalFilesDir(null).getAbsolutePath()+"/"+ Constant.DB_NAME);

                        boolean deleted = file.delete();
                        if(deleted == true){
                            if (!checkPermissions()) {
                                requestPermissions();
                            }else {
                                createDb();
                                databesHelper = new DatabesHelper(getApplication());
                            }
                        }
                    }

                }

               /* String version= helper.getMobailConfig("APPVERSION");
                tvAppVersion.setText("Cpanel Invent v."+version);*/
            }

        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.e(TAG, e.getMessage());
        }




    }




    //STATUS BAR LOCKEND
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        currentFocus = hasFocus;

        if (!hasFocus) {

            // Method that handles loss of window focus
            collapseNow();
        }
    }

    public void collapseNow() {

        // Initialize 'collapseNotificationHandler'
        if (collapseNotificationHandler == null) {
            collapseNotificationHandler = new Handler();
        }

        // If window focus has been lost && activity is not in a paused state
        // Its a valid check because showing of notification panel
        // steals the focus from current activity's window, but does not
        // 'pause' the activity
        if (!currentFocus && !isPaused) {

            // Post a Runnable with some delay - currently set to 300 ms
            collapseNotificationHandler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    // Use reflection to trigger a method from 'StatusBarManager'
                    @SuppressLint("WrongConstant") Object statusBarService = getSystemService("statusbar");
                    Class<?> statusBarManager = null;

                    try {
                        statusBarManager = Class.forName("android.app.StatusBarManager");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    Method collapseStatusBar = null;

                    try {

                        // Prior to API 17, the method to call is 'collapse()'
                        // API 17 onwards, the method to call is `collapsePanels()`

                        if (Build.VERSION.SDK_INT > 16) {
                            collapseStatusBar = statusBarManager .getMethod("collapsePanels");
                        } else {
                            collapseStatusBar = statusBarManager .getMethod("collapse");
                        }
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }

                    collapseStatusBar.setAccessible(true);

                    try {
                        collapseStatusBar.invoke(statusBarService);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    // Check if the window focus has been returned
                    // If it hasn't been returned, post this Runnable again
                    // Currently, the delay is 100 ms. You can change this
                    // value to suit your needs.
                    if (!currentFocus && !isPaused) {
                        collapseNotificationHandler.postDelayed(this, 100L);
                    }

                }
            }, 300L);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Activity's been paused
        isPaused = true;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onStart() {
        super.onStart();


        if (!checkPermissions()) {
            requestPermissions();
        } else {
            createDb();
            cekVersionApp();

            //create folder and get icon ic_launcher dinamis
            try {
                ValidationHelper.createFolder(LoginActivity.this);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

      /*  createDb();
        cekVersionApp();*/
    }


    //create Db
    private void createDb(){
        db = new Database(this);
        try {
            db.createDatabase();
            db.open();
        } catch (SQLException e) {
        }
    }


    ////////////////////////////////////////////////////////////////////
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS_REQUEST_CODE);

    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
/*
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });*/
            startLocationPermissionRequest();

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
                startLocationPermissionRequest();
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                //getLastLocation();
                createDb();
                cekVersionApp();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
               /* showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });*/
                startLocationPermissionRequest();
            }
        }





    }

}
