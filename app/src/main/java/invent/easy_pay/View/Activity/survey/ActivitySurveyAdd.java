package invent.easy_pay.View.Activity.survey;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import id.zelory.compressor.Compressor;
import invent.easy_pay.Adapter.AdapterListFoto;
import invent.easy_pay.Adapter.QuestionAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HelperFile;
import invent.easy_pay.Helper.MRuntimePermission;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Interface.CallbackFinish;
import invent.easy_pay.Interface.CallbackOnLongClickItemListener;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModelGetLocation;
import invent.easy_pay.Model.ModelQuestion;
import invent.easy_pay.Model.ResultSurvey;
import invent.easy_pay.Model.ResultSurveyDetail;
import invent.easy_pay.R;

public class ActivitySurveyAdd extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 1;
    private static final int REQUEST_PREVIEW = 2;
    RecyclerView recyclerView;
    RecyclerView recyclerView_foto_display;
    AdapterListFoto adapterListFoto;
    AppCompatButton surveyAddImageBtn;

    DatabesHelper databaseHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;
    RelativeLayout ry_customer_detail;
    TextView tv_name_toko, tv_customerID;
    MaterialRippleLayout btn_next;
    String startTime, endTime;

    ArrayList<ModelQuestion> moduleQuestion = new ArrayList<>();

    QuestionAdapter questionAdapter;

    private MRuntimePermission mPermissionChecker;

    ValidationHelper validationHelper;


    String QuestionSetID="";
    String QuestionCategoryID="";
    String Title ="";
    private String visitID;
    private String customerID;
    private String employeeID;
    private File fileImage;
    private String imageFileTempPath;
    private int totalQuestionSet;
    private int totalCompletedQuestionSet;

    public ActivitySurveyAdd() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub

        if(fileImage!=null)
        outState.putString("photopath", fileImage.getAbsolutePath());


        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("photopath")) {
                fileImage = new File(savedInstanceState.getString("photopath"));
            }
        }

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_survey_add);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            QuestionCategoryID = bundle.getString("QuestionCategoryID");
            QuestionSetID = bundle.getString("QuestionSetID");
            Title = bundle.getString("Title");
            visitID = bundle.getString("visitID");
            customerID = bundle.getString("customerID");
            employeeID = bundle.getString("employeeID");
            totalQuestionSet = bundle.getInt("totalQuestionSet");
            totalCompletedQuestionSet = bundle.getInt("totalCompletedQuestionSet");
        }


        validationHelper = new ValidationHelper(getApplicationContext());
        databaseHelper = new DatabesHelper(getApplicationContext());

        initView(getWindow().getDecorView());
        initEvent();

        questionAdapter = new QuestionAdapter(ActivitySurveyAdd.this, moduleQuestion,true, true);

        setupRecyclerView(recyclerView,questionAdapter);
        setupRecyclerViewPhoto(recyclerView_foto_display, new ArrayList<AdapterListFoto.Model>());

        getData(new CallbackFinish() {
            @Override
            public void finish() {

            }
        });

        startTime = ValidationHelper.dateFormat().substring(11, 19);
    }


    private void initView(View view) {

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        btn_next = (MaterialRippleLayout)view.findViewById(R.id.btn_next);
        recyclerView_foto_display = (RecyclerView)view.findViewById(R.id.survey_image_recyclerView);
        surveyAddImageBtn = view.findViewById(R.id.survey_add_image);
        tv_toolbar_name_module.setText(Title);

        surveyAddImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] perms = {
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                };
                checkPermissions(perms);
            }
        });
    }

    private void checkPermissions(String[] perm) {
        if (ContextCompat.checkSelfPermission(this, perm[0]) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, perm[1]) == PackageManager.PERMISSION_GRANTED) {
                takeImageCamera();
            } else {
                // todo: misi mas..
            }
        } else {
            // todo: misi mbak..
        }
    }

    private void takeImageCamera() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            String EmployeeID = databaseHelper.getMobileConfig(Constant.DRIVERID);
            try {
                fileImage = HelperFile.createFolderCacheCameraJPG(getApplicationContext(),EmployeeID);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Uri photoURI = Uri.fromFile(fileImage);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    photoURI);
            if ( Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP ) {
                takePictureIntent.setClipData( ClipData.newRawUri( "", photoURI ) );
                takePictureIntent.addFlags( Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION );
            }
            startActivityForResult(takePictureIntent, REQUEST_CAMERA);
        } else {
            Toast.makeText(this, "Tidak ada aplikasi yang tersedia untuk membuka kamera", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode){
                case REQUEST_CAMERA:
                    try {
                        new Compressor(ActivitySurveyAdd.this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(90)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()+"/cache_image_compress")
                                .compressToFile(fileImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    File fileImageCompress = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()+"/cache_image_compress/"+fileImage.getName());

                    ModelGetLocation modelGetLocation = new ValidationHelper(getApplicationContext()).getMobileLocation();

                    if(fileImageCompress.exists()){
                        fileImage.delete();
                        AdapterListFoto.Model m = new AdapterListFoto.Model();
                        m.setFilePath(fileImageCompress.getAbsolutePath());
                        m.setTime(ValidationHelper.dateFormat());
                        m.setLatitude(modelGetLocation.getLatitude());
                        m.setLongitude(modelGetLocation.getLongitude());
                        adapterListFoto.list.add(m);
                        adapterListFoto.notifyDataSetChanged();
                        //countFoto++;
                    }else{
                        AdapterListFoto.Model m = new AdapterListFoto.Model();
                        m.setFilePath(fileImage.getAbsolutePath());
                        m.setTime(ValidationHelper.dateFormat());
                        m.setLatitude(modelGetLocation.getLatitude());
                        m.setLongitude(modelGetLocation.getLongitude());
                        adapterListFoto.list.add(m);
                        adapterListFoto.notifyDataSetChanged();
                        //countFoto++;
                    }
                    break;


                case REQUEST_PREVIEW:
                    finish();
                    break;
            }


        }
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, QuestionAdapter questionAdapter) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }


    private void setupRecyclerViewPhoto(@NonNull final RecyclerView recyclerView, final ArrayList<AdapterListFoto.Model> models) {

        adapterListFoto = new AdapterListFoto(this,models);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(this);
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(MyLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterListFoto);
        adapterListFoto.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        adapterListFoto.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });

        adapterListFoto.setOnLongClickItemListener(new CallbackOnLongClickItemListener() {
            @Override
            public void onLongClick(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurveyAdd.this);
                builder.setTitle(R.string.dialog_confirmation);
                builder.setMessage("Apakah anda akan menghapus foto ini?")
                        .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapterListFoto.list.remove(position);
                                adapterListFoto.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("Batal", null).show();
            }
        });

    }



    private void initEvent(){

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //saveSurveyAnswer();
                if(validateSurvey()) previewSurveyAnswer();
            }
        });

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurveyAdd.this);
                builder.setTitle(R.string.dialog_confirmation);
                builder.setMessage("Jika halaman ini ditutup maka data form akan di buang")
                        .setPositiveButton("Tutup Halaman", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("Batal", null).show();


            }
        });


    }

    private Boolean validateSurvey() {
        Boolean valid = true;
        /**
         * ini buat ngecek pertanyaan apa yg mandatory gitu
         */
        for(int i=0;i<moduleQuestion.size();i++){
            if(moduleQuestion.get(i).getMandatory().equals("1") && moduleQuestion.get(i).getValue().equals("")){
                valid = false;
                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(this).create();
                alertDialog.setTitle("Survey Belum Lengkap");
                alertDialog.setMessage("Silakan Isi Pertanyaan '"+moduleQuestion.get(i).getQuestionText()+"'");
                alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }

        return valid;
    }


    public void getData(final CallbackFinish callbackFinish){
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(callbackFinish!=null){
                    questionAdapter.notifyDataSetChanged();
                    callbackFinish.finish();
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                ArrayList<ModelQuestion> customer = databaseHelper.getArrayListQuestionWithoutCategory(QuestionSetID);
                for(ModelQuestion m : customer){
                    moduleQuestion.add(m);
                }

                return null;
            }
        }.execute();
    }



    @Override
    public void onResume(){
        super.onResume();


    }

    private void previewSurveyAnswer(){
        boolean isSuccess = ValidationHelper.validateRegister(moduleQuestion,ActivitySurveyAdd.this);
        if (isSuccess){
            final ModelGetLocation modelGetLocation = new ValidationHelper(getApplicationContext()).getMobileLocation();


            final String ResultSurveyID = getUniqID("res");

            ResultSurvey resultSurvey = new ResultSurvey();
            resultSurvey.setResultSurveyID(ResultSurveyID);
            resultSurvey.setVisitID(visitID);
            resultSurvey.setBranchID("");
            /**
             * ambil element pertamanya aja karena seharusnya semua question punya category yg sama
             */
            resultSurvey.setQuestionCategoryID(moduleQuestion.get(0).getQuestionCategoryID());
            resultSurvey.setLongitude(modelGetLocation.getLongitude());
            resultSurvey.setLatitude(modelGetLocation.getLatitude());
            resultSurvey.setDate(ValidationHelper.dateFormat().substring(0, 10));
            resultSurvey.setIsUpload("0");
            resultSurvey.setIsSave("");
            resultSurvey.setEmployeeID(employeeID);
            resultSurvey.setCustomerID(customerID);

            ArrayList<ResultSurveyDetail> resultSurveyDetailList = new ArrayList<>();
            for(int i=0;i<moduleQuestion.size();i++){
                ResultSurveyDetail m = new ResultSurveyDetail();
                m.setResultSurveyID(ResultSurveyID);
                m.setQuestionID(moduleQuestion.get(i).getQuestionID());
                m.setQuestionSetID(QuestionSetID);
                m.setAnswerID(moduleQuestion.get(i).getAnswerID());
                m.setValue(moduleQuestion.get(i).getValue());
                m.setIsSave("");
                resultSurveyDetailList.add(m);

            }

            Intent intent = new Intent(getApplicationContext(), ActivitySurveyPreview.class);
            intent.putExtra("startTime", startTime);
            intent.putExtra("questionSetID", QuestionSetID);
            intent.putExtra("resultSurvey",resultSurvey);
            intent.putExtra("questionList",moduleQuestion);
            intent.putExtra("resultSurveyDetail",resultSurveyDetailList);
            intent.putExtra("resultSurveyImage",adapterListFoto.list);
            intent.putExtra("totalQuestionSet", totalQuestionSet);
            intent.putExtra("totalCompletedQuestionSet", totalCompletedQuestionSet);
            startActivityForResult(intent, REQUEST_PREVIEW);

        }
    }

    private void saveSurveyAnswer(){

        boolean isSuccess = ValidationHelper.validateRegister(moduleQuestion,ActivitySurveyAdd.this);
        if (isSuccess){
            final ModelGetLocation modelGetLocation = new ValidationHelper(getApplicationContext()).getMobileLocation();


            final String ResultSurveyID = getUniqID("res");

            ResultSurvey resultSurvey = new ResultSurvey();
            resultSurvey.setResultSurveyID(ResultSurveyID);
            resultSurvey.setVisitID(visitID);
            resultSurvey.setBranchID("");
            resultSurvey.setQuestionCategoryID("");
            resultSurvey.setLongitude(modelGetLocation.getLongitude());
            resultSurvey.setLatitude(modelGetLocation.getLatitude());
            resultSurvey.setDate("");
            resultSurvey.setIsUpload("0");
            resultSurvey.setIsSave("");
            resultSurvey.setEmployeeID(employeeID);
            resultSurvey.setCustomerID(customerID);



            boolean insertResultSurvey = databaseHelper.insertResultSurvey(resultSurvey);

            ArrayList<ResultSurveyDetail> resultSurveyDetailList = new ArrayList<>();
            for(int i=0;i<moduleQuestion.size();i++){
                ResultSurveyDetail m = new ResultSurveyDetail();
                m.setResultSurveyID(ResultSurveyID);
                m.setQuestionID(moduleQuestion.get(i).getQuestionID());
                m.setQuestionSetID(QuestionSetID);
                m.setAnswerID(moduleQuestion.get(i).getAnswerID());
                m.setValue(moduleQuestion.get(i).getValue());
                m.setIsSave("");
                resultSurveyDetailList.add(m);

            }



            boolean insertResultSurveyDetail = databaseHelper.insertResultSurveyDetail(resultSurveyDetailList);


            if(insertResultSurvey && insertResultSurveyDetail ){


                finish();
            }

        }
    }

    public static String getUniqID(String prefix){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String currentTimeStamp = dateFormat.format(new Date());
        return prefix+""+currentTimeStamp;
    }



    @Override
    public boolean dispatchTouchEvent (MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view
                .getClass().getName().startsWith("android.webkit."))
        {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
            {
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
            }
        }

        return super.dispatchTouchEvent(ev);
    }
}
