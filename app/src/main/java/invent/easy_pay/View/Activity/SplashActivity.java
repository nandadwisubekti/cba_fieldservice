package invent.easy_pay.View.Activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.List;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.View.Activity.Login.LoginActivity;
import invent.installupdate.DefaultUpdateWorker;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by kahfizain on 19/02/2018.
 */

public class SplashActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks{


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] perms = {Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.INSTALL_PACKAGES};

        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this,
                    Constant.PERMISSION_WRITE_EXTERNAL_STORAGE_CAMERA_RASSIONALE,
                    Constant.WRITE_EXTERNAL_STORAGE_CAMERA_REQUEST_CODE, perms);
        } else {


            getintent();

        }
    }

    @Override
    void initUI() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }

    @Override
    void initEvent() {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == Constant.WRITE_EXTERNAL_STORAGE_CAMERA_REQUEST_CODE) {
//            setupSplashScreen();
            getintent();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d("PERMISSION", "onPermissionsDenied:" + requestCode + ":" + perms.size());

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
//            new AppSettingsDialog.Builder(this).build().show();
        } else {
            this.finish();
        }
    }



    private void getintent() {
        Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(mainIntent);
        finish();
    }
}
