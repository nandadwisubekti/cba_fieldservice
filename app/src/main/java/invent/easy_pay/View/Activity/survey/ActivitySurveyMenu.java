package invent.easy_pay.View.Activity.survey;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import invent.easy_pay.Adapter.QuestionSetAdapter;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Model.QuestionSet;
import invent.easy_pay.R;

/**
 * Created by kahfizain on 25/08/2017.
 */

public class ActivitySurveyMenu extends AppCompatActivity {


    //Init Adapter
    QuestionSetAdapter adapter;
    DatabesHelper databaseHelper;

    //Init Database

    //Init View
    View additionalInformationContainer;
    AppCompatTextView additionalInfo1, additionalInfo2;
    RecyclerView recyclerView;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_title_data;
    ProgressBar progressBar;
    RelativeLayout ry_customer_detail;
    TextView tv_name_toko, tv_customerID;
    String customerID;
    String visitID;
    String employeeID;

    public ActivitySurveyMenu() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_module_list);


        databaseHelper = new DatabesHelper(getApplicationContext());
        initializeLayout(getWindow().getDecorView());

        /*AdapterMenu.Model m1 = new AdapterMenu.Model();
        m1.setId("0");
        m1.setTvA("Survey Kualitas Harga");
        m1.setType(AdapterMenu.Model.Type.TYPE_A);
        m1.setResIdIconEnable(R.drawable.bg_button_enable);
        m1.setVisibility(true);
        m1.setEnability(true);

        AdapterMenu.Model m2 = new AdapterMenu.Model();
        m2.setId("0");
        m2.setTvA("Survey Produk");
        m2.setType(AdapterMenu.Model.Type.TYPE_A);
        m2.setResIdIconEnable(R.drawable.bg_button_enable);
        m2.setVisibility(true);
        m2.setEnability(true);

        AdapterMenu.Model m3 = new AdapterMenu.Model();
        m3.setId("0");
        m3.setTvA("Survey Salesman");
        m3.setType(AdapterMenu.Model.Type.TYPE_A);
        m3.setResIdIconEnable(R.drawable.bg_button_enable);
        m3.setVisibility(true);
        m3.setEnability(true);



        list.clear();
        list.add(m1);
        list.add(m2);
        list.add(m3);*/

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            customerID = bundle.getString("customerID");
            visitID = bundle.getString("visitID");
            employeeID = bundle.getString("employeeID");
        }

        initEvent();
    }




    private void initializeLayout(View view) {

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_toolbar_name_module.setText("Checklist");
        additionalInformationContainer = view.findViewById(R.id.additional_information_container);
        additionalInfo1 = view.findViewById(R.id.additional_info1);
        additionalInfo2 = view.findViewById(R.id.additional_info2);
        tv_title_data.setVisibility(View.GONE);
    }


    private void initEvent(){


        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
    }




    private void setupRecyclerView() {
        adapter = new QuestionSetAdapter(this, customerID, employeeID);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListener(new QuestionSetAdapter.QuestionSetAdapterOnClickListener() {
            @Override
            public void onQuestionSetItemClick(QuestionSet questionSet, int position, View view) {
                String resultSurveyId = databaseHelper.getResultSurveyIDWithoutCategory(customerID, visitID, questionSet.getId());
                if (resultSurveyId == null) {
                    int totalQuestionSet = adapter.getCount();
                    int totalCompletedQuestionSet = adapter.getCompletedCount();
                    Intent intent = new Intent(getApplicationContext(), ActivitySurveyAdd.class);
                    intent.putExtra("Title", questionSet.getText());
                    intent.putExtra("QuestionSetID", questionSet.getId());
                    intent.putExtra("visitID", visitID);
                    intent.putExtra("customerID", customerID);
                    intent.putExtra("employeeID", employeeID);
                    intent.putExtra("totalQuestionSet", totalQuestionSet);
                    intent.putExtra("totalCompletedQuestionSet", totalCompletedQuestionSet);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), ActivitySurveyView.class);
                    intent.putExtra("Title", questionSet.getText());
                    intent.putExtra("visitID", visitID);
                    intent.putExtra("customerID", customerID);
                    intent.putExtra("QuestionSetID", questionSet.getId());
                    intent.putExtra("employeeID", employeeID);
                    intent.putExtra("ResultSurveyID", resultSurveyId);
                    startActivity(intent);
                }
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
    }



    @Override
    public void onResume(){
        super.onResume();
        setupRecyclerView();
        countCompletedChecklist();
    }

    private void countCompletedChecklist() {
        additionalInformationContainer.setVisibility(View.VISIBLE);
        Double totalQuestionSet = (double) adapter.getCount();
        Double totalCompletedQuestionSet = (double) adapter.getCompletedCount();
        Double percentage = (totalCompletedQuestionSet / totalQuestionSet) * 100d;
        percentage = Math.floor(percentage * 10) / 10;
        additionalInfo1.setText(String.valueOf(percentage) + "%");
        additionalInfo2.setText(String.valueOf(totalCompletedQuestionSet.intValue()) + " dari " + String.valueOf(totalQuestionSet.intValue()) + " checklist telah diisi");
    }


    @Override
    public void onStart(){
        super.onStart();

    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }
}
