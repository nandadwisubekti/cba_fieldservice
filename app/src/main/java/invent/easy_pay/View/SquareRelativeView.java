package invent.easy_pay.View;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by INVENT on 8/30/2018.
 */

public class SquareRelativeView extends RelativeLayout {
    public SquareRelativeView(Context context) {
        super(context);
    }

    public SquareRelativeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareRelativeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //noinspection SuspiciousNameCombination
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
