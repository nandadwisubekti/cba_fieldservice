package invent.easy_pay.View.Fragmentt.FotoDisplay;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Adapter.FotoDisplayAdapter;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.data.SharedPreferenceHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModuleCustomerImage;
import invent.easy_pay.Model.modelGlobal;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class FotoDisplayFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerView_foto_display;
    FotoDisplayAdapter fotoDisplayAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_persiapan_name_title,
            tv_title_data,
            tv_stock_save;

    ProgressBar progressBar;
    MaterialRippleLayout mr_foto_display_add;

    public FotoDisplayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_foto_display, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();

        return v;
    }


    private void initializeLayout(View view) {

        recyclerView_foto_display = (RecyclerView)view.findViewById(R.id.recyclerView_foto_display);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        mr_foto_display_add = (MaterialRippleLayout)view.findViewById(R.id.mr_foto_display_add);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_stock_save = (TextView)view.findViewById(R.id.tv_stock_save);
        tv_toolbar_name_module.setText(R.string.modul_title_persipan_ruete_photo_display);
    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_persiapan_name_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_title_data, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());



    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mr_foto_display_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                FotoDisplayAddFragment fotoDisplayAddFragment = new FotoDisplayAddFragment();
                fotoDisplayAddFragment.setArguments(bundle);
                transaction.replace(R.id.fragment, fotoDisplayAddFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
    }


    private void getStoreList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModuleCustomerImage> moduleCustomerImages = new ArrayList<>();
        moduleCustomerImages.clear();

        databesHelper = new DatabesHelper(getActivity());

        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        String customerId = sharedPreferenceHelper.getCustomerID();
        String visitID= sharedPreferenceHelper.getVisitID();

        moduleCustomerImages = databesHelper.getArrayListCustomerImage(customerId,visitID);

        if (moduleCustomerImages.size()>0){
            setupRecyclerView(recyclerView_foto_display,moduleCustomerImages);
            progressBar.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);

        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModuleCustomerImage> arrayListmoduleStore) {

        fotoDisplayAdapter = new FotoDisplayAdapter(getActivity(),arrayListmoduleStore);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(fotoDisplayAdapter);
        fotoDisplayAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        fotoDisplayAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                    DialogHelper.dialogViewFoto(getActivity(),"Info Foto",arrayListmoduleStore.get(position).getImageBase64());
            }
        });

    }



    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        getStoreList();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
