package invent.easy_pay.View.Fragmentt.RegisterCustomer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import invent.easy_pay.Adapter.QuestionAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.MarshMallowPermission;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Interface.ConnectionInterface;
import invent.easy_pay.Model.ModelQuestion;
import invent.easy_pay.R;
import invent.easy_pay.Service.ConnectionClient;
import invent.easy_pay.View.Activity.RegisterCustomer.RegisterCustomerDataKeuanganActivity;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class RegisterCustomerDataLokasiFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewSettingList;
    QuestionAdapter questionAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;
    MarshMallowPermission marshMallowPermission;
    ConnectionClient connectionClient;
    ConnectionInterface apiService;
    MaterialRippleLayout btn_next;
    ArrayList<ModelQuestion> modelQuestionArrayList = new ArrayList<>();
    SharedPreferences mPrefs;
    String RegisterID;
    public RegisterCustomerDataLokasiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

        mPrefs = getActivity().getSharedPreferences(Constant.PREFS_EASY_PAY, Context.MODE_PRIVATE);
        RegisterID = mPrefs.getString(Constant.PREFS_TAG_NEW_REGISTER_CUSTOMER_ID, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register_customer_data_pribadi, container, false);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setSteps(getActivity(),v,1);
        styleLayout();
        assignListener();
        getQuestion();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerViewSettingList = (RecyclerView)view.findViewById(R.id.recyclerView_setting_list);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        btn_next = (MaterialRippleLayout)view.findViewById(R.id.btn_next);
        tv_toolbar_name_module.setText(R.string.modul_title_data_counter_area);
    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateBtnNext();

            }
        });
    }


    private void getQuestion(){



        modelQuestionArrayList.clear();
        modelQuestionArrayList = databesHelper.getArrayListQuestion(Constant.PAGE_2,"1",Constant.QuestionVisit);
        if (modelQuestionArrayList.size()>0){
            setupRecyclerView(recyclerViewSettingList,modelQuestionArrayList);
        }
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelQuestion> modelQuestionArrayList) {

        questionAdapter = new QuestionAdapter(getActivity(),modelQuestionArrayList,true,true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(questionAdapter);
        questionAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


    private void validateBtnNext(){
        boolean isSuccess = ValidationHelper.validateRegister(modelQuestionArrayList,getActivity());
        if (isSuccess == true){
            if (modelQuestionArrayList.size()>0){

                Gson gson;
                JsonObject jsonObject = new JsonObject();;
                JsonElement jsonElement;
                JsonArray jsonArray;
                //save table
                for (int i=0; i<modelQuestionArrayList.size(); i++){

                    databesHelper.deleteRegisterNewCustomerDetailRegisterIDandQuestionID(
                            RegisterID,
                            modelQuestionArrayList.get(i).getQuestionID().toString());

                    if (modelQuestionArrayList.get(i).getAnswerTypeID().equals(Constant.CheckBox)) {
                        gson = new Gson();
                        jsonElement =  gson.fromJson(modelQuestionArrayList.get(i).getValue().toString(), JsonElement.class);
                        jsonArray  = jsonElement.getAsJsonArray();
                        if (jsonArray.size()>0){
                            for (int j=0; j<jsonArray.size(); j++){
                                jsonObject = jsonArray.get(j).getAsJsonObject();

                                if (jsonObject.get(Constant.IsStatus).getAsBoolean() == true){
                                    databesHelper.insertRegisterNewCustomerDetail(
                                            RegisterID,
                                            modelQuestionArrayList.get(i).getQuestionID().toString(),
                                            modelQuestionArrayList.get(i).getAnswerID().toString(),
                                            modelQuestionArrayList.get(i).getValue().toString(),
                                            "0",
                                            "0");
                                }
                            }

                        }
                    }else {
                        databesHelper.insertRegisterNewCustomerDetail(
                                RegisterID,
                                modelQuestionArrayList.get(i).getQuestionID().toString(),
                                modelQuestionArrayList.get(i).getAnswerID().toString(),
                                modelQuestionArrayList.get(i).getValue().toString(),
                                "0",
                                "0");
                    }


                }

            }

            Intent intent = new Intent(getActivity(), RegisterCustomerDataKeuanganActivity.class);
            startActivity(intent);


        }

    }
}
