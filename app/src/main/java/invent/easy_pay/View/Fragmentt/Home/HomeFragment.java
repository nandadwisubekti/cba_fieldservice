package invent.easy_pay.View.Fragmentt.Home;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Adapter.HomeAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Helper.data.SharedPreferenceHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModelCallPlanDetail;
import invent.easy_pay.Model.ModelModuleButton;
import invent.easy_pay.R;
import invent.easy_pay.Service.TrackingLocation;
import invent.easy_pay.View.Activity.AkhiriKegiatan.AkhiriKegiatanActivity;
import invent.easy_pay.View.Activity.DownloadUpload.DownloadUploadActivity;
import invent.easy_pay.View.Activity.Kunjungan.KunjunganActivity;
import invent.easy_pay.View.Activity.Kunjungan.KunjunganMenuActivity;
import invent.easy_pay.View.Activity.Persiapan.PersiapanActivity;
import invent.easy_pay.View.Activity.RegisterCustomer.RegisterCustomerActivity;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class HomeFragment extends Fragment {


    TextView tv_home_login_titel,
            tv_home_driver_id_titel,
            tv_home_co_driver_titel;

    TextView tv_home_nomer_kendaraan,
            tv_home_driver_id,
            tv_home_co_drive,
            tv_home_download_upload,
            tv_home_NameUser,
            tvNameModul;

    MaterialRippleLayout mr_item_module_download_upload;

    HeaderHelper headerHelper;

    DatabesHelper databesHelper;

    RecyclerView reyclerView_home_module;

    Intent intent;

    HomeAdapter homeAdapter;

    private static final String TAG = "HomeFragment";

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    ProgressDialog pDialog;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        databesHelper = new DatabesHelper(getActivity());
        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setToolbarHome(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getModuleList();
        return v;
    }


    private void initializeLayout(View view) {

        tv_home_login_titel = (TextView)view.findViewById(R.id.tv_home_login_titel);
        tv_home_driver_id_titel = (TextView)view.findViewById(R.id.tv_home_driver_id_titel);
        tv_home_co_driver_titel = (TextView)view.findViewById(R.id.tv_home_co_driver_titel) ;
        tv_home_nomer_kendaraan = (TextView)view.findViewById(R.id.tv_home_nomer_kendaraan);
        tv_home_driver_id = (TextView)view.findViewById(R.id.tv_home_driver_id);
        tv_home_co_drive = (TextView)view.findViewById(R.id.tv_home_co_drive);
        tv_home_download_upload = (TextView)view.findViewById(R.id.tv_home_download_upload);
        tv_home_NameUser = (TextView)view.findViewById(R.id.tv_home_NameUser);
        tvNameModul = (TextView)view.findViewById(R.id.tvNameModul);

        mr_item_module_download_upload = (MaterialRippleLayout) view.findViewById(R.id.mr_item_module_download_upload);
        reyclerView_home_module = (RecyclerView)view.findViewById(R.id.reyclerView_home_module);


    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_home_login_titel, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_driver_id_titel, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_co_driver_titel, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_NameUser, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());



        TextHelper.setFont(getActivity(), tvNameModul, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_nomer_kendaraan, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_driver_id, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_co_drive, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_download_upload, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

      /*  String nomerKendaraan = databesHelper.getMobileConfig(Constant.VEHICLENUMBER);
        String driver_id = databesHelper.getMobileConfig(Constant.DRIVERID);
        String co_drive = databesHelper.getMobileConfig(Constant.CODRIVER);

        tv_home_nomer_kendaraan.setText(nomerKendaraan);
        tv_home_driver_id.setText(driver_id);
        tv_home_co_drive.setText(co_drive);*/

        String driverName = databesHelper.getMobileConfig(Constant.DRIVER_NAME);
        tv_home_NameUser.setText("Welcome "+driverName);

        mr_item_module_download_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent = new Intent(getActivity(), DownloadUploadActivity.class);
                startActivity(intent);

            }
        });



    }

    private void getModuleList(){
        ArrayList<ModelModuleButton> arrayListModuleButton = new ArrayList<>();
        arrayListModuleButton.clear();

        databesHelper = new DatabesHelper(getActivity());
        String IsRole = databesHelper.getMobileConfig(Constant.Role);
        arrayListModuleButton = databesHelper.getArralyModuleButton(Constant.HOME,IsRole,"1");

        if (arrayListModuleButton.size()>0){
            setupRecyclerView(reyclerView_home_module,arrayListModuleButton);
        }
          databesHelper.close();
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelModuleButton> arrayListModuleButton) {

        homeAdapter = new HomeAdapter(getActivity(),arrayListModuleButton);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(homeAdapter);
        homeAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));



        homeAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String Module_Id = arrayListModuleButton.get(position).getModule_Id();


                if ((Module_Id.equals(Constant.PERSIAPAN))){


                     intent = new Intent(getActivity(), PersiapanActivity.class);
                    startActivity(intent);



                }

                if ((Module_Id.equals(Constant.MULAIPERJALANAN))){



                    dialogKonfirmasiMulaiStart();
                }


                if ((Module_Id.equals(Constant.KUNJUNGAN))){


                    intent = new Intent(getActivity(), KunjunganMenuActivity.class);
                    startActivity(intent);
                }

                if ((Module_Id.equals(Constant.ISSU))){

                  /*  SettingProfileFragment settingProfileFragment = new SettingProfileFragment();
                    android.support.v4.app.FragmentTransaction fragment = getActivity().getSupportFragmentManager().beginTransaction();
                    fragment.replace(R.id.fragment, settingProfileFragment);
                    fragment.addToBackStack(null);
                    fragment.commit();*/
                }

                if ((Module_Id.equals(Constant.AKHIRIKEGIATAN))){


                    intent = new Intent(getActivity(), AkhiriKegiatanActivity.class);
                    startActivity(intent);

                }

                if ((Module_Id.equals(Constant.REPORT))){

                  /*  SettingProfileFragment settingProfileFragment = new SettingProfileFragment();
                    android.support.v4.app.FragmentTransaction fragment = getActivity().getSupportFragmentManager().beginTransaction();
                    fragment.replace(R.id.fragment, settingProfileFragment);
                    fragment.addToBackStack(null);
                    fragment.commit();*/
                }

               /* if ((Module_Id.equals(Constant.NEWCOUNTER))){

                    intent = new Intent(getActivity(), RegisterCustomerActivity.class);
                    startActivity(intent);
                }*/



            }
        });



    }


    public  void dialogKonfirmasiMulaiStart(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage(R.string.dialog_home_mulai_perjalana)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                                new starPerjalanan().execute();

                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }



    private class starPerjalanan extends AsyncTask<Void, Void, Void> {

        int isSuccessInsert = 0;
        @Override
        protected Void doInBackground(Void... params) {

            databesHelper = new DatabesHelper(getActivity());
            ArrayList<ModelCallPlanDetail> modelCallPlanDetailArrayList;
            modelCallPlanDetailArrayList = new ArrayList<ModelCallPlanDetail>();
            modelCallPlanDetailArrayList.clear();


            String EmployeeID = databesHelper.getEmployeeID();
            String CallPlanID = databesHelper.getCallPlanID(EmployeeID);
            String CallPlanDate = databesHelper.getCallPlanDate(EmployeeID);
            String StartDate = ValidationHelper.dateFormat();
            modelCallPlanDetailArrayList = databesHelper.getCallPlanDetail(CallPlanID);

            SharedPreferenceHelper.saveCallPlanId(getActivity(),CallPlanID);
            if (EmployeeID !=null && CallPlanID!=null && CallPlanDate!=null && StartDate!=null){
                try {
                    if (modelCallPlanDetailArrayList.size()>0){

                        databesHelper.insertVisit(
                                CallPlanID,
                                "",
                                CallPlanDate,
                                "0",
                                "0",
                                "1",
                                "0",
                                StartDate,
                                "2000-01-01 00:00:00",
                                EmployeeID,"0");

                        for (int i=0; i<modelCallPlanDetailArrayList.size(); i++){

                            databesHelper.InsertVisitDetail(
                                    modelCallPlanDetailArrayList.get(i).getCallPlanID(),
                                    modelCallPlanDetailArrayList.get(i).getCustomerID(),
                                    "0",
                                    "0",
                                    "2000-01-01 00:00:00",
                                    "2000-01-01 00:00:00",
                                    "0",
                                    "0",
                                    "0",
                                    "",
                                    "",
                                    modelCallPlanDetailArrayList.get(i).getWarehouseID(),
                                    "0",
                                    "0",
                                    "0",
                                    modelCallPlanDetailArrayList.get(i).getSequence(),
                                    "0",
                                    "0",
                                    "0","0","0","0","0",
                                    modelCallPlanDetailArrayList.get(i).getTime(),"");

                        }

                        isSuccessInsert = 2;
                    }else {
                        isSuccessInsert = 1;
                    }



                } catch (SQLiteException ex) {
                    Log.e(getClass().getSimpleName(),
                            "tidak bisa  membuka database");
                    isSuccessInsert = 0;
                }
            }else {
                isSuccessInsert = 1;
            }


           // databesHelper.close();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Mohon tunggu!");
            pDialog.setMessage("Memuat");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Void args) {
            if (pDialog.isShowing())
                pDialog.dismiss();

                //berhasil di insert
                if (isSuccessInsert == 2){


                    databesHelper.updateModuleButton(Constant.MULAIPERJALANAN,Constant.HOME,"0");
                    databesHelper.updateModuleButton(Constant.DOWNLOAD,Constant.DOWNLOAD_UPLOAD,"1");
                    databesHelper.updateModuleButton(Constant.KUNJUNGAN,Constant.HOME,"1");
                    databesHelper.updateModuleButton(Constant.ISSU,Constant.HOME,"1");
                    databesHelper.updateModuleButton(Constant.AKHIRIKEGIATAN,Constant.HOME,"1");
                    databesHelper.updateModuleButton(Constant.NEWCOUNTER,Constant.HOME,"1");
                    databesHelper.updateModuleButton(Constant.REPORT,Constant.HOME,"1");

                    getModuleList();

                }else  if (isSuccessInsert == 1){
                    Toast.makeText(getActivity(), "Kesalahan parameter modelCallPlanDetailArrayList", Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(getActivity(), "Kesalahan parameter EmployeeID", Toast.LENGTH_LONG).show();

                }

        }

    }


    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        getModuleList();
    }


    @Override
    public void onStart() {
        super.onStart();


        if (!checkPermissions()) {
            requestPermissions();
        } else {
            //getLastLocation();
            trackingLocation();
        }

    }

    private void trackingLocation(){

        //---Aoutosave
        PendingIntent pendingIntent;
        AlarmManager manager;

        databesHelper = new DatabesHelper(getActivity());
        String isStart = databesHelper.getIsStart();
        String isFinish = databesHelper.getIsFinish();

        if (isStart != null && isFinish !=null) {
            if (isStart.equals("1") && isFinish.equals("0")) {
                String timeTracking = databesHelper.getMobilFlag(Constant.TIMERTRACKING);
                if (timeTracking != null) {

                    int minutes = Integer.parseInt(timeTracking);
                    long millis = minutes * 60 * 1000;

                    intent = new Intent(getActivity(), TrackingLocation.class);
                    pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);

                    manager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
                    // int interval = 120000; // millisecond > 12 seconds
                    long interval = millis; // millisecond > 12 seconds
                    manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                } else {

                    long millis = 2 * 60 * 1000;
                    intent = new Intent(getActivity(), TrackingLocation.class);
                    pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    manager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                    // int interval = 120000; // millisecond > 12 seconds
                    long interval = millis; // millisecond > 12 seconds
                    manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
                }

            }

        }
        databesHelper.close();

    }


    ////////////////////////////////////////////////////////////////////
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);

    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
/*
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });*/
            startLocationPermissionRequest();

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
                startLocationPermissionRequest();
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                //getLastLocation();
                trackingLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
               /* showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });*/
                startLocationPermissionRequest();
            }
        }





    }

}
