package invent.easy_pay.View.Fragmentt.Notif;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class NotifFragment extends Fragment  {


    HeaderHelper headerHelper;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;

    String downloaAppVersionNew,
            NameVersion;
    public NotifFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        if (getArguments()!=null) {
            Bundle extras = getArguments();
            if (extras.containsKey(Constant.DOWNLOAD_APP_VERSION_NEW)){
                downloaAppVersionNew = (String) extras.getSerializable(Constant.DOWNLOAD_APP_VERSION_NEW);
            }
            if (extras.containsKey(Constant.NAME_VERSION)){
                NameVersion = (String) extras.getSerializable(Constant.NAME_VERSION);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_notif, container, false);

        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();

        //DOWNLOAD VERSION NEW
        download_App_Version_New(NameVersion);
        return v;
    }


    private void initializeLayout(View view) {

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_notif);
    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        //
    }

    //DOWNLOAD APP VERSION NEW
    private void download_App_Version_New(String NameVersion){
        if (downloaAppVersionNew !=null){
            if(downloaAppVersionNew.equalsIgnoreCase(Constant.DOWNLOAD_APP_VERSION_NEW)){

                DialogHelper.dialogUpdateAppNews(getActivity(),Constant.DOWNLOAD_APP_VERSION_NEW,NameVersion);

            }
        }
    }


    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


}
