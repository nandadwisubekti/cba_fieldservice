package invent.easy_pay.View.Fragmentt.Kunjungan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Adapter.KunjunganPosmAdapter;
import invent.easy_pay.Adapter.KunjunganPosmVisitAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Helper.data.SharedPreferenceHelper;
import invent.easy_pay.Model.ModelPOSMStock;
import invent.easy_pay.Model.ModelStore;
import invent.easy_pay.Model.modelGlobal;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class KunjunganPOSMFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerView_stock;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_title_data,
            tv_stock_save,
            tv_name_toko,
            tvQtyOut,
            tvQtySisa,
            tv_posm_item_title,
            tv_posm_qty_in_title;
    MaterialRippleLayout mr_item_stock_save;
    ProgressBar progressBar;
    String customerId,visitID,EmployeeID;

    Dialog dialog,dialogReasonPromo;

    public KunjunganPOSMFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        customerId = sharedPreferenceHelper.getCustomerID();
        visitID= sharedPreferenceHelper.getVisitID();

        EmployeeID = databesHelper.getEmployeeID();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kunjungan_posm, container, false);

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getStoreList();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerView_stock = (RecyclerView)view.findViewById(R.id.recyclerView_stock);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        mr_item_stock_save = (MaterialRippleLayout)view.findViewById(R.id.mr_item_stock_save);

        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_name_toko = (TextView)view.findViewById(R.id.tv_name_toko);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_stock_save = (TextView)view.findViewById(R.id.tv_stock_save);
        tvQtyOut = (TextView)view.findViewById(R.id.tvQtyOut);
        tvQtySisa = (TextView)view.findViewById(R.id.tvQtySisa);
        tv_posm_item_title = (TextView)view.findViewById(R.id.tv_posm_item_title);
        tv_posm_qty_in_title = (TextView)view.findViewById(R.id.tv_posm_qty_in_title);
        tv_name_toko.setVisibility(View.VISIBLE);
        tv_toolbar_name_module.setText(R.string.modul_title_posm);



    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_name_toko, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_title_data, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_stock_save, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tvQtyOut, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tvQtySisa, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_posm_item_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_posm_qty_in_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


    }

    private void assignListener(){

        ModelStore modelStore;
        modelStore = databesHelper.getModelStore_CustomerName(customerId);
        tv_name_toko.setText(modelStore.getCustomerName());
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });



    }


    private void getStoreList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModelPOSMStock> modelPOSMStockArrayList = new ArrayList<>();
        modelPOSMStockArrayList.clear();

        databesHelper = new DatabesHelper(getActivity());

        //Check sudah melakukan posm atau belum
        String CheckPosmVisit = databesHelper.getPOSMStockVisit(visitID,customerId);
        if (CheckPosmVisit.equals("0")){
            tvQtyOut.setVisibility(View.VISIBLE);
            tvQtySisa.setVisibility(View.VISIBLE);
            modelPOSMStockArrayList = databesHelper.getArrayListPOSMStock();
            if (modelPOSMStockArrayList.size()>0){
                setupRecyclerView(recyclerView_stock,modelPOSMStockArrayList);
                progressBar.setVisibility(View.GONE);
                tv_title_data.setVisibility(View.GONE);
                mr_item_stock_save.setVisibility(View.VISIBLE);
            }else {
                tv_title_data.setVisibility(View.VISIBLE);
            }
        }else {
            tvQtyOut.setVisibility(View.GONE);
            tvQtySisa.setVisibility(View.GONE);
            modelPOSMStockArrayList = databesHelper.getArrayListPOSMStockVisitInput(visitID,customerId);
            if (modelPOSMStockArrayList.size()>0){
                setupRecyclerViewPOSMStockVisit(recyclerView_stock,modelPOSMStockArrayList);
                progressBar.setVisibility(View.GONE);
                tv_title_data.setVisibility(View.GONE);
                mr_item_stock_save.setVisibility(View.GONE);
            }else {
                tv_title_data.setVisibility(View.VISIBLE);
            }
        }


        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelPOSMStock> moduleSisaStockVisits) {

        KunjunganPosmAdapter kunjunganPosmAdapter = new KunjunganPosmAdapter(getActivity(),moduleSisaStockVisits);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(kunjunganPosmAdapter);
        kunjunganPosmAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        mr_item_stock_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nameItem = null;
                boolean isSave = true;
                for (int i=0; i<moduleSisaStockVisits.size(); i++){
                    int qtyIn = Integer.valueOf(moduleSisaStockVisits.get(i).getQuantity());
                    int qtyOut = Integer.valueOf(moduleSisaStockVisits.get(i).getQuantityOut());
                    if (qtyOut>qtyIn){
                        isSave =false;
                        nameItem = moduleSisaStockVisits.get(i).getPOSMName();
                        break;
                    }
                }

                if (isSave == false){
                    ///dialog validasi
                    DialogHelper.dialogMessage(getActivity(),"Informasi",""+nameItem +" Qty out jangan lebih dari Qty In" ,Constant.DIALOG_ERROR);
                }else {
                    Toast.makeText(getActivity(),  "Save", Toast.LENGTH_LONG).show();
                    dialogSave(moduleSisaStockVisits);
                }

            }
        });

    }


    public  void dialogSave(final ArrayList<ModelPOSMStock> moduleSisaStockVisits){
        databesHelper = new DatabesHelper(getActivity());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage(R.string.dialog_ind_save)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                                for (int i=0; i<moduleSisaStockVisits.size(); i++){
                                    //update POSMStock
                                    databesHelper.updatePOSMStockQuantityOut(
                                            moduleSisaStockVisits.get(i).getVisitID(),
                                            moduleSisaStockVisits.get(i).getPOSMID(),
                                            moduleSisaStockVisits.get(i).getQuantitySisa());

                                    databesHelper.insertPOSMStockVisit(
                                            moduleSisaStockVisits.get(i).getVisitID(),
                                            moduleSisaStockVisits.get(i).getPOSMID(),
                                            customerId,
                                            moduleSisaStockVisits.get(i).getQuantityOut(),
                                            ValidationHelper.dateFormat(),
                                            EmployeeID,
                                            "1");


                                }


                                dialog.dismiss();
                                getActivity().onBackPressed();
                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }



    ////
    private void setupRecyclerViewPOSMStockVisit(@NonNull final RecyclerView recyclerView, final ArrayList<ModelPOSMStock> moduleSisaStockVisits) {

        KunjunganPosmVisitAdapter kunjunganPosmVisitAdapter = new KunjunganPosmVisitAdapter(getActivity(),moduleSisaStockVisits);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(kunjunganPosmVisitAdapter);
        kunjunganPosmVisitAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);



    }



    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
