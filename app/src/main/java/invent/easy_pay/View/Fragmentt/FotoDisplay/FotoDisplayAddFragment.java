package invent.easy_pay.View.Fragmentt.FotoDisplay;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.MRuntimePermission;
import invent.easy_pay.Helper.TakeImage;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Helper.data.SharedPreferenceHelper;
import invent.easy_pay.Model.ModelGetLocation;
import invent.easy_pay.Model.modelGlobal;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class FotoDisplayAddFragment extends Fragment  {


    HeaderHelper headerHelper;

    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_btn_save;
    MaterialRippleLayout mr_item_foto_display_save,
            mr_foto_display_takCamer;
    ImageView ivViewPhoto;

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    String imgBase64;
    private MRuntimePermission mPermissionChecker;
    String customerId, visitID,EmployeeID;
    public FotoDisplayAddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        mPermissionChecker = new MRuntimePermission(getActivity());

        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        customerId = sharedPreferenceHelper.getCustomerID();
        visitID= sharedPreferenceHelper.getVisitID();

        EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_foto_display_add, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        mr_item_foto_display_save = (MaterialRippleLayout)view.findViewById(R.id.mr_item_foto_display_save);
        ivViewPhoto = (ImageView)view.findViewById(R.id.ivViewPhoto);
        mr_foto_display_takCamer = (MaterialRippleLayout)view.findViewById(R.id.mr_foto_display_takCamer);
        tv_btn_save = (TextView)view.findViewById(R.id.tv_btn_save);
        tv_toolbar_name_module.setText(R.string.modul_title_persipan_ruete_photo_display);
    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_btn_save, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mr_foto_display_takCamer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] perms = {
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                };
                checkPermissions(perms);
            }
        });


        mr_item_foto_display_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imgBase64 == null || imgBase64.equalsIgnoreCase("")){
                    DialogHelper.dialogMessage(getActivity(),"Informasi","Lakukan pengambilan foto terlebih dahulu!",Constant.DIALOG_ERROR);

                }else{
                    if (validate()){
                        dialogSave();
                    }

                }
            }
        });
    }
    private boolean validate()
    {

        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        customerId = sharedPreferenceHelper.getCustomerID();
        visitID= sharedPreferenceHelper.getVisitID();


        if (customerId==null){
            DialogHelper.dialogMessage(getActivity(),"Informasi","Customer ID null!",Constant.DIALOG_ERROR);

            return false;
        }
        if (visitID == null){
            DialogHelper.dialogMessage(getActivity(),"Informasi","Visit ID null!",Constant.DIALOG_ERROR);

            return false;
        }

        if (EmployeeID == null){
            DialogHelper.dialogMessage(getActivity(),"Informasi","EmployeeID ID null!",Constant.DIALOG_ERROR);

            return false;
        }

        return true;
    }


    //dialog save
    public void dialogSave(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirmation);
        builder.setMessage(R.string.dialog_ind_save)
                .setCancelable(false)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {


                                SimpleDateFormat id = new SimpleDateFormat("yyyMMddHHmmss");
                                String Dateimage = id.format(new Date());

                                String IdImage = "IMG_"+visitID+"_"+EmployeeID+"_"+Dateimage;


                                ModelGetLocation mobileLocation;
                                ValidationHelper validationHelper = new ValidationHelper(getActivity());
                                mobileLocation = validationHelper.getMobileLocation();

                                databesHelper.insertCustomerImage(
                                        IdImage,
                                        customerId,
                                        "",
                                        visitID,
                                        ValidationHelper.dateFormat(),
                                        imgBase64,
                                        "Visit","",
                                        mobileLocation.getLatitude(),
                                        mobileLocation.getLongitude());
                                databesHelper.close();

                              getActivity().onBackPressed();

                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }


    //Take camer
    //region PERMISSION CHECK
    private void checkPermissions(String[] perm) {
        if (ContextCompat.checkSelfPermission(getActivity(), perm[0]) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getActivity(), perm[1]) == PackageManager.PERMISSION_GRANTED) {
                if (mPermissionChecker.checkFragmentCameraPermission(FotoDisplayAddFragment.this)) {
                    //CAMERA
                    takeImageCamera();
                }
            } else {
                requestPermissions(perm,0);
            }
        } else {
            requestPermissions(perm,0);
        }
    }


    private void takeImageCamera() {
        if (TakeImage.isIntentAvailable(getContext(), MediaStore.ACTION_IMAGE_CAPTURE)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePictureIntent, REQUEST_CAMERA);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data,ivViewPhoto);
        }
    }



    private void onCaptureImageResult(Intent data, ImageView ivViewPhoto) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //compress IMAGE TO Base64
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bs);
        byte[] b = bs.toByteArray();
        //imgBase64 = Base64.encodeToString(b, Base64.NO_WRAP);
        imgBase64 = Base64.encodeToString(b, Base64.DEFAULT);
        //compress IMAGE TO Base64

        ivViewPhoto.setImageBitmap(thumbnail);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MRuntimePermission.PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    int grantResult = grantResults[i];

                    if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            //takeImagGallery();
                            break;
                        } else {
                            mPermissionChecker.checkFragmentWriteExternalStoragePermission(this);
                            break;
                        }

                    }
                }
                break;
            case MRuntimePermission.PERMISSIONS_REQUEST_CAMERA:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    int grantResult = grantResults[i];

                    if (permission.equals(Manifest.permission.CAMERA)) {

                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            takeImageCamera();
                            break;
                        } else {
                            mPermissionChecker.checkFragmentCameraPermission(this);
                            break;
                        }

                    }
                }
                break;
        }

    }



    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
