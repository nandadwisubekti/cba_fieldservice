package invent.easy_pay.View.Fragmentt.Setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class SettingpChangPasswordFragment extends Fragment  {


    TextView tv_setting_change_password_title;
    EditText et_setting_change_password_lama,
            et_setting_change_password_baru,
            et_setting_change_password_konfirmasi;

    MaterialRippleLayout mr_btn_setting_change_password_save;

    HeaderHelper headerHelper;
    DatabesHelper databesHelper;

    private String errorMsg = "";

    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;

    public SettingpChangPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting_chenge_password, container, false);

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

       /* Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.modul_title_change_password);
        // ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/

        tv_setting_change_password_title = (TextView)view.findViewById(R.id.tv_setting_change_password_title);

        et_setting_change_password_lama = (EditText)view.findViewById(R.id.et_setting_change_password_lama);
        et_setting_change_password_baru = (EditText) view.findViewById(R.id.et_setting_change_password_baru);
        et_setting_change_password_konfirmasi = (EditText)view.findViewById(R.id.et_setting_change_password_konfirmasi);

        mr_btn_setting_change_password_save = (MaterialRippleLayout) view.findViewById(R.id.mr_btn_setting_change_password_save);


        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_change_password);

    }

    private void styleLayout() {

        TextHelper.styleEditText(getActivity(), et_setting_change_password_lama, "PASSWORD LAMA *", true);
        TextHelper.styleEditText(getActivity(), et_setting_change_password_baru,"PASSWORD BARU *", true);
        TextHelper.styleEditText(getActivity(), et_setting_change_password_konfirmasi,  "KONFIRMASI PASSWORD *", true);



        TextHelper.setFont(getActivity(), tv_setting_change_password_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), mr_btn_setting_change_password_save, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());



    }


    private void assignListener(){

        String password = databesHelper.getMobileConfig(Constant.PASSWORD);
        databesHelper.close();

        et_setting_change_password_lama.setText(password);


        mr_btn_setting_change_password_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()){
                    saveData();
                }else {
                    DialogHelper.dialogMessage(getActivity(),"Warning",errorMsg,Constant.DIALOG_WRONG);
                }
            }
        });

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


    }


    private Boolean validateForm() {
        if (et_setting_change_password_lama.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi password lama";
            return false;
        }


        if (et_setting_change_password_baru.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi password baru";
            return false;
        }

        if (et_setting_change_password_konfirmasi.getText().toString().equalsIgnoreCase("")) {
            errorMsg = "Mohon isi password konfrimasi";
            return false;

        }
        if (!isValidPassowrd(et_setting_change_password_konfirmasi.getText().toString())){
            errorMsg = "Password harus lebih dari 5 karakter";
        }

        if (!isValidPassowrdKonfrimasi(et_setting_change_password_baru.getText().toString(),et_setting_change_password_konfirmasi.getText().toString())){
            errorMsg = "Password Tidak Sesuai";
        }



        return true;
    }


    private boolean isValidPassowrd(String pass){
        if(pass !=null && pass.length() >=5){
            return true;
        }
        return false;
    }

    private boolean isValidPassowrdKonfrimasi(String passbaru, String passbaruKonfirmasi){

        if(passbaru.equals(passbaruKonfirmasi)){
            return true;

        }
        return false;
    }

    private void saveData(){

        //SAVE PASSWORD
        databesHelper.updateMobileConfig(Constant.PASSWORD, et_setting_change_password_baru.getText().toString());
       // Toast.makeText(getActivity(), "Save berhasil", Toast.LENGTH_SHORT).show();
        DialogHelper.dialogMessage(getActivity(),"Success","Save Success",Constant.DIALOG_SUCCESS);

    }


    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


}
