package invent.easy_pay.View.Fragmentt.RegisterCustomer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import invent.easy_pay.Adapter.NewCustomerAdapter;
import invent.easy_pay.Adapter.SettingAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.MarshMallowPermission;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Helper.data.SharedPreferenceHelper;
import invent.easy_pay.Interface.ConnectionInterface;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModelGetLocation;
import invent.easy_pay.Model.ModelModuleButton;
import invent.easy_pay.Model.ModelRegisterNewCustomer;
import invent.easy_pay.Model.modelGlobal;
import invent.easy_pay.R;
import invent.easy_pay.Service.ConnectionClient;
import invent.easy_pay.View.Activity.Home.HomeActivity;
import invent.easy_pay.View.Activity.Kunjungan.KunjunganStartActivity;
import invent.easy_pay.View.Activity.RegisterCustomer.RegisterCustomerActivity;
import invent.easy_pay.View.Activity.RegisterCustomer.RegisterCustomerDataPribadiActivity;
import invent.easy_pay.View.Activity.RegisterCustomer.RegisterCustomerDetailActivity;
import invent.easy_pay.View.Fragmentt.Setting.SettingDeviceFragment;
import invent.easy_pay.View.Fragmentt.Setting.SettingIPFragment;
import invent.easy_pay.View.Fragmentt.Setting.SettingProfileFragment;
import invent.easy_pay.View.Fragmentt.Setting.SettingpChangPasswordFragment;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class RegisterCustomerFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewSettingList;
    SettingAdapter settingAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back,
            ry_toolbar_right;
    TextView tv_toolbar_name_module,
            tv_title_data;
    MarshMallowPermission marshMallowPermission;
    ConnectionClient connectionClient;
    ConnectionInterface apiService;

    public RegisterCustomerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        marshMallowPermission = new MarshMallowPermission(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting, container, false);

        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        styleLayout();
        assignListener();
        getRegisterCustomerNew();
        return v;
    }


    private void initializeLayout(View view) {
        recyclerViewSettingList = (RecyclerView)view.findViewById(R.id.recyclerView_setting_list);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        ry_toolbar_right = (RelativeLayout)view.findViewById(R.id.ry_toolbar_right);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        ry_toolbar_right.setVisibility(View.VISIBLE);
        tv_title_data.setVisibility(View.GONE);
        tv_toolbar_name_module.setText(R.string.modul_title_list_register_customer);

    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){
        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), KunjunganStartActivity.class);
                startActivity(intent);
               // getActivity().finish();
            }
        });

        ry_toolbar_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                createNewCustomer();

            }
        });
    }

    private void getRegisterCustomerNew(){
        ArrayList<ModelRegisterNewCustomer> modelRegisterNewCustomerArrayList = new ArrayList<>();
        modelRegisterNewCustomerArrayList.clear();



        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        String  customerId = sharedPreferenceHelper.getCustomerID();
        String visitID= sharedPreferenceHelper.getVisitID();

        modelRegisterNewCustomerArrayList = databesHelper.getArrayListResultSurvey("1",Constant.QuestionCategoryRegisterCustomer,visitID,customerId);
        if (modelRegisterNewCustomerArrayList.size()>0){
            setupRecyclerView(recyclerViewSettingList,modelRegisterNewCustomerArrayList);
            tv_title_data.setVisibility(View.GONE);

        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }

    }



    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelRegisterNewCustomer> modelRegisterNewCustomerArrayList) {

        NewCustomerAdapter newCustomerAdapter = new NewCustomerAdapter(getActivity(),modelRegisterNewCustomerArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(newCustomerAdapter);
        newCustomerAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);

        newCustomerAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

               /* Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.RegisterID, modelRegisterNewCustomerArrayList.get(position).getRegisterID());
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                RegisterCustomerDetailFragment fragment = new RegisterCustomerDetailFragment();
                fragment.setArguments(bundle);
                transaction.replace(R.id.fragment, fragment);
                transaction.addToBackStack(null);
                transaction.commit();*/

                Intent intent = new Intent(getActivity(), RegisterCustomerDetailActivity.class);
                intent.putExtra(Constant.RegisterID,modelRegisterNewCustomerArrayList.get(position).getRegisterID());
                startActivity(intent);


            }
        });



    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        //delete tabel register new customer ketika kondisi isSave 0
        databesHelper.deleteRegisterNewCustomerIsSave("0");
        //delete tabel register new customer detail ketika kondisi isSave 0
        databesHelper.deleteRegisterNewCustomerDetailIsSave("0");
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    private void createNewCustomer(){
        final ModelGetLocation mobileLocation;
        ValidationHelper validationHelper = new ValidationHelper(getActivity());
        mobileLocation = validationHelper.getMobileLocation();

        if(mobileLocation.getLatitude()!=null && mobileLocation.getLongitude() !=null){


            String EmployeeID = databesHelper.getEmployeeID();
            String VisitID = databesHelper.getVisitID(EmployeeID);
            SimpleDateFormat srcDf = new SimpleDateFormat("yyyMMddHHmmss");
            String date = srcDf.format(new Date());
            String RegisterID = "REG-"+date;
            String StartDate = ValidationHelper.dateFormat();

            SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
            String customerId = sharedPreferenceHelper.getCustomerID();

            if (customerId!=null && VisitID!=null ){
                //insert RegisterNewCustomer
                databesHelper.insertRegisterNewCustomer(
                        RegisterID,
                        EmployeeID,
                        VisitID,
                        StartDate,
                        mobileLocation.getLatitude(),
                        mobileLocation.getLongitude(),
                        "0",
                        "0",
                        customerId,
                        Constant.QuestionCategoryRegisterCustomer);

                SharedPreferences prefs = getActivity().getSharedPreferences(Constant.PREFS_EASY_PAY, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constant.PREFS_TAG_NEW_REGISTER_CUSTOMER_ID, RegisterID);
                editor.apply();


                Intent intent = new Intent(getActivity(), RegisterCustomerDataPribadiActivity.class);
                startActivity(intent);
            }else {
                DialogHelper.dialogMessage(getActivity(), "Info", "Customer id atau visit id ada yang null!", Constant.DIALOG_WRONG);

            }


        }else {

            DialogHelper.dialogMessage(getActivity(), "Info", "Register membutuhkan GPS dan koneksi internet,hidupkan terlebih dahulu GPS  dan koneksi internet anda!", Constant.DIALOG_WRONG);
        }



    }
}
