package invent.easy_pay.View.Fragmentt.Kunjungan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import invent.easy_pay.Adapter.AdapterMenu;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.Etc;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.CallbackOnClickItemListener;
import invent.easy_pay.Model.ModelModuleButton;
import invent.easy_pay.R;

/**
 * Created by kahfizain on 25/08/2017.
 */

public class NonVisitCustomerFragment extends Fragment {

    HeaderHelper headerHelper;
    RecyclerView recyclerViewPersiapan;

    AdapterMenu adapterMenu;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_title_data;
    ProgressBar progressBar;

    public NonVisitCustomerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_module_list, container, false);

        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());
        styleLayout();
        assignListener();
        return v;
    }

    private void initializeLayout(View view) {

        recyclerViewPersiapan = (RecyclerView)view.findViewById(R.id.recyclerView);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_toolbar_name_module.setText("Daftar Customer");

    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (getActivity()).finish();
            }
        });
    }
    ArrayList<ModelModuleButton> arrayListModuleButton;
    private void getModuleList(){
        progressBar.setVisibility(View.VISIBLE);
        arrayListModuleButton = new ArrayList<>();


        try {
            databesHelper.openDataBase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String IsRole = databesHelper.getMobileConfig(Constant.Role);
        arrayListModuleButton = databesHelper.getNonVisitCustomerAsModuleButton();
        databesHelper.close();

        List<AdapterMenu.Model> list = new ArrayList<>();

        for(ModelModuleButton module : arrayListModuleButton){
            AdapterMenu.Model m = new AdapterMenu.Model();
            m.setId(module.getModule_Id());
            m.setTvA(module.getTitle());
            m.setType(AdapterMenu.Model.Type.TYPE_B);


            /*m.setResIdIconEnable(R.drawable.ic_kunjungan_dalam_rute);
            m.setResIdIconDisable(R.drawable.ic_kunjungan_dalam_rute);*/


            if(module.getIsActive().contentEquals("1"))
                m.setVisibility(true);
            else
                m.setVisibility(false);


            if(module.getValue().contentEquals("1"))
                m.setEnability(true);
            else
                m.setEnability(false);

            list.add(m);
        }
        if (arrayListModuleButton.size()>0){
            setupRecyclerView(recyclerViewPersiapan,list);
            tv_title_data.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.GONE);
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final List<AdapterMenu.Model> list) {

        adapterMenu = new AdapterMenu(getActivity(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterMenu);
        adapterMenu.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        adapterMenu.setOnClickItemListener(new CallbackOnClickItemListener() {
            @Override
            public void onClick(final int position) {

                /**
                 * tadinya mau nampilin detil dari tiap store, tapi karena gak ada data
                 * apa-apa di db selain customerID dan customerName, jadi langsung dialog aja
                 * untuk konfirmasi apakah mau dimasukin ke dalam daftar visit apa engga
                 */
                /*Intent intent = new Intent(getActivity(), NonVisitCustomerDetailActivity.class);
                intent.putExtra("customerID", arrayListModuleButton.get(position).getModule_Id());
                startActivity(intent);*/

                new AlertDialog.Builder(getActivity())
                        .setMessage("Masukkan store ini ke dalam daftar visit?")
                        .setPositiveButton("YA", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                String EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);
                                String visitID = databesHelper.getVisitID(EmployeeID);
                                addStoreToVisit(arrayListModuleButton.get(position).getModule_Id(), visitID);
                            }
                        })
                        .setNegativeButton("BATAL", null)
                        .show();
            }
        });


    }

    private void addStoreToVisit(final String storeID, final String visitID) {
        /**
         * cek dulu lagi visit apa kagak
         */
        String customerIdVisit = databesHelper.getVisitCustomer();

        if (customerIdVisit == null || customerIdVisit.equals("")) {
            databesHelper.insertVisitDetail(storeID, visitID, new Etc.GenericCallback() {
                @Override
                public void run(Object... o) {
                    Long inserted = (Long) o[0];
                    if(inserted!=-1){
                        DialogHelper.dialogKonfirmasiVisitStart(getActivity(), "ON_TIME", visitID, storeID, "");
                    }
                }
            });
        } else {
            String customerName = databesHelper.getVisitCustomerName(customerIdVisit);
            if (customerName != null) {
                DialogHelper.dialogMessage(getActivity(), "Info", "Selesaikan aktifitas " + customerName + " telebih dahulu!", Constant.DIALOG_WRONG);

            } else {
                DialogHelper.dialogMessage(getActivity(), "Info", "Selesaikan aktifitas sebelumnya!", Constant.DIALOG_WRONG);
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        getModuleList();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();

    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


}
