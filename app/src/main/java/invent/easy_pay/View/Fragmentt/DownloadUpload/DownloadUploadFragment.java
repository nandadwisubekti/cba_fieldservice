package invent.easy_pay.View.Fragmentt.DownloadUpload;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import invent.easy_pay.Adapter.DownloadUploadAdapter;
import invent.easy_pay.Helper.CheckConnection;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.Etc;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Interface.ConnectionInterface;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModelModuleButton;
import invent.easy_pay.R;
import invent.easy_pay.Service.ConnectionClient;
import invent.easy_pay.Service.TestConnection;
import invent.easy_pay.Service.Upload;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class DownloadUploadFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewPersiapan;
    DownloadUploadAdapter downloadUploadAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back,ryButtonUpload;
    TextView tv_toolbar_name_module,
            tv_title_data,
            tv_button_name_module;
    CheckConnection checkConnection;
    ConnectionClient connectionClient;
    ConnectionInterface apiService;
    ProgressBar progressBar;
    MaterialRippleLayout mr_button_upload_file_Sqlite;
    private String currentServerDate;

    public DownloadUploadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
        checkConnection = new CheckConnection(getActivity());
        connectionClient = new ConnectionClient(getActivity());
        apiService = connectionClient.getApiService();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getModuleList();

        return v;
    }


    private void initializeLayout(View view) {

        /*Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.modul_title_persipan);
       // ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/
        recyclerViewPersiapan = (RecyclerView)view.findViewById(R.id.recyclerView_persiapan);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        ryButtonUpload = (RelativeLayout)view.findViewById(R.id.ryButtonUpload);
        tv_button_name_module = (TextView)view.findViewById(R.id.tv_button_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_download_upload);
        mr_button_upload_file_Sqlite = (MaterialRippleLayout)view.findViewById(R.id.mr_button_upload_file_Sqlite);
        ryButtonUpload.setVisibility(View.VISIBLE);
    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_title_data, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_button_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


    }

    private void assignListener(){



        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mr_button_upload_file_Sqlite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkConnection.Connect() == false) {
                    //TIDAK ADA KONESKI INTERNET
                    DialogHelper.dialogMessage(getActivity(),"Informasi","Tidak ada koneksi internet",Constant.DIALOG_WRONG);

                } else {
                    connectionClient.uploadFileSqlite(getActivity());

                }

            }
        });
    }


    private void getModuleList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModelModuleButton> arrayListModuleButton = new ArrayList<>();
        arrayListModuleButton.clear();

         databesHelper = new DatabesHelper(getActivity());
        String IsRole = databesHelper.getMobileConfig(Constant.Role);
        arrayListModuleButton = databesHelper.getArralyModuleButton(Constant.DOWNLOAD_UPLOAD,IsRole,"1");

        if (arrayListModuleButton.size()>0){
            setupRecyclerView(recyclerViewPersiapan,arrayListModuleButton);
            tv_title_data.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }else{
            tv_title_data.setVisibility(View.VISIBLE);
        }

        databesHelper.close();
        progressBar.setVisibility(View.GONE);
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelModuleButton> arrayListModuleButton) {

        downloadUploadAdapter = new DownloadUploadAdapter(getActivity(),arrayListModuleButton);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(downloadUploadAdapter);
        downloadUploadAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        downloadUploadAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String Module_Id = arrayListModuleButton.get(position).getModule_Id();


                //DOWNLOAD
                if ((Module_Id.equals(Constant.DOWNLOAD))){


                    checkConnectionDownload();

                }


                //UPLOAD
                if (Module_Id.equals(Constant.UPLOAD)){


                    new Upload(getActivity());

                }

                //TEST KONEKSI
                if (Module_Id.equals(Constant.TESTKONEKSI)){


                    if (checkConnection.Connect() == false) {
                        DialogHelper.dialogMessage(getActivity(),"Error","Tidak ada koneksi intrenet,cek kembali IP dan Port!",Constant.DIALOG_ERROR);

                    } else {
                        new TestConnection(getActivity());

                    }

                }



            }
        });

    }


    //Check koneksi download
    private void checkConnectionDownload(){

        if (checkConnection.Connect() == false) {
            //TIDAK ADA KONESKI INTERNET
            DialogHelper.dialogMessage(getActivity(),"Informasi","Tidak ada koneksi internet",Constant.DIALOG_WRONG);

        } else {
            //dialogDate();
            downloadData(currentServerDate);
        }
    }

    //DIALOG DATE
    private void dialogDate(){

        int mYear, mMonth, mDay;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog =
                new DatePickerDialog(getActivity(),R.style.datepicker, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        // harus pake if begini kalo gak bakal dipanggil 2x
                        if (view.isShown()) {
                            //    System.out.println("datetime :"+year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            downloadData(date);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime()); //MAX DATE
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //MIN DATE
        datePickerDialog.show();


    }

    private void downloadData(String date){

        databesHelper.updateDownloadUpload(Constant.DOWNLOAD, ValidationHelper.dateFormat());

        String driverId = databesHelper.getMobileConfig(Constant.DRIVERID);
        String deviceID = ValidationHelper.getMacAddrss();
        String branchId = databesHelper.getMobileConfig(Constant.BRANCHID);
        String Role = databesHelper.getMobileConfig(Constant.Role);

        if (date !=null && deviceID !=null && driverId !=null && branchId !=null && Role!=null){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(Constant.EmployeeID,driverId);
            jsonObject.addProperty(Constant.BranchID, branchId);
            jsonObject.addProperty(Constant.Date, date);
            jsonObject.addProperty(Constant.deviceID, deviceID);
            jsonObject.addProperty(Constant.Role, Role);

             connectionClient.downloadDataAll(jsonObject);

        }else {
            DialogHelper.dialogMessage(getActivity(),"Informasi","parameter ada yang null",Constant.DIALOG_WRONG);
        }

    }



    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        // disable dulu tombol downloadnya
        downloadUploadAdapter.disableDownloadButton();
        // ambil current date dari server, baru nyalain tombol downloadnya
        GetCurrentServerDateTask getCurrentServerDateTask = new GetCurrentServerDateTask(apiService, new Etc.GenericCallback() {
            @Override
            public void run(Object... o) {
                String serverDate = (String) o[0];
                Boolean isDownloadCompleted = databesHelper.isDownloadIsCompleted();
                if(!serverDate.equals("") && !isDownloadCompleted) {
                    currentServerDate = serverDate;
                    downloadUploadAdapter.enableDownloadButton();
                }
            }
        });
        getCurrentServerDateTask.execute();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    static class GetCurrentServerDateTask extends AsyncTask<Void, Void, String>{

        Etc.GenericCallback cb;
        ConnectionInterface api;

        GetCurrentServerDateTask(ConnectionInterface api, Etc.GenericCallback cb){
            this.api = api;
            this.cb = cb;
        }

        @Override
        protected String doInBackground(Void... params) {
            String serverDate = "";
            Call<String> call = api.getCurrentServerDate();
            try {
                Response<String> response = call.execute();
                if(response.code()==200) serverDate = response.body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return serverDate;
        }

        @Override
        protected void onPostExecute(String result) {
            if(cb!=null) cb.run(result);
        }
    }
}
