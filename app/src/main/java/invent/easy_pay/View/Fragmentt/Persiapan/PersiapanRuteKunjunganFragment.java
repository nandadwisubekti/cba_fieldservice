package invent.easy_pay.View.Fragmentt.Persiapan;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import invent.easy_pay.Adapter.PersiapanRuteKunjunganAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModelStore;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class PersiapanRuteKunjunganFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewPersiapan;
    PersiapanRuteKunjunganAdapter persiapanRuteKunjunganAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_persiapan_name_title,
            tv_title_data;

    ProgressBar progressBar;
    public PersiapanRuteKunjunganFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getStoreList();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerViewPersiapan = (RecyclerView)view.findViewById(R.id.recyclerView_persiapan);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_persiapan_name_title = (TextView)view.findViewById(R.id.tv_persiapan_name_title);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_persiapan_name_title.setVisibility(View.VISIBLE);
        tv_toolbar_name_module.setText(R.string.modul_title_persipan_ruete_kunjungan);
        tv_persiapan_name_title.setText(R.string.titel_persian_store_list);
    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_persiapan_name_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());


    }

    private void assignListener(){



        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }


    private void getStoreList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModelStore> arrayListmoduleStore = new ArrayList<>();
        arrayListmoduleStore.clear();

        databesHelper = new DatabesHelper(getActivity());
        String EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);
        String CallPlanID = databesHelper.getCallPlanID(EmployeeID);
        arrayListmoduleStore = databesHelper.getArrayStore(CallPlanID);
        if (arrayListmoduleStore.size()>0){
            setupRecyclerView(recyclerViewPersiapan,arrayListmoduleStore);
            progressBar.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);

        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }
        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelStore> arrayListmoduleStore) {

        persiapanRuteKunjunganAdapter = new PersiapanRuteKunjunganAdapter(getActivity(),arrayListmoduleStore);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(persiapanRuteKunjunganAdapter);
        persiapanRuteKunjunganAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        persiapanRuteKunjunganAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.CustomerName,  arrayListmoduleStore.get(position).getCustomerName());
                bundle.putSerializable(Constant.CustomerAddress,  arrayListmoduleStore.get(position).getCustomerAddress());
                bundle.putSerializable(Constant.CustomerID,  arrayListmoduleStore.get(position).getCustomerID());
                bundle.putSerializable(Constant.Distributor,  arrayListmoduleStore.get(position).getDistributor());
                bundle.putSerializable(Constant.CountryRegionCode,  arrayListmoduleStore.get(position).getCountryRegionCode());
                bundle.putSerializable(Constant.City,  arrayListmoduleStore.get(position).getCity());
                bundle.putSerializable(Constant.Account,  arrayListmoduleStore.get(position).getAccount());
                bundle.putSerializable(Constant.Time,  arrayListmoduleStore.get(position).getTime());
                bundle.putSerializable(Constant.Channel,  arrayListmoduleStore.get(position).getChannel());

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                PersiapanRuteKunjunganDetailFragment persiapanRuteKunjunganDetailFragment = new PersiapanRuteKunjunganDetailFragment();
                persiapanRuteKunjunganDetailFragment.setArguments(bundle);
                transaction.replace(R.id.fragment, persiapanRuteKunjunganDetailFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });



    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
