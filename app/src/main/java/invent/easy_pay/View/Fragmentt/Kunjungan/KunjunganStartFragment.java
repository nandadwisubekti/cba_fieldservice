package invent.easy_pay.View.Fragmentt.Kunjungan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import invent.easy_pay.Adapter.KunjunganStartAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.DialogHelper;
import invent.easy_pay.Helper.Etc;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Helper.data.SharedPreferenceHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModelCompetitorActivityType;
import invent.easy_pay.Model.ModelModuleButton;
import invent.easy_pay.Model.ModelProduct;
import invent.easy_pay.Model.ModelStore;
import invent.easy_pay.Model.ModuleCompetitor;
import invent.easy_pay.Model.ModuleSisaStockType;
import invent.easy_pay.Model.QuestionSet;
import invent.easy_pay.Model.modelGlobal;
import invent.easy_pay.R;
import invent.easy_pay.View.Activity.FotoDisplay.FotoDisplayActivity;
import invent.easy_pay.View.Activity.Kunjungan.KunjunganCanvasserActivity;
import invent.easy_pay.View.Activity.Kunjungan.KunjunganCompetitroActivity;
import invent.easy_pay.View.Activity.Kunjungan.KunjunganPOSM;
import invent.easy_pay.View.Activity.Kunjungan.KunjunganStockActivity;
import invent.easy_pay.View.Activity.RegisterCustomer.RegisterCustomerActivity;
import invent.easy_pay.View.Activity.survey.ActivitySurveyMenu;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class KunjunganStartFragment extends Fragment {


    TextView tv_kunjungan_CustomerName,
            tv_kunjungan_CustomerAdress,
            tv_kunjungan_finish,
            tv_toolbar_name_module;

    MaterialRippleLayout mr_item_kujungan_finish;

    HeaderHelper headerHelper;

    DatabesHelper databesHelper;

    RecyclerView reyclerView_kunjungan_button;

    Intent intent;

    KunjunganStartAdapter kunjunganStartAdapter;

    RelativeLayout ry_toolbar_back;

    String customerId,visitID,EmployeeID;

    private String errorMsg = "";

    ProgressDialog pDialogCompetitor,pDialog;

    public KunjunganStartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kujungan, container, false);

        databesHelper = new DatabesHelper(getActivity());

        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        customerId = sharedPreferenceHelper.getCustomerID();
        visitID= sharedPreferenceHelper.getVisitID();

        EmployeeID = databesHelper.getEmployeeID();

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getModuleList();
        return v;
    }

    private void initializeLayout(View view) {

        tv_kunjungan_CustomerName = (TextView)view.findViewById(R.id.tv_kunjungan_CustomerName);
        tv_kunjungan_CustomerAdress = (TextView)view.findViewById(R.id.tv_kunjungan_CustomerAdress);
        tv_kunjungan_finish = (TextView)view.findViewById(R.id.tv_kunjungan_finish) ;
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        mr_item_kujungan_finish = (MaterialRippleLayout) view.findViewById(R.id.mr_item_kujungan_finish);
        reyclerView_kunjungan_button = (RecyclerView)view.findViewById(R.id.reyclerView_kunjungan_button);
        ry_toolbar_back.setVisibility(View.GONE);
        tv_toolbar_name_module.setText(R.string.modul_title_kunjungan_toko);


    }

    private void styleLayout() {

        TextHelper.setFont(getActivity(), tv_kunjungan_CustomerName, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_kunjungan_CustomerAdress, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_kunjungan_finish, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }

    private void assignListener(){
        ModelStore modelStore;
        databesHelper = new DatabesHelper(getActivity());
        modelStore = databesHelper.getModelStore_CustomerName(customerId);
        if (modelStore!=null){
            tv_kunjungan_CustomerName.setText(modelStore.getCustomerName());
            tv_kunjungan_CustomerAdress.setText(modelStore.getCustomerAddress());
        }


        mr_item_kujungan_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<QuestionSet> questionSets = databesHelper.getQuestionSets(customerId, EmployeeID);
                Double totalQuestionSet = (double) questionSets.size();
                int counter = 0;
                for (QuestionSet questionSet: questionSets) {
                    if(questionSet.getAnswersCount()>0) counter++;
                }
                Double totalCompletedQuestionSet = (double) counter;
                Double percentage = (totalCompletedQuestionSet / totalQuestionSet) * 100d;
                percentage = Math.floor(percentage * 10) / 10;
                if(percentage!=100.0){
                    DialogHelper.dialogKonfirmasiSurveyNotCompleted(getActivity(), totalQuestionSet.intValue() - totalCompletedQuestionSet.intValue(), new Etc.GenericCallback() {
                        @Override
                        public void run(Object... o) {
                            DialogHelper.dialogKonfirmasiFinish(getActivity());
                        }
                    });
                } else DialogHelper.dialogKonfirmasiFinish(getActivity());
            }
        });



    }

    private void getModuleList(){
        ArrayList<ModelModuleButton> arrayListModuleButton = new ArrayList<>();
        arrayListModuleButton.clear();

        databesHelper = new DatabesHelper(getActivity());
        String IsRole = databesHelper.getMobileConfig(Constant.Role);
        arrayListModuleButton = databesHelper.getArralyModuleButton(Constant.KUNJUNGAN,IsRole,"1");

        if (arrayListModuleButton.size()>0){
            setupRecyclerView(reyclerView_kunjungan_button,arrayListModuleButton);
        }
          databesHelper.close();
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelModuleButton> arrayListModuleButton) {

        kunjunganStartAdapter = new KunjunganStartAdapter(getActivity(),arrayListModuleButton);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(kunjunganStartAdapter);
        kunjunganStartAdapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));


        kunjunganStartAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String Module_Id = arrayListModuleButton.get(position).getModule_Id();

                if ((Module_Id.equals(Constant.STOCKSTORE))){

                    //new checkStock().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    intent = new Intent(getActivity(), KunjunganStockActivity.class);
                    startActivity(intent);
                }

                if ((Module_Id.equals(Constant.POSM))){
                    intent = new Intent(getActivity(), KunjunganPOSM.class);
                    startActivity(intent);
                }

                if ((Module_Id.equals(Constant.ISSUE))){


                }

                if ((Module_Id.equals(Constant.KOMPETITOR))){

                    new  checkCompetitorVisit().execute();

                }

                if ((Module_Id.equals(Constant.DATAPEMBELIAN))){

                }

                if ((Module_Id.equals(Constant.FOTODISPLAY))){
                    intent = new Intent(getActivity(), FotoDisplayActivity.class);
                    startActivity(intent);
                }


                if ((Module_Id.equals(Constant.CANVASSER))){
                    intent = new Intent(getActivity(), KunjunganCanvasserActivity.class);
                    startActivity(intent);
                }


                if ((Module_Id.equals(Constant.NEWCUSTOMER))){
                    intent = new Intent(getActivity(), RegisterCustomerActivity.class);
                    startActivity(intent);

                }

                if ((Module_Id.equals(Constant.SURVEY))){
                    intent = new Intent(getActivity(), ActivitySurveyMenu.class);
                    intent.putExtra("customerID", customerId);
                    intent.putExtra("visitID", visitID);
                    intent.putExtra("employeeID", EmployeeID);
                    startActivity(intent);

                }


            }
        });



    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        getModuleList();
    }

    @Override
    public void onStart() {
        super.onStart();



    }

    @Override
    public void onPause(){

        super.onPause();
        if(pDialogCompetitor != null)
            pDialogCompetitor.dismiss();

        if (pDialog!=null)
            pDialog.dismiss();
    }

    //CHECK STOCK
    private class checkStock extends AsyncTask<Void, Void, Void> {
        boolean isSuccessful = true;
        boolean getSisaStock = false;


        @Override
        protected Void doInBackground(Void... params) {


            try {
                databesHelper = new DatabesHelper(getActivity());
                getSisaStock = databesHelper.getCekModuleSisaStockVisit(visitID,customerId);
                //jika tabel SisaStockVisit null maka insert tabel SisaStockVisit
                if (getSisaStock == false){
                    ArrayList<ModelProduct> modelProductArrayList;
                    ArrayList<ModuleSisaStockType> moduleSisaStockTypeArrayList;

                    modelProductArrayList = new ArrayList<ModelProduct>();
                    moduleSisaStockTypeArrayList  = new ArrayList<ModuleSisaStockType>();

                    modelProductArrayList.clear();
                    moduleSisaStockTypeArrayList.clear();

                    modelProductArrayList = databesHelper.getArrayModelProduct();
                    moduleSisaStockTypeArrayList = databesHelper.getArrayModuleSisaStockType();

                    if (validateForm()){

                        for (int i=0; i<modelProductArrayList.size(); i++){
                            for (int k=0; k<moduleSisaStockTypeArrayList.size(); k++ ){
                                databesHelper.insertSisaStockVisit(
                                        visitID,
                                        modelProductArrayList.get(i).getProductID(),
                                        customerId,
                                        modelProductArrayList.get(i).getUOM(),
                                        moduleSisaStockTypeArrayList.get(k).getSisaStockTypeID(),
                                        "0",
                                        "",
                                        "",
                                        ValidationHelper.dateFormat(),
                                        EmployeeID,
                                        "0");
                            }

                        }

                    }else {
                        isSuccessful =false;
                    }
                }


            } catch (SQLiteException ex) {
                Log.e(getClass().getSimpleName(),
                        "tidak bisa  membuka database");
                isSuccessful =false;
            }


            //databesHelper.close();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Mohon tunggu!");
            pDialog.setMessage("Memuat");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Void args) {
            if (pDialog.isShowing())
                pDialog.dismiss();

                if (isSuccessful !=false){

                    intent = new Intent(getActivity(), KunjunganStockActivity.class);
                    startActivity(intent);

                }else {
                    DialogHelper.dialogMessage(getActivity(),"Warning",errorMsg,Constant.DIALOG_WRONG);
                }

        }

    }

    //CHECK COMPETITOR VISIT
    private class checkCompetitorVisit extends AsyncTask<Void, Void, Void> {

        boolean isSuccessful = true;
        String getCompetitorVisit;
        @Override
        protected Void doInBackground(Void... params) {


            try {
                databesHelper = new DatabesHelper(getActivity());
                getCompetitorVisit = databesHelper.getCheckCompetitorActivityVisit(visitID,customerId,EmployeeID);
                //jika tabel SisaStockVisit null maka insert tabel SisaStockVisit
                if (getCompetitorVisit.equals("0")){
                    ArrayList<ModuleCompetitor> moduleCompetitorArrayList;
                    ArrayList<ModelCompetitorActivityType> modelCompetitorActivityTypeArrayList;


                    moduleCompetitorArrayList = new ArrayList<ModuleCompetitor>();
                    modelCompetitorActivityTypeArrayList = new ArrayList<ModelCompetitorActivityType>();
                    moduleCompetitorArrayList.clear();
                    modelCompetitorActivityTypeArrayList.clear();

                    moduleCompetitorArrayList = databesHelper.getArrayListCompetitorProduct();
                    modelCompetitorActivityTypeArrayList = databesHelper.getArrayListtCompetitorActivityType();


                    if (validateForm()){
                        for (int i=0; i<moduleCompetitorArrayList.size(); i++){
                            for (int k=0; k<modelCompetitorActivityTypeArrayList.size(); k++){
                                databesHelper.insertCompetitorActivityVisit(
                                        visitID,
                                        EmployeeID,
                                        moduleCompetitorArrayList.get(i).getCompetitorID(),
                                        customerId,
                                        moduleCompetitorArrayList.get(i).getCompetitorProductID(),
                                        ValidationHelper.dateFormat(),
                                        "",
                                        "0",
                                        modelCompetitorActivityTypeArrayList.get(k).getCompetitorActivityTypeID(),
                                        "0");
                            }
                        }

                    }else {
                        isSuccessful =false;
                    }
                }


            } catch (SQLiteException ex) {
                Log.e(getClass().getSimpleName(),
                        "tidak bisa  membuka database");
                isSuccessful =false;
            }


            databesHelper.close();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogCompetitor = new ProgressDialog(getActivity());
            pDialogCompetitor.setTitle("Mohon tunggu!");
            pDialogCompetitor.setMessage("Memuat");
            pDialogCompetitor.setIndeterminate(false);
            pDialogCompetitor.setCancelable(false);
            pDialogCompetitor.show();
        }

        @Override
        protected void onPostExecute(Void args) {

            if (pDialogCompetitor.isShowing())
                pDialogCompetitor.dismiss();

                if (isSuccessful !=false){

                    intent = new Intent(getActivity(), KunjunganCompetitroActivity.class);
                    startActivity(intent);
                    getActivity().finish();

                }else {
                    DialogHelper.dialogMessage(getActivity(),"Warning",errorMsg,Constant.DIALOG_WRONG);
                }

        }

    }

    private Boolean validateForm() {
        if (customerId == null || customerId.equals("")) {
            errorMsg = "customerId null atau kosong";
            return false;
        }
        if (visitID == null || visitID.equals("")) {
            errorMsg = "visitID null atau kosong";
            return false;
        }
        if (EmployeeID == null || EmployeeID.equals("")) {
            errorMsg = "EmployeeID null";
            return false;
        }

        return true;
    }

}
