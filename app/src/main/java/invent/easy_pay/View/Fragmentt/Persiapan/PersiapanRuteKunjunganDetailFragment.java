package invent.easy_pay.View.Fragmentt.Persiapan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class PersiapanRuteKunjunganDetailFragment extends Fragment  {


    HeaderHelper headerHelper;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_persiapan_name_title,
            tv_info_store_kode,
            tv_info_store_nama_toko,
            tv_info_store_alamt,
            tv_info_store_jam_kunjungan,
            tv_info_store_jam_distiributor,
            tv_info_store_area,
            tv_info_store_chenel,
            tv_info_store_account,
            tv_info_store_regional,
            tv_perispan_store_kode_title,
            tv_perispan_store_namatoko_title,
            tv_perispan_store_alamat_title,
            tv_perispan_store_jam_title,
            tv_perispan_store_distibutor_title,
            tv_perispan_store_regional_title,
            tv_perispan_store_area_title,
            tv_perispan_store_channel_title,
            tv_perispan_store_account_title;
    String CustomerName,
            CustomerAddress,
            CustomerID,
            Distributor,
            CountryRegionCode,
            City,
            Account,
            Time,
            Channel;


    public PersiapanRuteKunjunganDetailFragment() {
        // Required empty public constructor



    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        if (getArguments()!=null) {
            Bundle extras = getArguments();
            if (extras.containsKey(Constant.CustomerName)){
                CustomerName = (String) extras.getSerializable(Constant.CustomerName);
            }
            if (extras.containsKey(Constant.CustomerAddress)){
                CustomerAddress = (String) extras.getSerializable(Constant.CustomerAddress);

            }
            if (extras.containsKey(Constant.CustomerID)){
                CustomerID = (String) extras.getSerializable(Constant.CustomerID);

            }
            if (extras.containsKey(Constant.Distributor)){
                Distributor = (String) extras.getSerializable(Constant.Distributor);

            }
            if (extras.containsKey(Constant.CountryRegionCode)){
                CountryRegionCode = (String) extras.getSerializable(Constant.CountryRegionCode);

            }
            if (extras.containsKey(Constant.City)){
                City = (String) extras.getSerializable(Constant.City);

            }
            if (extras.containsKey(Constant.Account)){
                Account = (String) extras.getSerializable(Constant.Account);

            }
            if (extras.containsKey(Constant.Time)){
                Time = (String) extras.getSerializable(Constant.Time);

            }

            if (extras.containsKey(Constant.Channel)){
                Channel = (String) extras.getSerializable(Constant.Channel);

            }

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan_store_detail, container, false);

        initializeLayout(v);

        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_persiapan_name_title = (TextView)view.findViewById(R.id.tv_persiapan_name_title);

        tv_info_store_kode = (TextView)view.findViewById(R.id.tv_info_store_kode);
        tv_info_store_nama_toko = (TextView)view.findViewById(R.id.tv_info_store_nama_toko);
        tv_info_store_alamt = (TextView)view.findViewById(R.id.tv_info_store_alamt);
        tv_info_store_jam_kunjungan = (TextView)view.findViewById(R.id.tv_info_store_jam_kunjungan);
        tv_info_store_jam_distiributor = (TextView)view.findViewById(R.id.tv_info_store_jam_distiributor);
        tv_info_store_area = (TextView)view.findViewById(R.id.tv_info_store_area);
        tv_info_store_chenel = (TextView)view.findViewById(R.id.tv_info_store_chenel);
        tv_info_store_account = (TextView)view.findViewById(R.id.tv_info_store_account);
        tv_info_store_regional = (TextView)view.findViewById(R.id.tv_info_store_regional);
        tv_perispan_store_kode_title = (TextView)view.findViewById(R.id.tv_perispan_store_kode_title);
        tv_perispan_store_namatoko_title = (TextView)view.findViewById(R.id.tv_perispan_store_namatoko_title);
        tv_perispan_store_alamat_title = (TextView)view.findViewById(R.id.tv_perispan_store_alamat_title);
        tv_perispan_store_jam_title = (TextView)view.findViewById(R.id.tv_perispan_store_jam_title);
        tv_perispan_store_distibutor_title = (TextView)view.findViewById(R.id.tv_perispan_store_distibutor_title);
        tv_perispan_store_regional_title = (TextView)view.findViewById(R.id.tv_perispan_store_regional_title);
        tv_perispan_store_area_title = (TextView)view.findViewById(R.id.tv_perispan_store_area_title);
        tv_perispan_store_channel_title = (TextView)view.findViewById(R.id.tv_perispan_store_channel_title);
        tv_perispan_store_account_title = (TextView)view.findViewById(R.id.tv_perispan_store_account_title);

        tv_info_store_kode.setText(CustomerID);
        tv_info_store_nama_toko.setText(CustomerName);
        tv_info_store_alamt.setText(CustomerAddress);
        tv_info_store_jam_kunjungan.setText("");
        tv_info_store_jam_distiributor.setText(Distributor);
        tv_info_store_area.setText(City);
        tv_info_store_chenel.setText("");
        tv_info_store_account.setText(Account);
        tv_info_store_regional.setText(CountryRegionCode);
        tv_info_store_jam_kunjungan.setText(Time);
        tv_info_store_chenel.setText(Channel);
        tv_toolbar_name_module.setText(R.string.modul_title_persipan_ruete_kunjungan_info_pelanggan);


    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_persiapan_name_title, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


        TextHelper.setFont(getActivity(), tv_info_store_chenel, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_jam_kunjungan, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_regional, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_account, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_area, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_jam_distiributor, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_alamt, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_nama_toko, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_info_store_kode, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

        TextHelper.setFont(getActivity(), tv_perispan_store_kode_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_namatoko_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_alamat_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_jam_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_distibutor_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_regional_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_area_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_channel_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_store_account_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());

    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }



    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
