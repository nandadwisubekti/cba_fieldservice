package invent.easy_pay.View.Fragmentt.Persiapan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Adapter.PersiapanToolsAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Model.ModuleTools;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class PersiapanToolsFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView reyclerView_tools;
    PersiapanToolsAdapter persiapanToolsAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module,
            tv_perispan_tools_title_item,
            tv_perispan_tools_title_qty,
            tv_title_data,
            tv_home_download_upload;
    MaterialRippleLayout mr_persiapan_tools_terima;

    ProgressBar progressBar;
    public PersiapanToolsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan_tools, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getStoreList();
        return v;
    }


    private void initializeLayout(View view) {

        reyclerView_tools = (RecyclerView)view.findViewById(R.id.reyclerView_tools);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_perispan_tools_title_item = (TextView)view.findViewById(R.id.tv_perispan_tools_title_item);
        tv_perispan_tools_title_qty = (TextView)view.findViewById(R.id.tv_perispan_tools_title_qty);
        mr_persiapan_tools_terima = (MaterialRippleLayout)view.findViewById(R.id.mr_persiapan_tools_terima);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        tv_home_download_upload = (TextView)view.findViewById(R.id.tv_home_download_upload);
        tv_toolbar_name_module.setText(R.string.titel_persian_tools);
        mr_persiapan_tools_terima.setVisibility(View.GONE);

    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_tools_title_item, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_perispan_tools_title_qty, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_home_download_upload, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());



    }

    private void assignListener(){




        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });



    }


    private void getStoreList(){

        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModuleTools> arrayListTools = new ArrayList<>();
        arrayListTools.clear();

        String EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);
        String CallPlanID = databesHelper.getCallPlanID(EmployeeID);

        arrayListTools = databesHelper.getArralyTools(CallPlanID);

        if (arrayListTools.size()>0){
            setupRecyclerView(reyclerView_tools,arrayListTools);
            mr_persiapan_tools_terima.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);
        }else {
            tv_title_data.setVisibility(View.VISIBLE);
        }

        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModuleTools> arrayListTools) {

        persiapanToolsAdapter = new PersiapanToolsAdapter(getActivity(),arrayListTools);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(persiapanToolsAdapter);
        persiapanToolsAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        mr_persiapan_tools_terima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogKonfirmasiTools(arrayListTools);

            }
        });

    }

    public  void dialogKonfirmasiTools(final ArrayList<ModuleTools> arrayListTools){

        final DatabesHelper databesHelper = new DatabesHelper(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage(R.string.dialog_persipan_tools)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {

                                String EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);
                                String createdDate = ValidationHelper.dateFormat();

                                if (EmployeeID !=null && createdDate!=null){
                                    for (int i=0; i<arrayListTools.size(); i++){
                                        databesHelper.updatePOSMStock(
                                                arrayListTools.get(i).getVisitID(),
                                                arrayListTools.get(i).getPOSMID(),
                                                arrayListTools.get(i).getQuantity(),
                                                createdDate,
                                                EmployeeID,
                                                arrayListTools.get(i).getQuantity());

                                    }

                                    databesHelper.updateModuleButton(Constant.MULAIPERJALANAN,Constant.HOME,"1");
                                    databesHelper.updateMobileFlag(Constant.ITEMGOODS, "1");

                                    validateButton();
                                    getStoreList();
                                    dialog.dismiss();
                                }else{
                                    Toast.makeText(getActivity(),  "Ada terjadi kesalahan EmployeeID atau tanggal ", Toast.LENGTH_LONG).show();

                                }


                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }


    public void validateButton(){

        String value = databesHelper.getMobilFlag(Constant.ITEMGOODS,Constant.CHECKLIST);
        if (value.equals("1")){
            mr_persiapan_tools_terima.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.bg_button_persiapan_diseble));
            mr_persiapan_tools_terima.setEnabled(false);

        }else {
            mr_persiapan_tools_terima.setEnabled(true);
            mr_persiapan_tools_terima.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.bg_button_persiapan_enable));


        }
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

        validateButton();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
