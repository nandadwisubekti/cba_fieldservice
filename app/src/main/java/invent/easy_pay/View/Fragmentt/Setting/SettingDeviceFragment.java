package invent.easy_pay.View.Fragmentt.Setting;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class SettingDeviceFragment extends Fragment  {


    TextView tv_setting_divice_id_titel,
            tv_setting_divce_version_titel,
            tv_setting_divce_model_titel;

    TextView tv_setting_divice_id,
            tv_setting_divice_version_android,
            tv_setting_divice_model;

    RelativeLayout ry_toolbar_back;
    TextView tv_toolbar_name_module;

    HeaderHelper headerHelper;
    DatabesHelper databesHelper;

    private String errorMsg = "";
    public SettingDeviceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting_divice_id, container, false);

        initializeLayout(v);
        headerHelper.setToolbarTop(v,getActivity());
        styleLayout();
        assignListener();
        return v;
    }


    private void initializeLayout(View view) {

     /*   Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.modul_title_divice);
        // ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/


        tv_setting_divice_id_titel = (TextView)view.findViewById(R.id.tv_setting_divice_id_titel);
        tv_setting_divce_version_titel = (TextView)view.findViewById(R.id.tv_setting_divce_version_titel);
        tv_setting_divce_model_titel = (TextView)view.findViewById(R.id.tv_setting_divce_model_titel);



        tv_setting_divice_id = (TextView)view.findViewById(R.id.tv_setting_divice_id);
        tv_setting_divice_version_android = (TextView)view.findViewById(R.id.tv_setting_divice_version_android);
        tv_setting_divice_model = (TextView)view.findViewById(R.id.tv_setting_divice_model);



        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_toolbar_name_module.setText(R.string.modul_title_divice);


    }

    private void styleLayout() {


        TextHelper.setFont(getActivity(), tv_setting_divice_id_titel, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_divce_version_titel, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_divce_model_titel, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

        TextHelper.setFont(getActivity(), tv_setting_divice_id, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_divice_version_android, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_setting_divice_model, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());

    }


    private void assignListener(){

        //-------Android deviceID------------
     /*   WifiManager manager = (WifiManager)getActivity().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String  deviceID = info.getMacAddress();*/

        String deViceID = ValidationHelper.getMacAddrss();
        tv_setting_divice_id.setText(deViceID);

        //-------Android version------------
        String androidOS = Build.VERSION.RELEASE;
        tv_setting_divice_version_android.setText(androidOS);

        //-------Android Model-----------
        String deviceName = android.os.Build.MODEL;
        tv_setting_divice_model.setText(deviceName);


        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }




    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


}
