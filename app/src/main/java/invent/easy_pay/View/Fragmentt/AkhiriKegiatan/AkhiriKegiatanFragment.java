package invent.easy_pay.View.Fragmentt.AkhiriKegiatan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import invent.easy_pay.Adapter.AkhiriKegiatanAdapter;
import invent.easy_pay.Helper.Constant;
import invent.easy_pay.Helper.DatabesHelper;
import invent.easy_pay.Helper.HeaderHelper;
import invent.easy_pay.Helper.TextHelper;
import invent.easy_pay.Helper.ValidationHelper;
import invent.easy_pay.Interface.OnItemClickListener;
import invent.easy_pay.Model.ModelStore;
import invent.easy_pay.R;


/**
 * Created by kahfizain on 25/08/2017.
 */

public class AkhiriKegiatanFragment extends Fragment  {


    HeaderHelper headerHelper;
    RecyclerView recyclerViewPersiapan;
    AkhiriKegiatanAdapter akhiriKegiatanAdapter;
    DatabesHelper databesHelper;
    RelativeLayout ry_toolbar_back,ryAkhirikegiatan;
    TextView tv_toolbar_name_module,
            tv_persiapan_name_title,
            tv_title_data;

    MaterialRippleLayout mr_akhirkegian;
    ProgressBar progressBar;
    public AkhiriKegiatanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_persiapan, container, false);

        initializeLayout(v);


        headerHelper.setToolbarTop(v,getActivity());
        headerHelper.setTolbarBottomVersion(v,getActivity());

        styleLayout();
        assignListener();
        getStoreList();
        return v;
    }


    private void initializeLayout(View view) {

        recyclerViewPersiapan = (RecyclerView)view.findViewById(R.id.recyclerView_persiapan);
        ry_toolbar_back = (RelativeLayout)view.findViewById(R.id.ry_toolbar_back);
        tv_toolbar_name_module = (TextView)view.findViewById(R.id.tv_toolbar_name_module);
        tv_persiapan_name_title = (TextView)view.findViewById(R.id.tv_persiapan_name_title);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        tv_title_data = (TextView)view.findViewById(R.id.tv_title_data);
        ryAkhirikegiatan = (RelativeLayout)view.findViewById(R.id.ryAkhirikegiatan);
        mr_akhirkegian = (MaterialRippleLayout)view.findViewById(R.id.mr_akhirkegian);
        tv_persiapan_name_title.setVisibility(View.VISIBLE);
        tv_toolbar_name_module.setText(R.string.modul_title_akhiri_kegiatan);
        tv_persiapan_name_title.setText(R.string.titel_akhiri_kegiatan);
    }

    private void styleLayout() {
        TextHelper.setFont(getActivity(), tv_persiapan_name_title, TextHelper.FontFamily.OPENSANS_SEMIBOLD.getFontFamily());
        TextHelper.setFont(getActivity(), tv_toolbar_name_module, TextHelper.FontFamily.VAG_ROUNDED_BOLD.getFontFamily());


    }

    private void assignListener(){

        ry_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mr_akhirkegian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogKonfirmasiAkhirikegiatan();
            }
        });
    }


    private void getStoreList(){
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ModelStore> arrayListmoduleStore = new ArrayList<>();
        arrayListmoduleStore.clear();

        databesHelper = new DatabesHelper(getActivity());
        String EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);
        String VisitID = databesHelper.getVisitID(EmployeeID);
        arrayListmoduleStore = databesHelper.getArrayStoreVisitFinish(VisitID);

        if (arrayListmoduleStore.size()>0){
            setupRecyclerView(recyclerViewPersiapan,arrayListmoduleStore);
            progressBar.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);
            ryAkhirikegiatan.setVisibility(View.GONE);
            tv_persiapan_name_title.setVisibility(View.VISIBLE);


        }else {
            recyclerViewPersiapan.setVisibility(View.GONE);
            tv_title_data.setVisibility(View.GONE);
            ryAkhirikegiatan.setVisibility(View.VISIBLE);
            tv_persiapan_name_title.setVisibility(View.GONE);

        }
        databesHelper.close();
        progressBar.setVisibility(View.GONE);

    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView, final ArrayList<ModelStore> arrayListmoduleStore) {

        akhiriKegiatanAdapter = new AkhiriKegiatanAdapter(getActivity(),arrayListmoduleStore);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(akhiriKegiatanAdapter);
        akhiriKegiatanAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        akhiriKegiatanAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String customerId = arrayListmoduleStore.get(position).getCustomerID();
                String visitId = arrayListmoduleStore.get(position).getVisitID();
                String isDelivery = arrayListmoduleStore.get(position).getIsDeliver();

                dialogFinishReason("Pilih alasan",customerId,visitId,isDelivery);

            }
        });

    }


    //DIALOG FINISH REASON
    private  void dialogFinishReason(String pesanTitle, final String customerId, final String visitId, final String isDelivery){
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_reason, null);
        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alert.setTitle(pesanTitle);
        alert.setView(promptView);

        final DatabesHelper databesHelper = new DatabesHelper(getActivity());

        final EditText et_reason = (EditText) promptView.findViewById(R.id.et_reason);
        final Spinner spPrinter = (Spinner)promptView.findViewById(R.id.spPrinter);

       // TextHelper.styleEditText(getActivity(), et_reason, "", true);

        final ArrayList<String> arrayReasonID;
        final ArrayList<String> arrayValue;
        final String[] ReasonID = {null};
        arrayReasonID = new ArrayList<String>();
        arrayReasonID.clear();
        arrayValue = new ArrayList<String>();
        arrayValue.clear();
        SQLiteDatabase db = databesHelper.getReadableDatabase();
        Cursor cursormsater = db.rawQuery("SELECT ReasonID,Value FROM Reason WHERE ReasonType = '"+Constant.visit+"' ", null);
        if (cursormsater != null) {
            if (cursormsater.moveToFirst()) {
                do {
                    try {
                        arrayReasonID.add(cursormsater.getString(cursormsater.getColumnIndex("ReasonID")));
                        arrayValue.add(cursormsater.getString(cursormsater.getColumnIndex("Value")));
                    } catch (NullPointerException ex) {

                    } catch (Exception e) {

                    }
                } while (cursormsater.moveToNext());
            }
        }
        cursormsater.close();
        db.close();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spiner_item);
        for (int i = 0; i < arrayValue.size(); i++) {
            adapter.add(arrayValue.get(i));
        }

        spPrinter.setAdapter(adapter);
        spPrinter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                int x = spPrinter.getSelectedItemPosition();

                ReasonID[0] = arrayReasonID.get(x);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        alert.setCancelable(false)
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {


                        String reasonDesc = "";
                        if (et_reason.getText().length()>0){
                            reasonDesc = et_reason.getText().toString();
                        }

                        if (isDelivery.equals("2")){
                            databesHelper.iupdateVisitDetailFinsihEndTundaKunjungan(ReasonID[0],customerId,visitId,reasonDesc);
                        }else {
                            databesHelper.updateVisitDetailFinsihEnd(visitId,customerId,"1","0",ReasonID[0],reasonDesc,ValidationHelper.dateFormat(),ValidationHelper.dateFormat());
                        }


                        dialogBox.dismiss();
                        getStoreList();
                    }
                })

                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog alertDialogAndroid = alert.create();
        alertDialogAndroid.show();
    }


    public  void dialogKonfirmasiAkhirikegiatan(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirmation);
        builder.setCancelable(false);
        builder.setMessage(R.string.dialog_akhiri_kegiatan)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_ya,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int _id) {


                                databesHelper.updateModuleButton(Constant.UPLOAD,Constant.DOWNLOAD_UPLOAD,"1");

                                databesHelper.updateModuleButton(Constant.KUNJUNGAN,Constant.HOME,"0");
                                databesHelper.updateModuleButton(Constant.AKHIRIKEGIATAN,Constant.HOME,"0");

                                databesHelper.updateModuleButton(Constant.REPORT,Constant.HOME,"1");
                                databesHelper.updateModuleButton(Constant.ISSU,Constant.HOME,"1");

                                String EmployeeID = databesHelper.getMobileConfig(Constant.DRIVERID);
                                String VisitID = databesHelper.getVisitID(EmployeeID);
                                databesHelper.updateVisitIsFinish(VisitID,ValidationHelper.dateFormat());
                                getActivity().onBackPressed();
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(R.string.dialog_batal,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onResume(){
        super.onResume();

        headerHelper = new HeaderHelper(getActivity());
        databesHelper = new DatabesHelper(getActivity());
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
}
