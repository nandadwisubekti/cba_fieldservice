package invent.easy_pay.Interface;

import android.widget.CompoundButton;

/**
 * Created by Kodelokus-Dell on 3/29/2016.
 */
public interface OnCheckedChangeListener {
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked, int position);

}
