package invent.easy_pay.Interface;

import android.view.View;

/**
 * Created by Kodelokus-Dell on 3/29/2016.
 */
public interface OnItemClickListener {
    public void onItemClick(View view, int position);

}
