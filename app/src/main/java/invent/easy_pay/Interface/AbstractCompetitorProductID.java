package invent.easy_pay.Interface;

import android.content.Context;

public interface AbstractCompetitorProductID {

    String getCompetitorProductID();

    void setCompetitorProductID(Context context, String competitorProductID);

}
