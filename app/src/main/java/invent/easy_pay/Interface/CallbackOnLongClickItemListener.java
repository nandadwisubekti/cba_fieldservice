package invent.easy_pay.Interface;

/**
 * Created by INVENT on 8/2/2018.
 */

public interface CallbackOnLongClickItemListener {
    void onLongClick(int position);
}
